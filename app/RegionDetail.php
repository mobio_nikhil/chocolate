<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegionDetail extends Model
{
    //
       protected $table = "region_detail";
       public $timestamps = false;
       protected $fillable = [
                                "region_id",
                                "state_id",
                                "created_on",
                             ];       
       
       
       
}
