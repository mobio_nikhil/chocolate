<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FollowupMeeting extends Model
{
       protected $table = "folloup_meeting_customer";
       public $timestamps = false;
       protected $fillable = [
                                "user_id",
                                "customer_id",
                                "meeting_id",
                                "followup_date_time",
                                "extra_note",
                                "created_on",
                             ];   
}
