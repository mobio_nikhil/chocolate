<?php

// Code within app\Helpers\Helper.php

namespace App\Helpers;

use JWTAuth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use App\User;
use Input;
use Validator;
use App\Helpers\Helper;
use App\GpsData;
use App\Brand;
use App\Meeting;
use App\Region;
use App\Customer;
use App\State;
use App\RegionDetail;
use App\Purpose;
use App\Area;
use App\FollowupMeeting;
use DB;
use Redirect;
use Config;
use Session;
use View;
use Excel;
use Mail;
use PDO;

class Helper {

    public static function shout($string) {
        return strtoupper($string);
    }

    public static function response_api($status, $message, $data) {
        $response_array = array();
        $response_array['status'] = $status;
        $response_array['message'] = $message;
        $response_array['data'] = $data;
        return response()->json($response_array, 200);
    }

    public static function get_user_info($token) {
        return JWTAuth::toUser($token)->toArray();
    }

    public static function validation_error_response($input_all, $rules) {
        $validator = Validator::make($input_all, $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            //print_r($validator->messages()->toArray());
            foreach ($validator->messages()->toArray() as $value) {
                foreach ($value as $v)
                    $err_msg[] = $v;
            }
            if (count($err_msg) > 0)
                return Helper::response_api(false, $err_msg, '');
            else
                return false;
        }
    }

    public static function get_customers($number) {



        $session_user_info = Session::get('session_user_info');
        $user_ids = array();
        if ($session_user_info['user_type'] == 0) {
            $user_ids = Helper::vp_under_user_list();
        }
        if ($session_user_info['user_type'] == 1) {
            $user_ids = Helper::rsm_under_user_list($session_user_info);
        }
        if ($session_user_info['user_type'] == 2) {
            $user_ids = Helper::asm_under_user_list($session_user_info);
        }
        //$user_ids = trim($user_ids, ",");

        $customer_list = array();


        $customer_list = Customer::select('customer.*', 'region_name', 'area_name', 'user_name', DB::raw("count(meeting_customer.id) as meeting_count"))
                ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                ->leftJoin('area', 'area.id', '=', 'customer.area_id')
                ->leftJoin('user', 'user.id', '=', 'customer.information_enter_by')
                ->leftJoin('meeting_customer', 'meeting_customer.customer_id', '=', 'customer.id')
                ->wherein('information_enter_by', $user_ids)
                ->where('customer.delete_status', 0)
                ->where("customer_type_id", 1)
                ->groupby('customer.id');
        if ($number != 0 && $number != '*') {
            $customer_list->limit($number);
        }

        $customer_list = $customer_list->get()->toArray();

        //        echo '<pre>';
        //        print_r($customer_list);
        //        echo '</pre>';
        // $data = array();
        // $data['customer_list'] = $customer_list;
        // return view('pages.customer.customer_list',$data);
        // dd($customers);
        return $customer_list;
    }

    public static function validation_form_error_response($input_all, $rules) {
        $validator = Validator::make($input_all, $rules);
        if ($validator->fails()) {
            $messages = $validator->messages();
            return $validator;
        } else {
            return false;
        }
    }

    public static function on_date_customer_list($userid, $on_start_date, $on_end_date, $report) {

        $userdata = User::find($userid)->toArray();

        $rsmuserdata = array();
        $asmuserdata = array();
        $smuserdata = array();

        $user_ids = array();
        if ($userdata['user_type'] == 0) {
            $user_ids = Helper::vp_under_user_list();
        }
        if ($userdata['user_type'] == 1) {
            $user_ids = Helper::rsm_under_user_list($userdata);
        }
        if ($userdata['user_type'] == 2) {
            $user_ids = Helper::asm_under_user_list($userdata);
        }

        $excel_array = array();
        $customer_list = array();
        $customer_list = Customer::select('customer.*','pincode','city_name','customertype_name', 'region_name', 'area_name', 'user_name', 'state_name')
                ->leftJoin('customer_type', 'customer_type.id', '=', 'customer.customer_type_id')
                ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                ->leftJoin('area', 'area.id', '=', 'customer.area_id')
                ->leftJoin('state', 'state.id', '=', 'customer.state_id')
                ->leftJoin('city', 'city.id', '=', 'customer.city_id')   
                ->leftJoin('user', 'user.id', '=', 'customer.information_enter_by')
                ->wherein('information_enter_by', $user_ids)
                ->where('customer.delete_status', 0)
                //->where('customer.customer_type_id',1)
                //->where(DB::raw("DATE_FORMAT(customer.created_on,'%d-%m-%Y')"),$on_date)
                ->where(DB::raw("DATE(customer.created_on)"), '>=', $on_start_date)
                ->where(DB::raw("DATE(customer.created_on)"), '<=', $on_end_date)
                ->groupby('customer.id')
                ->orderby('customer.created_on', 'desc')
                ->get()
                ->toArray();
        if ($report == 1) {
            $excel_array = $customer_list;
        }


        if ($report == 2) {
            $excel_array[] = ['id', 'customer name','customertype_name', 'business name', 'business address', 'secondary contact name', 'email', 'phone no', 'region', 'area', 'city name', 'pincode', 'added by', 'create date'];

            for ($i = 0; $i < count($customer_list); $i++) {
                $excel_array[] = array($customer_list[$i]['id'],
                    $customer_list[$i]['customer_name'],
                    $customer_list[$i]['customertype_name'],
                    $customer_list[$i]['business_name'],
                    $customer_list[$i]['business_address'],
                    $customer_list[$i]['secondary_contact_name'],
                    $customer_list[$i]['customer_email'],
                    $customer_list[$i]['customer_phone'],
                    $customer_list[$i]['region_name'],
                    $customer_list[$i]['area_name'],
                    $customer_list[$i]['city_name'],
                    $customer_list[$i]['pincode'],
                    $customer_list[$i]['user_name'],
                    date('d-M-Y', strtotime($customer_list[$i]['created_on']))
                );
            }
        }

        return $excel_array;
    }

    public static function on_date_customer_meeting($userid, $on_start_date, $on_end_date) {

        $userdata = User::find($userid)->toArray();

        $rsmuserdata = array();
        $asmuserdata = array();
        $smuserdata = array();

        $user_ids = array();
        if ($userdata['user_type'] == 0) {
            $user_ids = Helper::vp_under_user_list();
        }
        if ($userdata['user_type'] == 1) {
            $user_ids = Helper::rsm_under_user_list($userdata);
        }
        if ($userdata['user_type'] == 2) {
            $user_ids = Helper::asm_under_user_list($userdata);
        }

        $customer_meeting_date_log = Meeting::select('*','pincode','city_name', 'meeting_customer.brand_id as brand_ids', 'user.id as uid', 'meeting_customer.created_on as meeting_date', 'meeting_customer.id as mid', 'area.area_name as carea', 'state.state_name as cstate', 'region.region_name as cregion', 'meeting_customer.extra_note as mnote')
                ->leftJoin('user', 'user.id', '=', 'meeting_customer.user_id')
                ->leftJoin('folloup_meeting_customer', 'folloup_meeting_customer.meeting_id', '=', 'meeting_customer.id')
                ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')
                ->leftJoin('customer_type', 'customer_type.id', '=', 'customer.customer_type_id')					
                ->leftJoin('brand', 'brand.id', '=', 'meeting_customer.brand_id')
                ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                ->leftJoin('state', 'state.id', '=', 'customer.state_id')
                ->leftJoin('city', 'city.id', '=', 'customer.city_id')
                ->leftJoin('area', 'area.id', '=', 'customer.area_id')
                ->leftJoin('meeting_purpose', 'meeting_purpose.id', '=', 'meeting_customer.purpose_id')
                ->wherein('meeting_customer.user_id', $user_ids)
                //->where(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y')"),$on_date)
                ->where(DB::raw("DATE(meeting_customer.created_on)"), '>=', $on_start_date)
                ->where(DB::raw("DATE(meeting_customer.created_on)"), '<=', $on_end_date)
                ->orderby('meeting_customer.created_on', 'desc')
                ->get()
                ->toArray();

        for ($i = 0; $i < count($customer_meeting_date_log); $i++) {
            $brand_ids = Brand::select('*')->wherein('id', explode(',', $customer_meeting_date_log[$i]['brand_ids']))->get()->toArray();
            $br_text = array();
            foreach ($brand_ids as $br) {
                $br_text[] = $br['brand_name'];
            }
            $customer_meeting_date_log[$i]['brand_name'] = implode(',', $br_text);
        }


        return $customer_meeting_date_log;
    }

    public static function GetUserDashboardData() {
        $userInfo = Auth::user();
        $user_ids = self::getChildEmployees();
        $todayMeeting = Meeting::select('meeting_customer.user_id', 'meeting_customer.customer_id', 'user.id as uid', 'meeting_customer.created_on as meeting_date', 'meeting_customer.id as mid')
                        ->leftJoin('user', 'user.id', '=', 'meeting_customer.user_id')
                        ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')
                        ->where(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y')"), date("d-m-Y"))
                        ->wherein('meeting_customer.user_id', $user_ids)
                        ->orderby('meeting_customer.created_on', 'desc')
                        ->get()->toArray();

        $TotalCustomer = Customer::select('customer.*')
                        ->leftJoin('user', 'user.id', '=', 'customer.information_enter_by')
                        ->where(DB::raw("DATE_FORMAT(customer.created_on,'%d-%m-%Y')"), date("d-m-Y"))
                        ->wherein('customer.information_enter_by', $user_ids)
                        ->where('customer.delete_status', 0)
                        ->orderby('customer.created_on', 'desc')
                        ->get()->toArray();
        $TodayFollowUp = Meeting::select('*','meeting_customer.brand_id as brand_ids','user.id as uid','meeting_customer.created_on as meeting_date','meeting_customer.id as mid','area.area_name as carea','region.region_name as cregion','state.state_name as cstate')
                        ->leftJoin('user', 'user.id', '=', 'meeting_customer.user_id')
                        ->leftJoin('folloup_meeting_customer', 'folloup_meeting_customer.meeting_id', '=', 'meeting_customer.id')
                        ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')                                
                        //->leftJoin('brand', 'brand.id', '=', 'meeting_customer.brand_id')                                
                        ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                        ->leftJoin('area', 'area.id', '=', 'customer.area_id')                
                        ->leftJoin('state', 'state.id', '=', 'customer.state_id')                
                        ->leftJoin('meeting_purpose', 'meeting_purpose.id', '=', 'meeting_customer.purpose_id')                                                                                
                        ->wherein('meeting_customer.user_id',$user_ids)
                        ->where(DB::raw("DATE_FORMAT(folloup_meeting_customer.followup_date_time,'%Y-%m-%d')"), '=', date("Y-m-d"))
                        //->where(DB::raw("DATE_FORMAT(folloup_meeting_customer.followup_date_time,'%Y-%m-%d')"), '<=', date('Y-m-d', strtotime("+1 week")))
                        //->where(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y')"), date("d-m-Y"))
                        ->orderby('meeting_customer.created_on','desc')
                        ->get()
                        ->toArray();  
        /*$TodayFollowUp = FollowupMeeting::select('folloup_meeting_customer.*')
                        ->leftJoin('user', 'user.id', '=', 'folloup_meeting_customer.user_id')
                        ->leftJoin('customer', 'customer.id', '=', 'folloup_meeting_customer.customer_id')
                        ->where(DB::raw("DATE_FORMAT(folloup_meeting_customer.followup_date_time,'%Y-%m-%d')"), date("Y-m-d"))
                        ->wherein('folloup_meeting_customer.user_id', $user_ids)
                        ->orderby('folloup_meeting_customer.created_on', 'desc')
                        ->where('customer.delete_status', 0)
                        ->orderby('customer.created_on', 'desc')
                        ->get()->toArray();


        $UpcomingFollowUp = FollowupMeeting::select('folloup_meeting_customer.*')
                        ->leftJoin('user', 'user.id', '=', 'folloup_meeting_customer.user_id')
                        ->leftJoin('customer', 'customer.id', '=', 'folloup_meeting_customer.customer_id')
                        ->where(DB::raw("DATE_FORMAT(folloup_meeting_customer.followup_date_time,'%Y-%m-%d')"), '>=', date("Y-m-d"))
                        ->where(DB::raw("DATE_FORMAT(folloup_meeting_customer.followup_date_time,'%Y-%m-%d')"), '<=', date('Y-m-d', strtotime("+1 week")))
                        ->wherein('folloup_meeting_customer.user_id', $user_ids)
                        ->orderby('folloup_meeting_customer.created_on', 'desc')
                        ->where('customer.delete_status', 0)
                        ->orderby('customer.created_on', 'desc')
                        ->get()->toArray();*/
         $UpcomingFollowUp = Meeting::select('*','meeting_customer.brand_id as brand_ids','user.id as uid','meeting_customer.created_on as meeting_date','meeting_customer.id as mid','area.area_name as carea','region.region_name as cregion','state.state_name as cstate')
                        ->leftJoin('user', 'user.id', '=', 'meeting_customer.user_id')
                        ->leftJoin('folloup_meeting_customer', 'folloup_meeting_customer.meeting_id', '=', 'meeting_customer.id')
                        ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')                                
                        //->leftJoin('brand', 'brand.id', '=', 'meeting_customer.brand_id')                                
                        ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                        ->leftJoin('area', 'area.id', '=', 'customer.area_id')                
                        ->leftJoin('state', 'state.id', '=', 'customer.state_id')                
                        ->leftJoin('meeting_purpose', 'meeting_purpose.id', '=', 'meeting_customer.purpose_id')                                                                                
                        ->wherein('meeting_customer.user_id',$user_ids)
                        ->where(DB::raw("DATE_FORMAT(folloup_meeting_customer.followup_date_time,'%Y-%m-%d')"), '>', date("Y-m-d"))
                        //->where(DB::raw("DATE_FORMAT(folloup_meeting_customer.followup_date_time,'%Y-%m-%d')"), '<=', date('Y-m-d', strtotime("+1 week")))
                        //->where(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y')"), date("d-m-Y"))
                        ->orderby('meeting_customer.created_on','desc')
                        ->get()
                        ->toArray();  


        $TodayFollowUpCount = count($TodayFollowUp);
        $TotalUpcomingFollowUp = count($UpcomingFollowUp);
        $TodayMeetingCount = count($todayMeeting);
        $TotalCustomerCount = count($TotalCustomer);



        return array(
            "TodayMeeting" => $TodayMeetingCount,
            "TodayMeetingList" => $todayMeeting,
            "TodayFollowup" => $TodayFollowUpCount,
            "UpcomingFollowup" => $TotalUpcomingFollowUp,
            "TotalCustomer" => $TotalCustomerCount
        );
    }

    public static function getChildEmployees() {
        $userInfo = Session::get('session_user_info');
        $UserType = $userInfo['user_type'];

        $user_ids = array();
        if ($UserType == 0) {
            $user_ids = Helper::vp_under_user_list();
        }
        if ($UserType == 1) {
            $user_ids = Helper::rsm_under_user_list($userInfo);
        }
        if ($UserType == 2) {
            $user_ids = Helper::asm_under_user_list($userInfo);
        }

        return $user_ids;
    }

    public static function getUserLocationData() {

        $session_user_info = Session::get('session_user_info');

        if ($session_user_info['user_type'] == 0) {
            $region = Region::select('*')->get()->toArray();
            $area = Area::select('*')->get()->toArray();
        }

        if ($session_user_info['user_type'] == 1) {
            $region = Region::select('*')->where('delete_status', 0)->where('disable_status', 0)->where('id', $session_user_info['region_id'])->get()->toArray();
            $area = Area::select('*')->where('delete_status', 0)->where('disable_status', 0)->where('region_id', $session_user_info['region_id'])->get()->toArray();
        }

        if ($session_user_info['user_type'] == 2) {
            $region = Region::select('*')->where('delete_status', 0)->where('disable_status', 0)->where('id', $session_user_info['region_id'])->get()->toArray();
            $area = Area::select('*')->where('delete_status', 0)->where('disable_status', 0)->where('region_id', $session_user_info['region_id'])->where('id', $session_user_info['area_id'])->get()->toArray();
        }

        return array(
            "region" => $region,
            "area" => $area
        );
    }

    public static function getUserMapData() {
        $userIds = self::getChildEmployees();
        return self::FilterMapData($userIds);
    }

    public static function FilterMapData($userIds) {

        $userData = User::select('user.*', 'region_name', 'area_name', 'assignment_user.user_name as assignment_username')
                        ->leftJoin('region', 'region.id', '=', 'user.region_id')
                        ->leftJoin('area', 'area.id', '=', 'user.area_id')
                        ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                        ->where('user.delete_status', 0)
                        ->where('user.disable_status', 0)
                        ->whereIn('user.id', $userIds)
                        ->orderby('user_name')
                        ->get()->toArray();


        $mapDataArray = array();
        if (count($userData) > 0) {

            foreach ($userData as $key => $values) {
                $user_gps = GpsData::select('gps_data.*', 'user.user_name', 'customer.*', 'gps_data.created_on as log_time', 'gps_data.id as gps_id')
                                ->leftJoin('meeting_customer', 'meeting_customer.id', '=', 'gps_data.meeting_id')
                                ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')
                                ->leftJoin('user', 'user.id', '=', 'gps_data.user_id')
                                ->where(DB::raw("DATE_FORMAT(gps_data.created_on,'%d-%m-%Y')"), date("d-m-Y"))
                                ->where('gps_data.user_id', $values['id'])
                                ->orderby('gps_data.created_on', 'DESC')
                                ->take(1)->get()->toArray();

                if (count($user_gps) > 0) {
                    $mapDataArray[] = $user_gps[0];
                }
            }
        }

        if (count($mapDataArray) > 0) {

            for ($i = 0; $i < count($mapDataArray); $i++) {

                if ($mapDataArray[$i]['gps_data_type'] == 0) {
                    $mapDataArray[$i]['map_header'] = 'Start Day';
                    $mapDataArray[$i]['map_content'] = 'Start of the day';
                    $mapDataArray[$i]['map_time_date'] = date('d-m-Y h:i:s A', strtotime($mapDataArray[$i]['log_time']));
                }

                if ($mapDataArray[$i]['gps_data_type'] == 1) {
                    $mapDataArray[$i]['map_header'] = $mapDataArray[$i]['business_name'];
                    $mapDataArray[$i]['map_content'] = $mapDataArray[$i]['customer_name'] . '<br/>' . $mapDataArray[$i]['business_address'] . '<br/>' . $mapDataArray[$i]['customer_phone'];
                    $mapDataArray[$i]['map_time_date'] = date('d-m-Y h:i:s A', strtotime($mapDataArray[$i]['log_time']));
                }

                if ($mapDataArray[$i]['gps_data_type'] == 2) {
                    $mapDataArray[$i]['map_header'] = 'End Day';
                    $mapDataArray[$i]['map_content'] = 'End of the day';
                    $mapDataArray[$i]['map_time_date'] = date('d-m-Y h:i:s A', strtotime($mapDataArray[$i]['log_time']));
                }
            }
        }

        return $mapDataArray;
    }

    public static function getUnTrackUserData() {
        $userIds = self::getChildEmployees();
        $today = date('Y-m-d');
        
        $NotStartUserArray = array();
        $NotUpdateUserArray = array();
        for($i=0;$i<count($userIds);$i++){
            DB::setFetchMode(PDO::FETCH_ASSOC);
            $user = DB::connection('mysql')->select("select * from user where id = $userIds[$i] and id not in (select user_id from gps_data where DATE_FORMAT(gps_data.created_on,'%Y-%m-%d') = '$today')");
            if(count($user) > 0)
            $NotStartUserArray[] = $user[0];
            DB::setFetchMode(PDO::FETCH_CLASS);            
        }
        
        $start_time = date('Y-m-d').' 00:00:00';
        $hour = Config::get('constants.not_active_hour');
        $end_time = date('Y-m-d H:i:s', strtotime("-$hour hour"));
        
        
        for($i=0;$i<count($userIds);$i++){
            DB::setFetchMode(PDO::FETCH_ASSOC);
            $user = DB::connection('mysql')->select("select * from user where id = $userIds[$i] and id in (select user_id from gps_data where DATE_FORMAT(gps_data.created_on,'%Y-%m-%d %H:%i:%s') >= '$start_time' and DATE_FORMAT(gps_data.created_on,'%Y-%m-%d %H:%i:%s') <= '$end_time')");
            if(count($user) > 0)
            $NotUpdateUserArray[] = $user[0];
            DB::setFetchMode(PDO::FETCH_CLASS);            
        }

        return array(
            "workingUser" => $NotUpdateUserArray,
            "NotworkingUser" => $NotStartUserArray,
        );         

        /*
        $userIds = self::getChildEmployees();
        $NotWorkingUser = array();
        $NotUpdatesUser = array();
        $userInfo = array();
        if (count($userIds) > 0) {
            foreach ($userIds as $id) {
                $user_gps = GpsData::select('gps_data.*', 'customer.*', 'gps_data.created_on as log_time', 'gps_data.id as gps_id')
                                ->leftJoin('meeting_customer', 'meeting_customer.id', '=', 'gps_data.meeting_id')
                                ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')
                                ->where(DB::raw("DATE_FORMAT(gps_data.created_on,'%d-%m-%Y')"), '=', date("d-m-Y"))
                                ->where('gps_data.user_id', $id)
                                ->orderby('gps_data.created_on', 'DESC')
                                ->take(1)->get()->toArray();

                if (count($user_gps) > 0) {
                    $createdTime = $user_gps[0]['log_time'];
                    $date1 = $createdTime;
                    $date2 = date('Y-m-d H:i:s');
                    $timestamp1 = strtotime($date1);
                    $timestamp2 = strtotime($date2);


                    $hour = abs($timestamp2 - $timestamp1) / (60 * 60);
                    if ($hour > 3) {
                        $NotUpdatesUser[$id] = $id;
                    }
                } else {
                    $NotWorkingUser[$id] = $id;
                }
            }
        }

        $NotStartUserArray = array();
        $NotUpdateUserArray = array();
        if (count($NotWorkingUser) > 0) {
            foreach ($NotWorkingUser as $keys) {
                $userData = User::select('user.id', 'user_name', 'user_phone')
                                ->where('user.delete_status', 0)
                                ->where(function ($query) use($userInfo) {
                                    $query->where('user.user_type', '=', 2)
                                    ->orWhere('user.user_type', '=', 3);
                                })
                                ->where('user.id', $keys)
                                ->where('user.disable_status', 0)->get()->toArray();
                if (count($userData) > 0) {
                    $NotStartUserArray[] = $userData[0];
                }
            }
        }

        if (count($NotUpdatesUser) > 0) {
            foreach ($NotUpdatesUser as $keys) {
                $userData = User::select('user.id', 'user_name', 'user_phone')
                                ->where('user.delete_status', 0)
                                ->where(function ($query) use($userInfo) {
                                    $query->where('user.user_type', '=', 2)
                                    ->orWhere('user.user_type', '=', 3);
                                })
                                ->where('user.id', $keys)
                                ->where('user.disable_status', 0)->get()->toArray();
                if (count($userData) > 0) {
                    $NotUpdateUserArray[] = $userData[0];
                }
            }
        }
        */
    }

    public static function get_my_region_month_meeting() {
        $session_user_info = Session::get('session_user_info');
        $region = array();
        if ($session_user_info['user_type'] == 0) {
            $region = Region::select('*')->where('delete_status', 0)->get()->toArray();
        }
        if ($session_user_info['user_type'] == 1) {
            $region = Region::select('*')->where('id', $session_user_info['region_id'])->where('delete_status', 0)->get()->toArray();
        }
        $region_ids = array();
        $data = array();
        $data['region'] = array();
        $on_start_date = date('Y-m-01');
        $on_end_date = date('Y-m-t');
        //foreach($region as $r){
        for ($i = 0; $i < count($region); $i++) {
            //$region_ids[] = $r['id'];
            $customer_list = Customer::select('*')->where('region_id', $region[$i]['id'])->get()->toArray();
            $customer_ids = array();
            foreach ($customer_list as $cst) {
                $customer_ids[] = $cst['id'];
            }



            $meeting_data = Meeting::select('*')->wherein('customer_id', $customer_ids)
                    ->where(DB::raw("DATE(meeting_customer.created_on)"), '>=', $on_start_date)
                    ->where(DB::raw("DATE(meeting_customer.created_on)"), '<=', $on_end_date)
                    ->get()
                    ->toArray();

            $data['region'][$i]['region_id'] = $region[$i]['id'];
            $data['region'][$i]['region_name'] = $region[$i]['region_name'];
            $data['region'][$i]['meeting_count'] = count($meeting_data);
            $data['region'][$i]['state'] = array();
            //state start
            $state_list = RegionDetail::select('*')->where('region_id', $region[$i]['id'])->get()->toArray();
            for ($j = 0; $j < count($state_list); $j++) {
                $state_detail = State::select('*')->where('id', $state_list[$j]['state_id'])->get()->toArray();

                $customer_list = Customer::select('*')->where('state_id', $state_list[$j]['state_id'])->get()->toArray();
                $customer_ids = array();
                foreach ($customer_list as $cst) {
                    $customer_ids[] = $cst['id'];
                }

                $meeting_data = Meeting::select('*')->wherein('customer_id', $customer_ids)
                        ->where(DB::raw("DATE(meeting_customer.created_on)"), '>=', $on_start_date)
                        ->where(DB::raw("DATE(meeting_customer.created_on)"), '<=', $on_end_date)
                        ->get()
                        ->toArray();

                $data['region'][$i]['state'][$j]['state_id'] = $state_detail[0]['id'];
                $data['region'][$i]['state'][$j]['state_name'] = $state_detail[0]['state_name'];
                $data['region'][$i]['state'][$j]['meeting_count'] = count($meeting_data);
            }
            //state end
        }
        return $data;
    }

    public static function vp_under_user_list() {
        $user = User::select('*')
                ->where('disable_status', 0)
                ->where('delete_status', 0)
                ->get()
                ->toArray();
        $user_ids = array();
        for ($i = 0; $i < count($user); $i++) {
            //$user_ids = $user[$i]['id'].','.$user_ids;
            $user_ids[] = $user[$i]['id'];
        }

        return $user_ids;
    }

    public static function rsm_under_user_list($rsm_user) {

        $user = User::select('user.*', 'region_name', 'area_name')
                        ->leftJoin('region', 'region.id', '=', 'user.region_id')
                        ->leftJoin('area', 'area.id', '=', 'user.area_id')
                        ->where(function ($query) {
                            $query->where('user.user_type', '=', 2)
                            ->orWhere('user.user_type', '=', 3)
                            ->orWhere('user.user_type', '=', 1);
                        })
                        ->where('user.region_id', $rsm_user['region_id'])
                        ->where('user.delete_status', 0)
                        ->where('user.disable_status', 0)
                        ->orderby('user_name')
                        ->get()->toArray();
        $user_ids = array();
        for ($i = 0; $i < count($user); $i++) {
            //$user_ids = $user[$i]['id'].','.$user_ids;
            $user_ids[] = $user[$i]['id'];
        }

        return $user_ids;
    }

    public static function asm_under_user_list($asm_user) {

        $user = User::select('user.*', 'region_name', 'area_name')
                        ->leftJoin('region', 'region.id', '=', 'user.region_id')
                        ->leftJoin('area', 'area.id', '=', 'user.area_id')
                        ->where('user.assign_to_user', $asm_user['id'])
                        ->where('user.disable_status', 0)
                        ->where('user.delete_status', 0)
                        //->where('user.id', $asm_id)
                        ->orderby('user_name')
                        ->get()->toArray();
        $user_ids = array();
        for ($i = 0; $i < count($user); $i++) {
            //$user_ids = $user[$i]['id'].','.$user_ids;
            $user_ids[] = $user[$i]['id'];
        }
        $user_ids[] = $asm_user['id'];
        return $user_ids;
    }

    public static function new_customer_list_added_by_users($user_ids, $start_date, $end_date) {
        $excel_array = array();
        $customer_list = array();
        $customer_list = Customer::select('customer.*', 'pincode', 'city_name','region_name', 'area_name', 'user_name', 'state_name','customertype_name')
                ->leftJoin('customer_type', 'customer_type.id', '=', 'customer.customer_type_id')
                ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                ->leftJoin('area', 'area.id', '=', 'customer.area_id')
                ->leftJoin('city', 'city.id', '=', 'customer.city_id')
                ->leftJoin('state', 'state.id', '=', 'customer.state_id')
                ->leftJoin('user', 'user.id', '=', 'customer.information_enter_by')
                ->wherein('information_enter_by', $user_ids)
                ->where('customer.delete_status', 0)
                //->where('customer.customer_type_id',1)
                //->where(DB::raw("DATE_FORMAT(customer.created_on,'%d-%m-%Y')"),$on_date)
                ->where(DB::raw("DATE(customer.created_on)"), '>=', $start_date)
                ->where(DB::raw("DATE(customer.created_on)"), '<=', $end_date)
                ->groupby('customer.id')
                ->get()
                ->toArray();
        $excel_array[] = ['id', 'customer name', 'customer Type','business name', 'business address', 'secondary contact name', 'email', 'phone no', 'region', 'area', 'state', 'city name', 'pincode','added by', 'create date'];

        for ($i = 0; $i < count($customer_list); $i++) {
            $excel_array[] = array(
                //$customer_list[$i]['id'],
                $i + 1,
                $customer_list[$i]['customer_name'],
                $customer_list[$i]['customertype_name'],
                $customer_list[$i]['business_name'],
                $customer_list[$i]['business_address'],
                $customer_list[$i]['secondary_contact_name'],
                $customer_list[$i]['customer_email'],
                $customer_list[$i]['customer_phone'],
                $customer_list[$i]['region_name'],
                $customer_list[$i]['area_name'],
                $customer_list[$i]['state_name'],
                $customer_list[$i]['city_name'],
                $customer_list[$i]['pincode'],
                $customer_list[$i]['user_name'],
                date('d-M-Y', strtotime($customer_list[$i]['created_on']))
            );
        }
        return $excel_array;
    }

    public static function send_excel_report($file_path, $excel_array, $email_id, $filename,$mailsubject) {
        //echo $email_id;exit;
        if (count($excel_array) > 1) {            
            Excel::create($filename, function($excel) use($file_path, $excel_array, $email_id, $filename,$mailsubject) {
                $excel->sheet('Sheet 1', function($sheet) use($excel_array) {
                    //$sheet->fromArray($excel_array);
                    $sheet->fromArray($excel_array, null, 'A1', true, false);
                });                
                //})->export('xls');        
                //        echo storage_path('excel/exports');
                //        echo public_path('excel/exports');
                //        exit;          
            })->store('xls', $file_path);
        }
    //sleep(100);
    if(file_exists($file_path.'/'.$filename.'.xls')){    
        $user_data['mail_subject'] = $mailsubject;
        Mail::send(['html' => 'pages.report.email'], ['text' => $user_data], function ($message) use($file_path, $excel_array, $email_id, $filename,$mailsubject) {
         //print_r($user_data);exit;
         $message->subject($mailsubject);
         $message->from(Config::get('constants.from_mail'), Config::get('constants.from_mail_text'));
         $message->attach($file_path.'/'.$filename.'.xls');
         $message->to($email_id);
         });   
      //   sleep(100); 
    }
        
    }

    public static function new_customer_meeting_added_by_users($user_ids, $start_date, $end_date) {
        $excel_array = array();
        $customer_meeting_date_log = array();
        $customer_meeting_date_log = Meeting::select('*', 'pincode','city_name', 'customertype_name', 'meeting_customer.brand_id as brand_ids', 'user.id as uid', 'meeting_customer.created_on as meeting_date', 'meeting_customer.id as mid', 'area.area_name as carea', 'state.state_name as cstate', 'region.region_name as cregion', 'meeting_customer.extra_note as mnote')
                ->leftJoin('user', 'user.id', '=', 'meeting_customer.user_id')
                ->leftJoin('folloup_meeting_customer', 'folloup_meeting_customer.meeting_id', '=', 'meeting_customer.id')
                ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')
                ->leftJoin('customer_type', 'customer_type.id', '=', 'customer.customer_type_id')
                ->leftJoin('brand', 'brand.id', '=', 'meeting_customer.brand_id')
                ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                ->leftJoin('state', 'state.id', '=', 'customer.state_id')
                ->leftJoin('city', 'city.id', '=', 'customer.city_id')
                ->leftJoin('area', 'area.id', '=', 'customer.area_id')
                ->leftJoin('meeting_purpose', 'meeting_purpose.id', '=', 'meeting_customer.purpose_id')
                ->wherein('meeting_customer.user_id', $user_ids)
                //->where(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y')"),$on_date)
                ->where(DB::raw("DATE(meeting_customer.created_on)"), '>=', $start_date)
                ->where(DB::raw("DATE(meeting_customer.created_on)"), '<=', $end_date)
                ->orderby('meeting_customer.created_on', 'desc')
                ->get()
                ->toArray();

        for ($i = 0; $i < count($customer_meeting_date_log); $i++) {
            $brand_ids = Brand::select('*')->wherein('id', explode(',', $customer_meeting_date_log[$i]['brand_ids']))->get()->toArray();
            $br_text = array();
            foreach ($brand_ids as $br) {
                $br_text[] = $br['brand_name'];
            }
            $customer_meeting_date_log[$i]['brand_name'] = implode(',', $br_text);
        }

        $excel_array[] = ['id', 'Employee Name', 'Business Name', 'Customer Name','Customer Type','Business Address','Secondary Contact Name','Cstomer Email','Customer Phone','Customer Birthdate','Customer Anniversary Date', 'Note', 'Brand Name', 'Purpose Name', 'Other Purpose Note', 'Region', 'State', 'City Name', 'Area',  'Pincode','Followup Date', 'Meeting Date'];

        for ($i = 0; $i < count($customer_meeting_date_log); $i++) {
            
                if(empty($customer_meeting_date_log[$i]['customer_birth_date'])){
                    $bdate = '';
                }else{
                    //$bdate = date('d-M-Y',strtotime($customer_meeting_date_log[$i]['customer_birth_date']));
                    $bdate = $customer_meeting_date_log[$i]['customer_birth_date'];
                }

                if(empty($customer_meeting_date_log[$i]['customer_anniversary_date'])){
                    $adate = '';
                }else{
                    //$adate = date('d-M-Y',strtotime($customer_meeting_date_log[$i]['customer_anniversary_date']));
                    $adate = $customer_meeting_date_log[$i]['customer_anniversary_date'];
                }            
            
            
            $followup_date_time = (!(empty($customer_meeting_date_log[$i]['followup_date_time']) and $customer_meeting_date_log[$i]['followup_date_time'] == null)) ? date('d-M-Y h:i:s A', strtotime($customer_meeting_date_log[$i]['followup_date_time'])) : '';
            $meeting_date_time = date('d-M-Y h:i:s A', strtotime($customer_meeting_date_log[$i]['meeting_date']));
            $excel_array[] = array(
                $i + 1,
                $customer_meeting_date_log[$i]['user_name'],
                $customer_meeting_date_log[$i]['business_name'],
                $customer_meeting_date_log[$i]['customer_name'],
                $customer_meeting_date_log[$i]['customertype_name'],
                $customer_meeting_date_log[$i]['business_address'],
                $customer_meeting_date_log[$i]['secondary_contact_name'],
                $customer_meeting_date_log[$i]['customer_email'],
                $customer_meeting_date_log[$i]['customer_phone'],
                $bdate,
                $adate,
                $customer_meeting_date_log[$i]['mnote'],
                $customer_meeting_date_log[$i]['brand_name'],
                $customer_meeting_date_log[$i]['purpose_name'],
                $customer_meeting_date_log[$i]['meeting_other_purpose'],
                $customer_meeting_date_log[$i]['cregion'],
                $customer_meeting_date_log[$i]['cstate'],
                $customer_meeting_date_log[$i]['city_name'],
                $customer_meeting_date_log[$i]['carea'],
                $customer_meeting_date_log[$i]['pincode'],
                $followup_date_time,
                $meeting_date_time
            );
        }
        return $excel_array;
    }    
    

    public static function brand_weekly_by_users($user_ids, $start_date, $end_date) {
        $brand_list = Brand::select('brand.*')
                        ->leftJoin('user_brand', 'user_brand.brand_id', '=', 'brand.id')
                        ->groupby('brand.id')
                        ->wherein('user_brand.user_id', $user_ids)->get()->toArray();
        
        $excel_array = array();
        $excel_array[] = ['id', 'Brand Name', 'Total Meeting'];
        for ($b = 0; $b < count($brand_list); $b++) {
            $bid = $brand_list[$b]['id'];
            $customer_meeting_date_log = Meeting::select('*')
                            ->where(DB::raw("find_in_set($bid,brand_id)"), '<>', 0)
                            ->wherein('user_id', $user_ids)
                            ->where(DB::raw("DATE(meeting_customer.created_on)"), '>=', $start_date)
                            ->where(DB::raw("DATE(meeting_customer.created_on)"), '<=', $end_date)
                            //->toSql();
                            ->get()->toArray();

            $excel_array[] = array(
                $b + 1,
                $brand_list[$b]['brand_name'],
                count($customer_meeting_date_log)
            );
        }
        return $excel_array;
    }

    public static function purpose_weekly_by_users($user_ids, $start_date, $end_date) {
        $purpose_list = Purpose::select('*')->where('delete_status', 0)->where('disable_status', 0)->get()->toArray();
        $excel_array = array();
        $excel_array[] = ['id', 'Purpose Name', 'Total Meeting'];
        for ($b = 0; $b < count($purpose_list); $b++) {
            $pid = $purpose_list[$b]['id'];
            $customer_meeting_date_log = Meeting::select('*')
                            ->where(DB::raw("find_in_set($pid,purpose_id)"), '<>', 0)
                            ->wherein('user_id', $user_ids)
                            ->where(DB::raw("DATE(meeting_customer.created_on)"), '>=', $start_date)
                            ->where(DB::raw("DATE(meeting_customer.created_on)"), '<=', $end_date)
                            //->toSql();
                            ->get()->toArray();

            $excel_array[] = array(
                $b + 1,
                $purpose_list[$b]['purpose_name'],
                count($customer_meeting_date_log)
            );
        }
        return $excel_array;
    }

    public static function region_weekly_by_users($user_ids, $region,$on_start_date, $on_end_date) {
        $region_ids = array();
        $data = array();
        $data['region'] = array();
        //foreach($region as $r){
        for ($i = 0; $i < count($region); $i++) {
            //$region_ids[] = $r['id'];
            $customer_list = Customer::select('*')->where('region_id', $region[$i]['id'])->get()->toArray();
            $customer_ids = array();
            foreach ($customer_list as $cst) {
                $customer_ids[] = $cst['id'];
            }



            $meeting_data = Meeting::select('*')->wherein('customer_id', $customer_ids)
                    ->where(DB::raw("DATE(meeting_customer.created_on)"), '>=', $on_start_date)
                    ->where(DB::raw("DATE(meeting_customer.created_on)"), '<=', $on_end_date)
                    ->get()
                    ->toArray();

            $data['region'][$i]['region_id'] = $region[$i]['id'];
            $data['region'][$i]['region_name'] = $region[$i]['region_name'];
            $data['region'][$i]['meeting_count'] = count($meeting_data);
            $data['region'][$i]['state'] = array();
            //state start
            $state_list = RegionDetail::select('*')->where('region_id', $region[$i]['id'])->get()->toArray();
            for ($j = 0; $j < count($state_list); $j++) {
                $state_detail = State::select('*')->where('id', $state_list[$j]['state_id'])->get()->toArray();

                $customer_list = Customer::select('*')->where('state_id', $state_list[$j]['state_id'])->get()->toArray();
                $customer_ids = array();
                foreach ($customer_list as $cst) {
                    $customer_ids[] = $cst['id'];
                }

                $meeting_data = Meeting::select('*')->wherein('customer_id', $customer_ids)
                        ->where(DB::raw("DATE(meeting_customer.created_on)"), '>=', $on_start_date)
                        ->where(DB::raw("DATE(meeting_customer.created_on)"), '<=', $on_end_date)
                        ->get()
                        ->toArray();

                $data['region'][$i]['state'][$j]['state_id'] = $state_detail[0]['id'];
                $data['region'][$i]['state'][$j]['state_name'] = $state_detail[0]['state_name'];
                $data['region'][$i]['state'][$j]['meeting_count'] = count($meeting_data);
            }
            //state end
        }
        
        
        $excel_array = array();
        $excel_array[] = ['id', 'Region Name', 'Total Meeting'];
        for ($b = 0; $b < count($data['region']); $b++) {
            $meet = 0;
            if($data['region'][$b]['meeting_count'] > 0){
                $meet = $data['region'][$b]['meeting_count'];
            }
            $excel_array[] = array(
                $b + 1,
                $data['region'][$b]['region_name'],
                $meet                
            );
        }
        return $excel_array;        
    }

    public static function state_weekly_by_users($user_ids, $state,$on_start_date, $on_end_date) {       
        
        $region_ids = array();
        $data = array();        
        //foreach($region as $r){
        for ($i = 0; $i < count($state); $i++) {
            //$region_ids[] = $r['id'];
            $customer_list = Customer::select('*')->where('state_id', $state[$i]['id'])->get()->toArray();
            $customer_ids = array();
            foreach ($customer_list as $cst) {
                $customer_ids[] = $cst['id'];
            }



            $meeting_data = Meeting::select('*')->wherein('customer_id', $customer_ids)
                    ->where(DB::raw("DATE(meeting_customer.created_on)"), '>=', $on_start_date)
                    ->where(DB::raw("DATE(meeting_customer.created_on)"), '<=', $on_end_date)
                    ->get()
                    ->toArray();

            $data['state'][$i]['state_id'] = $state[$i]['id'];
            $data['state'][$i]['state_name'] = $state[$i]['state_name'];
            $data['state'][$i]['meeting_count'] = count($meeting_data);            
            
        }
        
        $excel_array = array();
        $excel_array[] = ['id', 'State Name', 'Total Meeting'];
        for ($b = 0; $b < count($data['state']); $b++) {
            
            $meet = 0;
            if($data['state'][$b]['meeting_count'] > 0){
                $meet = $data['state'][$b]['meeting_count'];
            }
            $excel_array[] = array(
                $b + 1,
                $data['state'][$b]['state_name'],
                $meet                
            );
            
        }
        return $excel_array;        
    }

/**
 *
 * Methods for Decative
 *
 */


    public static function all_vp_under_user_list() {
        $user = User::select('*')
                ->where('delete_status', 0)
                ->get()
                ->toArray();
        $user_ids = array();
        for ($i = 0; $i < count($user); $i++) {
            //$user_ids = $user[$i]['id'].','.$user_ids;
            $user_ids[] = $user[$i]['id'];
        }

        return $user_ids;
    }

    public static function all_rsm_under_user_list($rsm_user) {
        
            $user = User::select('user.*', 'region_name', 'area_name')
                            ->leftJoin('region', 'region.id', '=', 'user.region_id')
                            ->leftJoin('area', 'area.id', '=', 'user.area_id')
                            ->where(function ($query) {
                                $query->where('user.user_type', '=', 2)
                                ->orWhere('user.user_type', '=', 3)
                                ->orWhere('user.user_type', '=', 1);
                            })
                            ->where('user.region_id', $rsm_user['region_id'])
                            ->where('user.delete_status', 0)
                            ->orderby('user_name')
                            ->get()->toArray();        $user_ids = array();
        for ($i = 0; $i < count($user); $i++) {
            //$user_ids = $user[$i]['id'].','.$user_ids;
            $user_ids[] = $user[$i]['id'];
        }

        return $user_ids;
    }

    public static function all_asm_under_user_list($asm_user) {
        
            $user = User::select('user.*', 'region_name', 'area_name')
                            ->leftJoin('region', 'region.id', '=', 'user.region_id')
                            ->leftJoin('area', 'area.id', '=', 'user.area_id')
                            ->where('user.assign_to_user', $asm_user['id'])
                            ->where('user.delete_status', 0)
                            //->where('user.id', $asm_id)
                            ->orderby('user_name')
                            ->get()->toArray();
        $user_ids = array();
        for ($i = 0; $i < count($user); $i++) {
            //$user_ids = $user[$i]['id'].','.$user_ids;
            $user_ids[] = $user[$i]['id'];
        }
        $user_ids[] = $asm_user['id'];
        return $user_ids;
    }

}
