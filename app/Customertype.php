<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customertype extends Model
{
         protected $table = "customer_type";
       public $timestamps = false;
       protected $fillable = [
                                "customertype_name",
                                "user_id",
                                "delete_status",
                                "disable_status",
                                "created_on",
                             ];  
}
