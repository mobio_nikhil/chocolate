<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
     protected $table = "meeting_customer";
       public $timestamps = false;
       protected $fillable = [
                                "user_id",
                                "customer_id",
                                "brand_id",
                                "purpose_id",
                                "extra_note",
                                "meeting_other_purpose",
                                "meeting_image_1",
                                "meeting_image_2",
                                "meeting_image_3",
                                "meeting_image_4",
                                "meeting_image_5",
                                "region_sales_manager_id",
                                "area_sales_manager_id",
                                "customer_name",
                                "created_on",
                             ];   
}
