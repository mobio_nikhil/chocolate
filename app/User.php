<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user';
    public $timestamps = false;
   
    protected $fillable = [
                            "user_name",
                            "user_email",
                            "user_phone",
                            "user_type",
                            "region_id",
                            "area_id",
                            "assign_to_user",
                            "user_code",
                            "user_password",
                            "user_passcode",
                            "device_token",
                            "app_os",
                            "notification_token",
                            "disable_status",
                            "delete_status",
                            "created_on",
                            "user_admin_backoffice_type",
                         ];     
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    //protected $hidden = ['password', 'remember_token'];
    
    public function getAuthPassword() {
        return $this->user_password;
    }    
    
}
