<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get ('/', function () {
    return view('welcome');
});

Route::post('api/v1/login', 'ApiUserController@login');
Route::post('api/v1/forgot_password', 'ApiUserController@forgot_password');    

Route::group(['prefix' => 'api/v1', 'middleware' => 'token_check'], function() {
    Route::post('startday', 'ApiUserController@startday');    
    Route::post('endday', 'ApiUserController@endday');    
    Route::post('customer_entry', 'ApiCustomerController@customer_entry');    
    Route::post('customer_add', 'ApiCustomerController@customer_add');    
    Route::post('customer_edit', 'ApiCustomerController@customer_edit');    
    Route::post('meeting_add', 'ApiCustomerController@meeting_add');    
    Route::post('customer_view', 'ApiCustomerController@customer_view');    
    Route::post('customer_edit_entry', 'ApiCustomerController@customer_edit_entry');    
    Route::post('my_history', 'ApiUserController@my_history');    
    Route::post('my_history_detail', 'ApiUserController@my_history_detail');    
    Route::post('customer_history', 'ApiCustomerController@customer_history');    
    Route::post('customer_history_detail', 'ApiCustomerController@customer_history_detail');    
    Route::post('change_password', 'ApiUserController@change_password');    
    Route::post('brand_list', 'ApiCustomerController@brand_list');    
    Route::post('purpose_list', 'ApiCustomerController@purpose_list');    
    Route::post('my_region', 'ApiCustomerController@my_region');    
    Route::post('my_area', 'ApiCustomerController@my_area');    
    Route::post('get_all_info', 'ApiCustomerController@get_all_info');    
    Route::post('business_list', 'ApiCustomerController@business_list');    
    Route::post('get_state_area', 'ApiUserController@get_state_area');    
    Route::post('get_city', 'ApiUserController@get_city');    
    Route::post('today_status', 'ApiUserController@today_status');    
});

Route::get("/helper/api/v1/stateArea/{id}","AdminAreaController@stateAreaApi");
Route::get("/helper/api/v1/getcitybystate/{id}","AdminAreaController@getcitybystate");
Route::get (Config::get('constants.admin_path'),'AdminUserController@index');
Route::post(Config::get('constants.admin_path').'login','AdminUserController@login');

Route::get  ('user/forgot','AdminUserController@forgotIndex');    
Route::post ('user/forgot','AdminUserController@forgotRequest');
// Route::any  ('user/reset/{data}','AdminUserController@forgotApply');
// Route::post  ('user/reset','AdminUserController@forgotApply');

Route::group(['prefix' => Config::get('constants.admin_path'), 'middleware' => 'login_check'], function() {

    Route::get ('dashboard','AdminUserController@dashboard');    
    Route::post('dashboard/map','AdminUserController@mapFilter');
    Route::post('dashboard/map-updates','AdminUserController@MapUdpates');    
    
    /**
     * Purpose
     */
    Route::get ('purpose/index','AdminPurposeController@index');    
    Route::get ('purpose/create','AdminPurposeController@create');    
    Route::post('purpose/store','AdminPurposeController@store');    
    Route::get ('purpose/edit/{id}','AdminPurposeController@edit');    
    Route::post('purpose/update/{id}','AdminPurposeController@update');
    Route::any ('purpose/destroy/{id}','AdminPurposeController@destroy');    
    Route::any ('purpose/status/{status}/{id}','AdminPurposeController@changeStatus'); 
    
    
    Route::get ('customertype/index','AdminCustomertypeController@index');    
    Route::get ('customertype/create','AdminCustomertypeController@create');    
    Route::post('customertype/store','AdminCustomertypeController@store');    
    Route::get ('customertype/edit/{id}','AdminCustomertypeController@edit');    
    Route::post('customertype/update/{id}','AdminCustomertypeController@update');
    Route::any ('customertype/destroy/{id}','AdminCustomertypeController@destroy');    
    Route::any ('customertype/status/{status}/{id}','AdminCustomertypeController@changeStatus'); 
    

    /**
     * Brand
     */
    Route::get ('brand/index','AdminBrandController@index');    
    Route::get ('brand/create','AdminBrandController@create');    
    Route::any ('brand/destroy/{id}','AdminBrandController@destroy');
    Route::post('brand/store','AdminBrandController@store');    
    Route::get ('brand/edit/{id}','AdminBrandController@edit'); 
    Route::post('brand/update/{id}','AdminBrandController@update');
    Route::any ('brand/status/{status}/{id}','AdminBrandController@changeStatus'); 


 /**
     * Region
     */
    Route::get ('region/index','AdminRegionController@index');    
    Route::get ('region/create','AdminRegionController@create');    
    Route::any ('region/destroy/{id}','AdminRegionController@destroy');
    Route::post('region/store','AdminRegionController@store');    
    Route::get ('region/edit/{id}','AdminRegionController@edit'); 
    Route::post('region/update/{id}','AdminRegionController@update');
    Route::any ('region/status/{status}/{id}','AdminRegionController@changeStatus'); 



 /**
     * Area
     */
    Route::get ('area/index','AdminAreaController@index');    
    Route::get ('area/create','AdminAreaController@create');    
    Route::any ('area/destroy/{id}','AdminAreaController@destroy');
    Route::post('area/store','AdminAreaController@store');    
    Route::get ('area/edit/{id}','AdminAreaController@edit'); 
    Route::post('area/update/{id}','AdminAreaController@update');
    Route::any ('area/status/{status}/{id}','AdminAreaController@changeStatus'); 


    /*Route::patch('purpose/update/{id}',[
        'as' => 'purpose.update',
        'uses' => 'AdminPurposeController@update'
    ]);    */
    
    Route::get ('user/create','AdminUserController@create');    
    Route::post('user/store','AdminUserController@store');    
    Route::get ('user/user_list','AdminUserController@user_list');    
    Route::get ('user/edit/{id}','AdminUserController@edit');    
    Route::post('user/update/{id}','AdminUserController@update');    
    Route::any('user/delete/{id}','AdminUserController@destroy');    
    Route::get ('user/assignment/{id}','AdminUserController@assignment');    
    Route::post('user/assignment_store/{id}','AdminUserController@assignment_store');    
    Route::post ('user/change_status/{status}','AdminUserController@changeStatus');    
    Route::get ('user/deactive/list','AdminUserController@deactiveList');    


    Route::get ('employee/employee_list','AdminEmployeeController@employee_list');    
    Route::get ('employee/track/{id}','AdminEmployeeController@track');    
    Route::get ('employee/employee_history/{id}','AdminEmployeeController@employee_history');    


    Route::get ('customer/customer_list','AdminCustomerController@customer_list');    
    Route::get ('customer/new_list','AdminCustomerController@new_list');    
    Route::get ('customer/create','AdminCustomerController@create');    
    Route::post('customer/store','AdminCustomerController@store');    
    Route::get ('customer/edit/{id}','AdminCustomerController@edit');    
    Route::post('customer/update/{id}','AdminCustomerController@update');    
    Route::get ('customer/customer_history/{id}','AdminCustomerController@customer_history');    
    
    Route::get('customer/trash/list','AdminCustomerController@trashList');    
    Route::any('customer/trash/{id}','AdminCustomerController@trashAdd');    
    Route::get('customer/trash/restore/{id}','AdminCustomerController@trashRestore');    

    Route::post('customer/change_status/{status}/','AdminCustomerController@changeStatus');  
    
    Route::get('customer/approve/{id}/','AdminCustomerController@approve');  
        
    Route::get('customer/deactive/list','AdminCustomerController@deactiveList');    
    // Route::get('customer/deactive/list','AdminCustomerController@deactiveList');    

    
    Route::get ('meeting/meeting_list','AdminMeetingController@meeting_list');    
    Route::get ('meeting/meeting_history/{id}','AdminMeetingController@meeting_history');    
    Route::get ('meeting/track/{id}','AdminMeetingController@track');   
    Route::get ('meeting/create','AdminMeetingController@create');    
    Route::post('meeting/store','AdminMeetingController@store');    
    Route::any('employee_track/create','AdminEmployeeTrackController@create');    
    Route::post('employee_track/store','AdminEmployeeTrackController@store');    
    Route::get ('user/logout','AdminUserController@logout');    
  
    Route::get ('setting/index','AdminSettingController@index');    
    Route::get ('setting/password','AdminSettingController@passwordIndex');    
    Route::post('setting/password','AdminSettingController@passwordChange');    
  
    Route::get ('report/index','AdminReportController@index');    
    Route::get ('report/create','AdminReportController@create');    
    Route::get ('report/on_date_meeting_list','AdminReportController@on_date_meeting_list');    
    Route::any ('report/admin_index','AdminReportController@admin_index');    
    
    Route::post('meeting/get_user_brand','AdminMeetingController@get_user_brand');    
    Route::post('meeting/get_user_customer','AdminMeetingController@get_user_customer');    
    Route::get('meeting/today_meeting','AdminMeetingController@today_meeting');    
    Route::get('meeting/upcoming_followup','AdminMeetingController@upcoming_followup');    
    Route::get('meeting/today_followup','AdminMeetingController@today_followup');    
    Route::get('customer/today_customer','AdminCustomerController@today_customer');    
});
    Route::get('cron/index','CronController@index');    
    Route::get('testmail','CronController@testmail');    
    Route::get('php_info','CronController@php_info');    
    //Route::get('fix_table','CronController@fix_table');    

Route::get('/clear-cache', function() {
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    Artisan::call('config:clear');
    Artisan::call('cache:clear');
    //Cache::flush();
    echo 'Done';
    // return what you want
});
