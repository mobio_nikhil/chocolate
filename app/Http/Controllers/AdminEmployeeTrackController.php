<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use App\User;
use Input;
use Validator;
use App\Helpers\Helper;
use App\GpsData;
use App\Meeting;
use App\Customer;
use App\Region;
use App\Brand;
use App\Purpose;
use App\Area;
use DB;
use Redirect;
use Config;
use Session;
use Route;
use View;

class AdminEmployeeTrackController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        if (Input::isMethod('post')) {

            $input_all = Input::all();

            $rules = array();

            $rules['track_date'] = 'required|date_format:"d-m-Y"';
            $rules['employee_name'] = 'required|numeric';


            $validator = Validator::make($input_all, $rules);
            // check if the validator failed -----------------------
            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to(Config::get('constants.admin_path') . 'employee_track/create')
                                ->withErrors($validator)
                                ->withInput();
            }


            $session_user_info = Session::get('session_user_info');
            $user_ids = array();
                if($session_user_info['user_type'] == 0){
                    $user_ids = Helper::vp_under_user_list();            
                }
                if($session_user_info['user_type'] == 1){            
                    $user_ids = Helper::rsm_under_user_list($session_user_info) ;
                }
                if($session_user_info['user_type'] == 2){            
                    $user_ids = Helper::asm_under_user_list($session_user_info) ;
                }
        $user = User::select('user.*','region_name','area_name')
                            ->leftJoin('region', 'region.id', '=', 'user.region_id')
                            ->leftJoin('area', 'area.id', '=', 'user.area_id')
                            ->wherein('user.id',$user_ids)
                            ->get()
                            ->toArray();                
                
            $id = $input_all['employee_name'];

            if (!(in_array($id, $user_ids))) {
                return Redirect::to(Config::get('constants.admin_path') . 'employee_track/create')->withErrors(['msg', 'The selected employee id is invalid.'])->withInput();
            }



            $user_to_search = User::select('user.*', 'region_name', 'area_name', 'assignment_user.user_name as assignment_username')
                            ->leftJoin('region', 'region.id', '=', 'user.region_id')
                            ->leftJoin('area', 'area.id', '=', 'user.area_id')
                            ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                            ->where('user.delete_status', 0)
                            ->where('user.disable_status', 0)
                            ->where('user.id', $id)
                            ->orderby('user_name')
                            ->get()->toArray();

            $td = explode('-', $input_all['track_date']);
            $td = $td[2] . '-' . $td[1] . '-' . $td[0];
            //$past_start = date('Y-m-d');
            $past_start = $td;
            $past_start = $past_start . " 00:00:00";
            //$past_end = date('Y-m-d H:i:s');
            $past_end = $td . ' ' . '23:59:59';

            $user_gps = GpsData::select('*', 'gps_data.created_on as log_time')
                    ->where('gps_data.user_id', $user_to_search[0]['id'])
                    ->leftJoin('meeting_customer', 'meeting_customer.id', '=', 'gps_data.meeting_id')
                    ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')
                    ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                    ->leftJoin('state', 'state.id', '=', 'customer.state_id')
                    ->leftJoin('area', 'area.id', '=', 'customer.area_id')
                    ->where(DB::raw("DATE(gps_data.created_on)"), '>=', $past_start)
                    ->where(DB::raw("DATE(gps_data.created_on)"), '<=', $past_end)
                    ->orderby('gps_data.created_on')
                    ->get()
                    ->toArray();

            for ($i = 0; $i < count($user_gps); $i++) {
                if ($user_gps[$i]['gps_data_type'] == 0) {
                    $user_gps[$i]['map_header'] = 'Start Day';
                    $user_gps[$i]['map_content'] = 'Start of the day';
                    $user_gps[$i]['map_time_date'] = date('d-m-Y h:i:s A', strtotime($user_gps[$i]['log_time']));
                }

                if ($user_gps[$i]['gps_data_type'] == 1) {
                    $user_gps[$i]['map_header'] = $user_gps[$i]['business_name'];
                    $user_gps[$i]['map_content'] = $user_gps[$i]['area_name'].' ('.$user_gps[$i]['customer_phone'].')';
                    $user_gps[$i]['map_time_date'] = date('d-m-Y h:i:s A', strtotime($user_gps[$i]['log_time']));
                }

                if ($user_gps[$i]['gps_data_type'] == 2) {
                    $user_gps[$i]['map_header'] = 'End Day';
                    $user_gps[$i]['map_content'] = 'End of the day';
                    $user_gps[$i]['map_time_date'] = date('d-m-Y h:i:s A', strtotime($user_gps[$i]['log_time']));
                }
            }

            $data = array();
            $data['user'] = $user_to_search;
            $data['user_gps'] = $user_gps;
            $data['user_list'] = $user;
            $data['input_all'] = Input::all();
            $data['user_list'] = $user;

//              return View::make('pages.employee_track.create_by_name')
//                        ->with('user', $user_to_search)
//                        ->with('user_gps', $user_gps)                        
//                        ->with('user_list', $user)
//                        ->with_input(Input::all());

            return view('pages/employee_track/create_by_name', $data)->withInput(Input::all());
            //return view('pages.employee_track.track',$data);
        } else {
            $session_user_info = Session::get('session_user_info');
            $user_ids = array();
            if($session_user_info['user_type'] == 0){
                $user_ids = Helper::vp_under_user_list();            
            }
            if($session_user_info['user_type'] == 1){            
                $user_ids = Helper::rsm_under_user_list($session_user_info) ;
            }
            if($session_user_info['user_type'] == 2){            
                $user_ids = Helper::asm_under_user_list($session_user_info) ;
            }
            $user = User::select('user.*','region_name','area_name')
                            ->leftJoin('region', 'region.id', '=', 'user.region_id')
                            ->leftJoin('area', 'area.id', '=', 'user.area_id')
                            ->wherein('user.id',$user_ids)
                            ->get()
                            ->toArray();            

            $data = array();
            $data['user_list'] = $user;
            $data['input_all'] = array('track_date' => '', 'employee_name' => '');

            return view('pages/employee_track/create_by_name', $data);
        }
    }

    public function create_old() {

        $session_user_info = Session::get('session_user_info');
        if ($session_user_info['user_type'] == 1) {
            $user = User::select('user.*', 'region_name', 'area_name')
                            ->leftJoin('region', 'region.id', '=', 'user.region_id')
                            ->leftJoin('area', 'area.id', '=', 'user.area_id')
                            //->leftJoin('user as rsm_assignment_user', 'rsm_assignment_user.id', '=', 'user.assign_to_user')                                
                            ->where(function ($query) use($session_user_info) {
                                $query->where('user.user_type', '=', 2)
                                ->orWhere('user.user_type', '=', 3);
                            })
                            ->orwhere(function ($query) use($session_user_info) {
                                $query->where('user.user_type', '=', 1)
                                ->Where('user.region_id', '=', $session_user_info['region_id']);
                            })
                            ->where('user.delete_status', 0)
                            ->where('user.disable_status', 0)
                            ->orderby('user_name')
                            ->get()->toArray();
        }

        if ($session_user_info['user_type'] == 2) {
            $user = User::select('user.*', 'region_name', 'area_name')
                            ->leftJoin('region', 'region.id', '=', 'user.region_id')
                            ->leftJoin('area', 'area.id', '=', 'user.area_id')
                            //->leftJoin('user as rsm_assignment_user', 'rsm_assignment_user.id', '=', 'user.assign_to_user')                                
                            ->where(function ($query) {
                                $query->where('user.user_type', '=', 3);
                                //->orWhere('user.user_type', '=', 3);
                            })
                            ->orwhere(function ($query) use($session_user_info) {
                                $query->where('user.user_type', '=', 2)
                                ->Where('user.area_id', '=', $session_user_info['area_id'])
                                ->Where('user.region_id', '=', $session_user_info['region_id']);
                            })
                            ->where('user.delete_status', 0)
                            ->where('user.disable_status', 0)
                            ->orderby('user_name')
                            ->get()->toArray();
        }

        if ($session_user_info['user_type'] == 0) {
            $user = User::select('user.*', 'region_name', 'area_name')
                            ->leftJoin('region', 'region.id', '=', 'user.region_id')
                            ->leftJoin('area', 'area.id', '=', 'user.area_id')
                            //->leftJoin('user as rsm_assignment_user', 'rsm_assignment_user.id', '=', 'user.assign_to_user')                
                            ->where('user.delete_status', 0)
                            ->where('user.disable_status', 0)
                            ->orderby('user_name')
                            ->get()->toArray();
        }

        $data = array();
        $data['user_list'] = $user;

        return view('pages/employee_track/create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_old(Request $request) {

        $input_all = Input::all();

        $rules = array();
        $rules['searchby'] = 'required';
        $rules['track_date'] = 'required|date_format:"d-m-Y"';
        if ($input_all['searchby'] == 1) {
            $rules['employee_id'] = 'required|numeric|exists:user,id';
        }
        if ($input_all['searchby'] == 2) {
            $rules['employee_name'] = 'required|numeric';
        }

        $validator = Validator::make($input_all, $rules);
        // check if the validator failed -----------------------
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to(Config::get('constants.admin_path') . 'employee_track/create')
                            ->withErrors($validator)
                            ->withInput();
        }


        $session_user_info = Session::get('session_user_info');
        if ($session_user_info['user_type'] == 1) {
            $user = User::select('user.*', 'region_name', 'area_name')
                            ->leftJoin('region', 'region.id', '=', 'user.region_id')
                            ->leftJoin('area', 'area.id', '=', 'user.area_id')
                            //->leftJoin('user as rsm_assignment_user', 'rsm_assignment_user.id', '=', 'user.assign_to_user')                                
                            ->where(function ($query) use($session_user_info) {
                                $query->where('user.user_type', '=', 2)
                                ->orWhere('user.user_type', '=', 3);
                            })
                            ->orwhere(function ($query) use($session_user_info) {
                                $query->where('user.user_type', '=', 1)
                                ->Where('user.region_id', '=', $session_user_info['region_id']);
                            })
                            ->where('user.delete_status', 0)
                            ->where('user.disable_status', 0)
                            ->orderby('user_name')
                            ->get()->toArray();
        }

        if ($session_user_info['user_type'] == 2) {
            $user = User::select('user.*', 'region_name', 'area_name')
                            ->leftJoin('region', 'region.id', '=', 'user.region_id')
                            ->leftJoin('area', 'area.id', '=', 'user.area_id')
                            //->leftJoin('user as rsm_assignment_user', 'rsm_assignment_user.id', '=', 'user.assign_to_user')                                
                            ->where(function ($query) {
                                $query->where('user.user_type', '=', 3);
                                //->orWhere('user.user_type', '=', 3);
                            })
                            ->orwhere(function ($query) use($session_user_info) {
                                $query->where('user.user_type', '=', 2)
                                ->Where('user.area_id', '=', $session_user_info['area_id'])
                                ->Where('user.region_id', '=', $session_user_info['region_id']);
                            })
                            ->where('user.delete_status', 0)
                            ->where('user.disable_status', 0)
                            ->orderby('user_name')
                            ->get()->toArray();
        }

        if ($session_user_info['user_type'] == 0) {
            $user = User::select('user.*', 'region_name', 'area_name')
                            ->leftJoin('region', 'region.id', '=', 'user.region_id')
                            ->leftJoin('area', 'area.id', '=', 'user.area_id')
                            //->leftJoin('user as rsm_assignment_user', 'rsm_assignment_user.id', '=', 'user.assign_to_user')                
                            ->where('user.delete_status', 0)
                            ->where('user.disable_status', 0)
                            ->orderby('user_name')
                            ->get()->toArray();
        }

        $user_ids = array();
        for ($i = 0; $i < count($user); $i++) {
            //$user_ids = $user[$i]['id'].','.$user_ids;
            $user_ids[] = $user[$i]['id'];
        }

        if (!(in_array($input_all['employee_id'], $user_ids))) {
            return Redirect::to(Config::get('constants.admin_path') . 'employee_track/create')->withErrors(['msg', 'The selected employee id is invalid.'])->withInput();
        }


        if ($input_all['searchby'] == 1) {
            $id = $input_all['employee_id'];
        }
        if ($input_all['searchby'] == 2) {
            $id = $input_all['employee_name'];
        }

        $user_to_search = User::select('user.*', 'region_name', 'area_name', 'assignment_user.user_name as assignment_username')
                        ->leftJoin('region', 'region.id', '=', 'user.region_id')
                        ->leftJoin('area', 'area.id', '=', 'user.area_id')
                        ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                        ->where('user.delete_status', 0)
                        ->where('user.disable_status', 0)
                        ->where('user.id', $id)
                        ->orderby('user_name')
                        ->get()->toArray();

        $td = explode('-', $input_all['track_date']);
        $td = $td[2] . '-' . $td[1] . '-' . $td[0];
        //$past_start = date('Y-m-d');
        $past_start = $td;
        $past_start = $past_start . " 00:00:00";
        //$past_end = date('Y-m-d H:i:s');
        $past_end = $td . ' ' . '23:59:59';

        $user_gps = GpsData::select('*', 'gps_data.created_on as log_time')
                ->where('gps_data.user_id', $user_to_search[0]['id'])
                ->leftJoin('meeting_customer', 'meeting_customer.id', '=', 'gps_data.meeting_id')
                ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')
                ->where(DB::raw("DATE(gps_data.created_on)"), '>=', $past_start)
                ->where(DB::raw("DATE(gps_data.created_on)"), '<=', $past_end)
                ->orderby('gps_data.created_on')
                ->get()
                ->toArray();

        for ($i = 0; $i < count($user_gps); $i++) {
            if ($user_gps[$i]['gps_data_type'] == 0) {
                $user_gps[$i]['map_header'] = 'Start Day';
                $user_gps[$i]['map_content'] = 'Start of the day';
                $user_gps[$i]['map_time_date'] = date('d-m-Y h:i:s A', strtotime($user_gps[$i]['log_time']));
            }

            if ($user_gps[$i]['gps_data_type'] == 1) {
                $user_gps[$i]['map_header'] = $user_gps[$i]['business_name'];
                $user_gps[$i]['map_content'] = $user_gps[$i]['customer_name'] . '<br/>' . $user_gps[$i]['business_address'] . '<br/>' . $user_gps[$i]['customer_phone'];
                $user_gps[$i]['map_time_date'] = date('d-m-Y h:i:s A', strtotime($user_gps[$i]['log_time']));
            }

            if ($user_gps[$i]['gps_data_type'] == 2) {
                $user_gps[$i]['map_header'] = 'End Day';
                $user_gps[$i]['map_content'] = 'End of the day';
                $user_gps[$i]['map_time_date'] = date('d-m-Y h:i:s A', strtotime($user_gps[$i]['log_time']));
            }
        }

        $data = array();
        $data['user'] = $user_to_search;
        $data['user_gps'] = $user_gps;
        return view('pages.employee_track.track', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
