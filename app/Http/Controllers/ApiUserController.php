<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Auth;
use Hash;
use App\User;
use App\UserArea;
use Input;
use Validator;
use App\Helpers\Helper;
use App\GpsData;
use App\Meeting;
use App\Area;
use App\City;
use App\State;
use App\Brand;
use DB;
use Mail;
use Config;

class ApiUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
//        DB::connection('mysql')->select(DB::raw('SET @@session.time_zone = "+05:30";')) ;      
//        $t = DB::connection('mysql')->select(DB::raw('SELECT @@system_time_zone')) ;      
        //DB::connection('mysql')->statement('SET @@session.time_zone = "+05:30"');
    }    
    
    public function login()
    {
        //
        //echo 'hello';
       // echo Hash::make('1234');
       //echo   Helper::shout('now');
       $input_all = Input::all();
        
        /*validation start*/
           $rules = array(
                'user_email' => 'required|email',                        // just a normal required validation
                'password'         => 'required',
                'notification_token'         => 'required',
                'device_token'         => 'required',
                'app_os'         => 'required',
            );        
           if(Helper::validation_error_response($input_all,$rules) != false)
                return Helper::validation_error_response($input_all,$rules);        
        /*validation end*/   
        
        $auth = Auth::attempt(['user_email' => $input_all['user_email'], 'password' => $input_all['password']]);
        
        if ($auth) {
            // Authentication passed...
            //print_r(Auth::user()->toArray());
            $user_info = Auth::user();
            
            if(!($user_info->disable_status == 0 && $user_info->delete_status == 0)){
                return Helper::response_api(false,'Login Faiil','');                
            }

            if($user_info->user_type == 1){
                if(!($user_info->region_id > 0)){
                    return Helper::response_api(false,'Region is not assign','');                
                }                
            }

            if($user_info->user_type == 2){
                if(!($user_info->region_id > 0)){
                    return Helper::response_api(false,'Region is not assign','');                
                }
                
                $user_area = UserArea::select('*')->where('user_id',$user_info->id)->get()->toArray();
                if(!(count($user_area) > 0)){
                    return Helper::response_api(false,'Area is not assign','');
                }
                
            }

            if($user_info->user_type == 3){
                if(($user_info->assign_to_user == null or $user_info->assign_to_user == '' )){
                    return Helper::response_api(false,'Area manager is not assign','');                
                }
                
                $user_area = UserArea::select('*')->where('user_id',$user_info->id)->get()->toArray();
                if(!(count($user_area) > 0)){
                    return Helper::response_api(false,'Area is not assign','');
                }
                
            }            

            
            $user_info->notification_token = $input_all['notification_token'];
            $user_info->device_token = $input_all['device_token'];
            $user_info->app_os = $input_all['app_os'];
            $user_info->save();
            
            $user_info = $user_info->toArray();
            if($user_info['disable_status'] == 0 && $user_info['delete_status'] == 0 ){
                $token = JWTAuth::fromUser(Auth::user());
                $data['token'] = $token;
                $data['user_info'] = Auth::user();
                return Helper::response_api(true,'Login Success',$data);                
            }else{
                return Helper::response_api(false,'Login Faiil','');                
            }            
        }else{
//            $response_array = array();
//            $response_array['status'] = ;
//            $response_array['message'] = ;
//            $response_array['data'] = '';
//            return response()->json($response_array, 200);
            return Helper::response_api(false,'Login Fail','');
            
            
        }        
        
        
    }
    
    public function startday(){
      $input_all = Input::all();
      
        /*validation start*/
           $rules = array(
                'token' => 'required',                        // just a normal required validation
                'latitude' => 'required',
                'longitude' => 'required',
            );        
           if(Helper::validation_error_response($input_all,$rules) != false)
                return Helper::validation_error_response($input_all,$rules);        
        /*validation end*/
        
        $user_info = (Helper::get_user_info($input_all['token']));
        $startday_data = GpsData::select('*')
                ->where('user_id',$user_info['id'])
                ->where('gps_data_type',0)
                ->where(DB::raw("DATE_FORMAT(gps_data.created_on,'%d/%m/%Y')"),date('d/m/Y'))
                ->get()->toArray();
        if(count($startday_data) > 0){
            return Helper::response_api(false,'Already Entred','');        
        }
        
        
        $insert_array = array();
        $insert_array['user_id'] = $user_info['id'];
        $insert_array['gps_data_type'] = 0;
        $insert_array['latitude'] = $input_all['latitude'];
        $insert_array['longitude'] = $input_all['longitude'];
        //print_r($insert_array);exit;
        $insert_array['created_on'] = date('Y-m-d H:i:s');
        if(GpsData::create($insert_array)) {
            return Helper::response_api(true,'Start Day Entered','');        
        }else{
            return Helper::response_api(false,'Start Day Failed','');                    
        }
    }

    public function endday(){
      $input_all = Input::all();
      
        /*validation start*/
           $rules = array(
                'token' => 'required',                        // just a normal required validation
                'latitude' => 'required',
                'longitude' => 'required',
            );        
           if(Helper::validation_error_response($input_all,$rules) != false)
                return Helper::validation_error_response($input_all,$rules);
        /*validation end*/
        
        $user_info = (Helper::get_user_info($input_all['token']));

            $td = date('Y-m-d');
            $past_start = $td;
            $past_start = $past_start . " 00:00:00";
            //$past_end = date('Y-m-d H:i:s');
            $past_end = $td . ' ' . '23:59:59';   
            
            $startday = GpsData::select('*')
                    ->where('gps_data_type',0)
                    ->where('user_id',$user_info['id'])
                    ->where(DB::raw("DATE(gps_data.created_on)"), '>=', $past_start)
                    ->where(DB::raw("DATE(gps_data.created_on)"), '<=', $past_end)                    
                    ->get()->toArray();
            
            if(!(count($startday) > 0)){
                return Helper::response_api(false,'Start day not started','');
            }        
        
        
        $startday_data = GpsData::select('*')
                ->where('user_id',$user_info['id'])
                ->where('gps_data_type',2)
                ->where(DB::raw("DATE_FORMAT(gps_data.created_on,'%d/%m/%Y')"),date('d/m/Y'))
                ->get()->toArray();
        if(count($startday_data) > 0){
            return Helper::response_api(false,'Already Entred','');        
        }
        
        
        $insert_array = array();
        $insert_array['user_id'] = $user_info['id'];
        $insert_array['gps_data_type'] = 2;
        $insert_array['latitude'] = $input_all['latitude'];
        $insert_array['longitude'] = $input_all['longitude'];
        //print_r($insert_array);exit;
        $insert_array['created_on'] = date('Y-m-d H:i:s');        
        if(GpsData::create($insert_array)) {
            return Helper::response_api(true,'End Day Entered','');        
        }else{
            return Helper::response_api(false,'End Day Failed','');                    
        }
    }
    
    public function my_history(){
          $input_all = Input::all();
      
        /*validation start*/
           $rules = array(
                'token' => 'required'                  
            );        
           if(Helper::validation_error_response($input_all,$rules) != false)
                return Helper::validation_error_response($input_all,$rules);
        /*validation end*/    
           
        $user_info = (Helper::get_user_info($input_all['token']));   
        /*$today_meeting_array = Meeting::select(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%r') as meeting_time"),DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y') as meeting_date"),'customer.business_name','customer.customer_name','customer.id')
                                ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')
                                ->where('user_id',$user_info['id'])
                                ->where(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d/%m/%Y')"),date('d/m/Y'))
                                ->where('user_id',$user_info['id'])
                                ->groupby('meeting_customer.customer_id')
                                ->orderBy('meeting_customer.created_on','desc')
                                ->get()
                                ->toArray();*/
        $today_meeting_array = Meeting::select(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%r') as meeting_time"),DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y') as meeting_date"),'customer.business_name','customer.customer_name','customer.id','meeting_customer.brand_id as brand_ids','area.area_name as carea','region.region_name as cregion','state.state_name as cstate','city_name')
                        ->leftJoin('user', 'user.id', '=', 'meeting_customer.user_id')
                        ->leftJoin('folloup_meeting_customer', 'folloup_meeting_customer.meeting_id', '=', 'meeting_customer.id')
                        ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')                                
                        //->leftJoin('brand', 'brand.id', '=', 'meeting_customer.brand_id')                                
                        ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                        ->leftJoin('area', 'area.id', '=', 'customer.area_id')                
                        ->leftJoin('city', 'city.id', '=', 'customer.city_id')                
                        ->leftJoin('state', 'state.id', '=', 'customer.state_id')                
                        ->leftJoin('meeting_purpose', 'meeting_purpose.id', '=', 'meeting_customer.purpose_id')                                                                                
                        ->where('meeting_customer.user_id',$user_info['id'])                        
                        ->where(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d/%m/%Y')"),date('d/m/Y'))
                        ->orderby('meeting_customer.created_on','desc')
                        ->get()
                        ->toArray();
        for($i=0;$i<count($today_meeting_array);$i++){
            $brand_ids = Brand::select('*')->wherein('id',  explode(',', $today_meeting_array[$i]['brand_ids']))->get()->toArray();
                $br_text = array();
                foreach($brand_ids as $br){
                    $br_text[] = $br['brand_name'];
                }
                $today_meeting_array[$i]['brand_name'] = implode(', ', $br_text);

        }        

        /*$yesterday_meeting_array = Meeting::select(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%r') as meeting_time"),DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y') as meeting_date"),'customer.business_name','customer.customer_name','customer.id')
                                ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')
                                ->where('user_id',$user_info['id'])
                                ->where(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d/%m/%Y')"),date('d/m/Y',strtotime( '-1 days' )))
                                ->where('user_id',$user_info['id'])
                                ->groupby('meeting_customer.customer_id')
                                ->orderBy('meeting_customer.created_on','desc')
                                ->get()
                                ->toArray();*/
        
        $yesterday_meeting_array = Meeting::select(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%r') as meeting_time"),DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y') as meeting_date"),'customer.business_name','customer.customer_name','customer.id','meeting_customer.brand_id as brand_ids','area.area_name as carea','region.region_name as cregion','state.state_name as cstate','city_name')
                        ->leftJoin('user', 'user.id', '=', 'meeting_customer.user_id')
                        ->leftJoin('folloup_meeting_customer', 'folloup_meeting_customer.meeting_id', '=', 'meeting_customer.id')
                        ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')                                
                        //->leftJoin('brand', 'brand.id', '=', 'meeting_customer.brand_id')                                
                        ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                        ->leftJoin('area', 'area.id', '=', 'customer.area_id')                
                        ->leftJoin('state', 'state.id', '=', 'customer.state_id')                
                        ->leftJoin('city', 'city.id', '=', 'customer.city_id')                
                        ->leftJoin('meeting_purpose', 'meeting_purpose.id', '=', 'meeting_customer.purpose_id')                                                                                
                        ->where('meeting_customer.user_id',$user_info['id'])                        
                        ->where(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d/%m/%Y')"),date('d/m/Y',strtotime( '-1 days' )))
                        ->orderby('meeting_customer.created_on','desc')
                        ->get()
                        ->toArray();
        for($i=0;$i<count($yesterday_meeting_array);$i++){
            $brand_ids = Brand::select('*')->wherein('id',  explode(',', $yesterday_meeting_array[$i]['brand_ids']))->get()->toArray();
                $br_text = array();
                foreach($brand_ids as $br){
                    $br_text[] = $br['brand_name'];
                }
                $yesterday_meeting_array[$i]['brand_name'] = implode(', ', $br_text);

        }  

        $past_start = date('Y-m-d',strtotime( '-2 days' ));
        $past_end = date('Y-m-d',strtotime( '-17 days' ));
        //echo $past_end;exit;
        /*$past_15_meeting_array = Meeting::select(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%r') as meeting_time"),DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y') as meeting_date"),'customer.business_name','customer.customer_name','customer.id')
                                ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')
                                ->where('user_id',$user_info['id'])
                                //->where(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d/%m/%Y')"),'<=',$past_start)
                                ->where(DB::raw("DATE(meeting_customer.created_on)"),'<=',$past_start)
                                ->where(DB::raw("DATE(meeting_customer.created_on)"),'>=',$past_end)
                                //->whereBetween(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d/%m/%Y')"), array($past_end, $past_start))
                                //->where('user_id',$user_info['id'])
                                ->groupby('meeting_customer.customer_id')
                                ->orderBy('meeting_customer.created_on','desc')
                                ->get()
                                ->toArray();*/
        $past_15_meeting_array = Meeting::select(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%r') as meeting_time"),DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y') as meeting_date"),'customer.business_name','customer.customer_name','customer.id','meeting_customer.brand_id as brand_ids','area.area_name as carea','region.region_name as cregion','state.state_name as cstate','city_name')
                        ->leftJoin('user', 'user.id', '=', 'meeting_customer.user_id')
                        ->leftJoin('folloup_meeting_customer', 'folloup_meeting_customer.meeting_id', '=', 'meeting_customer.id')
                        ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')                                
                        //->leftJoin('brand', 'brand.id', '=', 'meeting_customer.brand_id')                                
                        ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                        ->leftJoin('area', 'area.id', '=', 'customer.area_id')                
                        ->leftJoin('state', 'state.id', '=', 'customer.state_id')                
                        ->leftJoin('city', 'city.id', '=', 'customer.city_id')                
                        ->leftJoin('meeting_purpose', 'meeting_purpose.id', '=', 'meeting_customer.purpose_id')                                                                                
                        ->where('meeting_customer.user_id',$user_info['id'])                        
                        ->where(DB::raw("DATE(meeting_customer.created_on)"),'<=',$past_start)
                        ->where(DB::raw("DATE(meeting_customer.created_on)"),'>=',$past_end)
                        ->orderby('meeting_customer.created_on','desc')
                        ->get()
                        ->toArray();
        for($i=0;$i<count($past_15_meeting_array);$i++){
            $brand_ids = Brand::select('*')->wherein('id',  explode(',', $past_15_meeting_array[$i]['brand_ids']))->get()->toArray();
                $br_text = array();
                foreach($brand_ids as $br){
                    $br_text[] = $br['brand_name'];
                }
                $past_15_meeting_array[$i]['brand_name'] = implode(', ', $br_text);

        }          
        
        //print_r($last_query ) ;exit;
    
        
        if(count($today_meeting_array) > 0 ||  count($yesterday_meeting_array) > 0 || count($past_15_meeting_array) > 0 ){
            $data = array();
            $data['today'] = $today_meeting_array;
            $data['yesterday'] = $yesterday_meeting_array;
            $data['past'] = $past_15_meeting_array;
            return Helper::response_api(true,'History Found',$data);                                
        }else{
            return Helper::response_api(false,'No History Found','');                                
        }
        
    }

    public function my_history_detail(){
          $input_all = Input::all();
      
        /*validation start*/
           $rules = array(
                'token' => 'required',                  
                'customer_id' => 'required'                  
            );        
           if(Helper::validation_error_response($input_all,$rules) != false)
                return Helper::validation_error_response($input_all,$rules);
        /*validation end*/    
           
        $user_info = (Helper::get_user_info($input_all['token']));   
        /*$today_meeting_array = Meeting::select(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%r') as meeting_time"),DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y') as meeting_date"),'customer.business_name','customer.customer_name','customer.id')
                                ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')
                                ->where('user_id',$user_info['id'])
                                ->where(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d/%m/%Y')"),date('d/m/Y'))
                                ->where('customer_id',$input_all['customer_id'])
                                ->orderBy('meeting_customer.created_on','desc')
                                ->get()
                                ->take(3)
                                ->toArray();*/
         $today_meeting_array = Meeting::select(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%r') as meeting_time"),DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y') as meeting_date"),'customer.business_name','customer.customer_name','customer.id','meeting_customer.brand_id as brand_ids','area.area_name as carea','region.region_name as cregion','state.state_name as cstate')
                        ->leftJoin('user', 'user.id', '=', 'meeting_customer.user_id')
                        ->leftJoin('folloup_meeting_customer', 'folloup_meeting_customer.meeting_id', '=', 'meeting_customer.id')
                        ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')                                
                        //->leftJoin('brand', 'brand.id', '=', 'meeting_customer.brand_id')                                
                        ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                        ->leftJoin('area', 'area.id', '=', 'customer.area_id')                
                        ->leftJoin('state', 'state.id', '=', 'customer.state_id')                
                        ->leftJoin('meeting_purpose', 'meeting_purpose.id', '=', 'meeting_customer.purpose_id')                                                                                
                        ->where('meeting_customer.user_id',$user_info['id'])                        
                        ->where(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d/%m/%Y')"),date('d/m/Y'))
                        ->where('meeting_customer.customer_id',$input_all['customer_id'])
                        ->orderby('meeting_customer.created_on','desc')
                        ->get()
                        ->take(3)
                        ->toArray();
        for($i=0;$i<count($today_meeting_array);$i++){
            $brand_ids = Brand::select('*')->wherein('id',  explode(',', $today_meeting_array[$i]['brand_ids']))->get()->toArray();
                $br_text = array();
                foreach($brand_ids as $br){
                    $br_text[] = $br['brand_name'];
                }
                $today_meeting_array[$i]['brand_name'] = implode(', ', $br_text);

        }       

       /* $yesterday_meeting_array = Meeting::select(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%r') as meeting_time"),DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y') as meeting_date"),'customer.business_name','customer.customer_name','customer.id')
                                ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')
                                ->where('user_id',$user_info['id'])
                                //->where(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d/%m/%Y')"),date('d/m/Y'))
                                ->where('customer_id',$input_all['customer_id'])
                                ->where(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d/%m/%Y')"),date('d/m/Y',strtotime( '-1 days' )))
                                ->orderBy('meeting_customer.created_on','desc')
                                ->get()
                                ->take(3)
                                ->toArray();*/
        $yesterday_meeting_array = Meeting::select(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%r') as meeting_time"),DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y') as meeting_date"),'customer.business_name','customer.customer_name','customer.id','meeting_customer.brand_id as brand_ids','area.area_name as carea','region.region_name as cregion','state.state_name as cstate')
                        ->leftJoin('user', 'user.id', '=', 'meeting_customer.user_id')
                        ->leftJoin('folloup_meeting_customer', 'folloup_meeting_customer.meeting_id', '=', 'meeting_customer.id')
                        ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')                                
                        //->leftJoin('brand', 'brand.id', '=', 'meeting_customer.brand_id')                                
                        ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                        ->leftJoin('area', 'area.id', '=', 'customer.area_id')                
                        ->leftJoin('state', 'state.id', '=', 'customer.state_id')                
                        ->leftJoin('meeting_purpose', 'meeting_purpose.id', '=', 'meeting_customer.purpose_id')                                                                                
                        ->where('meeting_customer.user_id',$user_info['id'])                        
                        ->where(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d/%m/%Y')"),date('d/m/Y',strtotime( '-1 days' )))
                        ->where('meeting_customer.customer_id',$input_all['customer_id'])
                        ->orderby('meeting_customer.created_on','desc')
                        ->get()
                        ->take(3)
                        ->toArray();
        for($i=0;$i<count($yesterday_meeting_array);$i++){
            $brand_ids = Brand::select('*')->wherein('id',  explode(',', $yesterday_meeting_array[$i]['brand_ids']))->get()->toArray();
                $br_text = array();
                foreach($brand_ids as $br){
                    $br_text[] = $br['brand_name'];
                }
                $yesterday_meeting_array[$i]['brand_name'] = implode(', ', $br_text);

        }          
        
        $past_start = date('Y-m-d',strtotime( '-2 days' ));
        $past_end = date('Y-m-d',strtotime( '-17 days' ));        
        /*$past_my_meeting_array = Meeting::select(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%r') as meeting_time"),DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y') as meeting_date"),'customer.business_name','customer.customer_name','customer.id')
                                ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')
                                ->where('user_id',$user_info['id'])
                                ->where('customer_id',$input_all['customer_id'])
                                ->where(DB::raw("DATE(meeting_customer.created_on)"),'<=',$past_start)
                                ->where(DB::raw("DATE(meeting_customer.created_on)"),'>=',$past_end)                
                                ->orderBy('meeting_customer.created_on','desc')
                                ->get()
                                ->take(3)
                                ->toArray();*/
        $past_my_meeting_array = Meeting::select(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%r') as meeting_time"),DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y') as meeting_date"),'customer.business_name','customer.customer_name','customer.id','meeting_customer.brand_id as brand_ids','area.area_name as carea','region.region_name as cregion','state.state_name as cstate')
                        ->leftJoin('user', 'user.id', '=', 'meeting_customer.user_id')
                        ->leftJoin('folloup_meeting_customer', 'folloup_meeting_customer.meeting_id', '=', 'meeting_customer.id')
                        ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')                                
                        //->leftJoin('brand', 'brand.id', '=', 'meeting_customer.brand_id')                                
                        ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                        ->leftJoin('area', 'area.id', '=', 'customer.area_id')                
                        ->leftJoin('state', 'state.id', '=', 'customer.state_id')                
                        ->leftJoin('meeting_purpose', 'meeting_purpose.id', '=', 'meeting_customer.purpose_id')                                                                                
                        ->where('meeting_customer.user_id',$user_info['id'])                        
                        ->where('meeting_customer.customer_id',$input_all['customer_id'])
                        ->where(DB::raw("DATE(meeting_customer.created_on)"),'<=',$past_start)
                        ->where(DB::raw("DATE(meeting_customer.created_on)"),'>=',$past_end)
                        ->orderby('meeting_customer.created_on','desc')
                        ->get()
                        ->take(3)
                        ->toArray();
        for($i=0;$i<count($past_my_meeting_array);$i++){
            $brand_ids = Brand::select('*')->wherein('id',  explode(',', $past_my_meeting_array[$i]['brand_ids']))->get()->toArray();
                $br_text = array();
                foreach($brand_ids as $br){
                    $br_text[] = $br['brand_name'];
                }
                $past_my_meeting_array[$i]['brand_name'] = implode(', ', $br_text);

        }          
    
        
        if(count($today_meeting_array) > 0 ||  count($yesterday_meeting_array) > 0 || count($past_my_meeting_array) > 0 ){
            $data = array();
            $data['today'] = $today_meeting_array;
            $data['yesterday'] = $yesterday_meeting_array;
            $data['past'] = $past_my_meeting_array;
            return Helper::response_api(true,'History Found',$data);                                
        }else{
            return Helper::response_api(false,'No History Found','');                                
        }
        
    }
    
    public function change_password(){
          $input_all = Input::all();
      
        /*validation start*/
           $rules = array(
                'token' => 'required',                  
                'old_password' => 'required',                  
                'new_password' => 'required',                  
                'confirm_password' => 'required|same:new_password'                  
            );        
           if(Helper::validation_error_response($input_all,$rules) != false)
                return Helper::validation_error_response($input_all,$rules);
        /*validation end*/    
           $user_info = (Helper::get_user_info($input_all['token']));   
           $auth = Auth::attempt(['id' => $user_info['id'], 'password' => $input_all['old_password']]);
           
           if ($auth) {
               $user_detail = User::find($user_info['id']);
               $user_detail->user_password = Hash::make($input_all['new_password']);
               $user_detail->save();
                return Helper::response_api(true,'Password Changed','');
           }else{
                return Helper::response_api(false,'Fail To Change Password','');               
           }
        
    }

    public function forgot_password(){
          $input_all = Input::all();
      
        /*validation start*/
           $rules = array(
                //'token' => 'required',                  
                'user_email' => 'required|email'
            );        
           if(Helper::validation_error_response($input_all,$rules) != false)
                return Helper::validation_error_response($input_all,$rules);
        /*validation end*/    
           //$user_info = (Helper::get_user_info($input_all['token']));   
           
           $user_info =   User::where('user_email',$input_all['user_email'])->first();
           $count  =   User::where('user_email',$input_all['user_email'])->count();
            if( $count != 0 ){

            $rnum = rand(100000,999999);
            //$encrypted = Crypt::encrypt($ar['user_email']."|".$rnum."|".time());
            $pass = Hash::make($rnum);
            User::where('user_email',$input_all['user_email'])->update(["user_password"=>$pass]);  

            // mail the security key
            Mail::send('pages.user.email_forgot', ['pass'=>$rnum], function ($m) use ($user_info) {
                    //$m->from('helpdesk@durain.com', 'Durain');
                    $m->from(Config::get('constants.from_mail'), Config::get('constants.from_mail_text'));                  
                    $m->to( $user_info['user_email'] )->subject('Reset Password!');
                });
                return Helper::response_api(true,'Password Changed','');
            }else{
                return Helper::response_api(false,'Fail To Change Password','');               
            }           
    }
    
    public function get_state_area(){
              $input_all = Input::all();
      
        /*validation start*/
           $rules = array(
                'token' => 'required',                  
                'region_id' => 'required'
            );        
           if(Helper::validation_error_response($input_all,$rules) != false)
                return Helper::validation_error_response($input_all,$rules);
        /*validation end*/    
           $user_info = (Helper::get_user_info($input_all['token']));   
           $area_list = Area::select('area.*')->where('disable_status',0)->where('delete_status',0)->where('region_id',$input_all['region_id'])->get()->toArray();
           $state_list = State::select("state.*")->join("region_detail","region_detail.state_id","=","state.id")->where("region_id",$input_all['region_id'])->get()->toArray();
            $data = array();
           if(count($area_list) > 0 || count($state_list) > 0 ){
            $data['area'] = $area_list;
            $data['state'] = $state_list;
            return Helper::response_api(true,'state,area list',$data);                   
           }else{
            return Helper::response_api(false,'state,area list not found',$data);                                  
           }
    
    }

    public function get_city(){
              $input_all = Input::all();
      
        /*validation start*/
           $rules = array(
                'token' => 'required',                  
                'state_id' => 'required'
            );        
           if(Helper::validation_error_response($input_all,$rules) != false)
                return Helper::validation_error_response($input_all,$rules);
        /*validation end*/    
           $user_info = (Helper::get_user_info($input_all['token']));   
           
           $city_list = City::select('*')->where('state_id',$input_all['state_id'])->get()->toArray();
           
            $data = array();
           if(count($city_list) > 0){
            $data['city'] = $city_list;            
            return Helper::response_api(true,'city list',$data);                   
           }else{
            return Helper::response_api(false,'city list not found',$data);                                  
           }
    
    }
    
    public function today_status(){
        
        $input_all = Input::all();      
        /*validation start*/
           $rules = array(
                'token' => 'required'
            );        
           if(Helper::validation_error_response($input_all,$rules) != false)
                return Helper::validation_error_response($input_all,$rules);     
           $user_info = (Helper::get_user_info($input_all['token']));   
            $td = date('Y-m-d');
            $past_start = $td;
            $past_start = $past_start . " 00:00:00";
            //$past_end = date('Y-m-d H:i:s');
            $past_end = $td . ' ' . '23:59:59';           
           
            $startday = GpsData::select('*')
                    ->where('gps_data_type',0)
                    ->where('user_id',$user_info['id'])
                    ->where(DB::raw("DATE(gps_data.created_on)"), '>=', $past_start)
                    ->where(DB::raw("DATE(gps_data.created_on)"), '<=', $past_end)                    
                    ->get()->toArray();
            $data = array();
            if(count($startday) > 0){
                $data['start_day'] = false;
            }else{
                $data['start_day'] = true;                
            }
            $endday = GpsData::select('*')
                    ->where('gps_data_type',2)
                    ->where('user_id',$user_info['id'])
                    ->where(DB::raw("DATE(gps_data.created_on)"), '>=', $past_start)
                    ->where(DB::raw("DATE(gps_data.created_on)"), '<=', $past_end)                    
                    ->get()->toArray();
            
            if(count($endday) > 0){
                $data['end_day'] = false;
            }else{
                $data['end_day'] = true;                
            }
            
        return Helper::response_api(true,'Day status',$data);                                              
           
        
    }
}
