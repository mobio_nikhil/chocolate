<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use App\User;
use Input;
use Validator;
use App\Helpers\Helper;
use App\GpsData;
use App\Meeting;
use App\State;
use App\Region;
use App\UserArea;
use App\Area;
use DB;
use PDO;
use Redirect;
use Config;
use Session;

class AdminEmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function employee_list(){
        
        
        $session_user_info = Session::get('session_user_info');
        
        $user_ids = array();
        if($session_user_info['user_type'] == 0){
            $user_ids = Helper::vp_under_user_list();            
        }
        if($session_user_info['user_type'] == 1){            
            $user_ids = Helper::rsm_under_user_list($session_user_info) ;
        }
        if($session_user_info['user_type'] == 2){            
            $user_ids = Helper::asm_under_user_list($session_user_info) ;
        }        
        
        $user = User::select('user.*','region_name','area_name')
                            ->leftJoin('region', 'region.id', '=', 'user.region_id')
                            ->leftJoin('area', 'area.id', '=', 'user.area_id')
                            ->wherein('user.id',$user_ids)
                            ->get()
                            ->toArray();
                

        
        
        
        for($i=0;$i<count($user);$i++){
            if($user[$i]['user_type'] == 0){                
                $customer_meeting = Meeting::select('*')->where('user_id',$user[$i]['id'])->count();
                $user[$i]['customer_meeting'] = $customer_meeting;
            }
            
            if($user[$i]['user_type'] == 1){
                $vp_user = User::select('user.*','region_name','area_name','assignment_user.user_name as assignment_username')
                        ->leftJoin('region', 'region.id', '=', 'user.region_id')
                        ->leftJoin('area', 'area.id', '=', 'user.area_id')
                        ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                        ->where('user.delete_status',0)
                        ->where('user.disable_status',0)
                        ->where('user.user_type',0)                        
                        ->where('user.user_admin_backoffice_type',NULL)                        
                        ->orderby('user_name','region.region_name','area.area_name')
                        ->get()->toArray();   
                
                
                if(count($vp_user) > 0)
                $user[$i]['assign_to_user'] = $vp_user[0]['user_name'];
                else
                $user[$i]['assign_to_user'] = '';
                
                
                $user_region = array();
                $u_region = array();
                DB::setFetchMode(PDO::FETCH_ASSOC);
                $user_region = $state_list = DB::connection('mysql')->select("SELECT * FROM state where id in (select state_id from region_detail WHERE region_id in (select id from region where region.delete_status = '0' and region.disable_status = '0' and id in (select region_id from user where id = ".$user[$i]['id'].")))");
                DB::setFetchMode(PDO::FETCH_CLASS);
                
                if(count($user_region) > 0){
                    foreach($user_region as $ar){
                        $u_region[] = $ar['state_name'];                        
                    }
                }
                $user[$i]['area_name'] = implode(', ', $u_region);
                
                
                $customer_meeting = Meeting::select('*')->where('user_id',$user[$i]['id'])->count();
                $user[$i]['customer_meeting'] = $customer_meeting;
                
                $customer_meeting = Meeting::select('*')->where('user_id',$user[$i]['id'])->count();
                $user[$i]['customer_meeting'] = $customer_meeting;                
            }
            
            if($user[$i]['user_type'] == 2){
               /* $rsm_user = User::select('user.*','region_name','area_name','assignment_user.user_name as assignment_username')
                        ->leftJoin('region', 'region.id', '=', 'user.region_id')
                        ->leftJoin('area', 'area.id', '=', 'user.area_id')
                        ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                        ->where('user.delete_status',0)
                        ->where('user.disable_status',0)
                        ->where('user.user_type',1)
                        ->where('user.region_id',$user[$i]['region_id'])
                        ->orderby('user_name','region.region_name','area.area_name')
                        ->get()->toArray();    
                                
                if(count($rsm_user) > 0)
                $user[$i]['assign_to_user'] = $rsm_user[0]['user_name'];
                else
                $user[$i]['assign_to_user'] = '';  */              
                
                $rsm_user = User::select('user.*', 'region_name', 'user.user_name as assignment_username')
                                ->leftJoin('region', 'region.id', '=', 'user.region_id')
                                //->leftJoin('area', 'area.id', '=', 'user.area_id')
                                //->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                                ->where('user.delete_status', 0)
                                ->where('user.disable_status', 0)
                                ->where('user.user_type', 1)
                                ->where('user.region_id', $user[$i]['region_id'])
                                ->orderby('user_name')
                                ->get()->toArray();

                if (count($rsm_user) > 0)
                    $user[$i]['assign_to_user'] = $rsm_user[0]['user_name'];
                else
                    $user[$i]['assign_to_user'] = '';
                
                $user_area = UserArea::select('area.*')
                        ->leftJoin('area', 'area.id', '=', 'user_area.area_id')                        
                        ->where('user_area.user_id',$user[$i]['id'])->get()->toArray();
                $area_name = array();
                if(count($user_area) > 0){
                    foreach($user_area as $ar){
                        $area_name[] = $ar['area_name'];                        
                    }
                }
                $user[$i]['area_name'] = implode(', ', $area_name);                
                
                $customer_meeting = Meeting::select('*')->where('user_id',$user[$i]['id'])->count();
                $user[$i]['customer_meeting'] = $customer_meeting;                
            }

            if($user[$i]['user_type'] == 3){
                /*$asm_user = User::select('user.*','region_name','area_name','assignment_user.user_name as assignment_username')
                        ->leftJoin('region', 'region.id', '=', 'user.region_id')
                        ->leftJoin('area', 'area.id', '=', 'user.area_id')
                        ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                        ->where('user.delete_status',0)
                        ->where('user.disable_status',0)
                        ->where('user.user_type',2)
                        ->where('user.region_id',$user[$i]['region_id'])
                        ->where('user.area_id',$user[$i]['area_id'])
                        ->orderby('user_name','region.region_name','area.area_name')
                        ->get()->toArray();    
                
                if(count($asm_user) > 0)
                $user[$i]['assign_to_user'] = $asm_user[0]['user_name'];
                else
                $user[$i]['assign_to_user'] = '';     */                           
                if($user[$i]['assign_to_user'] != null && $user[$i]['assign_to_user'] != ''){
                    $asm_user = User::select('user.*', 'region_name', 'area_name', 'assignment_user.user_name as assignment_username')
                                ->leftJoin('region', 'region.id', '=', 'user.region_id')
                                ->leftJoin('area', 'area.id', '=', 'user.area_id')
                                ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                                ->where('user.delete_status', 0)
                                ->where('user.disable_status', 0)
                                ->where('user.user_type', 2)
                                ->where('user.id', $user[$i]['assign_to_user'])
                                //->where('user.region_id', $user[0]['region_id'])
                                //->where('user.area_id', $user[0]['area_id'])
                                ->orderby('user_name')
                                ->get()->toArray();

                    if (count($asm_user) > 0)
                        $user[$i]['assign_to_user'] = $asm_user[0]['user_name'];
                    else
                        $user[$i]['assign_to_user'] = '';                        

                }                
                $user_area = UserArea::select('area.*')
                        ->leftJoin('area', 'area.id', '=', 'user_area.area_id')                        
                        ->where('user_area.user_id',$user[$i]['id'])->get()->toArray();
                $area_name = array();
                if(count($user_area) > 0){
                    foreach($user_area as $ar){
                        $area_name[] = $ar['area_name'];                        
                    }
                }
                $user[$i]['area_name'] = implode(', ', $area_name);                     
                
                $customer_meeting = Meeting::select('*')->where('user_id',$user[$i]['id'])->count();
                $user[$i]['customer_meeting'] = $customer_meeting;                
                
            }
        }
        
        $region = Region::select('*')
                ->where('delete_status',0)
                ->where('disable_status',0)
                ->orderby('region_name')
                ->get()->toArray();
        
        $data = array();
        $data['user_list'] = $user;                
        $data['session_user_info'] = Session::get('session_user_info');
        //print_r($data);exit;
        return view('pages.employee.employee_list',$data);
    
    }

    public function track($id){
        
        $user = User::select('user.*','region_name','area_name','assignment_user.user_name as assignment_username')
                ->leftJoin('region', 'region.id', '=', 'user.region_id')
                ->leftJoin('area', 'area.id', '=', 'user.area_id')
                ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                ->where('user.delete_status',0)
                ->where('user.disable_status',0)
                ->where('user.id',$id)
                ->orderby('user_name')
                ->get()->toArray();        
        
        $past_start = date('Y-m-d');
        $past_start = $past_start." 00:00:00";
        $past_end = date('Y-m-d H:i:s');
        
        $user_gps = GpsData::select('*','gps_data.created_on as log_time','region_name','state_name','area_name')
                ->where('gps_data.user_id',$user[0]['id'])
                ->leftJoin('meeting_customer', 'meeting_customer.id', '=', 'gps_data.meeting_id')
                ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')
                ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                ->leftJoin('state', 'state.id', '=', 'customer.state_id')
                ->leftJoin('area', 'area.id', '=', 'customer.area_id')
                ->where(DB::raw("DATE(gps_data.created_on)"),'>=',$past_start)
                ->where(DB::raw("DATE(gps_data.created_on)"),'<=',$past_end)
                ->orderby('gps_data.created_on')
                ->get()
                ->toArray();
        
        for($i=0;$i<count($user_gps);$i++){
            if($user_gps[$i]['gps_data_type'] == 0){
                $user_gps[$i]['map_header'] = 'Start Day';
                $user_gps[$i]['map_content'] = 'Start of the day';
                $user_gps[$i]['map_time_date'] = date('d-m-Y h:i:s A',strtotime($user_gps[$i]['log_time']));
            }

            if($user_gps[$i]['gps_data_type'] == 1){
                $user_gps[$i]['map_header'] = $user_gps[$i]['business_name'];
                $user_gps[$i]['map_content'] = $user_gps[$i]['area_name'].' ('.$user_gps[$i]['customer_phone'].')';
                $user_gps[$i]['map_time_date'] = date('d-m-Y h:i:s A',strtotime($user_gps[$i]['log_time']));
            }

            if($user_gps[$i]['gps_data_type'] == 2){
                $user_gps[$i]['map_header'] = 'End Day';
                $user_gps[$i]['map_content'] = 'End of the day';
                $user_gps[$i]['map_time_date'] = date('d-m-Y h:i:s A',strtotime($user_gps[$i]['log_time']));
            }
            
        }
        
        $data = array();
        $data['user'] = $user;
        $data['user_gps'] = $user_gps;
        return view('pages.employee.track',$data);
    }

    
    public function employee_history($id){

        
        $user = User::select('user.*','region_name','area_name','assignment_user.user_name as assignment_username')
                ->leftJoin('region', 'region.id', '=', 'user.region_id')
                ->leftJoin('area', 'area.id', '=', 'user.area_id')
                ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                ->where('user.delete_status',0)
                ->where('user.disable_status',0)
                ->where('user.id',$id)
                ->orderby('user_name')
                ->get()->toArray();        
        
        $past_start = date('Y-m-d');
        $past_start = $past_start." 00:00:00";
        $past_end = date('Y-m-d H:i:s');
        
        $user_gps_date_log = GpsData::select(DB::raw('GROUP_CONCAT(gps_data.id) as gps_ids'),DB::raw("DATE_FORMAT(created_on,'%d-%m-%Y') as create_date"))
                        ->groupby(DB::raw("DATE_FORMAT(created_on,'%d/%m/%Y')"))
                        //->orderby(DB::raw("DATE_FORMAT(created_on,'%d/%m/%Y')"),'desc')        
                        ->where('gps_data.user_id',$id)
                        ->orderby("gps_data.created_on",'desc')        
                        ->get()
                        ->toArray();


        $employee_capture_day = array();
        
        for($i=0;$i<count($user_gps_date_log);$i++){
        $employee_capture_day[$i]['capture_day'] = $user_gps_date_log[$i]['create_date'];
            $gps_ids = explode(',', $user_gps_date_log[$i]['gps_ids']);                        
            
            for($j=0;$j<count($gps_ids);$j++){
                    $user_gps_log_data = GpsData::select('*','gps_data.created_on as log_time','meeting_customer.extra_note as meeting_note')
                                ->where('gps_data.id',$gps_ids[$j])
                                ->leftJoin('meeting_customer', 'meeting_customer.id', '=', 'gps_data.meeting_id')
                                ->leftJoin('folloup_meeting_customer', 'folloup_meeting_customer.meeting_id', '=', 'meeting_customer.id')
                                ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')                                
                                ->get()
                                ->toArray();
                        
                            if($user_gps_log_data[0]['gps_data_type'] == 0){
                                $employee_capture_day[$i]['capture_data'][$j]['log_type'] = $user_gps_log_data[0]['gps_data_type'];
                                $employee_capture_day[$i]['capture_data'][$j]['log_header'] = 'Start Day';
                                $employee_capture_day[$i]['capture_data'][$j]['log_content'] = 'Start of the day';
                                $employee_capture_day[$i]['capture_data'][$j]['log_time_date'] = date('d-m-Y h:i:s A',strtotime($user_gps_log_data[0]['log_time']));
                            }

                            if($user_gps_log_data[0]['gps_data_type'] == 1){                            
                                $employee_capture_day[$i]['capture_data'][$j]['log_type'] = $user_gps_log_data[0]['gps_data_type'];
                                $employee_capture_day[$i]['capture_data'][$j]['log_header'] = 'Meeting';
                                //$employee_capture_day[$i][$j]['log_content'] = 'Business Name: '.$user_gps_log_data[0]['business_name'].'<br/>'.'Customer Name: '.$user_gps_log_data[0]['customer_name'].'<br/>'.'Business Address: '.$user_gps_log_data[0]['business_address'].'<br/>'.'Customer No: '.$user_gps_log_data[0]['customer_phone'];
                                $employee_capture_day[$i]['capture_data'][$j]['log_content'] = $user_gps_log_data[0];
                                $employee_capture_day[$i]['capture_data'][$j]['log_time_date'] = date('d-m-Y h:i:s A',strtotime($user_gps_log_data[0]['log_time']));
                            }

                            if($user_gps_log_data[0]['gps_data_type'] == 2){
                                $employee_capture_day[$i]['capture_data'][$j]['log_type'] = $user_gps_log_data[0]['gps_data_type'];
                                $employee_capture_day[$i]['capture_data'][$j]['log_header'] = 'End Day';
                                $employee_capture_day[$i]['capture_data'][$j]['log_content'] = 'End of the day';
                                $employee_capture_day[$i]['capture_data'][$j]['log_time_date'] = date('d-m-Y h:i:s A',strtotime($user_gps_log_data[0]['log_time']));
                            }

                                    
            }
        }

        $data = array();
        $data['user'] = $user;        
        $data['employee_capture_day'] = $employee_capture_day;
        return view('pages.employee.employee_history',$data);
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
