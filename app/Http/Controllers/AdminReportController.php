<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use App\User;
use Input;
use Validator;
use App\Helpers\Helper;
use App\GpsData;
use App\Meeting;
use App\Region;
use App\Customer;
use App\Area;
use DB;
use Redirect;
use Config;
use Session;
use Form;
use View;
use Excel;
use Mail;

class AdminReportController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('pages.report.index');
    }

    public function admin_index() {
        if (Input::isMethod('post')) {
            $input_all = Input::all();
            $user_info = Session::get('session_user_info');

            /* validation start */
            $rules = array(
                    "start_date" => 'required|date_format:"d-M-Y"',
                    "end_date" => 'required|date_format:"d-M-Y"',                   
            );

            $validator = Validator::make($input_all, $rules);
            // check if the validator failed -----------------------
            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to(Config::get('constants.admin_path') . 'report/admin_index')
                                ->withErrors($validator)
                                ->withInput();
            }
            
            // Start date
            $startdate = date('Y-m-d',strtotime($input_all['start_date']));
            $enddate = date('Y-m-d',strtotime($input_all['end_date']));
            
            if($input_all['report_type'] == 1){
                $excel_array = Helper::on_date_customer_list($user_info['id'],$startdate,$enddate,1);
            }else{
                $excel_array = Helper::on_date_customer_meeting($user_info['id'], $startdate,$enddate);                
            }
            
            //$user_info = (Helper::get_user_info($input_all['token']));
            

            $data = array();
            $data['excel_array'] = $excel_array;
            $data['report_type'] = $input_all['report_type'];
            $data['start_date'] = $input_all['start_date'];
            $data['end_date'] = $input_all['end_date'];
            $data['report_type'] = $input_all['report_type'];
            /*while (strtotime($startdate) <= strtotime($enddate)) {
                           echo "$startdate\n";
                           $startdate = date ("Y-m-d", strtotime("+1 day", strtotime($startdate)));
            }            
            */
            //return view::make('pages.report.admin_index')->withInput(['start_date' => $input_all['start_date']]);
            return view::make('pages.report.admin_index',$data);
        } else {
            $data = array();            
            $data['report_type'] = 1;            
            return view('pages.report.admin_index',$data);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $user_info = Session::get('session_user_info');
        $userid = $user_info['id'];
        

        $on_date = date('Y-m-d');

//        $startdate = date('Y-m-d',strtotime($input_all['start_date']));
//        $enddate = date('Y-m-d',strtotime($input_all['end_date']));
        
        $excel_array = Helper::on_date_customer_list($userid, $startdate,$enddate);


        $file_path = public_path('excel/exports/' . $on_date . '_' . $userid);

        if (count($excel_array) > 1) {
            Excel::create('customer', function($excel) use($excel_array, $file_path) {
                $excel->sheet('Sheet 1', function($sheet) use($excel_array) {
                    //$sheet->fromArray($excel_array);
                    $sheet->fromArray($excel_array, null, 'A1', false, false);
                });
                $user_data = '';
                /* Mail::send(['html' => 'pages.report.email'], ['text' => $user_data], function ($message) use($user_data,$file_path) {
                  //print_r($user_data);exit;
                  $message->subject('Welcome to cedarindia');
                  $message->from('us@example.com', 'Laravel');
                  $message->attach($file_path.'/customer.xls');
                  $message->to('vivek.mobio@gmail.com');
                  }); */

                //})->export('xls');        
                //        echo storage_path('excel/exports');
                //        echo public_path('excel/exports');
                //        exit;          
            })->store('xls', $file_path);
        }
    }

    public function on_date_meeting_list() {

        $user_info = Session::get('session_user_info');
        $userid = $user_info['id'];
        $on_date = date('d-m-Y');

        $excel_array = Helper::on_date_customer_meeting($userid, $on_date,$on_date);
        //$excel_array = Helper::on_date_customer_meeting($userid, $on_date);
        echo '<pre>';
        print_r($excel_array);
        echo '</pre>';
        exit;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //d
    }

}
