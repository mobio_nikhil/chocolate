<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Input;
use Validator;
use App\Helpers\Helper;
use App\GpsData;
use App\Meeting;
use App\Brand;
use App\UserBrand;
use DB;
use Redirect;
use Config;
use Session;
use Form;
use View;


class AdminBrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brand = Brand::select('*')->where('delete_status',0)->orderby('brand_name')->get()->toArray();
        $data = array();
        $data['brand_list'] = $brand;        
        return view('pages/brand/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages/brand/create');         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //public function store(Request $request)
    public function store()
    {
                $input_all = Input::all();
        /*validation start*/
                $deletevalue = 1;
           $rules = array(
                'brand_name' => 'required||unique:brand,brand_name,'.$deletevalue.',delete_status',
            );        
           
            $validator = Validator::make($input_all, $rules);
            // check if the validator failed -----------------------
            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to(Config::get('constants.admin_path').'brand/create')
                    ->withErrors($validator)
                    ->withInput();
            }
            
            $user_info = Session::get('session_user_info');
            $input_all['user_id'] = $user_info['id'];
            $input_all['created_on'] = date('Y-m-d H:i:s');
            Brand::create($input_all);
            Session::flash('message', 'New Brand created'); 
            //Session::flash('alert-class', 'alert-danger');                         
            return Redirect::to(Config::get('constants.admin_path').'brand/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $brand = Brand::findOrFail($id);
            //return view('pages/brand/edit')->with(compact('brand'));       
        return View::make('pages.brand.edit')
        ->with('brand', $brand);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // echo $this->method();
        
                 $input_all = Input::all();
                 $deletevalue = 0;
         /*validation start*/
            $rules = array(
                 'brand_name' => 'required||unique:brand,brand_name,'.$id.',id,delete_status,'.$deletevalue,
             );        
           
             $validator = Validator::make($input_all, $rules);
             // check if the validator failed -----------------------
             if ($validator->fails()) {
                 $messages = $validator->messages();
                 return Redirect::to(Config::get('constants.admin_path').'brand/edit/'.$id)
                     ->withErrors($validator)
                     ->withInput();
             }
            
             $brand = Brand::findOrFail($id);
             $brand->brand_name = $request['brand_name'];
             $brand->save();
             Session::flash('message', 'Brand has been updated'); 
             //Session::flash('alert-class', 'alert-danger');                         
             return Redirect::to(Config::get('constants.admin_path').'brand/index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $brand = Brand::findOrFail($id);        
        $input_array = Input::all();
        
            $counter = array();
            $counter[ $id ]  = 0;
            
            $brands = Meeting::select("brand_id")->get()->toArray();
            $usersData = UserBrand::select("brand_id")->get()->toArray();
            $brands= array_merge($brands, $usersData );            

            foreach ($brands as $b) {  
                $tmp = explode(",",$b['brand_id']);
                foreach ($tmp as $t) {
                    if( isset($counter[$t]) ){
                        $counter[$t] ++;
                    }else{
                        $counter[$t] = 1;
                    }
                }
            }
            if(  $counter[ $id ]  != 0  ){ 
                return Redirect::back()->withErrors(["Brand can not be deleted. It is associated with records"]); 
            }


        $brand->delete_status = 1;
        $brand->disable_status = 1;
        $user_info = Session::get('session_user_info');
        $brand->delete_by = $user_info['id'];
        $brand->delete_reason = $input_array['delete_reason'];

            if($brand->save()){
                Session::flash('message', 'Brand deleted'); 
                //Session::flash('alert-class', 'alert-danger');                         
                return Redirect::to(Config::get('constants.admin_path').'brand/index');            
            }else{
                Session::flash('message', 'Brand not deleted! Try again later'); 
                Session::flash('alert-class', 'alert-danger');
                return Redirect::to(Config::get('constants.admin_path').'brand/index');                        
            }        
    }


    public function changeStatus($status,$id) {

         $input_array = Input::all();
        // $id = $ar['id'];
        $user_info = Session::get('session_user_info');
        if ($status == "enable") {
            Brand::where('id', $id)->update(["disable_status" => 0,'active_by'=>$user_info['id'],'disable_reason'=>NULL]);
            Session::flash('message', 'Brand has been Activated');
        } elseif ($status == "disable") {
            Brand::where('id', $id)->update(["disable_status" => 1,"disable_reason" => $input_array['disable_reason'],'disable_by'=>$user_info['id']]);
            Session::flash('message', 'Brand has been Dectivated');
        }
        return Redirect::back();
    }


}
