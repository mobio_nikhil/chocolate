<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use App\User;
use Input;
use Validator;
use App\Helpers\Helper;
use App\GpsData;
use App\Meeting;
use App\FollowupMeeting;
use App\Customer;
use App\UserBrand;
use App\Region;
use App\Brand;
use App\Purpose;
use App\Area;
use DB;
use Redirect;
use Config;
use Session;


class AdminMeetingController extends Controller
{

    
    public function meeting_list(){
        
   $session_user_info = Session::get('session_user_info');
        $user_ids = array();
        if($session_user_info['user_type'] == 0){
            $user_ids = Helper::vp_under_user_list();            
        }
        if($session_user_info['user_type'] == 1){            
            $user_ids = Helper::rsm_under_user_list($session_user_info) ;
        }
        if($session_user_info['user_type'] == 2){            
            $user_ids = Helper::asm_under_user_list($session_user_info) ;
        }
        //$user_ids = trim($user_ids, ",");        
        
        
        $customer_meeting_date_log = Meeting::select('*','meeting_customer.brand_id as brand_ids','user.id as uid','meeting_customer.created_on as meeting_date','meeting_customer.id as mid','area.area_name as carea','region.region_name as cregion','state.state_name as cstate')
                        ->leftJoin('user', 'user.id', '=', 'meeting_customer.user_id')
                        ->leftJoin('folloup_meeting_customer', 'folloup_meeting_customer.meeting_id', '=', 'meeting_customer.id')
                        ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')                                
                        //->leftJoin('brand', 'brand.id', '=', 'meeting_customer.brand_id')                                
                        ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                        ->leftJoin('area', 'area.id', '=', 'customer.area_id')                
                        ->leftJoin('state', 'state.id', '=', 'customer.state_id')                
                        ->leftJoin('meeting_purpose', 'meeting_purpose.id', '=', 'meeting_customer.purpose_id')                                                                                
                        ->wherein('meeting_customer.user_id',$user_ids)
                        ->orderby('meeting_customer.created_on','desc')
                        ->get()
                        ->toArray();  
        
        
        for($i=0;$i<count($customer_meeting_date_log);$i++){
            $brand_ids = Brand::select('*')->wherein('id',  explode(',', $customer_meeting_date_log[$i]['brand_ids']))->get()->toArray();
                $br_text = array();
                foreach($brand_ids as $br){
                    $br_text[] = $br['brand_name'];
                }
                $customer_meeting_date_log[$i]['brand_name'] = implode(', ', $br_text);

        }

        $data = array();
        $data['meeting_list'] = $customer_meeting_date_log;
        return view('pages.meeting.meeting_list',$data);
        
        
    }
    
    
    public function meeting_history($id){
        
        $meeting_detail = Meeting::select('*')
                            ->where('id',$id)
                            ->get()
                            ->toArray();
        
        $user_id = $meeting_detail[0]['user_id'];
        $customer_id = $meeting_detail[0]['customer_id'];
        
        $cust_data = Customer::select('*')->where('id',$customer_id)->get()->toArray();
        $user_data = User::select('*')->where('id',$user_id)->get()->toArray();

//        $meeting_user_customer_detail = Meeting::select('*')
//                            ->where('user_id',$user_id)
//                            ->where('customer_id',$customer_id)
//                            ->get()
//                            ->toArray();
        
        $customer_meeting_date_log = Meeting::select(DB::raw('GROUP_CONCAT(meeting_customer.id) as meetings_ids'),DB::raw("DATE_FORMAT(created_on,'%d-%m-%Y') as create_date"))
                        ->groupby(DB::raw("DATE_FORMAT(created_on,'%d/%m/%Y')"))
                        ->orderby(DB::raw("DATE_FORMAT(created_on,'%d/%m/%Y')"),'desc')                                
                        ->where('meeting_customer.user_id',$user_id)
                        ->where('meeting_customer.customer_id',$customer_id)
                        ->get()
                        ->toArray();    
        
    $customer_meeting_data = array();
        for($i=0;$i<count($customer_meeting_date_log);$i++){            
            $meeting_ids = explode(',', $customer_meeting_date_log[$i]['meetings_ids']);                        
                $meeting_data = Meeting::select('*','meeting_customer.created_on as meeting_date_time','meeting_customer.extra_note as meeting_note','folloup_meeting_customer.extra_note as followup_note')
                        ->leftJoin('user', 'user.id', '=', 'meeting_customer.user_id')
                        ->leftJoin('folloup_meeting_customer', 'folloup_meeting_customer.meeting_id', '=', 'meeting_customer.id')
                        ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')                                
                        ->wherein('meeting_customer.id',$meeting_ids)
                        ->get()
                        ->toArray();
                if(count($meeting_data) > 0){
                    $customer_meeting_data[] = array(
                        'meeting_day' => $customer_meeting_date_log[$i]['create_date'],
                        'meeting_data' => $meeting_data
                    );                    
                }
        }

        $data = array();        
        $data['customer_meeting'] = $customer_meeting_data;
        $data['user_data'] = $user_data;
        $data['cust_data'] = $cust_data;
        //return view('pages.customer.customer_history',$data);         
        return view('pages.meeting.meeting_history',$data);
        
        
    }
    
    
    public function track($id){
    
        $meeting_detail = Meeting::select('*')
                            ->where('id',$id)
                            ->get()
                            ->toArray();
        
        $user_id = $meeting_detail[0]['user_id'];
        $customer_id = $meeting_detail[0]['customer_id'];                
        
        $user = User::select('user.*','region_name','area_name','assignment_user.user_name as assignment_username')
                ->leftJoin('region', 'region.id', '=', 'user.region_id')
                ->leftJoin('area', 'area.id', '=', 'user.area_id')
                ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                ->where('user.delete_status',0)
                ->where('user.disable_status',0)
                ->where('user.id',$user_id)
                ->orderby('user_name')
                ->get()->toArray();                
        
        $user_gps = GpsData::select('*','gps_data.created_on as log_time')
                ->where('gps_data.meeting_id',$id)
                ->leftJoin('meeting_customer', 'meeting_customer.id', '=', 'gps_data.meeting_id')
                ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')
                ->leftJoin('user', 'user.id', '=', 'meeting_customer.user_id')
                ->orderby('gps_data.created_on')
                ->get()
                ->toArray();
        
        for($i=0;$i<count($user_gps);$i++){

            if($user_gps[$i]['gps_data_type'] == 1){
                $user_gps[$i]['map_header'] = $user_gps[$i]['business_name'];
                $user_gps[$i]['map_content'] = $user_gps[$i]['customer_name'].'<br/>'.$user_gps[$i]['business_address'].'<br/>'.$user_gps[$i]['customer_phone'];
                $user_gps[$i]['map_time_date'] = date('d-m-Y h:i:s A',strtotime($user_gps[$i]['log_time']));
            }
        }
        
        $data = array();
        $data['user'] = $user;
        $data['user_gps'] = $user_gps;
        return view('pages.meeting.track',$data);
    }    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brand = Brand::select('*')->where('delete_status',0)->where('disable_status',0)->get()->toArray();
        $purpose = Purpose::select('*')->where('delete_status',0)->where('disable_status',0)->get()->toArray();
        
        /*Customer list start*/ 
                
         $session_user_info = Session::get('session_user_info');
         
        $user_ids = array();
        if($session_user_info['user_type'] == 0){
            $user_ids = Helper::vp_under_user_list();            
            $region = Region::select('*')->get()->toArray();
           $area = Area::select('*')->get()->toArray();                        
        }
        if($session_user_info['user_type'] == 1){            
            $user_ids = Helper::rsm_under_user_list($session_user_info) ;
            $region = Region::select('*')->where('delete_status',0)->where('disable_status',0)->where('id',$session_user_info['region_id'])->get()->toArray();
            $area = Area::select('*')->where('delete_status',0)->where('disable_status',0)->where('region_id',$session_user_info['region_id'])->get()->toArray();                                         
        }
        if($session_user_info['user_type'] == 2){            
            $user_ids = Helper::asm_under_user_list($session_user_info) ;
            $region = Region::select('*')->where('delete_status',0)->where('disable_status',0)->where('id',$session_user_info['region_id'])->get()->toArray();
            $area = Area::select('*')->where('delete_status',0)->where('disable_status',0)->where('region_id',$session_user_info['region_id'])->where('id',$session_user_info['area_id'])->get()->toArray();                                                                         
        }         
        $user = User::select('user.*','region_name','area_name')
        ->leftJoin('region', 'region.id', '=', 'user.region_id')
        ->leftJoin('area', 'area.id', '=', 'user.area_id')
        ->wherein('user.id',$user_ids)
        ->get()
        ->toArray();                     
        
        $customer_list = array();

        
        $customer_list = Customer::select('customer.*','region_name','area_name','user_name',DB::raw("count(meeting_customer.id) as meeting_count"))
                    ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                    ->leftJoin('area', 'area.id', '=', 'customer.area_id')
                    ->leftJoin('user', 'user.id', '=', 'customer.information_enter_by')
                    ->leftJoin('meeting_customer', 'meeting_customer.customer_id', '=', 'customer.id')
                    ->wherein('information_enter_by',$user_ids)
                    ->where('customer.delete_status',0)
                    ->groupby('customer.id')
                    ->get()
                    ->toArray();
//        echo '<pre>';
//        print_r($customer_list);
//        echo '</pre>';
        
        
        
        
        /*Customer list end*/
        
        $data = array();
        $data['brand_list'] = array();
        $data['customer_list'] = array();

        if((!(empty(Input::old('employee')))) && Input::old('employee') != ''){
            
            $selected_user_brand = UserBrand::select('*')->where('user_id',Input::old('employee'))->get()->toArray();       
            $user_brand = array();
            for($i=0;$i<count($selected_user_brand);$i++){
                $brand_ids = Brand::select('*')->wherein('id',  explode(',', $selected_user_brand[$i]['brand_id']))->get()->toArray();
                    $br_text = array();
                    foreach($brand_ids as $br){
                        $br_text[] = $br['brand_name'];
                    }
                    $selected_user_brand[$i]['brand_name'] = implode(', ', $br_text);
                    $selected_user_brand[$i]['id'] = $selected_user_brand[$i]['brand_id'];

            }            
            
            //$selected_user_brand = UserBrand::select('*')->where('user_id',$id)->get()->toArray();
            $data['brand_list'] = $selected_user_brand;
            
            $user_info_customer = (User::select('*')->where('id',Input::old('employee'))->first()->toArray());               
            $customer_list = Customer::select('customer.*')
                        ->where('customer.region_id',$user_info_customer['region_id'])
                        ->where('customer.delete_status',0)                    
                        ->get()
                        ->toArray();        
            $data['customer_list'] = $customer_list;
            
            
        }
        //$data['customer_list'] = $customer_list;
        $data['user_list'] = $user;
        $data['purpose_list'] = $purpose;
        $data['region_list'] = $region;
        $data['area_list'] = $area;    
        
        return view('pages/meeting/create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input_all = Input::all();
        //print_r($input_all);exit;
        //$user_info = (Helper::get_user_info($input_all['token']));
        //$user_info = Session::get('session_user_info');
        
     /*validation start*/
               $rules = array(                                       
                    "employee" => 'required',
                    "customer" => 'required|numeric',
                    "brand" => 'required',
                    "purpose" => 'required',                    
                    "latitude" => 'required',
                    "longitude" => 'required',
                    "meeting_date" => 'required|date_format:"d-m-Y"',
                    "meeting_time" => 'required',                   
                    //"followup_reminder" => 'required',                   
                );  

               /*if (array_key_exists("brand",$input_all)){
                foreach($input_all['brand'] as $key => $val){
                   $rules['brand.'.$key] = 'required|numeric';
                }                              
               }*/
               
               if($input_all['purpose'] == 1){
                   $rules['meeting_other_purpose'] = 'required';
               }
               
                $validator = Validator::make($input_all, $rules);
                // check if the validator failed -----------------------
                if ($validator->fails()) {
                    $messages = $validator->messages();
                    return Redirect::to(Config::get('constants.admin_path').'meeting/create')
                        ->withErrors($validator)
                        ->withInput();
                }
               
               
               if((!empty($input_all['followup_reminder'])) && $input_all['followup_reminder'] == 'yes'){
                    $rules = array(                   
                        "followup_date" => 'required|date_format:"d-m-Y"',
                        "followup_time" => 'required',                   
                        "followup_reminder" => 'required',                   
                    );        
                    
                $validator = Validator::make($input_all, $rules);
                // check if the validator failed -----------------------
                if ($validator->fails()) {
                    $messages = $validator->messages();
                    return Redirect::to(Config::get('constants.admin_path').'meeting/create')
                        ->withErrors($validator)
                        ->withInput();
                }                    

               }
               

               
               if((Input::hasFile('meeting_image_1'))){
                   if((Input::file('meeting_image_1')->isValid())){
                        $extension = Input::file('meeting_image_1')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){                            
                            return Redirect::to(Config::get('constants.admin_path').'meeting/create')->withErrors(['Image 1 Fail To Upload'])->withInput();
                        }                                      
                   }else{
                            return Redirect::to(Config::get('constants.admin_path').'meeting/create')->withErrors(['Image 1 Fail To Upload'])->withInput();
                   }               
               }
               
               if((Input::hasFile('meeting_image_2'))){
                   if((Input::file('meeting_image_2')->isValid())){
                        $extension = Input::file('meeting_image_2')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Redirect::to(Config::get('constants.admin_path').'meeting/create')->withErrors(['Image 2 Fail To Upload'])->withInput();
                        }                                      
                   }else{
                            return Redirect::to(Config::get('constants.admin_path').'meeting/create')->withErrors(['Image 2 Fail To Upload'])->withInput();
                   }               
               }
               
               if((Input::hasFile('meeting_image_3'))){
                   if((Input::file('meeting_image_3')->isValid())){
                        $extension = Input::file('meeting_image_3')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Redirect::to(Config::get('constants.admin_path').'meeting/create')->withErrors(['Image 3 Fail To Upload'])->withInput();
                        }                                      
                   }else{
                            return Redirect::to(Config::get('constants.admin_path').'meeting/create')->withErrors(['Image 3 Fail To Upload'])->withInput();           
                   }               
               }
               
               if((Input::hasFile('meeting_image_4'))){
                   if((Input::file('meeting_image_4')->isValid())){
                        $extension = Input::file('meeting_image_4')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Redirect::to(Config::get('constants.admin_path').'meeting/create')->withErrors(['Image 4 Fail To Upload'])->withInput();
                        }                                      
                   }else{
                            return Redirect::to(Config::get('constants.admin_path').'meeting/create')->withErrors(['Image 4 Fail To Upload'])->withInput();           
                   }               
               }
               
               if((Input::hasFile('meeting_image_5'))){
                   if((Input::file('meeting_image_5')->isValid())){
                        $extension = Input::file('meeting_image_5')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Redirect::to(Config::get('constants.admin_path').'meeting/create')->withErrors(['Image 5 Fail To Upload'])->withInput();
                        }                                      
                   }else{
                            return Redirect::to(Config::get('constants.admin_path').'meeting/create')->withErrors(['Image 5 Fail To Upload'])->withInput();           
                   }               
               }               
               
               $user_info = (User::select('*')->where('id',$input_all['employee'])->first()->toArray());               
               $customer_detaillist = Customer::select('*')->where('id',$input_all['customer'])->get()->toArray();
               
               $meeting_insert_array = array();
               $meeting_insert_array['user_id'] = $user_info['id'];
               $meeting_insert_array['customer_id'] = $input_all['customer'];
               $meeting_insert_array['customer_name'] = $customer_detaillist[0]['customer_name'];
               $meeting_insert_array['brand_id'] = implode(',', $input_all['brand']);
               $meeting_insert_array['purpose_id'] = $input_all['purpose'];               
               $meeting_insert_array['extra_note'] = $input_all['other_note'];               
               $meeting_insert_array['meeting_other_purpose'] = $input_all['meeting_other_purpose'];                              
               $dat = explode('-', $input_all['meeting_date']);
               $create_date = $dat[2].'-'.$dat[1].'-'.$dat[0].' ';
               $meeting_insert_array['created_on'] = date('Y-m-d H:i:s',strtotime($create_date .$input_all['meeting_time']));
               
               
               if($user_info['user_type'] == 1){
                $get_region_sales_manager_id = User::select('*')->where('user_type',1)->where('region_id',$user_info['region_id'])->where('disable_status',0)->where('delete_status',0)->get()->toArray();
                $meeting_insert_array['region_sales_manager_id'] = $get_region_sales_manager_id[0]['id'];
                //$meeting_insert_array['area_sales_manager_id'] = $get_area_sales_manager_id[0]['id'];                   
               }
               /*if($user_info['user_type'] == 2 || $user_info['user_type'] == 3){
                $get_region_sales_manager_id = User::select('*')->where('user_type',1)->where('region_id',$user_info['region_id'])->where('disable_status',0)->where('delete_status',0)->get()->toArray();
                $get_area_sales_manager_id = User::select('*')->where('user_type',2)->where('region_id',$user_info['region_id'])->where('area_id',$user_info['area_id'])->where('disable_status',0)->where('delete_status',0)->get()->toArray();
                //print_r($get_area_sales_manager_id);exit;
                $meeting_insert_array['region_sales_manager_id'] = $get_region_sales_manager_id[0]['id'];
                $meeting_insert_array['area_sales_manager_id'] = $get_area_sales_manager_id[0]['id'];
                   
               }*/
                if($user_info['user_type'] == 2){
                            $get_region_sales_manager_id = User::select('*')->where('user_type',1)->where('region_id',$user_info['region_id'])->where('disable_status',0)->where('delete_status',0)->get()->toArray();
                            $get_area_sales_manager_id = User::select('*')->where('user_type',2)->where('region_id',$user_info['region_id'])->where('area_id',$user_info['area_id'])->where('disable_status',0)->where('delete_status',0)->get()->toArray();
                            //print_r($get_area_sales_manager_id);exit;
                            $meeting_insert_array['region_sales_manager_id'] = $get_region_sales_manager_id[0]['id'];
                            $meeting_insert_array['area_sales_manager_id'] = $get_area_sales_manager_id[0]['id'];                     
                }
               
                if($user_info['user_type'] == 3){
                       if($user_info['assign_to_user'] != null && $user_info['assign_to_user'] != ''){
                           $get_region_sales_manager_id = User::select('*')->where('user_type',1)->where('region_id',$user_info['region_id'])->where('disable_status',0)->where('delete_status',0)->get()->toArray();
                           //$get_area_sales_manager_id = User::select('*')->where('user_type',2)->where('region_id',$user_info['region_id'])->where('area_id',$user_info['area_id'])->where('disable_status',0)->where('delete_status',0)->get()->toArray();
                           //print_r($get_area_sales_manager_id);exit;
                           $meeting_insert_array['region_sales_manager_id'] = $get_region_sales_manager_id[0]['id'];
                           $meeting_insert_array['area_sales_manager_id'] = $user_info['assign_to_user'];

                       }                     
                }               
               
              if($meeting_id = Meeting::create($meeting_insert_array))
                   {
                  
                  
                        if((Input::hasFile('meeting_image_1'))){
                            if(Input::file('meeting_image_1')->isValid()){ 
                                $extension = Input::file('meeting_image_1')->getClientOriginalExtension();
                                if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                    $destinationPath = 'upload_images/customer_'.$input_all['customer']; 
                                    $image = Input::file('meeting_image_1');                        
                                    $fileName = 'meeting_image_1_'.$meeting_id->id.'.'.$extension;
                                    Input::file('meeting_image_1')->move($destinationPath, $fileName);                        
                                    $meeting_id->meeting_image_1 = $destinationPath.'/'.$fileName;
                                    $meeting_id->save();
                                }else{
                                    return Redirect::to(Config::get('constants.admin_path').'meeting/create')->withErrors(['Image 1 Fail To Upload'])->withInput();           
                                }
                            }                   
                        }  
                        if((Input::hasFile('meeting_image_2'))){
                            if(Input::file('meeting_image_2')->isValid()){ 
                                $extension = Input::file('meeting_image_2')->getClientOriginalExtension();
                                if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                    $destinationPath = 'upload_images/customer_'.$input_all['customer']; 
                                    $image = Input::file('meeting_image_2');                        
                                    $fileName = 'meeting_image_2_'.$meeting_id->id.'.'.$extension;
                                    Input::file('meeting_image_2')->move($destinationPath, $fileName);                        
                                    $meeting_id->meeting_image_2 = $destinationPath.'/'.$fileName;
                                    $meeting_id->save();
                                }else{
                                    return Redirect::to(Config::get('constants.admin_path').'meeting/create')->withErrors(['Image 2 Fail To Upload'])->withInput();           
                                }
                            }                   
                        }  
                        if((Input::hasFile('meeting_image_3'))){
                            if(Input::file('meeting_image_3')->isValid()){ 
                                $extension = Input::file('meeting_image_3')->getClientOriginalExtension();
                                if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                    $destinationPath = 'upload_images/customer_'.$input_all['customer']; 
                                    $image = Input::file('meeting_image_3');                        
                                    $fileName = 'meeting_image_3_'.$meeting_id->id.'.'.$extension;
                                    Input::file('meeting_image_3')->move($destinationPath, $fileName);                        
                                    $meeting_id->meeting_image_3 = $destinationPath.'/'.$fileName;
                                    $meeting_id->save();
                                }else{
                                    return Redirect::to(Config::get('constants.admin_path').'meeting/create')->withErrors(['Image 3 Fail To Upload'])->withInput();           
                                }
                            }                   
                        }  
                        if((Input::hasFile('meeting_image_4'))){
                            if(Input::file('meeting_image_4')->isValid()){ 
                                $extension = Input::file('meeting_image_4')->getClientOriginalExtension();
                                if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                    $destinationPath = 'upload_images/customer_'.$input_all['customer']; 
                                    $image = Input::file('meeting_image_4');                        
                                    $fileName = 'meeting_image_4_'.$meeting_id->id.'.'.$extension;
                                    Input::file('meeting_image_4')->move($destinationPath, $fileName);                        
                                    $meeting_id->meeting_image_4 = $destinationPath.'/'.$fileName;
                                    $meeting_id->save();
                                }else{
                                    return Redirect::to(Config::get('constants.admin_path').'meeting/create')->withErrors(['Image 4 Fail To Upload'])->withInput();           
                                }
                            }                   
                        }  
                        if((Input::hasFile('meeting_image_5'))){
                            if(Input::file('meeting_image_5')->isValid()){ 
                                $extension = Input::file('meeting_image_5')->getClientOriginalExtension();
                                if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                    $destinationPath = 'upload_images/customer_'.$input_all['customer']; 
                                    $image = Input::file('meeting_image_5');                        
                                    $fileName = 'meeting_image_5_'.$meeting_id->id.'.'.$extension;
                                    Input::file('meeting_image_5')->move($destinationPath, $fileName);                        
                                    $meeting_id->meeting_image_5 = $destinationPath.'/'.$fileName;
                                    $meeting_id->save();
                                }else{
                                    return Redirect::to(Config::get('constants.admin_path').'meeting/create')->withErrors(['Image 5 Fail To Upload'])->withInput();           
                                }
                            }                   
                        }  
                  
                       
                        $gps_data_insert_array = array();
                        $gps_data_insert_array['user_id'] = $user_info['id'];
                        $gps_data_insert_array['gps_data_type'] = 1;
                        $gps_data_insert_array['meeting_id'] = $meeting_id->id;
                        $gps_data_insert_array['latitude'] = $input_all['latitude'];
                        $gps_data_insert_array['longitude'] = $input_all['longitude'];
                        $dat = explode('-', $input_all['meeting_date']);
                        $create_date = $dat[2].'-'.$dat[1].'-'.$dat[0].' ';                        
                        $gps_data_insert_array['created_on'] = date('Y-m-d H:i:s',strtotime($create_date .$input_all['meeting_time']));
                        //print_r($insert_array);exit;
                        GpsData::create($gps_data_insert_array);
                           
                           
                       if((!empty($input_all['followup_reminder'])) && $input_all['followup_reminder'] == 'yes'){
                           $followup_meeting_insert_array = array();
                           $followup_meeting_insert_array['user_id'] = $user_info['id'];
                           $followup_meeting_insert_array['meeting_id'] = $meeting_id->id;
                           $followup_meeting_insert_array['customer_id'] = $input_all['customer'];
                           $export_date = explode('-', $input_all['followup_date']);
                           $f_date = $export_date[2].'-'.$export_date[1].'-'.$export_date[0];
                           $followup_meeting_insert_array['followup_date_time'] = date('Y-m-d H:i:s',strtotime($f_date.' '.$input_all['followup_time']));
                           //$followup_meeting_insert_array['extra_note'] = $input_all['followup_note'];
                           $followup_meeting_insert_array['created_on'] = date('Y-m-d H:i:s',strtotime($create_date .$input_all['meeting_time']));
                           FollowupMeeting::create($followup_meeting_insert_array);
                       }
                       
                        Session::flash('message', 'Log Meeting Entry Success'); 
                        //Session::flash('alert-class', 'alert-danger');                         
                        return Redirect::to(Config::get('constants.admin_path').'meeting/meeting_list');                       
                   }else{
                        Session::flash('message', 'Log Meeting Entry Failed'); 
                        Session::flash('alert-class', 'alert-danger');                         
                        return Redirect::to(Config::get('constants.admin_path').'meeting/meeting_list');                       

                   }               
        
           
    }
    
    public function get_user_brand(){
        $input_all = Input::all();
        $user_info = User::select('*')->where('id',$input_all['user_id'])->get()->toArray();       
        $selected_user_brand = UserBrand::select('*')->where('user_id',$input_all['user_id'])->get()->toArray();       

        $user_brand = array();
        if($user_info[0]['user_type'] == 0){
            $selected_user_brand = Brand::select('*')->where('delete_status',0)->where('disable_status',0)->get()->toArray();
        }else{
            for($i=0;$i<count($selected_user_brand);$i++){
                $brand_ids = Brand::select('*')->wherein('id',  explode(',', $selected_user_brand[$i]['brand_id']))->get()->toArray();
                    $br_text = array();
                    foreach($brand_ids as $br){
                        $br_text[] = $br['brand_name'];
                    }
                    $selected_user_brand[$i]['brand_name'] = implode(', ', $br_text);

            }
        }
        if(count($selected_user_brand) > 0){
            $msg = "yes";
            return response()->json(array('msg'=> $msg,'user_brand' => $selected_user_brand), 200);
        }else{
            $msg = "no";
            return response()->json(array('msg'=> $msg), 200);            
        }
    }

    public function get_user_customer(){
        $input_all = Input::all();
        
        $user_info = (User::select('*')->where('id',$input_all['user_id'])->first()->toArray());               
        
        $user_brand = array();
        if($user_info['user_type'] == 0){
            $customer_list = Customer::select('customer.*')                        
                        ->where('customer.delete_status',0)                    
                        ->where('customer.disable_status',0)                    
                        ->get()
                        ->toArray();      
        }else{
            $customer_list = Customer::select('customer.*')
                        ->where('customer.region_id',$user_info['region_id'])
                        ->where('customer.delete_status',0)                    
                        ->where('customer.disable_status',0)                    
                        ->get()
                        ->toArray();        
            
        }        
        

        if(count($customer_list) > 0){
            $msg = "yes";
            return response()->json(array('msg'=> $msg,'user_customer' => $customer_list), 200);
        }else{
            $msg = "no";
            return response()->json(array('msg'=> $msg), 200);            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function today_meeting(){

        
   $session_user_info = Session::get('session_user_info');
        $user_ids = array();
        if($session_user_info['user_type'] == 0){
            $user_ids = Helper::vp_under_user_list();            
        }
        if($session_user_info['user_type'] == 1){            
            $user_ids = Helper::rsm_under_user_list($session_user_info) ;
        }
        if($session_user_info['user_type'] == 2){            
            $user_ids = Helper::asm_under_user_list($session_user_info) ;
        }
        //$user_ids = trim($user_ids, ",");        
        
        
        $customer_meeting_date_log = Meeting::select('*','meeting_customer.brand_id as brand_ids','user.id as uid','meeting_customer.created_on as meeting_date','meeting_customer.id as mid','area.area_name as carea','region.region_name as cregion','state.state_name as cstate')
                        ->leftJoin('user', 'user.id', '=', 'meeting_customer.user_id')
                        ->leftJoin('folloup_meeting_customer', 'folloup_meeting_customer.meeting_id', '=', 'meeting_customer.id')
                        ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')                                
                        //->leftJoin('brand', 'brand.id', '=', 'meeting_customer.brand_id')                                
                        ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                        ->leftJoin('area', 'area.id', '=', 'customer.area_id')                
                        ->leftJoin('state', 'state.id', '=', 'customer.state_id')                
                        ->leftJoin('meeting_purpose', 'meeting_purpose.id', '=', 'meeting_customer.purpose_id')                                                                                
                        ->wherein('meeting_customer.user_id',$user_ids)
                        ->where(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y')"), date("d-m-Y"))
                        ->orderby('meeting_customer.created_on','desc')
                        ->get()
                        ->toArray();  
        
        
        for($i=0;$i<count($customer_meeting_date_log);$i++){
            $brand_ids = Brand::select('*')->wherein('id',  explode(',', $customer_meeting_date_log[$i]['brand_ids']))->get()->toArray();
                $br_text = array();
                foreach($brand_ids as $br){
                    $br_text[] = $br['brand_name'];
                }
                $customer_meeting_date_log[$i]['brand_name'] = implode(', ', $br_text);

        }

        $data = array();
        $data['meeting_list'] = $customer_meeting_date_log;
        return view('pages.meeting.today_meeting_list',$data);
        
        
            
    }

    public function upcoming_followup(){

        
   $session_user_info = Session::get('session_user_info');
        $user_ids = array();
        if($session_user_info['user_type'] == 0){
            $user_ids = Helper::vp_under_user_list();            
        }
        if($session_user_info['user_type'] == 1){            
            $user_ids = Helper::rsm_under_user_list($session_user_info) ;
        }
        if($session_user_info['user_type'] == 2){            
            $user_ids = Helper::asm_under_user_list($session_user_info) ;
        }
        //$user_ids = trim($user_ids, ",");        
        
        
        $customer_meeting_date_log = Meeting::select('*','meeting_customer.brand_id as brand_ids','user.id as uid','meeting_customer.created_on as meeting_date','meeting_customer.id as mid','area.area_name as carea','region.region_name as cregion','state.state_name as cstate','folloup_meeting_customer.followup_date_time')
                        ->leftJoin('user', 'user.id', '=', 'meeting_customer.user_id')
                        ->leftJoin('folloup_meeting_customer', 'folloup_meeting_customer.meeting_id', '=', 'meeting_customer.id')
                        ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')                                
                        //->leftJoin('brand', 'brand.id', '=', 'meeting_customer.brand_id')                                
                        ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                        ->leftJoin('area', 'area.id', '=', 'customer.area_id')                
                        ->leftJoin('state', 'state.id', '=', 'customer.state_id')                
                        ->leftJoin('meeting_purpose', 'meeting_purpose.id', '=', 'meeting_customer.purpose_id')                                                                                
                        ->wherein('meeting_customer.user_id',$user_ids)
                        ->where(DB::raw("DATE_FORMAT(folloup_meeting_customer.followup_date_time,'%Y-%m-%d')"), '>', date("Y-m-d"))
                        //->where(DB::raw("DATE_FORMAT(folloup_meeting_customer.followup_date_time,'%Y-%m-%d')"), '<=', date('Y-m-d', strtotime("+1 week")))
                        //->where(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y')"), date("d-m-Y"))
                        ->orderby('meeting_customer.created_on','desc')
                        ->get()
                        ->toArray();  
        
        
        for($i=0;$i<count($customer_meeting_date_log);$i++){
            $brand_ids = Brand::select('*')->wherein('id',  explode(',', $customer_meeting_date_log[$i]['brand_ids']))->get()->toArray();
                $br_text = array();
                foreach($brand_ids as $br){
                    $br_text[] = $br['brand_name'];
                }
                $customer_meeting_date_log[$i]['brand_name'] = implode(', ', $br_text);

        }

        $data = array();
        $data['meeting_list'] = $customer_meeting_date_log;
        return view('pages.meeting.upcoming_followup_meeting_list',$data);
        
        
            
    }

    public function today_followup(){

        
   $session_user_info = Session::get('session_user_info');
        $user_ids = array();
        if($session_user_info['user_type'] == 0){
            $user_ids = Helper::vp_under_user_list();            
        }
        if($session_user_info['user_type'] == 1){            
            $user_ids = Helper::rsm_under_user_list($session_user_info) ;
        }
        if($session_user_info['user_type'] == 2){            
            $user_ids = Helper::asm_under_user_list($session_user_info) ;
        }
        //$user_ids = trim($user_ids, ",");        
        
        
        $customer_meeting_date_log = Meeting::select('*','meeting_customer.brand_id as brand_ids','user.id as uid','meeting_customer.created_on as meeting_date','meeting_customer.id as mid','area.area_name as carea','region.region_name as cregion','state.state_name as cstate')
                        ->leftJoin('user', 'user.id', '=', 'meeting_customer.user_id')
                        ->leftJoin('folloup_meeting_customer', 'folloup_meeting_customer.meeting_id', '=', 'meeting_customer.id')
                        ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')                                
                        //->leftJoin('brand', 'brand.id', '=', 'meeting_customer.brand_id')                                
                        ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                        ->leftJoin('area', 'area.id', '=', 'customer.area_id')                
                        ->leftJoin('state', 'state.id', '=', 'customer.state_id')                
                        ->leftJoin('meeting_purpose', 'meeting_purpose.id', '=', 'meeting_customer.purpose_id')                                                                                
                        ->wherein('meeting_customer.user_id',$user_ids)
                        ->where(DB::raw("DATE_FORMAT(folloup_meeting_customer.followup_date_time,'%Y-%m-%d')"), '=', date("Y-m-d"))
                        //->where(DB::raw("DATE_FORMAT(folloup_meeting_customer.followup_date_time,'%Y-%m-%d')"), '<=', date('Y-m-d', strtotime("+1 week")))
                        //->where(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y')"), date("d-m-Y"))
                        ->orderby('meeting_customer.created_on','desc')
                        ->get()
                        ->toArray();
        
        
        for($i=0;$i<count($customer_meeting_date_log);$i++){
            $brand_ids = Brand::select('*')->wherein('id',  explode(',', $customer_meeting_date_log[$i]['brand_ids']))->get()->toArray();
                $br_text = array();
                foreach($brand_ids as $br){
                    $br_text[] = $br['brand_name'];
                }
                $customer_meeting_date_log[$i]['brand_name'] = implode(', ', $br_text);

        }

        $data = array();
        $data['meeting_list'] = $customer_meeting_date_log;
        return view('pages.meeting.today_followup_meeting_list',$data);
        
        
            
    }
}
