<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\Hash;
use Auth;
use Session;
use App\User;


class AdminSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('pages.setting.setting');
    }

    public function passwordIndex()
    {
        return view('pages.setting.pass_setting');
    }

    public function passwordChange( Request $request )
    {

        $ar = $request->all();
        $validator = Validator::make($ar, [
            'pass_old' => 'required',
            'pass_new' => 'required',
            'pass_new_conf' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();        
        }
        if($ar['pass_new'] != $ar['pass_new_conf']){
            return redirect()->back()
                        ->withErrors("New password does not match with confirm password.")
                        ->withInput();        
        }

        $session_user_info = Session::get('session_user_info');
        
        $auth = Auth::attempt(['id' => $session_user_info['id'], 'password' => $ar['pass_old']]);
        
         
        if ($auth)
        {
            $user_detail = User::find($session_user_info['id']);
            $user_detail->user_password = Hash::make($ar['pass_new']);
            $user_detail->save();
            
            Session::flash("message","Password has been changed successfully.");
            return redirect()->back();
        }
        else
        {
             return redirect()->back()
                        ->withErrors("You have entered wrong current password.")
                        ->withInput();
        }

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
