<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use App\Helpers\Helper;
use App\Customer;
use App\Meeting;
use App\FollowupMeeting;
use App\GpsData;
use App\Brand;
use App\Purpose;
use App\State;
use App\Region;
use App\RegionDetail;
use App\User;
use App\Area;
use DB;
use Image;
use Excel;
use Mail;
use Config;
use PDO;

class CronController extends Controller {

    public function index() {
        
        error_reporting(E_ALL);
        set_time_limit(0);
        ini_set('display_errors', 1);
        $today_date = date('Y-m-d', strtotime(' -1 day'));
        /*gps end day start*/
        $gps_start_date = $today_date. ' 00:00:00';
        $gps_end_date = $today_date. ' 23:59:59';
        
        $start_day_user = GpsData::select('*')
                    ->where('gps_data_type',0)                    
                    ->where(DB::raw("DATE(gps_data.created_on)"), '>=', $gps_start_date)
                    ->where(DB::raw("DATE(gps_data.created_on)"), '<=', $gps_end_date)                    
                    ->get()->toArray();
        foreach ($start_day_user as $start_employee){
            $end_day_emp_status =   GpsData::select('*')
                    ->where('gps_data_type',2)
                    ->where('user_id',$start_employee['user_id'])
                    ->where(DB::raw("DATE(gps_data.created_on)"), '>=', $gps_start_date)
                    ->where(DB::raw("DATE(gps_data.created_on)"), '<=', $gps_end_date)                    
                    ->get()->toArray();
            
            if(!(count($end_day_emp_status) > 0)){
                
                $lastgps = GpsData::select('*')
                    ->where('user_id',$start_employee['user_id'])                    
                    ->where(DB::raw("DATE(gps_data.created_on)"), '>=', $gps_start_date)
                    ->where(DB::raw("DATE(gps_data.created_on)"), '<=', $gps_end_date)
                    ->orderby('gps_data.created_on','desc')
                    ->get()->toArray();     
                if(count($lastgps) > 0){
                $gps_data_insert = array();
                $gps_data_insert['user_id'] = $lastgps[0]['user_id'];
                $gps_data_insert['gps_data_type'] = 2;
                $gps_data_insert['cron_job_end_day'] = 1;
                $gps_data_insert['latitude'] = $lastgps[0]['latitude'];
                $gps_data_insert['longitude'] = $lastgps[0]['longitude'];
                $gps_data_insert['created_on'] = $gps_end_date;
                
                GpsData::create($gps_data_insert);
                }
            }
            
        }        
        /*gps end day end*/
        //===========================================================================
        /* VP Daily new customer MAil start */
        $vp_user_list = User::select('*')
                ->where('disable_status', 0)
                ->where('delete_status', 0)
                ->where('user_type', 0)
                //->where('user_admin_backoffice_type', '=', NULL)
                ->get()
                ->toArray();
        for($vpuser=0;$vpuser<count($vp_user_list);$vpuser++){
            $user_ids = Helper::vp_under_user_list();
            $excel_array = Helper::new_customer_list_added_by_users($user_ids, $today_date, $today_date);
            $file_path = 'daily_new_customer_' . $today_date . '_' . $vp_user_list[$vpuser]['id'];
            $file_path = public_path('excel/exports/' . $file_path);
            $mailsubject = 'Daily customer report - '.date('d-M-Y');        
            Helper::send_excel_report($file_path, $excel_array, $vp_user_list[$vpuser]['user_email'], 'Customer',$mailsubject);            
        }

        
        /* VP Daily new customer MAil end */
        //===========================================================================
        //============================================================================
        /* RSM Daily new customer MAil start */
        $rsm_user_list = User::select('*')
                ->where('disable_status', 0)
                ->where('delete_status', 0)
                ->where('user_type', 1)
                ->get()
                ->toArray();
        for ($i = 0; $i < count($rsm_user_list); $i++) {
            $user_ids = Helper::rsm_under_user_list($rsm_user_list[$i]);
            $excel_array = Helper::new_customer_list_added_by_users($user_ids, $today_date, $today_date);
            $file_path = 'daily_new_customer_' . $today_date . '_' . $rsm_user_list[$i]['id'];
            $file_path = public_path('excel/exports/' . $file_path);
            $mailsubject = 'Daily customer report - '.date('d-M-Y');        
            Helper::send_excel_report($file_path, $excel_array, $rsm_user_list[$i]['user_email'], 'Customer',$mailsubject);
        }
        /* RSM Daily new customer MAil end */
        //===========================================================================                    
        //============================================================================
        /* ASM Daily new customer  MAil start */
        $asm_user_list = User::select('*')
                ->where('disable_status', 0)
                ->where('delete_status', 0)
                ->where('user_type', 2)
                ->get()
                ->toArray();
        for ($i = 0; $i < count($asm_user_list); $i++) {
            $user_ids = Helper::asm_under_user_list($asm_user_list[$i]);
            //$user_ids[] = $asm_user_list[$i]['id'];
            $excel_array = Helper::new_customer_list_added_by_users($user_ids, $today_date, $today_date);
            $file_path = 'daily_new_customer_' . $today_date . '_' . $asm_user_list[$i]['id'];
            $file_path = public_path('excel/exports/' . $file_path);
            $mailsubject = 'Daily customer report - '.date('d-M-Y');  
            Helper::send_excel_report($file_path, $excel_array, $asm_user_list[$i]['user_email'], 'Customer',$mailsubject);
        }
        /* ASM Daily new customer MAil end */
        //===========================================================================                    

        /* VP Daily new meeting MAil start */
        $vp_user_list = User::select('*')
                ->where('disable_status', 0)
                ->where('delete_status', 0)
                ->where('user_type', 0)
                //->where('user_admin_backoffice_type', '=', NULL)
                ->get()
                ->toArray();
        for($vpuser=0;$vpuser<count($vp_user_list);$vpuser++){
        $user_ids = Helper::vp_under_user_list();
        $excel_array = Helper::new_customer_meeting_added_by_users($user_ids, $today_date, $today_date);
        $file_path = 'daily_new_meeting_' . $today_date . '_' . $vp_user_list[$vpuser]['id'];
        $file_path = public_path('excel/exports/' . $file_path);
        $mailsubject = 'Daily meeting report - '.date('d-M-Y');  
        Helper::send_excel_report($file_path, $excel_array, $vp_user_list[$vpuser]['user_email'], 'Meeting',$mailsubject);
        }
        /* VP Daily new meeting MAil end */


        //============================================================================
        /* RSM Daily new meeting  MAil start */
        $rsm_user_list = User::select('*')
                ->where('disable_status', 0)
                ->where('delete_status', 0)
                ->where('user_type', 1)
                ->get()
                ->toArray();
        for ($i = 0; $i < count($rsm_user_list); $i++) {
            $user_ids = Helper::rsm_under_user_list($rsm_user_list[$i]);
            $excel_array = Helper::new_customer_meeting_added_by_users($user_ids, $today_date, $today_date);
            $file_path = 'daily_new_meeting_' . $today_date . '_' . $rsm_user_list[$i]['id'];
            $file_path = public_path('excel/exports/' . $file_path);
            $mailsubject = 'Daily meeting report - '.date('d-M-Y');  
            Helper::send_excel_report($file_path, $excel_array, $rsm_user_list[$i]['user_email'], 'Meeting',$mailsubject);
        }
        /* RSM Daily new meeting  MAil end */
        //===========================================================================                    
        //============================================================================
        /* ASM Daily new meeting  MAil start */
        $asm_user_list = User::select('*')
                ->where('disable_status', 0)
                ->where('delete_status', 0)
                ->where('user_type', 2)
                ->get()
                ->toArray();
        for ($i = 0; $i < count($asm_user_list); $i++) {
            $user_ids = Helper::asm_under_user_list($asm_user_list[$i]);
            //$user_ids[] = $asm_user_list[$i]['id'];
            $excel_array = Helper::new_customer_meeting_added_by_users($user_ids, $today_date, $today_date);
            $file_path = 'daily_new_meeting_' . $today_date . '_' . $asm_user_list[$i]['id'];
            $file_path = public_path('excel/exports/' . $file_path);
            $mailsubject = 'Daily meeting report - '.date('d-M-Y');  
            Helper::send_excel_report($file_path, $excel_array, $asm_user_list[$i]['user_email'], 'Meeting',$mailsubject);
        }
        /* ASM Daily new meeting MAil end */
        //===========================================================================            

        /*Weekly Process start*/
        $day = date('w');
        if ($day == 0) {
            $weekstartday = 7;
            /*$week_start = date('Y-m-d', strtotime('-' . $day . ' days'));
            $week_end = date('Y-m-d', strtotime('+' . (6 - $day) . ' days'));
            $week_start_text = date('d-M-Y', strtotime('-' . $day . ' days'));
            $week_end_text = date('d-M-Y', strtotime('+' . (6 - $day) . ' days'));*/
            
            $week_start = date('Y-m-d', strtotime('-' . $weekstartday . ' days'));
            $week_end = date('Y-m-d', strtotime('+' . (6 - $weekstartday) . ' days'));
            $week_start_text = date('d-M-Y', strtotime('-' . $weekstartday . ' days'));
            $week_end_text = date('d-M-Y', strtotime('+' . (6 - $weekstartday) . ' days'));

            /* VP Weekly new customer MAil start */
            $vp_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 0)
                    //->where('user_admin_backoffice_type', '<>', 1)
                    ->get()
                    ->toArray();
            for($vp=0;$vp<count($vp_user_list);$vp++){
                $user_ids = Helper::vp_under_user_list();
                $excel_array = Helper::new_customer_list_added_by_users($user_ids, $week_start, $week_end);
                $file_path = 'weekly_new_customer_' . $today_date . '_' . $vp_user_list[$vp]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Weekly customer report - '.$week_start_text.' to '.$week_end_text;  
                Helper::send_excel_report($file_path, $excel_array, $vp_user_list[$vp]['user_email'], 'Weekly_Customer',$mailsubject);
            }

            /* VP Weekly new customer MAil end */
            //===========================================================================            
            //============================================================================
            /* RSM weekly new customer MAil start */
            $rsm_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 1)
                    ->get()
                    ->toArray();
            for ($i = 0; $i < count($rsm_user_list); $i++) {
                $user_ids = Helper::rsm_under_user_list($rsm_user_list[$i]);
                $excel_array = Helper::new_customer_list_added_by_users($user_ids, $week_start, $week_end);
                $file_path = 'weekly_new_customer_' . $today_date . '_' . $rsm_user_list[$i]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Weekly customer report - '.$week_start_text.' to '.$week_end_text;  
                Helper::send_excel_report($file_path, $excel_array, $rsm_user_list[$i]['user_email'], 'Weekly_Customer',$mailsubject);
            }
            /* RSM weekly new customer MAil end */
            //===========================================================================                    
            //============================================================================
            /* ASM weekly new customer  MAil start */
            $asm_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 2)
                    ->get()
                    ->toArray();
            for ($i = 0; $i < count($asm_user_list); $i++) {
                $user_ids = Helper::asm_under_user_list($asm_user_list[$i]);
                //$user_ids[] = $asm_user_list[$i]['id'];
                $excel_array = Helper::new_customer_list_added_by_users($user_ids, $week_start, $week_end);
                $file_path = 'weekly_new_customer_' . $today_date . '_' . $asm_user_list[$i]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Weekly customer report - '.$week_start_text.' to '.$week_end_text;  
                Helper::send_excel_report($file_path, $excel_array, $asm_user_list[$i]['user_email'], 'Weekly_Customer',$mailsubject);
            }
            /* ASM weekly new customer MAil end */
            //===========================================================================         

            /* VP Weekly new meeting MAil start */
            $vp_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 0)
                    //->where('user_admin_backoffice_type', '=', NULL)
                    ->get()
                    ->toArray();
            for($vp=0;$vp<count($vp_user_list);$vp++){
                $user_ids = Helper::vp_under_user_list();
                $excel_array = Helper::new_customer_meeting_added_by_users($user_ids, $week_start, $week_end);
                $file_path = 'weekly_new_meeting_' . $today_date . '_' . $vp_user_list[$vp]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Weekly meeting report - '.$week_start_text.' to '.$week_end_text;  
                Helper::send_excel_report($file_path, $excel_array, $vp_user_list[$vp]['user_email'], 'Weekly_Meeting',$mailsubject);    
            }
            /* VP Weekly new meeting MAil end */
            //============================================================================        
//============================================================================
            /* RSM Weekly new meeting  MAil start */
            $rsm_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 1)
                    ->get()
                    ->toArray();
            for ($i = 0; $i < count($rsm_user_list); $i++) {
                $user_ids = Helper::rsm_under_user_list($rsm_user_list[$i]);
                $excel_array = Helper::new_customer_meeting_added_by_users($user_ids, $week_start, $week_end);
                $file_path = 'weekly_new_meeting_' . $today_date . '_' . $rsm_user_list[$i]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Weekly meeting report - '.$week_start_text.' to '.$week_end_text;  
                Helper::send_excel_report($file_path, $excel_array, $rsm_user_list[$i]['user_email'], 'Weekly_Meeting',$mailsubject);
            }
            /* RSM Weekly new meeting  MAil end */
            //===========================================================================         
            //============================================================================
            /* ASM Weekly new meeting  MAil start */
            $asm_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 2)
                    ->get()
                    ->toArray();
            for ($i = 0; $i < count($asm_user_list); $i++) {
                $user_ids = Helper::asm_under_user_list($asm_user_list[$i]);
                //$user_ids[] = $asm_user_list[$i]['id'];
                $excel_array = Helper::new_customer_meeting_added_by_users($user_ids, $week_start, $week_end);
                $file_path = 'weekly_new_meeting_' . $today_date . '_' . $asm_user_list[$i]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Weekly meeting report - '.$week_start_text.' to '.$week_end_text;  
                Helper::send_excel_report($file_path, $excel_array, $asm_user_list[$i]['user_email'], 'Weekly_Meeting',$mailsubject);
            }
            /* ASM Weekly new meeting MAil end */
            //===========================================================================            


            /* VP Brand weekly report start */
            $vp_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 0)
                    //->where('user_admin_backoffice_type', '=', NULL)
                    ->get()
                    ->toArray();
        for($vp=0;$vp<count($vp_user_list);$vp++){
            $user_ids = Helper::vp_under_user_list();
            $brand_list = Brand::select('*')->where('delete_status', 0)->where('disable_status', 0)->get()->toArray();
            $excel_array = array();
            $excel_array[] = ['id', 'Brand Name', 'Total Meeting'];
            for ($b = 0; $b < count($brand_list); $b++) {
                $bid = $brand_list[$b]['id'];
                $customer_meeting_date_log = Meeting::select('*')
                                ->where(DB::raw("find_in_set($bid,brand_id)"), '<>', 0)
                                ->wherein('user_id', $user_ids)
                                ->where(DB::raw("DATE(meeting_customer.created_on)"), '>=', $week_start)
                                ->where(DB::raw("DATE(meeting_customer.created_on)"), '<=', $week_end)
                                //->toSql();
                                ->get()->toArray();

                $excel_array[] = array(
                    $b + 1,
                    $brand_list[$b]['brand_name'],
                    count($customer_meeting_date_log)
                );
            }
            $file_path = 'weekly_brand_' . $today_date . '_' . $vp_user_list[$vp]['id'];
            $file_path = public_path('excel/exports/' . $file_path);
            $mailsubject = 'Weekly brand report - '.$week_start_text.' to '.$week_end_text;  
            Helper::send_excel_report($file_path, $excel_array, $vp_user_list[$vp]['user_email'], 'Weekly_Brand',$mailsubject);
        }
            /* VP Brand weekly report end */

//============================================================================
            /* RSM Weekly brand  MAil start */
            $rsm_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 1)
                    ->get()
                    ->toArray();
            for ($i = 0; $i < count($rsm_user_list); $i++) {
                $user_ids = Helper::rsm_under_user_list($rsm_user_list[$i]);
                //$brand_list = Brand::select('*')->where('delete_status',0)->where()->where('disable_status',0)->get()->toArray();
                $excel_array = Helper::brand_weekly_by_users($user_ids, $week_start, $week_end);
                //$excel_array = Helper::new_customer_meeting_added_by_users($user_ids, $week_start, $week_end);
                $file_path = 'weekly_brand_' . $today_date . '_' . $rsm_user_list[$i]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Weekly brand report - '.$week_start_text.' to '.$week_end_text;  
                Helper::send_excel_report($file_path, $excel_array, $rsm_user_list[$i]['user_email'], 'Weekly_Brand',$mailsubject);
            }
            /* RSM Weekly brand MAil end */
            //===========================================================================                   
            //============================================================================
            /* ASM Weekly brand  MAil start */
            $asm_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 2)
                    ->get()
                    ->toArray();
            for ($i = 0; $i < count($asm_user_list); $i++) {
                $user_ids = Helper::asm_under_user_list($asm_user_list[$i]);
                $excel_array = Helper::brand_weekly_by_users($user_ids, $week_start, $week_end);
                $file_path = 'weekly_brand_' . $today_date . '_' . $asm_user_list[$i]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Weekly brand report - '.$week_start_text.' to '.$week_end_text;  
                Helper::send_excel_report($file_path, $excel_array, $asm_user_list[$i]['user_email'], 'Weekly_Brand',$mailsubject);
            }
            /* ASM Weekly brand MAil end */
            //===========================================================================                        

            /* VP purpose weekly report start */
            $vp_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 0)
                    //->where('user_admin_backoffice_type', '=', NULL)
                    ->get()
                    ->toArray();
            for($vp=0;$vp<count($vp_user_list);$vp++){
                $user_ids = Helper::vp_under_user_list();
                $excel_array = Helper::purpose_weekly_by_users($user_ids, $week_start, $week_end);
                $file_path = 'weekly_purpose_' . $today_date . '_' . $vp_user_list[$vp]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Weekly purpose report - '.$week_start_text.' to '.$week_end_text;  
                Helper::send_excel_report($file_path, $excel_array, $vp_user_list[$vp]['user_email'], 'Weekly_Purpose',$mailsubject );
            }
            /* VP purpose weekly report end */

//============================================================================
            /* RSM Weekly purpose  MAil start */
            $rsm_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 1)
                    ->get()
                    ->toArray();
            for ($i = 0; $i < count($rsm_user_list); $i++) {
                $user_ids = Helper::rsm_under_user_list($rsm_user_list[$i]);
                //$brand_list = Brand::select('*')->where('delete_status',0)->where()->where('disable_status',0)->get()->toArray();
                $excel_array = Helper::purpose_weekly_by_users($user_ids, $week_start, $week_end);
                //$excel_array = Helper::new_customer_meeting_added_by_users($user_ids, $week_start, $week_end);
                $file_path = 'weekly_purpose_' . $today_date . '_' . $rsm_user_list[$i]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Weekly purpose report - '.$week_start_text.' to '.$week_end_text;  
                Helper::send_excel_report($file_path, $excel_array, $rsm_user_list[$i]['user_email'], 'Weekly_Purpose',$mailsubject);
            }
            /* RSM Weekly purpose MAil end */
            //===========================================================================               
            //============================================================================
            /* ASM Weekly purpose  MAil start */
            $asm_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 2)
                    ->get()
                    ->toArray();
            for ($i = 0; $i < count($asm_user_list); $i++) {
                $user_ids = Helper::asm_under_user_list($asm_user_list[$i]);
                $excel_array = Helper::purpose_weekly_by_users($user_ids, $week_start, $week_end);
                $file_path = 'weekly_purpose_' . $today_date . '_' . $asm_user_list[$i]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Weekly purpose report - '.$week_start_text.' to '.$week_end_text;  
                Helper::send_excel_report($file_path, $excel_array, $asm_user_list[$i]['user_email'], 'Weekly_Purpose',$mailsubject);
            }
            /* ASM Weekly purpose MAil end */
            //===========================================================================                                    


            /* VP region weekly report start */
            $vp_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 0)
                    //->where('user_admin_backoffice_type', '=', NULL)
                    ->get()
                    ->toArray();
            for($vp=0;$vp<count($vp_user_list);$vp++){
                $user_ids = Helper::vp_under_user_list();
                $region = Region::select('*')->where('delete_status', 0)->get()->toArray();        
                $excel_array = Helper::region_weekly_by_users($user_ids, $region,$week_start, $week_end);

                $file_path = 'weekly_region_' . $today_date . '_' . $vp_user_list[$vp]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Weekly region report - '.$week_start_text.' to '.$week_end_text;  
                Helper::send_excel_report($file_path, $excel_array, $vp_user_list[$vp]['user_email'], 'Weekly_Region',$mailsubject );
            }
            /* VP region weekly report end */

//============================================================================            
            //============================================================================
            /* RSM Weekly region  MAil start */
            $rsm_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 1)
                    ->get()
                    ->toArray();
            for ($i = 0; $i < count($rsm_user_list); $i++) {
                $user_ids = Helper::rsm_under_user_list($rsm_user_list[$i]);
                //$region = Region::select('*')->where('delete_status', 0)->get()->toArray();        
                $region = Region::select('*')->where('id', $rsm_user_list[$i]['region_id'])->where('delete_status', 0)->get()->toArray();
                $excel_array = Helper::region_weekly_by_users($user_ids, $region,$week_start, $week_end);

                $file_path = 'weekly_region_' . $today_date . '_' . $rsm_user_list[$i]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Weekly region report - '.$week_start_text.' to '.$week_end_text;  
                Helper::send_excel_report($file_path, $excel_array, $rsm_user_list[$i]['user_email'], 'Weekly_Region',$mailsubject);
            }
            /* RSM Weekly region MAil end */
            //===========================================================================  

   /* VP state weekly report start */
            $vp_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 0)
                    //->where('user_admin_backoffice_type', '=', NULL)
                    ->get()
                    ->toArray();
for($vp=0;$vp<count($vp_user_list);$vp++){
            $user_ids = Helper::vp_under_user_list();
            $state = State::select('*')->get()->toArray();        
            $excel_array = Helper::state_weekly_by_users($user_ids, $state,$week_start, $week_end);

            $file_path = 'weekly_state_' . $today_date . '_' . $vp_user_list[$vp]['id'];
            $file_path = public_path('excel/exports/' . $file_path);
            $mailsubject = 'Weekly state report - '.$week_start_text.' to '.$week_end_text;  
            Helper::send_excel_report($file_path, $excel_array, $vp_user_list[$vp]['user_email'], 'Weekly_State',$mailsubject );
        }
            /* VP state weekly report end */            
            
   /* RSM Weekly state  MAil start */
            $rsm_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 1)
                    ->get()
                    ->toArray();
            for ($i = 0; $i < count($rsm_user_list); $i++) {
                $user_ids = Helper::rsm_under_user_list($rsm_user_list[$i]);
                $region_user = User::select('*')->where('id',$rsm_user_list[$i]['id'])->get()->toArray();
                $state_list = RegionDetail::select('*','region_detail.state_id')->where('region_id',$rsm_user_list[$i]['region_id'])->get()->toArray();
                $state_array = array();
                foreach($state_list as $s){
                    $state_array[] = $s['state_id'];
                }

                
                $state = State::select('*')->wherein('id',$state_array)->get()->toArray();        
                //$region = Region::select('*')->where('delete_status', 0)->get()->toArray();        
                $excel_array = Helper::state_weekly_by_users($user_ids, $state,$week_start, $week_end);
                


                $file_path = 'weekly_state_' . $today_date . '_' . $rsm_user_list[$i]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Weekly state report - '.$week_start_text.' to '.$week_end_text;  
                Helper::send_excel_report($file_path, $excel_array, $rsm_user_list[$i]['user_email'], 'Weekly_State',$mailsubject);
            }
            /* RSM Weekly state MAil end */
            //===========================================================================              
            
//            echo $week_start . '<br/>';
//            echo $week_end . '<br/>';
        }
        /*Weekly Process end*/
        
        /*Monthly process start*/
        /*$month_first_date = date('Y-m-01');         
        $month_last_date = date('Y-m-t');
        $month_first_date_text = date('01-m-Y');         
        $month_last_date_text = date('t-M-Y');*/
        $month_first_date = date('Y-m-01',strtotime(' -1 month'));         
        $month_last_date = date('Y-m-t',strtotime(' -1 month'));
        $month_first_date_text = date('01-M-Y',strtotime(' -1 month'));         
        $month_last_date_text = date('t-M-Y',strtotime(' -1 month'));

        
        
        //if($today_date == $month_last_date){
        if(date('Y-m-d') == date('Y-m-01')){           
      
            
            //$month_first_date = date('Y-m-d', strtotime('-' . $day . ' days'));
            //$month_last_date = date('Y-m-d', strtotime('+' . (6 - $day) . ' days'));

            /* VP Monthly new customer MAil start */
            $vp_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 0)
                    //->where('user_admin_backoffice_type', '=', NULL)
                    ->get()
                    ->toArray();
for($vp=0;$vp<count($vp_user_list);$vp++){
            $user_ids = Helper::vp_under_user_list();
            $excel_array = Helper::new_customer_list_added_by_users($user_ids, $month_first_date, $month_last_date);
            $file_path = 'monthly_new_customer_' . $today_date . '_' . $vp_user_list[$vp]['id'];
            $file_path = public_path('excel/exports/' . $file_path);
            $mailsubject = 'Monthly customer report - '.$month_first_date_text.' to '.$month_last_date_text;  
            Helper::send_excel_report($file_path, $excel_array, $vp_user_list[$vp]['user_email'], 'Monthly_Customer',$mailsubject);
}
            /* VP Monthly new customer MAil end */
            //===========================================================================            
            //============================================================================
            /* RSM monthly new customer MAil start */
            $rsm_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 1)
                    ->get()
                    ->toArray();
            for ($i = 0; $i < count($rsm_user_list); $i++) {
                $user_ids = Helper::rsm_under_user_list($rsm_user_list[$i]);
                $excel_array = Helper::new_customer_list_added_by_users($user_ids, $month_first_date, $month_last_date);
                $file_path = 'monthly_new_customer_' . $today_date . '_' . $rsm_user_list[$i]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Monthly customer report - '.$month_first_date_text.' to '.$month_last_date_text;  
                Helper::send_excel_report($file_path, $excel_array, $rsm_user_list[$i]['user_email'], 'Monthly_Customer',$mailsubject);
            }
            /* RSM monthly new customer MAil end */
            //===========================================================================                    
            //============================================================================
            /* ASM monthly new customer  MAil start */
            $asm_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 2)
                    ->get()
                    ->toArray();
            for ($i = 0; $i < count($asm_user_list); $i++) {
                $user_ids = Helper::asm_under_user_list($asm_user_list[$i]);
                //$user_ids[] = $asm_user_list[$i]['id'];
                $excel_array = Helper::new_customer_list_added_by_users($user_ids, $month_first_date, $month_last_date);
                $file_path = 'monthly_new_customer_' . $today_date . '_' . $asm_user_list[$i]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Monthly customer report - '.$month_first_date_text.' to '.$month_last_date_text;  
                Helper::send_excel_report($file_path, $excel_array, $asm_user_list[$i]['user_email'], 'Monthly_Customer',$mailsubject);
            }
            /* ASM monthly new customer MAil end */
            //===========================================================================         

            /* VP Monthly new meeting MAil start */
            $vp_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 0)
                    //->where('user_admin_backoffice_type', '=', NULL)
                    ->get()
                    ->toArray();
for($vp=0;$vp<count($vp_user_list);$vp++){
            $user_ids = Helper::vp_under_user_list();
            $excel_array = Helper::new_customer_meeting_added_by_users($user_ids, $month_first_date, $month_last_date);
            $file_path = 'monthly_new_meeting_' . $today_date . '_' . $vp_user_list[$vp]['id'];
            $file_path = public_path('excel/exports/' . $file_path);
            $mailsubject = 'Monthly meeting report - '.$month_first_date_text.' to '.$month_last_date_text;  
            Helper::send_excel_report($file_path, $excel_array, $vp_user_list[$vp]['user_email'], 'Monthly_Meeting',$mailsubject);
}
            /* VP Monthly new meeting MAil end */
            //============================================================================        
//============================================================================
            /* RSM Monthly new meeting  MAil start */
            $rsm_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 1)
                    ->get()
                    ->toArray();
            for ($i = 0; $i < count($rsm_user_list); $i++) {
                $user_ids = Helper::rsm_under_user_list($rsm_user_list[$i]);
                $excel_array = Helper::new_customer_meeting_added_by_users($user_ids, $month_first_date, $month_last_date);
                $file_path = 'monthly_new_meeting_' . $today_date . '_' . $rsm_user_list[$i]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Monthly meeting report - '.$month_first_date_text.' to '.$month_last_date_text;  
                Helper::send_excel_report($file_path, $excel_array, $rsm_user_list[$i]['user_email'], 'Monthly_Meeting',$mailsubject);
            }
            /* RSM Monthly new meeting  MAil end */
            //===========================================================================         
            //============================================================================
            /* ASM Monthly new meeting  MAil start */
            $asm_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 2)
                    ->get()
                    ->toArray();
            for ($i = 0; $i < count($asm_user_list); $i++) {
                $user_ids = Helper::asm_under_user_list($asm_user_list[$i]);
                //$user_ids[] = $asm_user_list[$i]['id'];
                $excel_array = Helper::new_customer_meeting_added_by_users($user_ids, $month_first_date, $month_last_date);
                $file_path = 'monthly_new_meeting_' . $today_date . '_' . $asm_user_list[$i]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Monthly meeting report - '.$month_first_date_text.' to '.$month_last_date_text;  
                Helper::send_excel_report($file_path, $excel_array, $asm_user_list[$i]['user_email'], 'Monthly_Meeting',$mailsubject);
            }
            /* ASM Monthly new meeting MAil end */
            //===========================================================================            


            /* VP Brand monthly report start */
            $vp_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 0)
                    //->where('user_admin_backoffice_type', '=', NULL)
                    ->get()
                    ->toArray();
for($vp=0;$vp<count($vp_user_list);$vp++){
            $user_ids = Helper::vp_under_user_list();
            $brand_list = Brand::select('*')->where('delete_status', 0)->where('disable_status', 0)->get()->toArray();
            $excel_array = array();
            $excel_array[] = ['id', 'Brand Name', 'Total Meeting'];
            for ($b = 0; $b < count($brand_list); $b++) {
                $bid = $brand_list[$b]['id'];
                $customer_meeting_date_log = Meeting::select('*')
                                ->where(DB::raw("find_in_set($bid,brand_id)"), '<>', 0)
                                ->wherein('user_id', $user_ids)
                                ->where(DB::raw("DATE(meeting_customer.created_on)"), '>=', $month_first_date)
                                ->where(DB::raw("DATE(meeting_customer.created_on)"), '<=', $month_last_date)
                                //->toSql();
                                ->get()->toArray();

                $excel_array[] = array(
                    $b + 1,
                    $brand_list[$b]['brand_name'],
                    count($customer_meeting_date_log)
                );
            }
            $file_path = 'monthly_brand_' . $today_date . '_' . $vp_user_list[$vp]['id'];
            $file_path = public_path('excel/exports/' . $file_path);
            $mailsubject = 'Monthly brand report - '.$month_first_date_text.' to '.$month_last_date_text;  
            Helper::send_excel_report($file_path, $excel_array, $vp_user_list[$vp]['user_email'], 'Monthly_Brand',$mailsubject);
}
            /* VP Brand monthly report end */

//============================================================================
            /* RSM Monthly brand  MAil start */
            $rsm_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 1)
                    ->get()
                    ->toArray();
            for ($i = 0; $i < count($rsm_user_list); $i++) {
                $user_ids = Helper::rsm_under_user_list($rsm_user_list[$i]);
                //$brand_list = Brand::select('*')->where('delete_status',0)->where()->where('disable_status',0)->get()->toArray();
                $excel_array = Helper::brand_weekly_by_users($user_ids, $month_first_date, $month_last_date);
                //$excel_array = Helper::new_customer_meeting_added_by_users($user_ids, $month_first_date, $month_last_date);
                $file_path = 'monthly_brand_' . $today_date . '_' . $rsm_user_list[$i]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Monthly brand report - '.$month_first_date_text.' to '.$month_last_date_text;  
                Helper::send_excel_report($file_path, $excel_array, $rsm_user_list[$i]['user_email'], 'Monthly_Brand',$mailsubject);
            }
            /* RSM Monthly brand MAil end */
            //===========================================================================                   
            //============================================================================
            /* ASM Monthly brand  MAil start */
            $asm_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 2)
                    ->get()
                    ->toArray();
            for ($i = 0; $i < count($asm_user_list); $i++) {
                $user_ids = Helper::asm_under_user_list($asm_user_list[$i]);
                $excel_array = Helper::brand_weekly_by_users($user_ids, $month_first_date, $month_last_date);
                $file_path = 'monthly_brand_' . $today_date . '_' . $asm_user_list[$i]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Monthly brand report - '.$month_first_date_text.' to '.$month_last_date_text;  
                Helper::send_excel_report($file_path, $excel_array, $asm_user_list[$i]['user_email'], 'Monthly_Brand',$mailsubject);
            }
            /* ASM Monthly brand MAil end */
            //===========================================================================                        

            /* VP purpose monthly report start */
            $vp_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 0)
                    //->where('user_admin_backoffice_type', '=', NULL)
                    ->get()
                    ->toArray();
for($vp=0;$vp<count($vp_user_list);$vp++){
            $user_ids = Helper::vp_under_user_list();
            $excel_array = Helper::purpose_weekly_by_users($user_ids, $month_first_date, $month_last_date);
            $file_path = 'monthly_purpose_' . $today_date . '_' . $vp_user_list[$vp]['id'];
            $file_path = public_path('excel/exports/' . $file_path);
            $mailsubject = 'Monthly purpose report - '.$month_first_date_text.' to '.$month_last_date_text;  
            Helper::send_excel_report($file_path, $excel_array, $vp_user_list[$vp]['user_email'], 'Monthly_Purpose',$mailsubject);
}
            /* VP purpose monthly report end */

//============================================================================
            /* RSM Monthly purpose  MAil start */
            $rsm_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 1)
                    ->get()
                    ->toArray();
            for ($i = 0; $i < count($rsm_user_list); $i++) {
                $user_ids = Helper::rsm_under_user_list($rsm_user_list[$i]);
                //$brand_list = Brand::select('*')->where('delete_status',0)->where()->where('disable_status',0)->get()->toArray();
                $excel_array = Helper::purpose_weekly_by_users($user_ids, $month_first_date, $month_last_date);
                //$excel_array = Helper::new_customer_meeting_added_by_users($user_ids, $month_first_date, $month_last_date);
                $file_path = 'monthly_purpose_' . $today_date . '_' . $rsm_user_list[$i]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Monthly purpose report - '.$month_first_date_text.' to '.$month_last_date_text;  
                Helper::send_excel_report($file_path, $excel_array, $rsm_user_list[$i]['user_email'], 'Monthly_Purpose',$mailsubject);
            }
            /* RSM Monthly purpose MAil end */
            //===========================================================================               
            //============================================================================
            /* ASM Monthly purpose  MAil start */
            $asm_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 2)
                    ->get()
                    ->toArray();
            for ($i = 0; $i < count($asm_user_list); $i++) {
                $user_ids = Helper::asm_under_user_list($asm_user_list[$i]);
                $excel_array = Helper::purpose_weekly_by_users($user_ids, $month_first_date, $month_last_date);
                $file_path = 'monthly_purpose_' . $today_date . '_' . $asm_user_list[$i]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Monthly purpose report - '.$month_first_date_text.' to '.$month_last_date_text;  
                Helper::send_excel_report($file_path, $excel_array, $asm_user_list[$i]['user_email'], 'Monthly_Purpose',$mailsubject);
            }
            /* ASM Monthly purpose MAil end */
            //===========================================================================                                    


            /* VP region monthly report start */
            $vp_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 0)
                    //->where('user_admin_backoffice_type', '=', NULL)
                    ->get()
                    ->toArray();
for($vp=0;$vp<count($vp_user_list);$vp++){
            $user_ids = Helper::vp_under_user_list();
            $region = Region::select('*')->where('delete_status', 0)->get()->toArray();        
            $excel_array = Helper::region_weekly_by_users($user_ids, $region,$month_first_date, $month_last_date);

            $file_path = 'monthly_region_' . $today_date . '_' . $vp_user_list[$vp]['id'];
            $file_path = public_path('excel/exports/' . $file_path);
            $mailsubject = 'Monthly region report - '.$month_first_date_text.' to '.$month_last_date_text;  
            Helper::send_excel_report($file_path, $excel_array, $vp_user_list[$vp]['user_email'], 'Monthly_Region',$mailsubject);
}
            /* VP region monthly report end */

//============================================================================            
            //============================================================================
            /* RSM Monthly region  MAil start */
            $rsm_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 1)
                    ->get()
                    ->toArray();
            for ($i = 0; $i < count($rsm_user_list); $i++) {
                $user_ids = Helper::rsm_under_user_list($rsm_user_list[$i]);
                //$region = Region::select('*')->where('delete_status', 0)->get()->toArray();        
                $region = Region::select('*')->where('id', $rsm_user_list[$i]['region_id'])->where('delete_status', 0)->get()->toArray();
                $excel_array = Helper::region_weekly_by_users($user_ids, $region,$month_first_date, $month_last_date);

                $file_path = 'monthly_region_' . $today_date . '_' . $rsm_user_list[$i]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Monthly region report - '.$month_first_date_text.' to '.$month_last_date_text;  
                Helper::send_excel_report($file_path, $excel_array, $rsm_user_list[$i]['user_email'], 'Monthly_Region',$mailsubject);
            }
            /* RSM Monthly region MAil end */
            //===========================================================================  

   /* VP state monthly report start */
            $vp_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 0)
                    //->where('user_admin_backoffice_type', '=', NULL)
                    ->get()
                    ->toArray();
for($vp=0;$vp<count($vp_user_list);$vp++){
            $user_ids = Helper::vp_under_user_list();
            $state = State::select('*')->get()->toArray();        
            $excel_array = Helper::state_weekly_by_users($user_ids, $state,$month_first_date, $month_last_date);

            $file_path = 'monthly_state_' . $today_date . '_' . $vp_user_list[$vp]['id'];
            $file_path = public_path('excel/exports/' . $file_path);
            $mailsubject = 'Monthly state report - '.$month_first_date_text.' to '.$month_last_date_text;  
            Helper::send_excel_report($file_path, $excel_array, $vp_user_list[$vp]['user_email'], 'Monthly_State',$mailsubject);
}
            /* VP state monthly report end */            
            
   /* RSM Monthly state  MAil start */
            $rsm_user_list = User::select('*')
                    ->where('disable_status', 0)
                    ->where('delete_status', 0)
                    ->where('user_type', 1)
                    ->get()
                    ->toArray();

            
            for ($i = 0; $i < count($rsm_user_list); $i++) {
                $user_ids = Helper::rsm_under_user_list($rsm_user_list[$i]);
                $region_user = User::select('*')->where('id',$rsm_user_list[$i]['id'])->get()->toArray();
                $state_list = RegionDetail::select('*','region_detail.state_id')->where('region_id',$rsm_user_list[$i]['region_id'])->get()->toArray();
                $state_array = array();
                foreach($state_list as $s){
                    $state_array[] = $s['state_id'];
                }
                $state = State::select('*')->wherein('id',$state_array)->get()->toArray();        
                //$region = Region::select('*')->where('delete_status', 0)->get()->toArray();        
                $excel_array = Helper::state_weekly_by_users($user_ids, $state,$month_first_date, $month_last_date);

                $file_path = 'monthly_state_' . $today_date . '_' . $rsm_user_list[$i]['id'];
                $file_path = public_path('excel/exports/' . $file_path);
                $mailsubject = 'Monthly state report - '.$month_first_date_text.' to '.$month_last_date_text;  
                Helper::send_excel_report($file_path, $excel_array, $rsm_user_list[$i]['user_email'], 'Monthly_State',$mailsubject);
            }
            /* RSM Monthly state MAil end */
            //===========================================================================              
            
            //echo $month_first_date . '<br/>';
            //echo $month_last_date . '<br/>';
        }
        
        echo 'done';
        /*Monthly process end*/
        
        
    }
    public function testmail(){
        $rnum = 7747;
        
/*$config = array(
               'driver' => 'smtp',
               'host' => 'sg2plcpnl0037.prod.sin2.secureserver.net',
               'port' => 465,
               //'from' => array('address' => $mail->from_address, 'name' => $mail->from_name),
               'encryption' => 'ssl',
               'username' => 'no-reply@dokania.co',
               'password' => 'CEDARindia04881'
           );
   Config::set('mail',$config);  */      
        
        
            Mail::send('pages.user.email_forgot', ['pass'=>$rnum], function ($m) {
                $m->from('no-reply@dokania.co', 'Durain');
                $m->to('vivek.mobio@gmail.com')->subject('Reset Password!');
            });
    }
    
    public function php_info(){
        phpinfo();
    }
    
    public function fix_table(){
        echo 'dd';
        DB::connection()->setFetchMode(PDO::FETCH_ASSOC);
         /*$gps_data_old = DB::connection('mysql')->select("select * from gps_data_old");
         for($i=0;$i<count($gps_data_old);$i++){
             $setid = $gps_data_old[$i]['user_id'];
             DB::connection('mysql')->select("update gps_data_new set user_id = $setid where id=".$gps_data_old[$i]['id']);
         }*/
        
        $gps_data_old = DB::connection('mysql')->select("select * from gps_data_new where gps_data_type = '1' and id > 644");
        for($i=0;$i<count($gps_data_old);$i++){
            $meeting_id = $gps_data_old[$i]['meeting_id'];
            $meeting_data_new = DB::connection('mysql')->select("select * from meeting_customer_new where id = $meeting_id");            
            
            $setid = $meeting_data_new[0]['user_id'];
            print_r($meeting_data_new);
            
            DB::connection('mysql')->select("update gps_data_new set user_id = $setid where id=".$gps_data_old[$i]['id']);            
        }
         echo '<pre>';
         echo 'done';
         echo '</pre>';
         exit;
    }

}
