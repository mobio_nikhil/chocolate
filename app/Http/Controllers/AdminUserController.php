<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use App\User;
use Input;
use Validator;
use App\Helpers\Helper;
use App\GpsData;
use App\Meeting;
use App\Region;
use App\Area;
use App\Customer;
use App\Brand;
use App\UserBrand;
use App\UserArea;
use Exception;
use Crypt;
use DB;
use PDO;
use Redirect;
use Config;
use Session;
use Mail;

class AdminUserController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('layouts/login');
    }

    public function login() {
        $input_all = Input::all();

        /* validation start */
        $rules = array(
            // 'user_code' => 'required|numeric', // just a normal required validation
            'user_email' => 'required|email', // just a normal required validation
            'password' => 'required',
        );

        $validator = Validator::make($input_all, $rules);
        // check if the validator failed -----------------------
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to(Config::get('constants.admin_path'))
                            ->withErrors($validator)
                            ->withInput(Input::except('password'));
        }

        /* validation end */

        // $auth = Auth::attempt(['id' => $input_all['user_code'], 'password' => $input_all['password']]);
        $auth = Auth::attempt(['user_email' => $input_all['user_email'], 'password' => $input_all['password']]);

        if ($auth) {

            $user_info = Auth::user();

            if (!($user_info->disable_status == 0 && $user_info->delete_status == 0)) {
                Session::flash('message', 'Login Fail');
                Session::flash('alert-class', 'alert-danger');
                return Redirect::to(Config::get('constants.admin_path'));
            }

            if (($user_info->user_type == 3)) {
                Session::flash('message', 'Login Fail');
                Session::flash('alert-class', 'alert-danger');
                return Redirect::to(Config::get('constants.admin_path'));
            }

            if ($user_info->user_type == 1) {
                if (!($user_info->region_id > 0)) {
                    Session::flash('message', 'Region is not assign');
                    Session::flash('alert-class', 'alert-danger');
                    return Redirect::to(Config::get('constants.admin_path'));
                }
            }

            /*if ($user_info->user_type == 2 || $user_info->user_type == 3) {
                if (!($user_info->region_id > 0 && $user_info->area_id > 0)) {
                    Session::flash('message', 'Region or area is not assign');
                    Session::flash('alert-class', 'alert-danger');
                    return Redirect::to(Config::get('constants.admin_path'));
                }
            }*/
            if($user_info->user_type == 2){
                if(!($user_info->region_id > 0)){                    
                    Session::flash('message', 'Region is not assign');
                    Session::flash('alert-class', 'alert-danger');
                    return Redirect::to(Config::get('constants.admin_path'));                    
                }
                
                $user_area = UserArea::select('*')->where('user_id',$user_info->id)->get()->toArray();
                if(!(count($user_area) > 0)){                    
                    Session::flash('message', 'Area is not assign');
                    Session::flash('alert-class', 'alert-danger');
                    return Redirect::to(Config::get('constants.admin_path'));                    
                }
                
            }

            if($user_info->user_type == 3){
                if(($user_info->assign_to_user == null or $user_info->assign_to_user == '' )){                    
                    Session::flash('message', 'Area manager is not assign');
                    Session::flash('alert-class', 'alert-danger');
                    return Redirect::to(Config::get('constants.admin_path'));                                        
                }
                
                $user_area = UserArea::select('*')->where('user_id',$user_info->id)->get()->toArray();
                if(!(count($user_area) > 0)){
                    //return Helper::response_api(false,'Area is not assign','');
                    Session::flash('message', 'Area is not assign');
                    Session::flash('alert-class', 'alert-danger');
                    return Redirect::to(Config::get('constants.admin_path'));                                                            
                }
                
            }            


            Session::set('session_user_info', Auth::user()->toArray());
            return Redirect::to(Config::get('constants.admin_path') . 'dashboard');
        } else {
            //Session::flash('message', 'Login Fail'); 
            Session::flash('message', 'Login Failed');
            Session::flash('alert-class', 'alert-danger');

            return Redirect::to(Config::get('constants.admin_path'));
        }
    }

    public function dashboard() {

        $DashBoard = Helper::GetUserDashboardData();
        $session_user_info = Session::get('session_user_info');
        $LocationData = Helper::getUserLocationData();
        $MapData = Helper::getUserMapData();
        $EmployeeStatus = Helper::getUnTrackUserData();
        $chart = Helper::get_my_region_month_meeting();

        // print_r($EmployeeStatus);
        // exit();
        $data = array();
        $data['dashboard'] = $DashBoard;
        $data['chart'] = $chart;
        $data['region_list'] = $LocationData['region'];
        $data['area_list'] = $LocationData['area'];
        $data['user_session'] = $session_user_info;
        $data['user_gps'] = $MapData;
        $data['employee_status'] = $EmployeeStatus;

        return view('pages/dashboard', $data);
    }

    /*
     * Dashboard get dropdown value based on user login.
     * 
     */

    public function mapFilter() {

        if (Input::isMethod('post')) {

            $input_all = Input::all();
            $userInfo = Session::get('session_user_info');

            /* validation start */
            $rules = array(
                "regionId" => 'integer',
                "area_name" => 'integer',
                "areaId" => 'integer',
            );
            $validator = Validator::make($input_all, $rules);
            $messages = array();
            // check if the validator failed -----------------------
            if ($validator->fails()) {
                $messages = $validator->messages();
                $jsonResponse = array(
                    "statusCode" => "0",
                    "message" => $messages,
                );
                return response($jsonResponse, 400)->header('Content-Type', "json");
            }

            $regionId = Input::get("regionId");
            $areaId = Input::get("areaId");

            $areaArray = array();
            $regionArray = array();
            $employeeArray = array();
            $responseArray = array();

            if ($regionId == "") {
                $regionData = Helper::getUserLocationData();

                if (count($regionData['area']) > 0) {
                    foreach ($regionData['area'] as $key => $values) {
                        $areaArray[$values['id']] = $values['area_name'];
                    }
                }
                $jsonResponse = array(
                    "statusCode" => "1",
                    "message" => $messages,
                    "data" => array("area" => $areaArray, "employee" => $employeeArray),
                );
                return response($jsonResponse, 200)->header('Content-Type', "json");
            }

            if ($regionId != "" && (isset($areaId) && $areaId == "")) {

                $areaList = Area::select('*')->where('delete_status', 0)->where('disable_status', 0)->where('region_id', $regionId)->get()->toArray();
                if (count($areaList) > 0) {
                    foreach ($areaList as $key => $values) {
                        $areaArray[$values['id']] = $values['area_name'];
                    }
                }
                $jsonResponse = array(
                    "statusCode" => "1",
                    "message" => $messages,
                    "data" => array("area" => $areaArray, "employee" => $employeeArray),
                );
                return response($jsonResponse, 200)->header('Content-Type', "json");
            }

            if ($regionId != "" && $areaId != "") {

                $areaList = Area::select('*')->where('delete_status', 0)->where('disable_status', 0)->where('region_id', $regionId)->get()->toArray();
                if (count($areaList) > 0) {
                    foreach ($areaList as $key => $values) {
                        $areaArray[$values['id']] = $values['area_name'];
                    }
                }
                $userData = User::select('user.user_name', 'user.id', 'region_name', 'area_name')
                                ->leftJoin('region', 'region.id', '=', 'user.region_id')
                                ->leftJoin('area', 'area.id', '=', 'user.area_id')
                                ->where('user.region_id', $regionId)
                                ->where('area.region_id', $regionId)
                                ->where('area.id', $areaId)
                                ->where('user.user_type', '=', 3)
                                ->where('user.delete_status', 0)
                                ->where('user.disable_status', 0)
                                ->orderby('user_name')
                                ->get()->toArray();

                if (count($userData) > 0) {
                    foreach ($userData as $key => $values) {
                        $employeeArray[$values['id']] = $values['user_name'];
                    }
                }
                $jsonResponse = array(
                    "statusCode" => "1",
                    "message" => $messages,
                    "data" => array("area" => $areaArray, "employee" => $employeeArray),
                );
                return response($jsonResponse, 200)->header('Content-Type', "json");
            }
        }
    }

    public function MapUdpates() {

        if (Input::isMethod('post')) {

            $input_all = Input::all();
            $userInfo = Session::get('session_user_info');

            /* validation start */
            $rules = array(
                "regionId" => 'integer',
                "area_name" => 'integer',
                "areaId" => 'integer',
            );
            $validator = Validator::make($input_all, $rules);
            $messages = array();
            // check if the validator failed -----------------------
            if ($validator->fails()) {
                $messages = $validator->messages();
                $jsonResponse = array(
                    "statusCode" => "0",
                    "message" => $messages,
                );
                return response($jsonResponse, 400)->header('Content-Type', "json");
            }

            $regionId = Input::get("regionId");
            $areaId = Input::get("areaId");
            $employeeId = Input::get("employeeId");
            if ($regionId == "" && $areaId == "" && $employeeId == "") {
                $mapData = Helper::getUserMapData(); // Get all emaployee map location
                $jsonResponse = array(
                    "statusCode" => "1",
                    "message" => "Success",
                    "data" => array("mapdata" => $mapData, "count" => count($mapData)),
                );
                return response($jsonResponse, 200)->header('Content-Type', "json");
            } elseif ($regionId != "" && $areaId == "" && $employeeId == "") {
                $userData = User::select('user.*', 'region_name', 'area_name')
                                ->leftJoin('region', 'region.id', '=', 'user.region_id')
                                ->leftJoin('area', 'area.id', '=', 'user.area_id')
                                ->where(function ($query) use($userInfo) {
                                    $query->where('user.user_type', '=', 2)
                                    ->orWhere('user.user_type', '=', 3);
                                })
                                ->where('user.region_id', $regionId)
                                ->where('area.region_id', $regionId)
                                ->where('user.delete_status', 0)
                                ->where('user.disable_status', 0)
                                ->orderby('user_name')
                                ->get()->toArray();

                $user_ids = array();
                for ($i = 0; $i < count($userData); $i++) {
                    $user_ids[] = $userData[$i]['id'];
                }

                $mapData = Helper::FilterMapData($user_ids); // Get Particular region data
                $jsonResponse = array(
                    "statusCode" => "1",
                    "message" => "Success",
                    "data" => array("mapdata" => $mapData, "count" => count($mapData)),
                );
                return response($jsonResponse, 200)->header('Content-Type', "json");
            } elseif ($regionId != "" && $areaId != "" && $employeeId == "") {
                $userData = User::select('user.*', 'region_name', 'area_name')
                                ->leftJoin('region', 'region.id', '=', 'user.region_id')
                                ->leftJoin('area', 'area.id', '=', 'user.area_id')
                                ->where(function ($query) use($userInfo) {
                                    $query->where('user.user_type', '=', 2)
                                    ->orWhere('user.user_type', '=', 3);
                                })
                                ->where('user.region_id', $regionId)
                                ->where('area.region_id', $regionId)
                                ->where('user.area_id', $areaId)
                                ->where('user.delete_status', 0)
                                ->where('user.disable_status', 0)
                                ->orderby('user_name')
                                ->get()->toArray();

                $user_ids = array();
                for ($i = 0; $i < count($userData); $i++) {
                    $user_ids[] = $userData[$i]['id'];
                }

                $mapData = Helper::FilterMapData($user_ids); // Get Particular region data
                $jsonResponse = array(
                    "statusCode" => "1",
                    "message" => "Success",
                    "data" => array("mapdata" => $mapData, "count" => count($mapData)),
                );
                return response($jsonResponse, 200)->header('Content-Type', "json");
            } elseif ($regionId != "" && $areaId != "" && $employeeId != "") {

                $userData = User::select('user.*', 'region_name', 'area_name')
                                ->leftJoin('region', 'region.id', '=', 'user.region_id')
                                ->leftJoin('area', 'area.id', '=', 'user.area_id')
                                ->where(function ($query) use($userInfo) {
                                    $query->where('user.user_type', '=', 2)
                                    ->orWhere('user.user_type', '=', 3);
                                })
                                ->where('user.region_id', $regionId)
                                ->where('area.region_id', $regionId)
                                ->where('user.area_id', $areaId)
                                ->where('user.id', $employeeId)
                                ->where('user.delete_status', 0)
                                ->where('user.disable_status', 0)
                                ->orderby('user_name')
                                ->get()->toArray();

                $user_ids = array();
                for ($i = 0; $i < count($userData); $i++) {
                    $user_ids[] = $userData[$i]['id'];
                }

                $mapData = Helper::FilterMapData($user_ids); // Get Particular region data

                $jsonResponse = array(
                    "statusCode" => "1",
                    "message" => "Success",
                    "data" => array("mapdata" => $mapData, "count" => count($mapData)),
                );
                return response($jsonResponse, 200)->header('Content-Type', "json");
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $session_user_info = Session::get('session_user_info');
        $region = Region::select('*')
                        ->where('delete_status', 0)
                        ->where('disable_status', 0)
                        ->orderby('region_name')
                        ->get()->toArray();
        
        $brand = Brand::select('*')->get()->toArray();

        $data = array();
        $data['region_list'] = $region;
        $data['brand_list'] = $brand;
        $data['session_user_info'] = $session_user_info;
        return view('pages/user/create', $data);
    }

 public function user_list() {

        $session_user_info = Session::get('session_user_info');

        $user_ids = array();
        if($session_user_info['user_type'] == 0){
            $user_ids = Helper::all_vp_under_user_list();            
        }
        if($session_user_info['user_type'] == 1){            
            $user_ids = Helper::all_rsm_under_user_list($session_user_info) ;
        }
        if($session_user_info['user_type'] == 2){            
            $user_ids = Helper::all_asm_under_user_list($session_user_info) ;
        }
        
                    $user = User::select('user.*','region_name','area_name')
                            ->leftJoin('region', 'region.id', '=', 'user.region_id')
                            ->leftJoin('area', 'area.id', '=', 'user.area_id')
                            ->wherein('user.id',$user_ids)
                            ->get()
                            ->toArray(); 
                    
                    for($i=0;$i<count($user);$i++){
                        $userbrands = UserBrand::select('brand_id')->where('user_id',$user[$i]['id'])->get()->toArray();
                        
                            //$brand_ids = Brand::select('*')->wherein('id',  explode(',', $user[$i]['brand_id']))->get()->toArray();
                            $brand_ids = Brand::select('*')->wherein('id',  $userbrands)->get()->toArray();
                     
                                $br_text = array();
                                foreach($brand_ids as $br){
                                    $br_text[] = $br['brand_name'];
                                }
                                $user[$i]['brand_name'] = implode(', ', $br_text);

                    }                    


        for ($i = 0; $i < count($user); $i++) {
            if ($user[$i]['user_type'] == 1) {
                /*$vp_user = User::select('user.*', 'region_name', 'area_name', 'assignment_user.user_name as assignment_username')
                                ->leftJoin('region', 'region.id', '=', 'user.region_id')
                                ->leftJoin('area', 'area.id', '=', 'user.area_id')
                                ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                                ->where('user.delete_status', 0)

                                ->where('user.user_type', 0)
                                ->orderby('user_name')
                                ->get()->toArray();
                if (count($vp_user) > 0)
                    $user[$i]['assign_to_user'] = $vp_user[0]['user_name'];
                else
                    $user[$i]['assign_to_user'] = '';*/
                $vp_user = User::select('user.*','region_name','area_name','assignment_user.user_name as assignment_username')
                        ->leftJoin('region', 'region.id', '=', 'user.region_id')
                        ->leftJoin('area', 'area.id', '=', 'user.area_id')
                        ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                        ->where('user.delete_status',0)
                        ->where('user.disable_status',0)
                        ->where('user.user_type',0)                        
                        ->where('user.user_admin_backoffice_type',NULL)                        
                        ->orderby('user_name','region.region_name','area.area_name')
                        ->get()->toArray();   
                
                
                if(count($vp_user) > 0)
                $user[$i]['assign_to_user'] = $vp_user[0]['user_name'];
                else
                $user[$i]['assign_to_user'] = '';                
                
                $user_region = array();
                $u_region = array();
                DB::setFetchMode(PDO::FETCH_ASSOC);
                $user_region = $state_list = DB::connection('mysql')->select("SELECT * FROM state where id in (select state_id from region_detail WHERE region_id in (select id from region where region.delete_status = '0' and region.disable_status = '0' and id in (select region_id from user where id = ".$user[$i]['id'].")))");
                DB::setFetchMode(PDO::FETCH_CLASS);
                
                if(count($user_region) > 0){
                    foreach($user_region as $ar){
                        $u_region[] = $ar['state_name'];                        
                    }
                }
                $user[$i]['area_name'] = implode(', ', $u_region);                
            }

            if ($user[$i]['user_type'] == 2) {
                $rsm_user = User::select('user.*', 'region_name', 'user.user_name as assignment_username')
                                ->leftJoin('region', 'region.id', '=', 'user.region_id')
                                //->leftJoin('area', 'area.id', '=', 'user.area_id')
                                //->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                                ->where('user.delete_status', 0)

                                ->where('user.user_type', 1)
                                ->where('user.region_id', $user[$i]['region_id'])
                                ->orderby('user_name')
                                ->get()->toArray();

                if (count($rsm_user) > 0)
                    $user[$i]['assign_to_user'] = $rsm_user[0]['user_name'];
                else
                    $user[$i]['assign_to_user'] = '';
                
                $user_area = UserArea::select('area.*')
                        ->leftJoin('area', 'area.id', '=', 'user_area.area_id')                        
                        ->where('user_area.user_id',$user[$i]['id'])->get()->toArray();
                $area_name = array();
                if(count($user_area) > 0){
                    foreach($user_area as $ar){
                        $area_name[] = $ar['area_name'];                        
                    }
                }
                $user[$i]['area_name'] = implode(', ', $area_name);
                
            }

            if ($user[$i]['user_type'] == 3) {
                /*$asm_user = User::select('user.*', 'region_name', 'area_name', 'assignment_user.user_name as assignment_username')
                                ->leftJoin('region', 'region.id', '=', 'user.region_id')
                                ->leftJoin('area', 'area.id', '=', 'user.area_id')
                                ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                                ->where('user.delete_status', 0)

                                ->where('user.user_type', 2)
                                ->where('user.region_id', $user[$i]['region_id'])
                                ->where('user.area_id', $user[$i]['area_id'])
                                ->orderby('user_name')
                                ->get()->toArray();

                if (count($asm_user) > 0)
                    $user[$i]['assign_to_user'] = $asm_user[0]['user_name'];
                else
                    $user[$i]['assign_to_user'] = '';*/
                    if($user[$i]['assign_to_user'] != null && $user[$i]['assign_to_user'] != ''){
                        $asm_user = User::select('user.*', 'region_name', 'area_name', 'assignment_user.user_name as assignment_username')
                                    ->leftJoin('region', 'region.id', '=', 'user.region_id')
                                    ->leftJoin('area', 'area.id', '=', 'user.area_id')
                                    ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                                    ->where('user.delete_status', 0)

                                    ->where('user.user_type', 2)
                                    ->where('user.id', $user[$i]['assign_to_user'])
                                    //->where('user.region_id', $user[0]['region_id'])
                                    //->where('user.area_id', $user[0]['area_id'])
                                    ->orderby('user_name')
                                    ->get()->toArray();
                        
                        if (count($asm_user) > 0)
                            $user[$i]['assign_to_user'] = $asm_user[0]['user_name'];
                        else
                            $user[$i]['assign_to_user'] = '';                        
                        
                    }                
                
                $user_area = UserArea::select('area.*')
                        ->leftJoin('area', 'area.id', '=', 'user_area.area_id')                        
                        ->where('user_area.user_id',$user[$i]['id'])->get()->toArray();
                $area_name = array();
                if(count($user_area) > 0){
                    foreach($user_area as $ar){
                        $area_name[] = $ar['area_name'];                        
                    }
                }
                $user[$i]['area_name'] = implode(', ', $area_name);                
                
                
            }
        }

        $region = Region::select('*')
                        ->where('delete_status', 0)
                        ->where('disable_status', 0)
                        ->orderby('region_name')
                        ->get()->toArray();

        $data = array();
        $data['user_list'] = $user;
        $data['session_user_info'] = Session::get('session_user_info');
        //echo '<pre>';
        //print_r($data);exit;
        return view('pages.user.user_list', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $input_all = Input::all();
        /* validation start */
        $deletevalue = 1;

        $rules = array();
        $rules['user_name'] = 'required|alpha_spaces';
        $rules['user_email'] = 'required|email|unique:user,user_email,' . $deletevalue . ',delete_status';
        $rules['user_phone'] = 'required|numeric';
        
        $rules['user_type'] = 'required';
        //$rules['region_id'] = 'required';
        if(!($input_all['user_type'] == 4 || $input_all['user_type'] == 5)){
            $rules['brand'] = 'required';            
        }

        /*if ($input_all['user_type'] == 4) {   
            $vpuser = User::select('*')
                    ->where('user_type',0)
                    ->where('user_admin_backoffice_type',NULL)
                    ->where('disable_status',0)
                    ->where('delete_status',0)
                    ->get()
                    ->toArray();
            if(count($vpuser) > 0){
                Session::flash('message', 'The user type has already been taken.');
                Session::flash('alert-class', 'alert-danger');
                return Redirect::to(Config::get('constants.admin_path').'/user/create')->withInput();
            }
        }*/
        if ($input_all['user_type'] == 1) {
            $user_type = 1;
            $rules['region_id'] = 'required|unique:user,region_id,' . $deletevalue . ',delete_status,user_type,' . $user_type;
            $messages = array('region_id.unique' => 'The region has already been taken.');
            $validator = Validator::make($input_all, $rules, $messages);
        } else {
            $validator = Validator::make($input_all, $rules);
        }

        // check if the validator failed -----------------------
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to(Config::get('constants.admin_path') . 'user/create')
                            ->withErrors($validator)
                            ->withInput();
        }
        $user_info = Session::get('session_user_info');
        $user_insert_array = array();
        $user_insert_array['user_id'] = $user_info['id'];
        $user_insert_array['user_name'] = $input_all['user_name'];
        $user_insert_array['user_email'] = $input_all['user_email'];
        $user_insert_array['user_phone'] = $input_all['user_phone'];
        $user_insert_array['region_id'] = $input_all['region_id'];
        $user_insert_array['user_type'] = $input_all['user_type'];
        $password = rand(1111, 9999);
        //$input_all['user_password'] = Hash::make($password);
        $user_insert_array['user_password'] = Hash::make($password);
        //$user_insert_array['user_password'] = Hash::make(1234);
        $user_insert_array['created_on'] = date('Y-m-d H:i:s');
        
        if($input_all['user_type'] == 4 || $input_all['user_type'] == 5 ){
            $user_insert_array['user_type'] = 0;
            if($input_all['user_type'] == 5)
            $user_insert_array['user_admin_backoffice_type'] = 1;
        }

        $user = User::create($user_insert_array);

        if(!($input_all['user_type'] == 4 || $input_all['user_type'] == 5) ){
            for($i=0;$i<count($input_all['brand']);$i++){
                    $brand_data = array();                
                    $brand_data['user_id'] = $user->id;
                    $brand_data['brand_id'] = $input_all['brand'][$i];
                    $brand_data['created_on'] = date('Y-m-d H:i:s');
                    UserBrand::create($brand_data);            
            }
        }
        $user_data = array();
        $user_data['id'] = $user->id;
        $user_data['password'] = $password;
        $user_data['user_name'] = $user->user_name;
        $user_data['email'] = $user->user_email;

         Mail::send(['html' => 'pages.user.login_email'], ['text' => $user_data], function ($message) use($user_data) {
          //print_r($user_data);exit;
          $message->subject('Welcome to chocolate');
          $message->from(Config::get('constants.from_mail'), Config::get('constants.from_mail_text'));
          $message->to($user_data['email']);
          }); 

        Session::flash('message', 'User created');
        //Session::flash('alert-class', 'alert-danger');                         
        return Redirect::to(Config::get('constants.admin_path') . 'user/user_list');
    }

    public function assignment($id) {
        //echo $id;
        $user = User::select('user.*', 'region_name', 'area_name', 'assignment_user.user_name as assignment_username')
                        ->leftJoin('region', 'region.id', '=', 'user.region_id')
                        ->leftJoin('area', 'area.id', '=', 'user.area_id')
                        ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                        ->where('user.delete_status', 0)
                        ->where('user.disable_status', 0)
                        ->where('user.id', $id)
                        ->orderby('user_name')
                        ->get()->toArray();

        $rsm_user = array();
        $asm_user = array();
        $selected_user_area = array();
        $asm_user_list = array();
        if ($user[0]['user_type'] == 2) {
            $rsm_user = User::select('user.*', 'region_name', 'area_name', 'assignment_user.user_name as assignment_username')
                            ->leftJoin('region', 'region.id', '=', 'user.region_id')
                            ->leftJoin('area', 'area.id', '=', 'user.area_id')
                            ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                            ->where('user.delete_status', 0)
                            ->where('user.disable_status', 0)
                            ->where('user.user_type', 1)
                            ->where('user.region_id', $user[0]['region_id'])
                            ->orderby('user_name')
                            ->get()->toArray();
            $selected_user_area = UserArea::select('*')->where('user_id',$id)->get()->toArray();
        }

        if ($user[0]['user_type'] == 3) {
            $rsm_user = User::select('user.*', 'region_name', 'area_name', 'assignment_user.user_name as assignment_username')
                            ->leftJoin('region', 'region.id', '=', 'user.region_id')
                            ->leftJoin('area', 'area.id', '=', 'user.area_id')
                            ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                            ->where('user.delete_status', 0)
                            ->where('user.disable_status', 0)
                            ->where('user.user_type', 1)
                            ->where('user.region_id', $user[0]['region_id'])
                            ->orderby('user_name')
                            ->get()->toArray();

            /*$asm_user = User::select('user.*', 'region_name', 'area_name', 'assignment_user.user_name as assignment_username')
                            ->leftJoin('region', 'region.id', '=', 'user.region_id')
                            ->leftJoin('area', 'area.id', '=', 'user.area_id')
                            ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                            ->where('user.delete_status', 0)
                            ->where('user.disable_status', 0)
                            ->where('user.user_type', 2)
                            ->where('user.region_id', $user[0]['region_id'])
                            ->where('user.area_id', $user[0]['area_id'])
                            ->orderby('user_name')
                            ->get()->toArray();*/
            if($user[0]['assign_to_user'] != null && $user[0]['assign_to_user'] != ''){
                $asm_user = User::select('user.*', 'region_name', 'area_name', 'assignment_user.user_name as assignment_username')
                            ->leftJoin('region', 'region.id', '=', 'user.region_id')
                            ->leftJoin('area', 'area.id', '=', 'user.area_id')
                            ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                            ->where('user.delete_status', 0)
                            ->where('user.disable_status', 0)
                            ->where('user.user_type', 2)
                            ->where('user.id', $user[0]['assign_to_user'])
                            //->where('user.region_id', $user[0]['region_id'])
                            //->where('user.area_id', $user[0]['area_id'])
                            ->orderby('user_name')
                            ->get()->toArray();
            }

            $asm_user_list = User::select('user.*', 'region_name', 'area_name', 'assignment_user.user_name as assignment_username')
                            ->leftJoin('region', 'region.id', '=', 'user.region_id')
                            ->leftJoin('area', 'area.id', '=', 'user.area_id')
                            ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                            ->where('user.delete_status', 0)
                            ->where('user.disable_status', 0)
                            ->where('user.user_type', 2)
                            ->where('user.region_id', $user[0]['region_id'])
                            //->where('user.area_id',$user[0]['area_id'])
                            ->orderby('user_name')
                            ->get()->toArray();
            $selected_user_area = UserArea::select('*')->where('user_id',$id)->get()->toArray();
        }
        $area = array();
        /*if ($user[0]['user_type'] == 3) {

            $area = User::select('user.*', 'area.id', 'area.area_name')
                            ->leftJoin('area', 'area.id', '=', 'user.area_id')
                            ->where('user.delete_status', 0)
                            ->where('user.disable_status', 0)
                            ->where('user.user_type', 2)
                            ->where('user.region_id', $user[0]['region_id'])
                            ->get()->toArray();
        }*/

        if ($user[0]['user_type'] == 2 || $user[0]['user_type'] == 3) {


            $region = User::select('*')
                            ->where('user.delete_status', 0)
                            ->where('user.disable_status', 0)
                            ->where('user.user_type', 1)
                            ->where('user.region_id', $user[0]['region_id'])
                            ->get()->toArray();
            if (count($region) > 0) {
                $area = Area::select('*')
                                ->where('region_id', $region[0]['region_id'])
                                ->get()->toArray();
            }
        }

        $data = array();
        $data['user_detail'] = $user;
        $data['session_user_info'] = Session::get('session_user_info');
        $data['rsm_user'] = $rsm_user;
        $data['asm_user'] = $asm_user;
        $data['asm_user_list'] = $asm_user_list;
        $data['area_list'] = $area;        
        $data['selected_user_area'] = $selected_user_area;
        //$selected_user_area
//        echo '<pre>';
//        print_r($data);exit;
        return view('pages.user.assignment', $data);
    }

    public function assignment_store($id) {
        //echo $id;
        $input_all = Input::all();
        $user = User::select('user.*', 'region_name', 'area_name', 'assignment_user.user_name as assignment_username')
                        ->leftJoin('region', 'region.id', '=', 'user.region_id')
                        ->leftJoin('area', 'area.id', '=', 'user.area_id')
                        ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                        ->where('user.delete_status', 0)
                        ->where('user.disable_status', 0)
                        ->where('user.id', $id)
                        ->orderby('user_name')
                        ->get()->toArray();


        /* validation start */
        $deletevalue = 1;

        $rules = array();

        if ($user[0]['user_type'] == 3) {
            
           $rules['asm_user'] = 'required'; 
            
        }

        /*if ($user[0]['user_type'] == 2) {
            $rules['area_id'] = 'required|unique:user,area_id,' . $deletevalue . ',delete_status';
            $messages = array(
                'area_id.unique' => 'The area has already been taken.',
                'area_id.required' => 'The area field is required.'
            );
            $validator = Validator::make($input_all, $rules, $messages);
        } else {
            $rules['area_id'] = 'required';
            $validator = Validator::make($input_all, $rules);
        }*/
        $rules['area_id'] = 'required';
        $validator = Validator::make($input_all, $rules);        



        // check if the validator failed -----------------------
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to(Config::get('constants.admin_path') . 'user/assignment/' . $id)
                            ->withErrors($validator)
                            ->withInput();
        }
        $user_info = User::find($id);
        
        if($user_info){
            if ($user[0]['user_type'] == 3) {
                $user_info->assign_to_user = $input_all['asm_user'];
                $user_info->save();
            }
            
            UserArea::where('user_id',$user_info->id)->delete();            
            for($i=0;$i<count($input_all['area_id']);$i++){
                $area_array = array();
                $area_array['user_id'] = $user_info->id;
                $area_array['area_id'] = $input_all['area_id'][$i];
                $area_array['created_on'] = date('Y-m-d H:i:s');
                UserArea::create($area_array);
            }                
        }        
        
        //$input_array = array();
        //$user_info->area_id = $input_all['area_id'];
        if ($user_info) {
            Session::flash('message', 'User assign success');
        } else {
            Session::flash('message', 'User assign fail');
            Session::flash('alert-class', 'alert-danger');
        }
        return Redirect::to(Config::get('constants.admin_path') . 'user/user_list');


        /* if($user[0]['user_type'] == 3){

          $deletevalue = 1;

          $rules = array();
          $rules['asm_user_id'] = 'required';

          $messages = array('asm_user_id.required' => 'The asm field is required.');
          $validator = Validator::make($input_all, $rules,$messages );


          //$validator = Validator::make($input_all, $rules );

          // check if the validator failed -----------------------
          if ($validator->fails()) {
          $messages = $validator->messages();
          return Redirect::to(Config::get('constants.admin_path').'user/assignment/'.$id)
          ->withErrors($validator)
          ->withInput();
          }

          $assm_user_area_id = User::select('*')->where('id',$input_all['asm_user_id'])->where('area_id','>',0)->get()->toArray();
          if(count($assm_user_area_id ) >0){
          $user_info = User::find($id);
          $input_array = array();
          $user_info->area_id = $assm_user_area_id[0]['area_id'];
          if($user_info->save()){
          Session::flash('message', 'User assign success');
          }else{
          Session::flash('message', 'User assign fail');
          Session::flash('alert-class', 'alert-danger');
          }
          }else{
          Session::flash('message', 'User assign fail');
          Session::flash('alert-class', 'alert-danger');
          }

          return Redirect::to(Config::get('constants.admin_path').'user/user_list');
          } */
    }

    public function logout() {
        Session::flush();
        Auth::logout();
        Session::flash('message', 'Logout Successfully');
        //Session::flash('alert-class', 'alert-danger');             
        return Redirect::to(Config::get('constants.admin_path'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request) {
        $reqData = User::findOrFail($id);
        $region_list = Region::select("*")->get()->toArray();
        $brand = Brand::select('*')->get()->toArray();
        $selected_user_brand = UserBrand::select('*')->where('user_id',$id)->get()->toArray();       
        
        return view("pages.user.edit")
                        ->with('reqData', $reqData)
                        ->with('brand_list', $brand)
                        ->with('selected_user_brand', $selected_user_brand)                
                        ->with('region_list', $region_list);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $input_all = Input::all();
        /* validation start */
        $deletevalue = 0;

        $rules = array();
        $rules['user_name'] = 'required|alpha_spaces';
        $rules['brand'] = 'required';
        $rules['user_email'] = 'required|email|unique:user,user_email,' . $deletevalue . ',delete_status,id,' . $id;
        //$rules['region_id'] = 'required';
        // if($input_all['user_type'] == 1){
        //     $user_type = 1;
        //    $rules['region_id'] = 'required|unique:
        //    user,region_id,'.$deletevalue.',delete_status,user_type,'.$user_type;
        //         $messages = array('region_id.unique' => 'The region has already been taken.');
        //         $validator = Validator::make($input_all, $rules,$messages );
        // }else{
        $validator = Validator::make($input_all, $rules);
        // }
        // check if the validator failed -----------------------
        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to(Config::get('constants.admin_path') . 'user/edit/' . $id)
                            ->withErrors($validator)
                            ->withInput();
        }


        $user_info = Session::get('session_user_info');
        $input_all['user_id'] = $user_info['id'];

        $user = User::findOrFail($id);
        $user->user_name = $request['user_name'];
        $user->user_email = $request['user_email'];
        $user->user_phone = $request['user_phone'];
        $user->user_type = $request['user_type'];
        $user->region_id = $request['region_id'];
        $user->save();

        UserBrand::where('user_id',$id)->delete();
        for($i=0;$i<count($input_all['brand']);$i++){
                $brand_data = array();                
                $brand_data['user_id'] = $id;
                $brand_data['brand_id'] = $input_all['brand'][$i];
                $brand_data['created_on'] = date('Y-m-d H:i:s');
                UserBrand::create($brand_data);            
        }
        
        
        
        Session::flash('message', 'User Updated');
        //Session::flash('alert-class', 'alert-danger');                         
        // print_r($user);
        // exit();?
        if ($user->user_type != 1 && $user->user_type != 2) {
            return Redirect::to(Config::get('constants.admin_path') . 'user/assignment/' . $id);
        } else {
            return Redirect::to(Config::get('constants.admin_path') . 'user/user_list');
        }
    }

    public function changeStatus($status) {

        $ar = Input::all();
        $id = $ar['id'];
         $user_info = Session::get('session_user_info');
        if ($status == "enable") {
            $disable_status = 0;
            
            User::where('id', $id)->update(["disable_status" => $disable_status, 'active_by' => $user_info['id'],'disable_reason'=>NULL ]);
            Session::flash('message', 'User has been Activated');
        } elseif ($status == "disable") {
            $disable_status = 1;            
            User::where('id', $id)->update(["disable_status" => $disable_status,'disable_reason'=>$ar['disable_reason'],'disable_by'=>$user_info['id']]);
            Session::flash('message', 'User has been Dectivated');
        }
        return redirect(Config::get('constants.admin_path').'user/user_list');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        $U_customerCount = Customer::where("information_enter_by",$id)->count();
        $U_meetingCount     = Meeting::where("user_id",$id)->get()->count();
         $input_array = Input::all();
        // print_r($U_customerCount);
        // print_r($U_meetingCount);
        // exit;
        if($U_customerCount != 0 || $U_meetingCount  != 0 )
        {
           return Redirect::back()->withErrors(["User cannot be deleted. It is associated with records."]);
        }
        $user_info = Session::get('session_user_info');
        User::where('id', $id)->update(["disable_status" => 1,"delete_status" => 1,"delete_by" => $user_info['id'],"delete_reason" => $input_array['delete_reason']]);
        Session::flash('message', 'User has been deleted');
        return redirect(Config::get('constants.admin_path').'user/user_list');
    }


    public function forgotIndex()
    {
        if( !Auth::check() ){
            
            return view("pages.user.forgot");
        }   
        
        return redirect("/dashboard");
    }

    public function forgotRequest(Request $request)
    {
        $ar     = $request->all();
        $rules  = array( 'user_email' => 'required|email' );

        $validator = Validator::make($ar, $rules);
        
        if ($validator->fails()) {
                return Redirect::back()
                            ->withErrors($validator);
        }


        $t  =   User::where('user_email',$ar['user_email'])->count();
        if( $t != 0 )
        {

            $rnum = rand(100000,999999);
            $encrypted = Crypt::encrypt($ar['user_email']."|".$rnum."|".time());


            // $lnk    = "http://durain.local/user/reset/".$encrypted;

            $pass = Hash::make($rnum);
            User::where('user_email',$ar['user_email'])->update(["user_password"=>$pass]);  

            // mail the security key
            Mail::send('pages.user.email_forgot', ['pass'=>$rnum], function ($m) use ($ar) {
                    //$m->from('helpdesk@durain.com', 'Durain');
                    $m->from(Config::get('constants.from_mail'), Config::get('constants.from_mail_text'));                  
                    $m->to( $ar['user_email'] )->subject('Reset Password!');
                });
         // return view('pages.user.email_forgot')
            Session::flash("message","Check your email. ");
            return Redirect::to(Config::get('constants.admin_path'));

        }
        else
        {
            return Redirect::back()
                        ->withErrors(["Sorry, You're not registered with us.","Check Email again."])
                        ->withInput( $ar );
        }

    }




    public function deactiveList()
    {
     
        $session_user_info = Session::get('session_user_info');
        if ($session_user_info['user_type'] == 1) {
            $user = User::select('user.*', 'region_name', 'area_name')
                            ->leftJoin('region', 'region.id', '=', 'user.region_id')
                            ->leftJoin('area', 'area.id', '=', 'user.area_id')
                            //->leftJoin('user as rsm_assignment_user', 'rsm_assignment_user.id', '=', 'user.assign_to_user')                                
                            /* ->where(function ($query) {
                              $query->where('user.user_type', '=', 2)
                              ->orWhere('user.user_type', '=', 3);
                              }) */
                            //->where('user.region_id',$session_user_info['region_id'])
                            ->where(function ($query) use($session_user_info) {
                                $query->where('user.user_type', '=', 2)
                                ->orWhere('user.user_type', '=', 3)
                                ->orWhere('user.user_type', '=', 1);
                            })
                            ->where('user.region_id', $session_user_info['region_id'])
                            ->where('user.delete_status', 0)
                            ->where('user.disable_status', 1)
                            ->orderby('user_name')
                            ->get()->toArray();
        }

        if ($session_user_info['user_type'] == 2) {
            $user = User::select('user.*', 'region_name', 'area_name')
                            ->leftJoin('region', 'region.id', '=', 'user.region_id')
                            ->leftJoin('area', 'area.id', '=', 'user.area_id')
                            //->leftJoin('user as rsm_assignment_user', 'rsm_assignment_user.id', '=', 'user.assign_to_user')                                
                            /* ->where(function ($query) {
                              $query->where('user.user_type', '=', 3);
                              //->orWhere('user.user_type', '=', 3);
                              }) */
                            //->where('user.region_id',$session_user_info['region_id'])
                            //->where('area.region_id',$session_user_info['region_id'])
                            ->where(function ($query) use($session_user_info) {
                                $query->where('user.user_type', '=', 2)
                                ->orWhere('user.user_type', '=', 3);
                            })
                            ->where('user.region_id', $session_user_info['region_id'])
                            ->where('area.region_id', $session_user_info['region_id'])
                            ->where('area.id', $session_user_info['area_id'])
                            ->where('user.delete_status', 0)
                            ->where('user.disable_status', 1)
                            ->orderby('user_name')
                            ->get()->toArray();
        }

        if ($session_user_info['user_type'] == 0) {
            $user = User::select('user.*', 'region_name', 'area_name')
                            ->leftJoin('region', 'region.id', '=', 'user.region_id')
                            ->leftJoin('area', 'area.id', '=', 'user.area_id')
                            //->leftJoin('user as rsm_assignment_user', 'rsm_assignment_user.id', '=', 'user.assign_to_user')                
                            ->where('user.delete_status', 0)
                            ->where('user.disable_status', 1)
                            ->orderby('user_name')
                            ->get()->toArray();
        }


        for ($i = 0; $i < count($user); $i++) {
            if ($user[$i]['user_type'] == 1) {
                $vp_user = User::select('user.*', 'region_name', 'area_name', 'assignment_user.user_name as assignment_username')
                                ->leftJoin('region', 'region.id', '=', 'user.region_id')
                                ->leftJoin('area', 'area.id', '=', 'user.area_id')
                                ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                                ->where('user.delete_status', 0)
                                ->where('user.disable_status', 1)
                                ->where('user.user_type', 0)
                                ->orderby('user_name')
                                ->get()->toArray();
                if (count($vp_user) > 0)
                    $user[$i]['assign_to_user'] = $vp_user[0]['user_name'];
                else
                    $user[$i]['assign_to_user'] = '';
                
                $user_region = array();
                $u_region = array();
                DB::setFetchMode(PDO::FETCH_ASSOC);
                $user_region = $state_list = DB::connection('mysql')->select("SELECT * FROM state where id in (select state_id from region_detail WHERE region_id in (select id from region where region.delete_status = '0' and region.disable_status = '0' and id in (select region_id from user where id = ".$user[$i]['id'].")))");
                DB::setFetchMode(PDO::FETCH_CLASS);
                
                if(count($user_region) > 0){
                    foreach($user_region as $ar){
                        $u_region[] = $ar['state_name'];                        
                    }
                }
                $user[$i]['area_name'] = implode(', ', $u_region);                
            }

            if ($user[$i]['user_type'] == 2) {
                $rsm_user = User::select('user.*', 'region_name', 'user.user_name as assignment_username')
                                ->leftJoin('region', 'region.id', '=', 'user.region_id')
                                //->leftJoin('area', 'area.id', '=', 'user.area_id')
                                //->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                                ->where('user.delete_status', 0)
                               ->where('user.disable_status', 1)
                                ->where('user.user_type', 1)
                                ->where('user.region_id', $user[$i]['region_id'])
                                ->orderby('user_name')
                                ->get()->toArray();

                if (count($rsm_user) > 0)
                    $user[$i]['assign_to_user'] = $rsm_user[0]['user_name'];
                else
                    $user[$i]['assign_to_user'] = '';
                
                $user_area = UserArea::select('area.*')
                        ->leftJoin('area', 'area.id', '=', 'user_area.area_id')                        
                        ->where('user_area.user_id',$user[$i]['id'])->get()->toArray();
                $area_name = array();
                if(count($user_area) > 0){
                    foreach($user_area as $ar){
                        $area_name[] = $ar['area_name'];                        
                    }
                }
                $user[$i]['area_name'] = implode(', ', $area_name);
                
            }

            if ($user[$i]['user_type'] == 3) {
                /*$asm_user = User::select('user.*', 'region_name', 'area_name', 'assignment_user.user_name as assignment_username')
                                ->leftJoin('region', 'region.id', '=', 'user.region_id')
                                ->leftJoin('area', 'area.id', '=', 'user.area_id')
                                ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                                ->where('user.delete_status', 0)
                                ->where('user.disable_status', 0)
                                ->where('user.user_type', 2)
                                ->where('user.region_id', $user[$i]['region_id'])
                                ->where('user.area_id', $user[$i]['area_id'])
                                ->orderby('user_name')
                                ->get()->toArray();

                if (count($asm_user) > 0)
                    $user[$i]['assign_to_user'] = $asm_user[0]['user_name'];
                else
                    $user[$i]['assign_to_user'] = '';*/
                    if($user[$i]['assign_to_user'] != null && $user[$i]['assign_to_user'] != ''){
                        $asm_user = User::select('user.*', 'region_name', 'area_name', 'assignment_user.user_name as assignment_username')
                                    ->leftJoin('region', 'region.id', '=', 'user.region_id')
                                    ->leftJoin('area', 'area.id', '=', 'user.area_id')
                                    ->leftJoin('user as assignment_user', 'assignment_user.id', '=', 'user.assign_to_user')
                                    ->where('user.delete_status', 0)
                                    ->where('user.disable_status', 1)
                                    ->where('user.user_type', 2)
                                    ->where('user.id', $user[$i]['assign_to_user'])
                                    //->where('user.region_id', $user[0]['region_id'])
                                    //->where('user.area_id', $user[0]['area_id'])
                                    ->orderby('user_name')
                                    ->get()->toArray();
                        
                        if (count($asm_user) > 0)
                            $user[$i]['assign_to_user'] = $asm_user[0]['user_name'];
                        else
                            $user[$i]['assign_to_user'] = '';                        
                        
                    }                
                
                $user_area = UserArea::select('area.*')
                        ->leftJoin('area', 'area.id', '=', 'user_area.area_id')                        
                        ->where('user_area.user_id',$user[$i]['id'])->get()->toArray();
                $area_name = array();
                if(count($user_area) > 0){
                    foreach($user_area as $ar){
                        $area_name[] = $ar['area_name'];                        
                    }
                }
                $user[$i]['area_name'] = implode(', ', $area_name);                
                
                
            }
        }

        $region = Region::select('*')
                        ->where('delete_status', 0)
                        ->where('disable_status', 0)
                        ->orderby('region_name')
                        ->get()->toArray();

        $data = array();
        $data['user_list'] = $user;
        $data['session_user_info'] = Session::get('session_user_info');
        //echo '<pre>';
        //print_r($data);exit;
        return view('pages.user.user_deactive', $data);   
    }

    public function forgotApply($data)
    {
        // try{
        //         if($data == "password" ){
        //             $ar     = Input::all();
        //             $num    = $ar['passcode'];
        //             $x      = User::select("user_passcode")->where("user_email",$email)->get()->toArray();
                    
        //                     if($x[0]['user_passcode'] == $num){
        //                         return view("pages.user.new_password");
        //                     }else{
        //                         return Redirect::back()->withErrors('Wrong Passcode. Check and try again.');
        //                     }
        //             }
        //             else
        //             {

        //                     $data = Crypt::decrypt($data);
        //                     $data = explode("|",$data);
                            
        //                     $num = $data[1];
        //                     $email = $data[0];
        //                     $time = $data[2];
        //                     if( time()-$time > (3600*4)){
        //                         die("410 expired link.");
        //                     }

        //                     $x = User::select("user_passcode")->where("user_email",$email)->get()->toArray();
        //                     if($x[0]['user_passcode'] == $num){
        //                         return view("pages.user.new_password");
        //                     }
        //                     else{
        //                         return Redirect::back()->withErrors('Wrong Passcode. Check and try again.');
        //                     }
        //                     // die(print_r($x));

        //             }
        //     }catch(Exception $e){
        //         // die($e->getMessage());
        //         die("404 not found");
        //     }
    }

}
