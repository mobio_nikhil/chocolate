<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use App\User;
use Input;
use Validator;
use App\Helpers\Helper;
use App\GpsData;
use App\Meeting;
use App\Region;
use App\Customer;
use App\Customertype;
use App\Area;
use App\State;
use App\City;
use DB;
use Redirect;
use Config;
use Session;
use Form;
use View;
use Mail;


class AdminCustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    
    public function new_list()
    {
      $customer_list = Helper::get_customers('*');
      return view('pages.customer.new_customer_list')->with('customer_list',$customer_list);
    }
    
    public function customer_list(){
        
        
         $session_user_info = Session::get('session_user_info');
       
        
        $user_ids = array();
        if($session_user_info['user_type'] == 0){
            $user_ids = Helper::vp_under_user_list();            
        }
        if($session_user_info['user_type'] == 1){            
            $user_ids = Helper::rsm_under_user_list($session_user_info) ;
        }
        if($session_user_info['user_type'] == 2){            
            $user_ids = Helper::asm_under_user_list($session_user_info) ;
        }
        //$user_ids = trim($user_ids, ",");
        
        $customer_list = array();

        
        $customer_list = Customer::select('customer.*','city_name','customertype_name','region_name','state_name','area_name','user_name',DB::raw("count(meeting_customer.id) as meeting_count"))
                    ->leftJoin('customer_type', 'customer_type.id', '=', 'customer.customer_type_id')
                    ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                    ->leftJoin('state', 'state.id', '=', 'customer.state_id')
                    ->leftJoin('area', 'area.id', '=', 'customer.area_id')
                    ->leftJoin('city', 'city.id', '=', 'customer.city_id')
                    ->leftJoin('user', 'user.id', '=', 'customer.information_enter_by')
                    ->leftJoin('meeting_customer', 'meeting_customer.customer_id', '=', 'customer.id')
                    ->wherein('information_enter_by',$user_ids)
                    ->where('customer.delete_status',0)
                    ->where('customer.disable_status',0)
                    ->groupby('customer.id')
                    ->orderby('customer.business_name')
                    ->get()
                    ->toArray();
        //echo '<pre>';
        //print_r($customer_list);
        //echo '</pre>';exit;

        $data = array();
        $data['customer_list'] = $customer_list;
        return view('pages.customer.customer_list',$data);
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $session_user_info = Session::get('session_user_info');
        if($session_user_info['user_type'] == 0){
            $region = Region::select('*')->get()->toArray();
            $area = Area::select('*')->get()->toArray();
        }
            $customertype = Customertype::select('*')->where('delete_status',0)->where('disable_status',0)->get()->toArray();

        if($session_user_info['user_type'] == 1){
            $region = Region::select('*')->where('delete_status',0)->where('disable_status',0)->where('id',$session_user_info['region_id'])->get()->toArray();
            $area = Area::select('*')->where('delete_status',0)->where('disable_status',0)->where('region_id',$session_user_info['region_id'])->get()->toArray();
        }

        if($session_user_info['user_type'] == 2){
            $region = Region::select('*')->where('delete_status',0)->where('disable_status',0)->where('id',$session_user_info['region_id'])->get()->toArray();
            $area = Area::select('*')->where('delete_status',0)->where('disable_status',0)->where('region_id',$session_user_info['region_id'])->where('id',$session_user_info['area_id'])->get()->toArray();
        }
        $data = array();
        $data['region_list'] = $region;
        $data['area_list'] = $area;
        $data['customertype_list'] = $customertype;
        
        return view('pages/customer/create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

             $input_all = Input::all();
            
             $user_info = Session::get('session_user_info');

            /*validation start*/
               $rules = array(                                       
                    "customer_name" => 'required|alpha_spaces',
                    "business_name" => 'required',
                    "business_address" => 'required',
                    "customertype" => 'required',
                    "state"         => 'required',
                    "area"          => 'required',
                    "city"          => 'required',
                    "pincode"          => 'required|numeric',
                    //"primary_contact_name" => 'required|alpha_spaces',
                    //"primary_image"  => 'required',
                    //"secondary_contact_name"  => 'required|alpha_spaces',
                    "customer_email" => 'required|email',
                    "customer_phone"  => 'required|numeric',                    
                    //"visit_card_image_1" => 'required',
                    //"customer_birth_date" => 'required|date_format:"d-m-Y"',
                    //"customer_anniversary_date" => 'required|date_format:"d-m-Y"',                   
                    "customer_birth_date" => 'only_date_month_text',
                    "customer_anniversary_date" => 'only_date_month_text',                   
                );        
                $validator = Validator::make($input_all, $rules);
                // check if the validator failed -----------------------
                if ($validator->fails()) {
                    $messages = $validator->messages();
                    return Redirect::to(Config::get('constants.admin_path').'customer/create')
                        ->withErrors($validator)
                        ->withInput();
                }
               
                       
               if((Input::hasFile('primary_image'))){
                   if((Input::file('primary_image')->isValid())){
                        $extension = Input::file('primary_image')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            //return Helper::response_api(false,'Primary Image Fail To Upload','');                        
                             return Redirect::to(Config::get('constants.admin_path').'customer/create')->withErrors(['Primary Image Fail To Upload'])->withInput();
                        }                                      
                   }else{
                            //return Helper::response_api(false,'Primary Image Fail To Upload','');                                           
                        return Redirect::to(Config::get('constants.admin_path').'customer/create')->withErrors(['msg', 'Primary Image Fail To Upload'])->withInput();                       
                   }               
               }
               
               
              if((Input::hasFile('visit_card_image_1'))){
                  if((Input::file('visit_card_image_1')->isValid())){
                        $extension = Input::file('visit_card_image_1')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){                             
                            return Redirect::to(Config::get('constants.admin_path').'customer/create')->withErrors(['Visiting Card  Image Fail To Upload'])->withInput();
                        }                                      
                   }else{
                            return Redirect::to(Config::get('constants.admin_path').'customer/create')->withErrors(['Visiting Card  Image Fail To Upload'])->withInput();
                   }               
               }
               
if((Input::hasFile('visit_card_image_2'))){
                   if((Input::file('visit_card_image_2')->isValid())){
                        $extension = Input::file('visit_card_image_2')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Visit Card Image 2 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Visit Card Image 2 Fail To Upload','');                                           
                   }
               }
              
               if((Input::hasFile('visit_card_image_3'))){
                   if((Input::file('visit_card_image_3')->isValid())){
                        $extension = Input::file('visit_card_image_3')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Visit Card Image 3 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Visit Card Image 3 Fail To Upload','');                                           
                   }
               }
             
               if((Input::hasFile('visit_card_image_4'))){
                   if((Input::file('visit_card_image_4')->isValid())){
                        $extension = Input::file('visit_card_image_4')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Visit Card Image 4 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Visit Card Image 4 Fail To Upload','');                                           
                   }
               }

               if((Input::hasFile('visit_card_image_5'))){
                   if((Input::file('visit_card_image_5')->isValid())){
                        $extension = Input::file('visit_card_image_5')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Visit Card Image 5 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Visit Card Image 5 Fail To Upload','');                                           
                   }
               }
               
               $insert_array = array();
               $insert_array['customer_name'] = $input_all['customer_name'];
               $insert_array['business_name'] = $input_all['business_name'];
               
               $insert_array['business_address'] = $input_all['business_address'];
               //$insert_array['primary_contact_name'] = $input_all['primary_contact_name'];
               $insert_array['secondary_contact_name'] = empty($input_all['secondary_contact_name']) ? NULL : $input_all['secondary_contact_name'];
               $insert_array['customer_email'] = $input_all['customer_email'];
               $insert_array['customer_phone'] = $input_all['customer_phone'];
               $insert_array['customer_type_id'] = $input_all['customertype'];
               //$insert_array['customer_birth_date'] = empty($input_all['customer_birth_date']) ? NULL : date('Y-m-d',strtotime($input_all['customer_birth_date']));
               //$insert_array['customer_anniversary_date'] = empty($input_all['customer_anniversary_date']) ? NULL : date('Y-m-d',strtotime($input_all['customer_anniversary_date']));
               
                if((!(empty($input_all['customer_birth_date'])))){
                   
                   
                   $datearray = explode('-', $input_all['customer_birth_date']);
                    $montharray = array(
                                  'Jan',
                                    'Feb',
                                    'Mar',
                                    'Apr',
                                    'May',
                                    'Jun',
                                    'Jul',
                                    'Aug',
                                    'Sep',
                                    'Oct',
                                    'Nov',
                                    'Dec'   
                                );
            
                        if ((in_array($datearray[1], $montharray))){
                            $key = array_search($datearray[1], $montharray);
                            $insert_array['customer_birth_date'] =  $datearray[0].'-'.($key + 1);
                        }                   
                   
                    //$insert_array['customer_birth_date'] =  date('Y-m-d',strtotime($input_all['customer_birth_date']));                   
                    //$insert_array['customer_birth_date'] =  $input_all['customer_birth_date'];
               }else{
                 $insert_array['customer_birth_date']= Null;
               }
               
               if((!(empty($input_all['customer_anniversary_date'])))){                    
                    //$insert_array['customer_anniversary_date'] = date('Y-m-d',strtotime($input_all['customer_anniversary_date']));
                    //$insert_array['customer_anniversary_date'] = $input_all['customer_anniversary_date'];
                   $datearray = explode('-', $input_all['customer_anniversary_date']);
                    $montharray = array(
                                  'Jan',
                                    'Feb',
                                    'Mar',
                                    'Apr',
                                    'May',
                                    'Jun',
                                    'Jul',
                                    'Aug',
                                    'Sep',
                                    'Oct',
                                    'Nov',
                                    'Dec'   
                                );
            
                        if ((in_array($datearray[1], $montharray))){
                            $key = array_search($datearray[1], $montharray);
                            $insert_array['customer_anniversary_date'] =  $datearray[0].'-'.($key + 1);
                        }
                    

               }else{
                    $insert_array['customer_anniversary_date'] = Null;
               }               
               
               $insert_array['information_enter_by'] = $user_info['id'];
               $insert_array['region_id'] = $input_all['region'];
               $insert_array['area_id'] = $input_all['area'];
               $insert_array['city_id'] = $input_all['city'];
               $insert_array['pincode'] = $input_all['pincode'];
               $insert_array['state_id'] = $input_all['state'];
               $insert_array['delete_status'] = 0;
               $insert_array['disable_status'] = 0;   
               $insert_array['created_on'] = date('Y-m-d H:i:s');
               
              
               if($cust_id = Customer::create($insert_array)){
                   
                   if((Input::hasFile('primary_image'))){
                       if(Input::file('primary_image')->isValid()){ 
                            $extension = Input::file('primary_image')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('primary_image');                        
                                $fileName = 'primary_image_1'.'.'.$extension;
                                Input::file('primary_image')->move($destinationPath, $fileName);                        
                                $cust_id->primary_image = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Redirect::to(Config::get('constants.admin_path').'customer/create')->withErrors(['Primary Image Fail To Upload'])->withInput();
                            }
                        }                   
                    }                   

                    if((Input::hasFile('visit_card_image_1'))){
                        if(Input::file('visit_card_image_1')->isValid()){ 
                            $extension = Input::file('visit_card_image_1')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_1');                        
                                $fileName = 'visit_card_image_1'.'.'.$extension;
                                Input::file('visit_card_image_1')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_1 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Redirect::to(Config::get('constants.admin_path').'customer/create')->withErrors(['Visiting Card  Image Fail To Upload'])->withInput();
                            }
                        }                   
                    }                   
             
                     if((Input::hasFile('visit_card_image_2'))){
                        if(Input::file('visit_card_image_2')->isValid()){ 
                            $extension = Input::file('visit_card_image_2')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_2');                        
                                $fileName = 'visit_card_image_2'.'.'.$extension;
                                Input::file('visit_card_image_2')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_2 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Visit Card 2 Image Fail To Upload','');
                            }
                        }                   
                    }                   
             
                    if((Input::hasFile('visit_card_image_3'))){
                        if(Input::file('visit_card_image_3')->isValid()){ 
                            $extension = Input::file('visit_card_image_3')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_3');                        
                                $fileName = 'visit_card_image_3'.'.'.$extension;
                                Input::file('visit_card_image_3')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_3 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Visit Card 3 Image Fail To Upload','');
                            }
                        }                   
                    }                   
                    if((Input::hasFile('visit_card_image_4'))){
                        if(Input::file('visit_card_image_4')->isValid()){ 
                            $extension = Input::file('visit_card_image_4')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_4');                        
                                $fileName = 'visit_card_image_4'.'.'.$extension;
                                Input::file('visit_card_image_4')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_4 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Visit Card 4 Image Fail To Upload','');
                            }
                        }                   
                    }                   

                    if((Input::hasFile('visit_card_image_5'))){
                        if(Input::file('visit_card_image_5')->isValid()){ 
                            $extension = Input::file('visit_card_image_5')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_5');                        
                                $fileName = 'visit_card_image_5'.'.'.$extension;
                                Input::file('visit_card_image_5')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_5 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Visit Card 5 Image Fail To Upload','');
                            }
                        }                   
                    }               
                $customer_data = Customer::find($cust_id->id);
                Session::flash('message', 'Customer created'); 
                //Session::flash('alert-class', 'alert-danger');                         
                return Redirect::to(Config::get('constants.admin_path').'customer/customer_list');
        }else{
            Session::flash('message', 'Customer entry failed'); 
            Session::flash('alert-class', 'alert-danger');                         
            return Redirect::to(Config::get('constants.admin_path').'customer/customer_list');
        }   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $session_user_info = Session::get('session_user_info');
        
        $customer = Customer::findOrFail($id);
        $region_id= $customer->region_id;        
        $state = State::select("state.id","state.state_name")->join("region_detail","region_detail.state_id","=","state.id")->where("region_id",$region_id)->get()->toArray();


        if(empty(Input::old('state'))){
        $city = City::select("*")->where("state_id",$customer->state_id)->get()->toArray();
        }else{
        $city = City::select("*")->where("state_id",Input::old('state'))->get()->toArray();            
        }
        
        if($session_user_info['user_type'] == 0){
            $region = Region::select('*')->get()->toArray();
            $area  = Area::select("*")->where("region_id",$region_id)->get()->toArray();
        }

        if($session_user_info['user_type'] == 1){
            $region = Region::select('*')->where('delete_status',0)->where('disable_status',0)->where('id',$session_user_info['region_id'])->get()->toArray();
            $area = Area::select('*')->where('delete_status',0)->where('disable_status',0)->where('region_id',$session_user_info['region_id'])->get()->toArray();           
        }

        if($session_user_info['user_type'] == 2){
            $region = Region::select('*')->where('delete_status',0)->where('disable_status',0)->where('id',$session_user_info['region_id'])->get()->toArray();
            $area = Area::select('*')->where('delete_status',0)->where('disable_status',0)->where('region_id',$session_user_info['region_id'])->where('id',$session_user_info['area_id'])->get()->toArray();
        }
        
        $customertype = Customertype::select('*')->where('delete_status',0)->where('disable_status',0)->get()->toArray();
        
        //return view('pages/purpose/edit')->with(compact('purpose'));   
        // print_r($customer->customer_anniversary_date);    
        // print_r($customer->customer_birth_date);
        // exit();   

        return View::make('pages.customer.edit')
            ->with('region_list', $region)
            ->with('area_list', $area)
            ->with('state_list', $state)
            ->with('city_list', $city)
            ->with('customertype_list', $customertype)
            ->with('customer', $customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

             $input_all = Input::all();
             
             $user_info = Session::get('session_user_info');

            /*validation start*/
               $rules = array(                   
                    "customer_name" => 'required|alpha_spaces',
                    "business_name" => 'required',
                    "business_address" => 'required',
                    "customertype" => 'required',
                    "state"         => 'required',
                    "area"          => 'required',
                    "city"          => 'required',
                    "pincode"          => 'required|numeric',
                    //"primary_contact_name" => 'required|alpha_spaces',
                    //"primary_image"  => 'required',
                    //"secondary_contact_name" => 'required|alpha_spaces',                    
                    "customer_email" => 'required|email',
                    "customer_phone"  => 'required|numeric',                    
                    //"visit_card_image_1" => 'required',
                    //"customer_birth_date" => 'date_format:"d-m-Y"',
                    //"customer_anniversary_date" => 'date_format:"d-m-Y"',                   
                    "customer_birth_date" => 'only_date_month_text',
                    "customer_anniversary_date" => 'only_date_month_text',                   
                );   
               
            $validator = Validator::make($input_all, $rules);
                // check if the validator failed -----------------------
                if ($validator->fails()) {
                    $messages = $validator->messages();
                    return Redirect::to(Config::get('constants.admin_path').'customer/edit/'.$id)
                        ->withErrors($validator)
                        ->withInput();
                }               

              
               if((Input::hasFile('primary_image'))){
                   if((Input::file('primary_image')->isValid())){
                        $extension = Input::file('primary_image')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            //return Helper::response_api(false,'Primary Image Fail To Upload','');                        
                            return Redirect::to(Config::get('constants.admin_path').'customer/edit/'.$id)->withErrors(['Primary Image Fail To Upload'])->withInput();
                        }                                      
                   }else{
                            return Redirect::to(Config::get('constants.admin_path').'customer/edit/'.$id)->withErrors(['Primary Image Fail To Upload'])->withInput();
                   }               
               }
               
               
              if((Input::hasFile('visit_card_image_1'))){
                  if((Input::file('visit_card_image_1')->isValid())){
                        $extension = Input::file('visit_card_image_1')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){                            
                            return Redirect::to(Config::get('constants.admin_path').'customer/edit/'.$id)->withErrors(['Visiting Card  Image Fail To Upload'])->withInput();
                        }                                      
                   }else{
                            return Redirect::to(Config::get('constants.admin_path').'customer/edit/'.$id)->withErrors(['Visiting Card  Image Fail To Upload'])->withInput();
                   }               
               }
               
if((Input::hasFile('visit_card_image_2'))){
                   if((Input::file('visit_card_image_2')->isValid())){
                        $extension = Input::file('visit_card_image_2')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Visit Card Image 2 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Visit Card Image 2 Fail To Upload','');                                           
                   }
               }
              
               if((Input::hasFile('visit_card_image_3'))){
                   if((Input::file('visit_card_image_3')->isValid())){
                        $extension = Input::file('visit_card_image_3')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Visit Card Image 3 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Visit Card Image 3 Fail To Upload','');                                           
                   }
               }
             
               if((Input::hasFile('visit_card_image_4'))){
                   if((Input::file('visit_card_image_4')->isValid())){
                        $extension = Input::file('visit_card_image_4')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Visit Card Image 4 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Visit Card Image 4 Fail To Upload','');                                           
                   }
               }

               if((Input::hasFile('visit_card_image_5'))){
                   if((Input::file('visit_card_image_5')->isValid())){
                        $extension = Input::file('visit_card_image_5')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Visit Card Image 5 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Visit Card Image 5 Fail To Upload','');                                           
                   }
               }
               
               $insert_array = array();
               $insert_array['customer_name'] = $input_all['customer_name'];
               $insert_array['business_name'] = $input_all['business_name'];
               $insert_array['customer_type_id'] = $input_all['customertype'];
               $insert_array['business_address'] = $input_all['business_address'];
               $insert_array['state_id'] = $input_all['state'];
               $insert_array['city_id'] = $input_all['city'];
               $insert_array['pincode'] = $input_all['pincode'];

               //$insert_array['primary_contact_name'] = $input_all['primary_contact_name'];
               if((!(empty($input_all['secondary_contact_name'])))){
                $insert_array['secondary_contact_name'] = $input_all['secondary_contact_name'];                   
               }
               $insert_array['customer_email'] = $input_all['customer_email'];
               $insert_array['customer_phone'] = $input_all['customer_phone'];
               //$insert_array['customer_type_id'] = 1;
               if((!(empty($input_all['customer_birth_date'])))){
                   
                   
                   $datearray = explode('-', $input_all['customer_birth_date']);
                    $montharray = array(
                                  'Jan',
                                    'Feb',
                                    'Mar',
                                    'Apr',
                                    'May',
                                    'Jun',
                                    'Jul',
                                    'Aug',
                                    'Sep',
                                    'Oct',
                                    'Nov',
                                    'Dec'   
                                );
            
                        if ((in_array($datearray[1], $montharray))){
                            $key = array_search($datearray[1], $montharray);
                            $insert_array['customer_birth_date'] =  $datearray[0].'-'.($key + 1);
                        }                   
                   
                    //$insert_array['customer_birth_date'] =  date('Y-m-d',strtotime($input_all['customer_birth_date']));                   
                    //$insert_array['customer_birth_date'] =  $input_all['customer_birth_date'];
               }else{
                 $insert_array['customer_birth_date']= Null;
               }
               
               if((!(empty($input_all['customer_anniversary_date'])))){                    
                    //$insert_array['customer_anniversary_date'] = date('Y-m-d',strtotime($input_all['customer_anniversary_date']));
                    //$insert_array['customer_anniversary_date'] = $input_all['customer_anniversary_date'];
                   $datearray = explode('-', $input_all['customer_anniversary_date']);
                    $montharray = array(
                                  'Jan',
                                    'Feb',
                                    'Mar',
                                    'Apr',
                                    'May',
                                    'Jun',
                                    'Jul',
                                    'Aug',
                                    'Sep',
                                    'Oct',
                                    'Nov',
                                    'Dec'   
                                );
            
                        if ((in_array($datearray[1], $montharray))){
                            $key = array_search($datearray[1], $montharray);
                            $insert_array['customer_anniversary_date'] =  $datearray[0].'-'.($key + 1);
                        }
                    

               }else{
                    $insert_array['customer_anniversary_date'] = Null;
               }


                //dd($input_all['customer_anniversary_date']);
               //$insert_array['information_enter_by'] = $user_info['id'];
               $insert_array['region_id'] = $input_all['region'];
               $insert_array['area_id'] = $input_all['area'];
               //$insert_array['delete_status'] = 0;
               //$insert_array['disable_status'] = 0;   
               Customer::where('id',$id)->update($insert_array);
               $cust_id = Customer::find($id);
               
               if($cust_id){
                   
                   if((Input::hasFile('primary_image'))){
                       if(Input::file('primary_image')->isValid()){ 
                            $extension = Input::file('primary_image')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('primary_image');                        
                                $fileName = 'primary_image_1'.'.'.$extension;
                                Input::file('primary_image')->move($destinationPath, $fileName);                        
                                $cust_id->primary_image = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Redirect::to(Config::get('constants.admin_path').'customer/edit/'.$id)->withErrors(['Primary Image Fail To Upload'])->withInput();
                            }
                        }                   
                    }                   

                    if((Input::hasFile('visit_card_image_1'))){
                        if(Input::file('visit_card_image_1')->isValid()){ 
                            $extension = Input::file('visit_card_image_1')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_1');                        
                                $fileName = 'visit_card_image_1'.'.'.$extension;
                                Input::file('visit_card_image_1')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_1 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Redirect::to(Config::get('constants.admin_path').'customer/edit/'.$id)->withErrors(['Visit Card 1 Image Fail To Upload'])->withInput();                                
                            }
                        }                   
                    }                   
             
                     if((Input::hasFile('visit_card_image_2'))){
                        if(Input::file('visit_card_image_2')->isValid()){ 
                            $extension = Input::file('visit_card_image_2')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_2');                        
                                $fileName = 'visit_card_image_2'.'.'.$extension;
                                Input::file('visit_card_image_2')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_2 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Visit Card 2 Image Fail To Upload','');
                            }
                        }                   
                    }                   
             
                    if((Input::hasFile('visit_card_image_3'))){
                        if(Input::file('visit_card_image_3')->isValid()){ 
                            $extension = Input::file('visit_card_image_3')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_3');                        
                                $fileName = 'visit_card_image_3'.'.'.$extension;
                                Input::file('visit_card_image_3')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_3 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Visit Card 3 Image Fail To Upload','');
                            }
                        }                   
                    }                   
                    if((Input::hasFile('visit_card_image_4'))){
                        if(Input::file('visit_card_image_4')->isValid()){ 
                            $extension = Input::file('visit_card_image_4')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_4');                        
                                $fileName = 'visit_card_image_4'.'.'.$extension;
                                Input::file('visit_card_image_4')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_4 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Visit Card 4 Image Fail To Upload','');
                            }
                        }                   
                    }                   

                    if((Input::hasFile('visit_card_image_5'))){
                        if(Input::file('visit_card_image_5')->isValid()){ 
                            $extension = Input::file('visit_card_image_5')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_5');                        
                                $fileName = 'visit_card_image_5'.'.'.$extension;
                                Input::file('visit_card_image_5')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_5 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Visit Card 5 Image Fail To Upload','');
                            }
                        }                   
                    }               
                $customer_data = Customer::find($cust_id->id);
                Session::flash('message', 'Customer updated'); 
                //Session::flash('alert-class', 'alert-danger');                         
                return Redirect::to(Config::get('constants.admin_path').'customer/customer_list');
        }else{
                Session::flash('message', 'Customer update failed'); 
                Session::flash('alert-class', 'alert-danger');                         
                return Redirect::to(Config::get('constants.admin_path').'customer/customer_list');
        }
            
    }

    public function customer_history($id){
 
        
        $customer_meeting_date_log = Meeting::select(DB::raw('GROUP_CONCAT(meeting_customer.id) as meetings_ids'),DB::raw("DATE_FORMAT(created_on,'%d-%m-%Y') as create_date"))
                        ->groupby(DB::raw("DATE_FORMAT(created_on,'%d/%m/%Y')"))
                        ->orderby(DB::raw("DATE_FORMAT(created_on,'%d/%m/%Y')"),'desc')        
                        ->where('meeting_customer.customer_id',$id)
                        ->get()
                        ->toArray();         
        
        $customer_data = Customer::select('*')->where('id',$id)->get()->toArray();
         
        $customer_meeting_data = array();
        for($i=0;$i<count($customer_meeting_date_log);$i++){            
            $meeting_ids = explode(',', $customer_meeting_date_log[$i]['meetings_ids']);                        
                $meeting_data = Meeting::select('*','meeting_customer.created_on as meeting_date_time','meeting_customer.extra_note as meeting_note','folloup_meeting_customer.extra_note as followup_note')
                        ->leftJoin('user', 'user.id', '=', 'meeting_customer.user_id')
                        ->leftJoin('folloup_meeting_customer', 'folloup_meeting_customer.meeting_id', '=', 'meeting_customer.id')
                        ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')                                
                        ->wherein('meeting_customer.id',$meeting_ids)
                        ->get()
                        ->toArray();
                if(count($meeting_data) > 0){
                    $customer_meeting_data[] = array(
                        'meeting_day' => $customer_meeting_date_log[$i]['create_date'],
                        'meeting_data' => $meeting_data
                    );
                    
                }
            
            
            
            
            
        }
         
//        echo '<pre>';
//        print_r($customer_meeting_data);
//        echo '</pre>';
        $data = array();        
        $data['customer_data'] = $customer_data;
        $data['customer_meeting'] = $customer_meeting_data;
        return view('pages.customer.customer_history',$data);         
         
    }
    public function trashAdd( $id ){
        $input_array = Input::all();
        $user_info = Session::get('session_user_info');
        
      Customer::where('id', $id)->update(["delete_status"=>1,'delete_by'=>$user_info['id'],'delete_reason'=>$input_array['delete_reason']]);
      return redirect(Config::get('constants.admin_path').'customer/customer_list');
      
    }
    public function trashList(){
            
        
        $session_user_info = Session::get('session_user_info');

        $user_ids = array();
        if($session_user_info['user_type'] == 0){
            $user_ids = Helper::vp_under_user_list();            
        }
        if($session_user_info['user_type'] == 1){            
            $user_ids = Helper::rsm_under_user_list($session_user_info) ;
        }
        if($session_user_info['user_type'] == 2){            
            $user_ids = Helper::asm_under_user_list($session_user_info) ;
        }
        
        $customer_list = array();

        
        $customer_list = Customer::select('customer.*','city_name','state_name','customertype_name','region_name','area_name','user_name',DB::raw("count(meeting_customer.id) as meeting_count"))
                    ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                    ->leftJoin('customer_type', 'customer_type.id', '=', 'customer.customer_type_id')					                
                    ->leftJoin('state', 'state.id', '=', 'customer.state_id')                    
                    ->leftJoin('city', 'city.id', '=', 'customer.city_id')                                
                    ->leftJoin('area', 'area.id', '=', 'customer.area_id')
                    ->leftJoin('user', 'user.id', '=', 'customer.information_enter_by')
                    ->leftJoin('meeting_customer', 'meeting_customer.customer_id', '=', 'customer.id')
                    ->wherein('information_enter_by',$user_ids)
                    ->where('customer.delete_status',1)
                    ->groupby('customer.id')
                    ->get()
                    ->toArray();

        $data = array();
        $data['customer_list'] = $customer_list;

        return view('pages.customer.customer_trash',$data);
    }
     public function deactiveList(){
            
        
        $session_user_info = Session::get('session_user_info');

        $user_ids = array();
        if($session_user_info['user_type'] == 0){
            $user_ids = Helper::vp_under_user_list();            
        }
        if($session_user_info['user_type'] == 1){            
            $user_ids = Helper::rsm_under_user_list($session_user_info) ;
        }
        if($session_user_info['user_type'] == 2){            
            $user_ids = Helper::asm_under_user_list($session_user_info) ;
        }
        
        $customer_list = array();

        
        $customer_list = Customer::select('customer.*','state_name','city_name','customertype_name','region_name','area_name','user_name',DB::raw("count(meeting_customer.id) as meeting_count"))
                    ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                    ->leftJoin('customer_type', 'customer_type.id', '=', 'customer.customer_type_id')                    
                    ->leftJoin('state', 'state.id', '=', 'customer.state_id')                    
                    ->leftJoin('city', 'city.id', '=', 'customer.city_id')                                
                    ->leftJoin('area', 'area.id', '=', 'customer.area_id')                    
                    ->leftJoin('user', 'user.id', '=', 'customer.information_enter_by')
                    ->leftJoin('meeting_customer', 'meeting_customer.customer_id', '=', 'customer.id')
                    ->wherein('information_enter_by',$user_ids)
                    ->where('customer.delete_status',0)
                    ->where('customer.disable_status',1)
                    ->groupby('customer.id')
                    ->get()
                    ->toArray();

        $data = array();
        $data['customer_list'] = $customer_list;

        return view('pages.customer.customer_deactive',$data);
    }

    public function trashRestore( $id ){
        Customer::where('id', $id)->update(["delete_status"=>0]);
        Session::flash('message', 'Customer Restored'); 
    
      return redirect(Config::get('constants.admin_path').'customer/customer_list');
    }

    public function changeStatus($status, Request $request)
    { 
        $st = '';
        $ar = $request->all();
        
        $input_array = Input::all();
        $user_info = Session::get('session_user_info');
        
        $id = $ar['id'];
        if( $status == "enable"){
          $disable_status = 0;
          $st = 'active';
          Customer::where('id', $id)->update(["disable_status"=>$disable_status,'active_by'=>$user_info['id']]);
          Session::flash('message', 'Customer has been Activated'); 
        }
        elseif( $status == "disable"){
          $st = 'deactive';
          $disable_status = 1;
          // send_mail
        $session_user_info = Session::get('session_user_info');

        $user_ids = array();
        if($session_user_info['user_type'] == 0){
            $user_ids = Helper::all_vp_under_user_list();            
        }
        if($session_user_info['user_type'] == 1){            
            $user_ids = Helper::all_rsm_under_user_list($session_user_info) ;
        }
        if($session_user_info['user_type'] == 2){            
            $user_ids = Helper::all_asm_under_user_list($session_user_info) ;
        }
        
                    $user = User::select('user.*')
                            ->wherein('user.id',$user_ids)
                            ->get()
                            ->toArray();            
          //$customer_data = Customer::where('id', $id)->first();
          $customer_data = Customer::select('customer.*','region_name','state_name','area_name')
                                                ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                                                ->leftJoin('state', 'state.id', '=', 'customer.state_id')
                                                ->leftJoin('area', 'area.id', '=', 'customer.area_id')
                                                ->where('customer.id', $id)->first();
             
          $user_email = array();
          for($i=0;$i<count($user);$i++){
              $user_email[] = $user[$i]['user_email'];
          }
          //return view('pages.customer.disable_customer_email',['customer' => $customer_data,"disable_reason"=>$ar['disable_reason']]);
          //exit;
          if(count($user_email) > 0){

              //$user_email[] = Config::get('constants.from_mail');

                Mail::send(['html' => 'pages.customer.disable_customer_email'], ['customer' => $customer_data,"disable_reason"=>$ar['disable_reason']], function ($message) use($user_email) {
                  //print_r($user_data);exit;
                  $message->subject('Customer Deactive');
                  $message->from(Config::get('constants.from_mail'), Config::get('constants.from_mail_text'));                  
                  $message->to($user_email);
                }); 
          }
          
          Customer::where('id', $id)->update(["disable_status"=>$disable_status,"disable_reason"=>$ar['disable_reason'],'disable_by'=>$user_info['id']]); 
        Session::flash('message', 'Customer has been Deactivated'); 
        }

        return redirect(Config::get('constants.admin_path').'customer/customer_list');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function approve($id)
    {
      Customer::where("id",$id)->update(["customer_type_id"=>0]);
      Session::flash("message","Customer has been approved");
      return Redirect::back();
    }

    public function today_customer(){        
         $session_user_info = Session::get('session_user_info');
       
        
        $user_ids = array();
        if($session_user_info['user_type'] == 0){
            $user_ids = Helper::vp_under_user_list();            
        }
        if($session_user_info['user_type'] == 1){            
            $user_ids = Helper::rsm_under_user_list($session_user_info) ;
        }
        if($session_user_info['user_type'] == 2){            
            $user_ids = Helper::asm_under_user_list($session_user_info) ;
        }
        //$user_ids = trim($user_ids, ",");
        
        $customer_list = array();

        
        $customer_list = Customer::select('customer.*','pincode','region_name','city_name','state_name','area_name','user_name',DB::raw("count(meeting_customer.id) as meeting_count"))
                    ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                    ->leftJoin('state', 'state.id', '=', 'customer.state_id')
                    ->leftJoin('area', 'area.id', '=', 'customer.area_id')
                    ->leftJoin('city', 'city.id', '=', 'customer.city_id')
                    ->leftJoin('user', 'user.id', '=', 'customer.information_enter_by')
                    ->leftJoin('meeting_customer', 'meeting_customer.customer_id', '=', 'customer.id')
                    ->wherein('information_enter_by',$user_ids)
                    ->where('customer.delete_status',0)
                    ->where('customer.disable_status',0)
                    ->where(DB::raw("DATE_FORMAT(customer.created_on,'%d-%m-%Y')"), date("d-m-Y"))
                    ->groupby('customer.id')
                    ->orderby('customer.business_name')
                    ->get()
                    ->toArray();
        //echo '<pre>';
        //print_r($customer_list);
        //echo '</pre>';exit;
        $data = array();
        $data['customer_list'] = $customer_list;
        return view('pages.customer.today_customer_list',$data);
        
            
    }
}


