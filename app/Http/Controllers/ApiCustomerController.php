<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use App\Helpers\Helper;
use App\Customer;
use App\Meeting;
use App\FollowupMeeting;
use App\GpsData;
use App\Brand;
use App\Purpose;
use App\Region;
use App\Customertype;
use App\User;
use App\Area;
use DB;
use Image;

class ApiCustomerController extends Controller
{
    
    public function __construct(){
//        DB::connection('mysql')->select(DB::raw('SET @@session.time_zone = "+05:30";')) ;      
//        $t = DB::connection('mysql')->select(DB::raw('SELECT @@system_time_zone')) ;      
        //DB::connection('mysql')->statement('SET @@session.time_zone = "+05:30"');
    }        
    
    public function customer_entry()
    {
        
             $input_all = Input::all();
             $user_info = (Helper::get_user_info($input_all['token']));

            /*validation start*/
               $rules = array(                   
                    //"token" => 'required',
                    "customer_name" => 'required|alpha_spaces',
                    "business_name" => 'required',
                    "business_address" => 'required',
                    "primary_contact_name" => 'required|alpha_spaces',
                    "primary_image"  => 'required',
                    "secondary_contact_no"  => 'required|numeric',
                    "customer_email" => 'required|email',
                    "customer_phone"  => 'required|numeric',                    
                    "brand_id" => 'required',
                    "purpose_id" => 'required',
                    "visit_card_image_1" => 'required',
                    "latitude" => 'required',
                    "longitude" => 'required',
                    //"customer_birth_date" => 'required|date_format:"d-m-Y"',
                    //"customer_anniversary_date" => 'required|date_format:"d-m-Y"',                   
                    "followup_reminder" => 'required',                   
                );        
               if(Helper::validation_error_response($input_all,$rules) != false)
                    return Helper::validation_error_response($input_all,$rules);   
               
               
               if($input_all['followup_reminder'] == 'yes'){
                    $rules = array(                   
                        "followup_date" => 'required|date_format:"d-m-Y"',
                        "followup_time" => 'required',                   
                        "followup_reminder" => 'required',                   
                    );        
                   if(Helper::validation_error_response($input_all,$rules) != false)
                        return Helper::validation_error_response($input_all,$rules);                    
               }
               
               if((Input::file('primary_image')->isValid())){
                    $extension = Input::file('primary_image')->getClientOriginalExtension();
                    if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                        return Helper::response_api(false,'Primary Image Fail To Upload','');                        
                    }                                      
               }else{
                        return Helper::response_api(false,'Primary Image Fail To Upload','');                                           
               }

               if((Input::file('visit_card_image_1')->isValid())){
                    $extension = Input::file('visit_card_image_1')->getClientOriginalExtension();
                    if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                        return Helper::response_api(false,'Visit Card Image 1 Fail To Upload','');                        
                    }                                      
               }else{
                        return Helper::response_api(false,'Visit Card Image 1 Fail To Upload','');                                           
               }

               if((Input::hasFile('visit_card_image_2'))){
                   if((Input::file('visit_card_image_2')->isValid())){
                        $extension = Input::file('visit_card_image_2')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Visit Card Image 2 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Visit Card Image 2 Fail To Upload','');                                           
                   }
               }
              
               if((Input::hasFile('visit_card_image_3'))){
                   if((Input::file('visit_card_image_3')->isValid())){
                        $extension = Input::file('visit_card_image_3')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Visit Card Image 3 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Visit Card Image 3 Fail To Upload','');                                           
                   }
               }
             
               if((Input::hasFile('visit_card_image_4'))){
                   if((Input::file('visit_card_image_4')->isValid())){
                        $extension = Input::file('visit_card_image_4')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Visit Card Image 4 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Visit Card Image 4 Fail To Upload','');                                           
                   }
               }

               if((Input::hasFile('visit_card_image_5'))){
                   if((Input::file('visit_card_image_5')->isValid())){
                        $extension = Input::file('visit_card_image_5')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Visit Card Image 5 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Visit Card Image 5 Fail To Upload','');                                           
                   }
               }

               
               $insert_array = array();
               $meeting_insert_array = array();
               $insert_array['customer_name'] = $input_all['customer_name'];
               $insert_array['business_name'] = $input_all['business_name'];
               $insert_array['business_address'] = $input_all['business_address'];
               $insert_array['primary_contact_name'] = $input_all['primary_contact_name'];
               

               

               
               $insert_array['secondary_contact_no'] = $input_all['secondary_contact_no'];
               $insert_array['customer_email'] = $input_all['customer_email'];
               $insert_array['customer_phone'] = $input_all['customer_phone'];
               $insert_array['customer_type_id'] = 1;
               $insert_array['customer_birth_date'] = date('Y-m-d',strtotime($input_all['customer_birth_date']));
               $insert_array['customer_anniversary_date'] = date('Y-m-d',strtotime($input_all['customer_anniversary_date']));
               $insert_array['information_enter_by'] = $user_info['id'];
               $insert_array['region_id'] = $user_info['region_id'];
               $insert_array['area_id'] = $user_info['area_id'];
               $insert_array['delete_status'] = 0;
               $insert_array['disable_status'] = 0;
               $insert_array['created_on'] = date('Y-m-d H:i:s');
               if($cust_id = Customer::create($insert_array)){
                   
                   if((Input::hasFile('primary_image'))){
                       if(Input::file('primary_image')->isValid()){ 
                            $extension = Input::file('primary_image')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('primary_image');                        
                                $fileName = 'primary_image_1'.'.'.$extension;
                                Input::file('primary_image')->move($destinationPath, $fileName);                        
                                $cust_id->primary_image = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Primary Image Fail To Upload','');
                            }
                        }                   
                    }                   

                    if((Input::hasFile('visit_card_image_1'))){
                        if(Input::file('visit_card_image_1')->isValid()){ 
                            $extension = Input::file('visit_card_image_1')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_1');                        
                                $fileName = 'visit_card_image_1'.'.'.$extension;
                                Input::file('visit_card_image_1')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_1 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Primary Image Fail To Upload','');
                            }
                        }                   
                    }                   
             
                     if((Input::hasFile('visit_card_image_2'))){
                        if(Input::file('visit_card_image_2')->isValid()){ 
                            $extension = Input::file('visit_card_image_2')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_2');                        
                                $fileName = 'visit_card_image_2'.'.'.$extension;
                                Input::file('visit_card_image_2')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_2 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Primary Image Fail To Upload','');
                            }
                        }                   
                    }                   
             
                    if((Input::hasFile('visit_card_image_3'))){
                        if(Input::file('visit_card_image_3')->isValid()){ 
                            $extension = Input::file('visit_card_image_3')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_3');                        
                                $fileName = 'visit_card_image_3'.'.'.$extension;
                                Input::file('visit_card_image_3')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_3 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Primary Image Fail To Upload','');
                            }
                        }                   
                    }                   
                    if((Input::hasFile('visit_card_image_4'))){
                        if(Input::file('visit_card_image_4')->isValid()){ 
                            $extension = Input::file('visit_card_image_4')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_4');                        
                                $fileName = 'visit_card_image_4'.'.'.$extension;
                                Input::file('visit_card_image_4')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_4 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Primary Image Fail To Upload','');
                            }
                        }                   
                    }                   

                    if((Input::hasFile('visit_card_image_5'))){
                        if(Input::file('visit_card_image_5')->isValid()){ 
                            $extension = Input::file('visit_card_image_5')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_5');                        
                                $fileName = 'visit_card_image_5'.'.'.$extension;
                                Input::file('visit_card_image_5')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_5 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Primary Image Fail To Upload','');
                            }
                        }                   
                    }                   
                   
                   $meeting_insert_array['user_id'] = $user_info['id'];
                   $meeting_insert_array['customer_id'] = $cust_id->id;
                   $meeting_insert_array['brand_id'] = $input_all['brand_id'];
                   $meeting_insert_array['purpose_id'] = $input_all['purpose_id'];               
                   $meeting_insert_array['extra_note'] = $input_all['other_note'];
                   $meeting_insert_array['created_on'] = date('Y-m-d H:i:s');
                   if($meeting_id = Meeting::create($meeting_insert_array))
                   {
                       
                        $gps_data_insert_array = array();
                        $gps_data_insert_array['user_id'] = $user_info['id'];
                        $gps_data_insert_array['gps_data_type'] = 1;
                        $gps_data_insert_array['meeting_id'] = $meeting_id->id;
                        $gps_data_insert_array['latitude'] = $input_all['latitude'];
                        $gps_data_insert_array['longitude'] = $input_all['longitude'];
                        //print_r($insert_array);exit;
                        $gps_data_insert_array['created_on'] = date('Y-m-d H:i:s');
                        GpsData::create($gps_data_insert_array);
                           
                           
                       if($input_all['followup_reminder'] == 'yes'){
                           $followup_meeting_insert_array = array();
                           $followup_meeting_insert_array['user_id'] = $user_info['id'];
                           $followup_meeting_insert_array['customer_id'] = $cust_id->id;
                           $export_date = explode('-', $input_all['followup_date']);
                           $f_date = $export_date[2].'-'.$export_date[1].'-'.$export_date[0];
                           $followup_meeting_insert_array['followup_date_time'] = $f_date.' '.$input_all['followup_time'];
                           $followup_meeting_insert_array['extra_note'] = $input_all['followup_note'];
                           $followup_meeting_insert_array['created_on'] = date('Y-m-d H:i:s');
                           FollowupMeeting::create($followup_meeting_insert_array);
                       }
                       return Helper::response_api(true,'Log Meeting Entry Success','');
                   }else{
                        return Helper::response_api(false,'Log Meeting Entry Failed','');                       
                   }
               }else{
                   return Helper::response_api(false,'Log Meeting Entry Failed','');
               }
               
               
               
            /*validation end*/ 
//               vcustomer_type_id
//               region_id
//               	area_id
//               delete_status
//               disable_status
                               
    }

    public function customer_edit_entry()
    {
        
             $input_all = Input::all();
             $user_info = (Helper::get_user_info($input_all['token']));

            /*validation start*/
               $rules = array(                   
                    //"token" => 'required',
                    "customer_id" => 'required|numeric',
                    "brand_id" => 'required',
                    "purpose_id" => 'required',                    
                    "latitude" => 'required',
                    "longitude" => 'required',
                    "customer_edit" => 'required',
                    "followup_reminder" => 'required',
                );        
               if(Helper::validation_error_response($input_all,$rules) != false)
                    return Helper::validation_error_response($input_all,$rules);                
             
                if($input_all['followup_reminder'] == 'yes'){
                    $rules = array(                   
                        "followup_date" => 'required|date_format:"d-m-Y"',
                        "followup_time" => 'required',                   
                        "followup_reminder" => 'required',                   
                    );        
                   if(Helper::validation_error_response($input_all,$rules) != false)
                        return Helper::validation_error_response($input_all,$rules);                    
               }               
               
             if($input_all['customer_edit'] == 'yes'){
                    /*validation start*/
                       $rules = array(
                            //"token" => 'required',
                            "customer_id" => 'required|numeric',
                            "customer_name" => 'required|alpha_spaces',
                            "business_name" => 'required',
                            "business_address" => 'required',
                            "primary_contact_name" => 'required|alpha_spaces',                    
                            "secondary_contact_no"  => 'required|numeric',
                            "customer_email" => 'required|email',
                            "customer_phone"  => 'required|numeric',                    
                            "brand_id" => 'required',
                            "purpose_id" => 'required',                    
                            "latitude" => 'required',
                            "longitude" => 'required',
                            "customer_birth_date" => 'required|date_format:"d-m-Y"',
                            "customer_anniversary_date" => 'required|date_format:"d-m-Y"',                   
                        );        
                       if(Helper::validation_error_response($input_all,$rules) != false)
                            return Helper::validation_error_response($input_all,$rules);   

                       if((Input::hasFile('primary_image'))){
                               if((Input::file('primary_image')->isValid())){
                                    $extension = Input::file('primary_image')->getClientOriginalExtension();
                                    if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                                        return Helper::response_api(false,'Primary Image Fail To Upload','');                        
                                    }                                      
                               }else{
                                        return Helper::response_api(false,'Primary Image Fail To Upload','');                                           
                               }
                       }

                       if((Input::hasFile('visit_card_image_1'))){
                           if((Input::file('visit_card_image_1')->isValid())){
                                $extension = Input::file('visit_card_image_1')->getClientOriginalExtension();
                                if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                                    return Helper::response_api(false,'Visit Card Image 1 Fail To Upload','');                        
                                }                                      
                           }else{
                                    return Helper::response_api(false,'Visit Card Image 1 Fail To Upload','');                                           
                           }
                       }

                       if((Input::hasFile('visit_card_image_2'))){
                           if((Input::file('visit_card_image_2')->isValid())){
                                $extension = Input::file('visit_card_image_2')->getClientOriginalExtension();
                                if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                                    return Helper::response_api(false,'Visit Card Image 2 Fail To Upload','');                        
                                }                                      
                           }else{
                                    return Helper::response_api(false,'Visit Card Image 2 Fail To Upload','');                                           
                           }
                       }

                       if((Input::hasFile('visit_card_image_3'))){
                           if((Input::file('visit_card_image_3')->isValid())){
                                $extension = Input::file('visit_card_image_3')->getClientOriginalExtension();
                                if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                                    return Helper::response_api(false,'Visit Card Image 3 Fail To Upload','');                        
                                }                                      
                           }else{
                                    return Helper::response_api(false,'Visit Card Image 3 Fail To Upload','');                                           
                           }
                       }

                       if((Input::hasFile('visit_card_image_4'))){
                           if((Input::file('visit_card_image_4')->isValid())){
                                $extension = Input::file('visit_card_image_4')->getClientOriginalExtension();
                                if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                                    return Helper::response_api(false,'Visit Card Image 4 Fail To Upload','');                        
                                }                                      
                           }else{
                                    return Helper::response_api(false,'Visit Card Image 4 Fail To Upload','');                                           
                           }
                       }

                       if((Input::hasFile('visit_card_image_5'))){
                           if((Input::file('visit_card_image_5')->isValid())){
                                $extension = Input::file('visit_card_image_5')->getClientOriginalExtension();
                                if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                                    return Helper::response_api(false,'Visit Card Image 5 Fail To Upload','');                        
                                }                                      
                           }else{
                                    return Helper::response_api(false,'Visit Card Image 5 Fail To Upload','');                                           
                           }
                       }


                       $insert_array = array();
                       $meeting_insert_array = array();
                       $insert_array['customer_name'] = $input_all['customer_name'];
                       //$insert_array['customer_id'] = $input_all['customer_id'];
                       $insert_array['business_name'] = $input_all['business_name'];
                       $insert_array['business_address'] = $input_all['business_address'];
                       $insert_array['primary_contact_name'] = $input_all['primary_contact_name'];





                       $insert_array['secondary_contact_no'] = $input_all['secondary_contact_no'];
                       $insert_array['customer_email'] = $input_all['customer_email'];
                       $insert_array['customer_phone'] = $input_all['customer_phone'];
                       //$insert_array['customer_type_id'] = 1;
                       $insert_array['customer_birth_date'] = date('Y-m-d',strtotime($input_all['customer_birth_date']));
                       $insert_array['customer_anniversary_date'] = date('Y-m-d',strtotime($input_all['customer_anniversary_date']));
                       $insert_array['information_enter_by'] = $user_info['id'];
                       $insert_array['region_id'] = $user_info['region_id'];
                       $insert_array['area_id'] = $user_info['area_id'];
                       $insert_array['delete_status'] = 0;
                       $insert_array['disable_status'] = 0;

                       Customer::where('id',$input_all['customer_id'])->update($insert_array);

             }
            
               
               $cust_id = Customer::find($input_all['customer_id']);

               if($cust_id){
                   if((Input::hasFile('primary_image'))){
                       if(Input::file('primary_image')->isValid()){ 
                            $extension = Input::file('primary_image')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('primary_image');                        
                                $fileName = 'primary_image_1'.'.'.$extension;
                                Input::file('primary_image')->move($destinationPath, $fileName);                        
                                $cust_id->primary_image = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Primary Image Fail To Upload','');
                            }
                        }                   
                    }

                    if((Input::hasFile('visit_card_image_1'))){
                        if(Input::file('visit_card_image_1')->isValid()){ 
                            $extension = Input::file('visit_card_image_1')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_1');                        
                                $fileName = 'visit_card_image_1'.'.'.$extension;
                                Input::file('visit_card_image_1')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_1 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Primary Image Fail To Upload','');
                            }
                        }   
                    }   
             
                     if((Input::hasFile('visit_card_image_2'))){
                        if(Input::file('visit_card_image_2')->isValid()){ 
                            $extension = Input::file('visit_card_image_2')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_2');                        
                                $fileName = 'visit_card_image_2'.'.'.$extension;
                                Input::file('visit_card_image_2')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_2 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Primary Image Fail To Upload','');
                            }
                        }                   
                    }                   
             
                    if((Input::hasFile('visit_card_image_3'))){
                        if(Input::file('visit_card_image_3')->isValid()){ 
                            $extension = Input::file('visit_card_image_3')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_3');                        
                                $fileName = 'visit_card_image_3'.'.'.$extension;
                                Input::file('visit_card_image_3')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_3 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Primary Image Fail To Upload','');
                            }
                        }                   
                    }                   
                    if((Input::hasFile('visit_card_image_4'))){
                        if(Input::file('visit_card_image_4')->isValid()){ 
                            $extension = Input::file('visit_card_image_4')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_4');                        
                                $fileName = 'visit_card_image_4'.'.'.$extension;
                                Input::file('visit_card_image_4')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_4 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Primary Image Fail To Upload','');
                            }
                        }                   
                    }                   

                    if((Input::hasFile('visit_card_image_5'))){
                        if(Input::file('visit_card_image_5')->isValid()){ 
                            $extension = Input::file('visit_card_image_5')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_5');                        
                                $fileName = 'visit_card_image_5'.'.'.$extension;
                                Input::file('visit_card_image_5')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_5 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Primary Image Fail To Upload','');
                            }
                        }                   
                    }                   
                   $meeting_insert_array = array();
                   $meeting_insert_array['user_id'] = $user_info['id'];
                   $meeting_insert_array['customer_id'] = $cust_id->id;
                   $meeting_insert_array['brand_id'] = $input_all['brand_id'];
                   $meeting_insert_array['purpose_id'] = $input_all['purpose_id'];               
                   $meeting_insert_array['extra_note'] = $input_all['other_note'];
                   $meeting_insert_array['created_on'] = date('Y-m-d H:i:s');
                   if($meeting_id = Meeting::create($meeting_insert_array))
                   {
                       
                        $gps_data_insert_array = array();
                        $gps_data_insert_array['user_id'] = $user_info['id'];
                        $gps_data_insert_array['gps_data_type'] = 1;
                        $gps_data_insert_array['meeting_id'] = $meeting_id->id;
                        $gps_data_insert_array['latitude'] = $input_all['latitude'];
                        $gps_data_insert_array['longitude'] = $input_all['longitude'];
                        //print_r($insert_array);exit;
                        $gps_data_insert_array['created_on'] = date('Y-m-d H:i:s');
                        GpsData::create($gps_data_insert_array);
                           
                           
                       if($input_all['followup_reminder'] == 'yes'){
                           $followup_meeting_insert_array = array();
                           $followup_meeting_insert_array['user_id'] = $user_info['id'];
                           $followup_meeting_insert_array['customer_id'] = $cust_id->id;
                           $export_date = explode('-', $input_all['followup_date']);
                           $f_date = $export_date[2].'-'.$export_date[1].'-'.$export_date[0];
                           $followup_meeting_insert_array['followup_date_time'] = $f_date.' '.$input_all['followup_time'];
                           $followup_meeting_insert_array['extra_note'] = $input_all['followup_note'];
                           $followup_meeting_insert_array['created_on'] = date('Y-m-d H:i:s');
                           FollowupMeeting::create($followup_meeting_insert_array);
                       }
                       return Helper::response_api(true,'Log Meeting  Entry Success','');
                   }else{
                        return Helper::response_api(false,'Log Meeting  Entry Failed','');                       
                   }
               }else{
                   return Helper::response_api(false,'Log Meeting  Entry Failed','');
               }              
    }
    
    public function customer_view(){
             $input_all = Input::all();
             $user_info = (Helper::get_user_info($input_all['token']));    
             
            /*validation start*/
               $rules = array(                   
                    //"token" => 'required',
                    "token" => 'required',
                    "customer_id" => 'required|numeric',                    
                );        
               if(Helper::validation_error_response($input_all,$rules) != false)
                    return Helper::validation_error_response($input_all,$rules); 
               
               $customer_data = Customer::select('customer.*', 'city_name','customertype_name','region.region_name as rn','area.area_name as an','state.state_name as sn')
                        ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                        ->leftJoin('customer_type', 'customer_type.id', '=', 'customer.customer_type_id')
                        ->leftJoin('area', 'area.id', '=', 'customer.area_id')
                        ->leftJoin('city', 'city.id', '=', 'customer.city_id')
                        ->leftJoin('state', 'state.id', '=', 'customer.state_id')
                       ->where('customer.id',$input_all['customer_id'])->get()->toArray();
               if(count($customer_data) > 0){
                   $customer_return = array();
                   $customer_return['id'] = $customer_data[0]['id'];
                   $customer_return['customer_name'] = $customer_data[0]['customer_name'];
                   $customer_return['customertype_name'] = $customer_data[0]['customertype_name'];
                   $customer_return['business_name'] = $customer_data[0]['business_name'];
                   $customer_return['business_address'] = $customer_data[0]['business_address'];
                   $customer_return['primary_contact_name'] = $customer_data[0]['primary_contact_name'];
                   $customer_return['primary_image'] = ($customer_data[0]['primary_image'] != null && $customer_data[0]['primary_image'] != '') ? Input::root().'/' .$customer_data[0]['primary_image'] : '';
                   $customer_return['secondary_contact_name'] = $customer_data[0]['secondary_contact_name'];
                   $customer_return['customer_email'] = $customer_data[0]['customer_email'];
                   $customer_return['customer_phone'] = $customer_data[0]['customer_phone'];
                   $customer_return['visit_card_image_1'] = ($customer_data[0]['visit_card_image_1'] != null && $customer_data[0]['visit_card_image_1'] != '') ? Input::root().'/' .$customer_data[0]['visit_card_image_1'] : '';
                   $customer_return['visit_card_image_2'] = $customer_data[0]['visit_card_image_2'];
                   $customer_return['visit_card_image_3'] = $customer_data[0]['visit_card_image_3'];
                   $customer_return['visit_card_image_4'] = $customer_data[0]['visit_card_image_4'];
                   $customer_return['visit_card_image_5'] = $customer_data[0]['visit_card_image_5'];
                   $customer_return['customer_birth_date'] = $customer_data[0]['customer_birth_date'];
                   $customer_return['customer_anniversary_date'] = $customer_data[0]['customer_anniversary_date'];
//                   $customer_return['customer_birth_date'] = date('d-m-Y',strtotime($customer_data[0]['customer_birth_date']));
//                   $customer_return['customer_anniversary_date'] = date('d-m-Y',strtotime($customer_data[0]['customer_anniversary_date']));
                   $customer_return['area_name'] = $customer_data[0]['an'];
                   $customer_return['region_name'] = $customer_data[0]['rn'];
                   $customer_return['city_name'] = $customer_data[0]['city_name'];
                   $customer_return['pincode'] = $customer_data[0]['pincode'];
                   $customer_return['state_name'] = $customer_data[0]['sn'];
                   //print_r($customer_return);
                   //print_r($customer_data);                   
                   return Helper::response_api(true,'Customer Found',$customer_return);                   
               }else{
                   return Helper::response_api(false,'Customer Not Found','');                   
               }
             
    }
    
    public function customer_history(){
          $input_all = Input::all();
      
        /*validation start*/
           $rules = array(
                'token' => 'required'                  
            );        
           if(Helper::validation_error_response($input_all,$rules) != false)
                return Helper::validation_error_response($input_all,$rules);
        /*validation end*/    
           
        $user_info = (Helper::get_user_info($input_all['token']));   
        $past_start = date('Y-m-d');
        $past_end = date('Y-m-d',strtotime( '-15 days' ));
        //echo $past_end;exit;
        /*$past_15_meeting_array = Meeting::select(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%r') as meeting_time"),DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y') as meeting_date"),'customer.business_name','customer.customer_name','customer.id')
                                ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')                                
                                //->where(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d/%m/%Y')"),'<=',$past_start)
                                //->where(DB::raw("DATE(meeting_customer.created_on)"),'<=',$past_start)
                                //->where(DB::raw("DATE(meeting_customer.created_on)"),'>=',$past_end)
                                //->whereBetween(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d/%m/%Y')"), array($past_end, $past_start))
                                //->where('user_id',$user_info['id'])
                                ->groupby('meeting_customer.customer_id')
                                ->orderBy('meeting_customer.created_on','desc')
                                ->get()
                                ->toArray();*/
        $past_15_meeting_array = Meeting::select(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%r') as meeting_time"),DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y') as meeting_date"),'customer.business_name','customer.customer_name','customer.id','meeting_customer.brand_id as brand_ids','area.area_name as carea','region.region_name as cregion','state.state_name as cstate','city_name')
                                ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')                                
                                ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                                ->leftJoin('area', 'area.id', '=', 'customer.area_id')                
                                ->leftJoin('state', 'state.id', '=', 'customer.state_id')       								                
                                ->leftJoin('city', 'city.id', '=', 'customer.city_id')
                                ->groupby('meeting_customer.customer_id')
                                ->orderBy('meeting_customer.created_on','desc')
                                ->get()
                                ->toArray();
        for($i=0;$i<count($past_15_meeting_array);$i++){
            $brand_ids = Brand::select('*')->wherein('id',  explode(',', $past_15_meeting_array[$i]['brand_ids']))->get()->toArray();
                $br_text = array();
                foreach($brand_ids as $br){
                    $br_text[] = $br['brand_name'];
                }
                $past_15_meeting_array[$i]['brand_name'] = implode(', ', $br_text);

        }        
        //print_r($last_query ) ;exit;
    
        
        if(count($past_15_meeting_array) > 0 ){
            return Helper::response_api(true,'Customer Found',$past_15_meeting_array);                                
        }else{
            return Helper::response_api(false,'No Customer Found','');                                
        }
        
    }
    
    public function customer_history_detail(){
          $input_all = Input::all();
      
        /*validation start*/
           $rules = array(
                'token' => 'required',                  
                'customer_id' => 'required'                  
            );        
           if(Helper::validation_error_response($input_all,$rules) != false)
                return Helper::validation_error_response($input_all,$rules);
        /*validation end*/    
           
        $user_info = (Helper::get_user_info($input_all['token']));   
        /*$past_my_meeting_array = Meeting::select(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y') as meeting_date"),DB::raw("DATE_FORMAT(meeting_customer.created_on,'%H:%i:%s') as meeting_time"),'customer.customer_name','customer.id','user.user_name','meeting_customer.extra_note as meeting_note')
                                ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')                                
                                ->leftJoin('user', 'user.id', '=', 'meeting_customer.user_id')                                
                                ->where('customer_id',$input_all['customer_id'])
                                ->orderBy('meeting_customer.created_on','desc')
                                ->paginate(3)
                                //->get()
                                //->take(3)
                                ->toArray();*/
        $past_my_meeting_array = Meeting::select(DB::raw("DATE_FORMAT(meeting_customer.created_on,'%d-%m-%Y') as meeting_date"),DB::raw("DATE_FORMAT(meeting_customer.created_on,'%r') as meeting_time"),'customer.customer_name','customer.id','user.user_name','meeting_customer.extra_note as meeting_note','meeting_customer.brand_id as brand_ids','area.area_name as carea','region.region_name as cregion','state.state_name as cstate','purpose_name','meeting_other_purpose')
                                ->leftJoin('customer', 'customer.id', '=', 'meeting_customer.customer_id')                                
                                ->leftJoin('user', 'user.id', '=', 'meeting_customer.user_id')   
                                ->leftJoin('region', 'region.id', '=', 'customer.region_id')
                                ->leftJoin('meeting_purpose', 'meeting_purpose.id', '=', 'meeting_customer.purpose_id')
                                ->leftJoin('area', 'area.id', '=', 'customer.area_id')                
                                ->leftJoin('state', 'state.id', '=', 'customer.state_id')       								
                                ->where('customer_id',$input_all['customer_id'])
                                ->orderBy('meeting_customer.created_on','desc')
                                ->paginate(3)
                                //->get()
                                //->take(3)
                                ->toArray();
        
        for($i=0;$i<count($past_my_meeting_array['data']);$i++){
            $brand_ids = Brand::select('*')->wherein('id',  explode(',', $past_my_meeting_array['data'][$i]['brand_ids']))->get()->toArray();
                $br_text = array();
                foreach($brand_ids as $br){
                    $br_text[] = $br['brand_name'];
                }
                $past_my_meeting_array['data'][$i]['brand_name'] = implode(', ', $br_text);

        }        
    
        
        if(count($past_my_meeting_array) > 0){
            return Helper::response_api(true,'History Found',$past_my_meeting_array);                                
        }else{
            return Helper::response_api(false,'No History Found','');                                
        }
        
    }
    
    public function customer_add(){
             $input_all = Input::all();
             $user_info = (Helper::get_user_info($input_all['token']));

            /*validation start*/
               $rules = array(                   
                    "token" => 'required',
                    "customer_name" => 'required|alpha_spaces',
                    "business_name" => 'required',
                    "business_address" => 'required',
                    "area_id" => 'required',
                    "state_id" => 'required',
                    "city_id" => 'required',
                    "pincode" => 'required|numeric',
                    "region_id" => 'required',
                    "customertype_id" => 'required',
                   // "primary_contact_name" => 'required|alpha_spaces',
                   // "primary_image"  => 'required',
                    "secondary_contact_name"  => 'alpha_spaces',
                    "customer_email" => 'required|email',
                    "customer_phone"  => 'required|numeric',                    
                   // "visit_card_image_1" => 'required',
                   "customer_birth_date" => 'only_date_month',
                   "customer_anniversary_date" => 'only_date_month',
                );        
               if(Helper::validation_error_response($input_all,$rules) != false)
                    return Helper::validation_error_response($input_all,$rules);   
               
               if((Input::hasFile('primary_image'))){
                   if((Input::file('primary_image')->isValid())){
                        $extension = Input::file('primary_image')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Primary Image Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Primary Image Fail To Upload','');                                           
                   }               
               }
               
               
              if((Input::hasFile('visit_card_image_1'))){
                  if((Input::file('visit_card_image_1')->isValid())){
                        $extension = Input::file('visit_card_image_1')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Visit Card Image 1 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Visit Card Image 1 Fail To Upload','');                                           
                   }               
               }
               
if((Input::hasFile('visit_card_image_2'))){
                   if((Input::file('visit_card_image_2')->isValid())){
                        $extension = Input::file('visit_card_image_2')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Visit Card Image 2 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Visit Card Image 2 Fail To Upload','');                                           
                   }
               }
              
               if((Input::hasFile('visit_card_image_3'))){
                   if((Input::file('visit_card_image_3')->isValid())){
                        $extension = Input::file('visit_card_image_3')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Visit Card Image 3 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Visit Card Image 3 Fail To Upload','');                                           
                   }
               }
             
               if((Input::hasFile('visit_card_image_4'))){
                   if((Input::file('visit_card_image_4')->isValid())){
                        $extension = Input::file('visit_card_image_4')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Visit Card Image 4 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Visit Card Image 4 Fail To Upload','');                                           
                   }
               }

               if((Input::hasFile('visit_card_image_5'))){
                   if((Input::file('visit_card_image_5')->isValid())){
                        $extension = Input::file('visit_card_image_5')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Visit Card Image 5 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Visit Card Image 5 Fail To Upload','');                                           
                   }
               }
               
               $insert_array = array();
               $insert_array['customer_name'] = $input_all['customer_name'];
               $insert_array['business_name'] = $input_all['business_name'];
               $insert_array['business_address'] = $input_all['business_address'];
               //$insert_array['primary_contact_name'] = $input_all['primary_contact_name'];
               if(!empty($input_all['secondary_contact_name']))
               $insert_array['secondary_contact_name'] = $input_all['secondary_contact_name'];
               
               $insert_array['customer_email'] = $input_all['customer_email'];
               $insert_array['customer_phone'] = $input_all['customer_phone'];
               $insert_array['customer_type_id'] = $input_all['customertype_id'];
               $insert_array['customer_birth_date'] = $input_all['customer_birth_date'];
               $insert_array['customer_anniversary_date'] = $input_all['customer_anniversary_date'];
//               $insert_array['customer_birth_date'] = date('Y-m-d',strtotime($input_all['customer_birth_date']));
//               $insert_array['customer_anniversary_date'] = date('Y-m-d',strtotime($input_all['customer_anniversary_date']));
               $insert_array['information_enter_by'] = $user_info['id'];
               $insert_array['region_id'] = $input_all['region_id'];
               $insert_array['state_id'] = $input_all['state_id'];
               $insert_array['city_id'] = $input_all['city_id'];
               $insert_array['pincode'] = $input_all['pincode'];
               $insert_array['area_id'] = $input_all['area_id'];
               $insert_array['delete_status'] = 0;
               $insert_array['disable_status'] = 0;   
               $insert_array['created_on'] = date('Y-m-d H:i:s');

               if($cust_id = Customer::create($insert_array)){
                   
                   if((Input::hasFile('primary_image'))){
                       if(Input::file('primary_image')->isValid()){ 
                            $extension = Input::file('primary_image')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('primary_image');                        
                                $fileName = 'primary_image_1'.'.'.$extension;
                                Input::file('primary_image')->move($destinationPath, $fileName);                        
                                $cust_id->primary_image = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Primary Image Fail To Upload','');
                            }
                        }                   
                    }                   

                    if((Input::hasFile('visit_card_image_1'))){
                        if(Input::file('visit_card_image_1')->isValid()){ 
                            $extension = Input::file('visit_card_image_1')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_1');                        
                                $fileName = 'visit_card_image_1'.'.'.$extension;
                                Input::file('visit_card_image_1')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_1 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Visit Card 1 Image Fail To Upload','');
                            }
                        }                   
                    }                   
             
                     if((Input::hasFile('visit_card_image_2'))){
                        if(Input::file('visit_card_image_2')->isValid()){ 
                            $extension = Input::file('visit_card_image_2')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_2');                        
                                $fileName = 'visit_card_image_2'.'.'.$extension;
                                Input::file('visit_card_image_2')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_2 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Visit Card 2 Image Fail To Upload','');
                            }
                        }                   
                    }                   
             
                    if((Input::hasFile('visit_card_image_3'))){
                        if(Input::file('visit_card_image_3')->isValid()){ 
                            $extension = Input::file('visit_card_image_3')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_3');                        
                                $fileName = 'visit_card_image_3'.'.'.$extension;
                                Input::file('visit_card_image_3')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_3 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Visit Card 3 Image Fail To Upload','');
                            }
                        }                   
                    }                   
                    if((Input::hasFile('visit_card_image_4'))){
                        if(Input::file('visit_card_image_4')->isValid()){ 
                            $extension = Input::file('visit_card_image_4')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_4');                        
                                $fileName = 'visit_card_image_4'.'.'.$extension;
                                Input::file('visit_card_image_4')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_4 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Visit Card 4 Image Fail To Upload','');
                            }
                        }                   
                    }                   

                    if((Input::hasFile('visit_card_image_5'))){
                        if(Input::file('visit_card_image_5')->isValid()){ 
                            $extension = Input::file('visit_card_image_5')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_5');                        
                                $fileName = 'visit_card_image_5'.'.'.$extension;
                                Input::file('visit_card_image_5')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_5 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Visit Card 5 Image Fail To Upload','');
                            }
                        }                   
                    }               
                $customer_data = Customer::find($cust_id->id);
                return Helper::response_api(true,'Customer Entry Success',$customer_data);
        }else{
                return Helper::response_api(false,'Customer Entry Failed','');            
        }
               
               
    }

    public function customer_edit(){
             $input_all = Input::all();
             $user_info = (Helper::get_user_info($input_all['token']));

            /*validation start*/
               $rules = array(                   
                    "token" => 'required',
                    "customer_id" => 'required|numeric',
                    "customer_name" => 'required|alpha_spaces',
                    "business_name" => 'required',
                    "business_address" => 'required',
                    "area_id" => 'required',
                    "state_id" => 'required',
                    "city_id" => 'required',
                    "pincode" => 'required|numeric',
                    "customertype_id" => 'required',
                    "region_id" => 'required',
                    //"primary_contact_name" => 'required|alpha_spaces',
                    //"primary_image"  => 'required',
                    "secondary_contact_name" => 'alpha_spaces',                    
                    "customer_email" => 'required|email',
                    "customer_phone"  => 'required|numeric',                    
                   "customer_birth_date" => 'only_date_month',
                   "customer_anniversary_date" => 'only_date_month',                   
                    //"visit_card_image_1" => 'required',
                    //"customer_birth_date" => 'required|date_format:"d-m-Y"',
                    //"customer_anniversary_date" => 'required|date_format:"d-m-Y"',                   
                );        
               if(Helper::validation_error_response($input_all,$rules) != false)
                    return Helper::validation_error_response($input_all,$rules);   
               
               if((Input::hasFile('primary_image'))){
                   if((Input::file('primary_image')->isValid())){
                        $extension = Input::file('primary_image')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Primary Image Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Primary Image Fail To Upload','');                                           
                   }               
               }
               
               
              if((Input::hasFile('visit_card_image_1'))){
                  if((Input::file('visit_card_image_1')->isValid())){
                        $extension = Input::file('visit_card_image_1')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Visit Card Image 1 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Visit Card Image 1 Fail To Upload','');                                           
                   }               
               }
               
if((Input::hasFile('visit_card_image_2'))){
                   if((Input::file('visit_card_image_2')->isValid())){
                        $extension = Input::file('visit_card_image_2')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Visit Card Image 2 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Visit Card Image 2 Fail To Upload','');                                           
                   }
               }
              
               if((Input::hasFile('visit_card_image_3'))){
                   if((Input::file('visit_card_image_3')->isValid())){
                        $extension = Input::file('visit_card_image_3')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Visit Card Image 3 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Visit Card Image 3 Fail To Upload','');                                           
                   }
               }
             
               if((Input::hasFile('visit_card_image_4'))){
                   if((Input::file('visit_card_image_4')->isValid())){
                        $extension = Input::file('visit_card_image_4')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Visit Card Image 4 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Visit Card Image 4 Fail To Upload','');                                           
                   }
               }

               if((Input::hasFile('visit_card_image_5'))){
                   if((Input::file('visit_card_image_5')->isValid())){
                        $extension = Input::file('visit_card_image_5')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Visit Card Image 5 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Visit Card Image 5 Fail To Upload','');                                           
                   }
               }
               
               $insert_array = array();
               $insert_array['customer_name'] = $input_all['customer_name'];
               $insert_array['business_name'] = $input_all['business_name'];
               $insert_array['business_address'] = $input_all['business_address'];
               //$insert_array['primary_contact_name'] = $input_all['primary_contact_name'];
               
               if(!empty($input_all['secondary_contact_name']))
               $insert_array['secondary_contact_name'] = $input_all['secondary_contact_name'];
               
               //$insert_array['secondary_contact_name'] = $input_all['secondary_contact_name'];
               $insert_array['customer_email'] = $input_all['customer_email'];
               $insert_array['customer_phone'] = $input_all['customer_phone'];
               $insert_array['area_id'] = $input_all['area_id'];
               $insert_array['state_id'] = $input_all['state_id'];
               $insert_array['city_id'] = $input_all['city_id'];
               $insert_array['pincode'] = $input_all['pincode'];
               $insert_array['region_id'] = $input_all['region_id'];
               $insert_array['customer_type_id'] = $input_all['customertype_id'];
               $insert_array['customer_birth_date'] = $input_all['customer_birth_date'];
               $insert_array['customer_anniversary_date'] = $input_all['customer_anniversary_date'];
//               $insert_array['customer_birth_date'] = date('Y-m-d',strtotime($input_all['customer_birth_date']));
//               $insert_array['customer_anniversary_date'] = date('Y-m-d',strtotime($input_all['customer_anniversary_date']));
               //$insert_array['information_enter_by'] = $user_info['id'];
               //$insert_array['region_id'] = $user_info['region_id'];
               //$insert_array['area_id'] = $user_info['area_id'];
               //$insert_array['delete_status'] = 0;
               //$insert_array['disable_status'] = 0;   
               Customer::where('id',$input_all['customer_id'])->update($insert_array);
               $cust_id = Customer::find($input_all['customer_id']);
               
               if($cust_id){
                   
                   if((Input::hasFile('primary_image'))){
                       if(Input::file('primary_image')->isValid()){ 
                            $extension = Input::file('primary_image')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('primary_image');                        
                                $fileName = 'primary_image_1'.'.'.$extension;
                                Input::file('primary_image')->move($destinationPath, $fileName);                        
                                $cust_id->primary_image = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Primary Image Fail To Upload','');
                            }
                        }                   
                    }                   

                    if((Input::hasFile('visit_card_image_1'))){
                        if(Input::file('visit_card_image_1')->isValid()){ 
                            $extension = Input::file('visit_card_image_1')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_1');                        
                                $fileName = 'visit_card_image_1'.'.'.$extension;
                                Input::file('visit_card_image_1')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_1 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Visit Card 1 Image Fail To Upload','');
                            }
                        }                   
                    }                   
             
                     if((Input::hasFile('visit_card_image_2'))){
                        if(Input::file('visit_card_image_2')->isValid()){ 
                            $extension = Input::file('visit_card_image_2')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_2');                        
                                $fileName = 'visit_card_image_2'.'.'.$extension;
                                Input::file('visit_card_image_2')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_2 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Visit Card 2 Image Fail To Upload','');
                            }
                        }                   
                    }                   
             
                    if((Input::hasFile('visit_card_image_3'))){
                        if(Input::file('visit_card_image_3')->isValid()){ 
                            $extension = Input::file('visit_card_image_3')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_3');                        
                                $fileName = 'visit_card_image_3'.'.'.$extension;
                                Input::file('visit_card_image_3')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_3 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Visit Card 3 Image Fail To Upload','');
                            }
                        }                   
                    }                   
                    if((Input::hasFile('visit_card_image_4'))){
                        if(Input::file('visit_card_image_4')->isValid()){ 
                            $extension = Input::file('visit_card_image_4')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_4');                        
                                $fileName = 'visit_card_image_4'.'.'.$extension;
                                Input::file('visit_card_image_4')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_4 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Visit Card 4 Image Fail To Upload','');
                            }
                        }                   
                    }                   

                    if((Input::hasFile('visit_card_image_5'))){
                        if(Input::file('visit_card_image_5')->isValid()){ 
                            $extension = Input::file('visit_card_image_5')->getClientOriginalExtension();
                            if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                $destinationPath = 'upload_images/customer_'.$cust_id->id; 
                                $image = Input::file('visit_card_image_5');                        
                                $fileName = 'visit_card_image_5'.'.'.$extension;
                                Input::file('visit_card_image_5')->move($destinationPath, $fileName);                        
                                $cust_id->visit_card_image_5 = $destinationPath.'/'.$fileName;
                                $cust_id->save();
                            }else{
                                return Helper::response_api(false,'Visit Card 5 Image Fail To Upload','');
                            }
                        }                   
                    }               
                $customer_data = Customer::find($cust_id->id);
                return Helper::response_api(true,'Customer Edit Success',$customer_data);
        }else{
                return Helper::response_api(false,'Customer Edit Failed','');            
        }
               
               
    }
    
    public function meeting_add(){
        $input_all = Input::all();
        $user_info = (Helper::get_user_info($input_all['token']));
        
     /*validation start*/
               $rules = array(                   
                    "token" => 'required',
                    "customer_id" => 'required|numeric',
                    //"brand_id" => 'required|numeric',
                    "purpose_id" => 'required',                    
                    "latitude" => 'required',
                    "longitude" => 'required',
                    "followup_reminder" => 'required',                   
                    "customer_name" => 'required',                   
                );  
               
               if (array_key_exists("brand_id",$input_all)){
                    foreach($input_all['brand_id'] as $key => $val){
                       $rules['brand_id.'.$key] = 'required|numeric';
                    }               
                }else{
                    $rules['brand_id'] = 'required|numeric';
                }               
               
               if($input_all['purpose_id'] == 1){
                   $rules['meeting_other_purpose'] = 'required';
               }
               
               if(Helper::validation_error_response($input_all,$rules) != false)
                    return Helper::validation_error_response($input_all,$rules);         
               
               
               if($input_all['followup_reminder'] == 'yes'){
                    $rules = array(                   
                        "followup_date" => 'required|date_format:"d-m-Y"',
                        "followup_time" => 'required',                   
                        "followup_reminder" => 'required',                   
                    );        
                   if(Helper::validation_error_response($input_all,$rules) != false)
                        return Helper::validation_error_response($input_all,$rules);                    
               }
               
               
               if((Input::hasFile('meeting_image_1'))){
                   if((Input::file('meeting_image_1')->isValid())){
                        $extension = Input::file('meeting_image_1')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Image 1 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Image 1 Fail To Upload','');                                           
                   }               
               }
               
               if((Input::hasFile('meeting_image_2'))){
                   if((Input::file('meeting_image_2')->isValid())){
                        $extension = Input::file('meeting_image_2')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Image 2 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Image 2 Fail To Upload','');                                           
                   }               
               }
               
               if((Input::hasFile('meeting_image_3'))){
                   if((Input::file('meeting_image_3')->isValid())){
                        $extension = Input::file('meeting_image_3')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Image 3 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Image 3 Fail To Upload','');                                           
                   }               
               }
               
               if((Input::hasFile('meeting_image_4'))){
                   if((Input::file('meeting_image_4')->isValid())){
                        $extension = Input::file('meeting_image_4')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Image 4 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Image 4 Fail To Upload','');                                           
                   }               
               }
               
               if((Input::hasFile('meeting_image_5'))){
                   if((Input::file('meeting_image_5')->isValid())){
                        $extension = Input::file('meeting_image_5')->getClientOriginalExtension();
                        if(!($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif')){
                            return Helper::response_api(false,'Image 5 Fail To Upload','');                        
                        }                                      
                   }else{
                            return Helper::response_api(false,'Image 5 Fail To Upload','');                                           
                   }               
               }               
               
               $meeting_insert_array = array();
               $meeting_insert_array['user_id'] = $user_info['id'];
               $meeting_insert_array['customer_id'] = $input_all['customer_id'];
               $meeting_insert_array['brand_id'] = implode(', ', $input_all['brand_id']);
               //$meeting_insert_array['brand_id'] = $input_all['brand_id'];
               $meeting_insert_array['purpose_id'] = $input_all['purpose_id'];               
               $meeting_insert_array['extra_note'] = $input_all['other_note'];               
               $meeting_insert_array['meeting_other_purpose'] = $input_all['meeting_other_purpose'];               
               $meeting_insert_array['customer_name'] = $input_all['customer_name'];               
               
               
               if($user_info['user_type'] == 1){
                $get_region_sales_manager_id = User::select('*')->where('user_type',1)->where('region_id',$user_info['region_id'])->where('disable_status',0)->where('delete_status',0)->get()->toArray();
                $meeting_insert_array['region_sales_manager_id'] = $get_region_sales_manager_id[0]['id'];
                //$meeting_insert_array['area_sales_manager_id'] = $get_area_sales_manager_id[0]['id'];                   
               }
               if($user_info['user_type'] == 2){
                            $get_region_sales_manager_id = User::select('*')->where('user_type',1)->where('region_id',$user_info['region_id'])->where('disable_status',0)->where('delete_status',0)->get()->toArray();
                            $get_area_sales_manager_id = User::select('*')->where('user_type',2)->where('region_id',$user_info['region_id'])->where('area_id',$user_info['area_id'])->where('disable_status',0)->where('delete_status',0)->get()->toArray();
                            //print_r($get_area_sales_manager_id);exit;
                            $meeting_insert_array['region_sales_manager_id'] = $get_region_sales_manager_id[0]['id'];
                            $meeting_insert_array['area_sales_manager_id'] = $get_area_sales_manager_id[0]['id'];                     
               }
               
                if($user_info['user_type'] == 3){
                       if($user_info['assign_to_user'] != null && $user_info['assign_to_user'] != ''){
                           $get_region_sales_manager_id = User::select('*')->where('user_type',1)->where('region_id',$user_info['region_id'])->where('disable_status',0)->where('delete_status',0)->get()->toArray();
                           //$get_area_sales_manager_id = User::select('*')->where('user_type',2)->where('region_id',$user_info['region_id'])->where('area_id',$user_info['area_id'])->where('disable_status',0)->where('delete_status',0)->get()->toArray();
                           //print_r($get_area_sales_manager_id);exit;
                           $meeting_insert_array['region_sales_manager_id'] = $get_region_sales_manager_id[0]['id'];
                           $meeting_insert_array['area_sales_manager_id'] = $user_info['assign_to_user'];

                       }                     
                }               
               
               //$meeting_insert_array['created_on'] = date('Y-m-d H:i:s');
               $meeting_insert_array['created_on'] = date('Y-m-d H:i:s',time());
               

              if($meeting_id = Meeting::create($meeting_insert_array))
                   {
                  
                  
                        if((Input::hasFile('meeting_image_1'))){
                            if(Input::file('meeting_image_1')->isValid()){ 
                                $extension = Input::file('meeting_image_1')->getClientOriginalExtension();
                                if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                    $destinationPath = 'upload_images/customer_'.$input_all['customer_id']; 
                                    $image = Input::file('meeting_image_1');                        
                                    $fileName = 'meeting_image_1_'.$meeting_id->id.'.'.$extension;
                                    Input::file('meeting_image_1')->move($destinationPath, $fileName);                        
                                    $meeting_id->meeting_image_1 = $destinationPath.'/'.$fileName;
                                    $meeting_id->save();
                                }else{
                                    return Helper::response_api(false,'Primary Image Fail To Upload','');
                                }
                            }                   
                        }  
                        if((Input::hasFile('meeting_image_2'))){
                            if(Input::file('meeting_image_2')->isValid()){ 
                                $extension = Input::file('meeting_image_2')->getClientOriginalExtension();
                                if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                    $destinationPath = 'upload_images/customer_'.$input_all['customer_id']; 
                                    $image = Input::file('meeting_image_2');                        
                                    $fileName = 'meeting_image_2_'.$meeting_id->id.'.'.$extension;
                                    Input::file('meeting_image_2')->move($destinationPath, $fileName);                        
                                    $meeting_id->meeting_image_2 = $destinationPath.'/'.$fileName;
                                    $meeting_id->save();
                                }else{
                                    return Helper::response_api(false,'Primary Image Fail To Upload','');
                                }
                            }                   
                        }  
                        if((Input::hasFile('meeting_image_3'))){
                            if(Input::file('meeting_image_3')->isValid()){ 
                                $extension = Input::file('meeting_image_3')->getClientOriginalExtension();
                                if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                    $destinationPath = 'upload_images/customer_'.$input_all['customer_id']; 
                                    $image = Input::file('meeting_image_3');                        
                                    $fileName = 'meeting_image_3_'.$meeting_id->id.'.'.$extension;
                                    Input::file('meeting_image_3')->move($destinationPath, $fileName);                        
                                    $meeting_id->meeting_image_3 = $destinationPath.'/'.$fileName;
                                    $meeting_id->save();
                                }else{
                                    return Helper::response_api(false,'Primary Image Fail To Upload','');
                                }
                            }                   
                        }  
                        if((Input::hasFile('meeting_image_4'))){
                            if(Input::file('meeting_image_4')->isValid()){ 
                                $extension = Input::file('meeting_image_4')->getClientOriginalExtension();
                                if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                    $destinationPath = 'upload_images/customer_'.$input_all['customer_id']; 
                                    $image = Input::file('meeting_image_4');                        
                                    $fileName = 'meeting_image_4_'.$meeting_id->id.'.'.$extension;
                                    Input::file('meeting_image_4')->move($destinationPath, $fileName);                        
                                    $meeting_id->meeting_image_4 = $destinationPath.'/'.$fileName;
                                    $meeting_id->save();
                                }else{
                                    return Helper::response_api(false,'Primary Image Fail To Upload','');
                                }
                            }                   
                        }  
                        if((Input::hasFile('meeting_image_5'))){
                            if(Input::file('meeting_image_5')->isValid()){ 
                                $extension = Input::file('meeting_image_5')->getClientOriginalExtension();
                                if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png'  || $extension == 'gif'){
                                    $destinationPath = 'upload_images/customer_'.$input_all['customer_id']; 
                                    $image = Input::file('meeting_image_5');                        
                                    $fileName = 'meeting_image_5_'.$meeting_id->id.'.'.$extension;
                                    Input::file('meeting_image_5')->move($destinationPath, $fileName);                        
                                    $meeting_id->meeting_image_5 = $destinationPath.'/'.$fileName;
                                    $meeting_id->save();
                                }else{
                                    return Helper::response_api(false,'Primary Image Fail To Upload','');
                                }
                            }                   
                        }  
                  
                       
                        $gps_data_insert_array = array();
                        $gps_data_insert_array['user_id'] = $user_info['id'];
                        $gps_data_insert_array['gps_data_type'] = 1;
                        $gps_data_insert_array['meeting_id'] = $meeting_id->id;
                        $gps_data_insert_array['latitude'] = $input_all['latitude'];
                        $gps_data_insert_array['longitude'] = $input_all['longitude'];
                        //print_r($insert_array);exit;
                        $gps_data_insert_array['created_on'] = date('Y-m-d H:i:s');
                        GpsData::create($gps_data_insert_array);
                           
                           
                       if($input_all['followup_reminder'] == 'yes'){
                           $followup_meeting_insert_array = array();
                           $followup_meeting_insert_array['user_id'] = $user_info['id'];
                           $followup_meeting_insert_array['customer_id'] = $input_all['customer_id'];
                           $followup_meeting_insert_array['meeting_id'] = $meeting_id->id;
                           $export_date = explode('-', $input_all['followup_date']);
                           $f_date = $export_date[2].'-'.$export_date[1].'-'.$export_date[0];
                           $followup_meeting_insert_array['followup_date_time'] = $f_date.' '.$input_all['followup_time'];
                           $followup_meeting_insert_array['extra_note'] = $input_all['followup_note'];
                           $followup_meeting_insert_array['created_on'] = date('Y-m-d H:i:s');
                           FollowupMeeting::create($followup_meeting_insert_array);
                       }
                       return Helper::response_api(true,'You have logged the Meeting Successfully.','');
                   }else{
                        return Helper::response_api(false,'Log Meeting Entry Failed','');                       
                   }               
        
    }
    
    public function brand_list(){
        $input_all = Input::all();
        $user_info = (Helper::get_user_info($input_all['token']));
        
        if($user_info['user_type'] == 0){
            $brand_list = Brand::select('*')->where('brand.delete_status',0)->where('brand.disable_status',0)->get()->toArray();            
        }else{
            $brand_list = Brand::select('brand.*')
                            ->leftJoin('user_brand', 'user_brand.brand_id', '=', 'brand.id')
                            ->where('brand.delete_status',0)->where('brand.disable_status',0)
                            ->where('user_brand.user_id',$user_info['id'])->get()->toArray();            
        }
        
        if(count($brand_list) > 0){
             return Helper::response_api(true,'Brand List',$brand_list);
        }else{
            return Helper::response_api(false,'Brand List Not Found','');                       
        }
        
    }

    public function purpose_list(){
        $input_all = Input::all();
        $user_info = (Helper::get_user_info($input_all['token']));
        
        $purpose_list = Purpose::select('*')->where('delete_status',0)->where('disable_status',0)->get()->toArray();
        
        if(count($purpose_list) > 0){
             return Helper::response_api(true,'Purpose List',$purpose_list);
        }else{
            return Helper::response_api(false,'Purpose List Not Found','');                       
        }
        
    }

    public function my_region(){
        $input_all = Input::all();
        $user_info = (Helper::get_user_info($input_all['token']));
        
        
        $region_list = Region::select('*')->where('delete_status',0)->where('disable_status',0)->where('id',$user_info['region_id'])->get()->toArray();
        
        if(count($region_list) > 0){
             return Helper::response_api(true,'Region Data',$region_list);
        }else{
            return Helper::response_api(false,'Region Not Found','');                       
        }
        
    }

    public function my_area(){
        $input_all = Input::all();
        $user_info = (Helper::get_user_info($input_all['token']));
        
        
        $area_list = Area::select('*')->where('delete_status',0)->where('disable_status',0)->where('region_id',$user_info['region_id'])->get()->toArray();
        
        if(count($area_list) > 0){
             return Helper::response_api(true,'Area Data',$area_list);
        }else{
            return Helper::response_api(false,'Area Not Found','');                       
        }
        
    }
    
    
    public function get_all_info(){
        $input_all = Input::all();
        $user_info = (Helper::get_user_info($input_all['token']));
        
        
        $data = array();
        
        //$brand_list = Brand::select('*')->get()->toArray();

        if($user_info['user_type'] == 0){
            $brand_list = Brand::select('*')->where('brand.delete_status',0)->where('brand.disable_status',0)->get()->toArray();            
        }else{
            $brand_list = Brand::select('brand.*')
                            ->leftJoin('user_brand', 'user_brand.brand_id', '=', 'brand.id')
                            ->where('brand.delete_status',0)->where('brand.disable_status',0)
                            ->where('user_brand.user_id',$user_info['id'])->get()->toArray();            
        }        
        
        if(count($brand_list) > 0){
            $data['brand_list'] = $brand_list;             
        }else{
            $data['brand_list'] = array();
        }        
        
        
        $purpose_list = Purpose::select('*')->where('delete_status',0)->where('disable_status',0)->get()->toArray();        
        if(count($purpose_list) > 0){
            $data['purpose_list'] = $purpose_list;             
        }else{
            $data['purpose_list'] = array();
        }
        if($user_info['user_type'] == 0){
            $region_list = Region::select('*')->get()->toArray();                    
        }else{
            $region_list = Region::select('*')->where('delete_status',0)->where('disable_status',0)->where('id',$user_info['region_id'])->get()->toArray();        
        }
        if(count($region_list) > 0){
            $data['region_list'] = $region_list;             
        }else{
            $data['region_list'] = array();            
        }   
        
        $customertype_list = Customertype::select('*')->where('delete_status',0)->where('disable_status',0)->get()->toArray();        
        
        if(count($customertype_list) > 0){
            $data['customertype_list'] = $customertype_list;             
        }else{
            $data['customertype_list'] = array();            
        }   
        
        
        /*$area_list = Area::select('*')->where('region_id',$user_info['region_id'])->get()->toArray();
        if(count($area_list) > 0){
            $data['area_list'] = $area_list;                          
        }else{
            $data['area_list'] = array();
        }*/        
       
        return Helper::response_api(true,'Information List',$data);
        
    }    
    
    public function business_list(){
        $input_all = Input::all();
        $user_info = (Helper::get_user_info($input_all['token']));    
        
        if($user_info['user_type'] == 0){
        $customer_list = Customer::select('*')->where('customer.delete_status',0)->where('customer.disable_status',0)->get()->toArray();
        }else{
        $customer_list = Customer::select('*')->where('customer.delete_status',0)->where('customer.disable_status',0)->where('region_id',$user_info['region_id'])->get()->toArray();
        }
        
        $list_array = array();
        for($i=0;$i<count($customer_list);$i++){
            $list_array[$i]['business_name'] = $customer_list[$i]['business_name'];
            $list_array[$i]['customer_id'] = $customer_list[$i]['id'];
            
            $meetingcount = Meeting::select('*')->where('customer_id',$customer_list[$i]['id'])->get()->toArray();
            
            if(count($meetingcount) > 0){
                $list_array[$i]['visit_customer'] = true;
            }else{
                $list_array[$i]['visit_customer'] = false;                
            }
            
            $list_array[$i]['customer_name'][0] = $customer_list[$i]['customer_name'];
            if($customer_list[$i]['secondary_contact_name'] != null or $customer_list[$i]['secondary_contact_name'] != ''){
                $list_array[$i]['customer_name'][1] = $customer_list[$i]['secondary_contact_name'];    
            }
        }
        if(count($list_array) > 0){
            return Helper::response_api(true,'Business List',$list_array);            
        }else{
            return Helper::response_api(false,'Business List not found','');                        
        }
        
    }




      
}
