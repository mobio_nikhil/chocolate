<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Input;
use Validator;
use App\Helpers\Helper;
use App\GpsData;
use App\Meeting;
use App\Area;
use App\City;
use App\State;
use App\Customer;
use App\Region;
use DB;
use Redirect;
use Config;
use Session;
use Response;
use Form;
use View;


class AdminAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $area = Area::select('area.id','area.area_name','region.region_name','area.created_on','area.user_id','area.disable_status as disable_status','area.disable_reason as disable_reason')->where('area.delete_status',0)
            ->leftJoin('region','region.id','=','area.region_id')
            ->orderby('region.region_name','area.area_name')->get()->toArray();
        $data = array();
        $data['area_list'] = $area;        
        // print_r($data);
        return view('pages/area/index',$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $region = Region::select('*')->get()->toArray();
        return view('pages/area/create')
             ->with('region',$region);         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //public function store(Request $request)
    public function store()
    {
                $input_all = Input::all();
        /*validation start*/
                $deletevalue = 1;
           $rules = array(
                'area_name' => 'required||unique:area,area_name,'.$deletevalue.',delete_status,region_id,'.$input_all['region_id']
            );        
           
            $validator = Validator::make($input_all, $rules);
            // check if the validator failed -----------------------
            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to(Config::get('constants.admin_path').'area/create')
                    ->withErrors($validator)
                    ->withInput();
            }
            
            $user_info = Session::get('session_user_info');
            $input_all['user_id'] = $user_info['id'];
            $input_all['created_on'] = date('Y-m-d H:i:s');
            area::create($input_all);
            Session::flash('message', 'New Area created'); 
            //Session::flash('alert-class', 'alert-danger');             

            return Redirect::to(Config::get('constants.admin_path').'area/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $area   = Area::findOrFail($id);
        $region = Region::select('*')->get()->toArray();


            //return view('pages/area/edit')->with(compact('area'));       
        return View::make('pages.area.edit')
        ->with('area', $area)
        ->with('region', $region);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // echo $this->method();
        
                 $input_all = Input::all();
                 $deletevalue = 0;
         /*validation start*/
            $rules = array(
                 'area_name' => 'required||unique:area,area_name,'.$id.',id,delete_status,'.$deletevalue,
             );        
           
             $validator = Validator::make($input_all, $rules);
             // check if the validator failed -----------------------
             if ($validator->fails()) {
                 $messages = $validator->messages();
                 return Redirect::to(Config::get('constants.admin_path').'area/edit/'.$id)
                     ->withErrors($validator)
                     ->withInput();
             }
            
             $area = area::findOrFail($id);
             $area->area_name = $request['area_name'];
             $area->save();
             Session::flash('message', 'Area has been updated'); 
             //Session::flash('alert-class', 'alert-danger');                         
             return Redirect::to(Config::get('constants.admin_path').'area/index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $area = area::findOrFail($id);        

        $A_CustomerCount = Customer::where("area_id",$id)->count();
        $A_UserCount     = User::where("area_id",$id)->where("disable_status",0)->where("delete_status",0)->count();
        $input_array = Input::all();
        
        if($A_CustomerCount != 0 || $A_UserCount  != 0 )
        {
           return Redirect::back()->withErrors(["Area can not be deleted. It is associated with records"]);
        }


        $area->delete_status = 1;
        $area->disable_status = 1;
        $user_info = Session::get('session_user_info');
        $area->delete_by = $user_info['id'];
        $area->delete_reason = $input_array['delete_reason'];
            if($area->save()){
                Session::flash('message', 'Area deleted'); 
                //Session::flash('alert-class', 'alert-danger');                         
                return Redirect::to(Config::get('constants.admin_path').'area/index');            
            }else{
                Session::flash('message', 'area not deleted! Try again later'); 
                Session::flash('alert-class', 'alert-danger');
                return Redirect::to(Config::get('constants.admin_path').'area/index');                        
            }        
    }

    public function stateAreaApi($id)
    {
        $res = Array(
                "area"  => Area::select("area.id as id","area_name")->where("region_id",$id)->get()->toArray(),
                "state" => State::select("state.id as id","state_name")->join("region_detail","region_detail.state_id","=","state.id")->where("region_id",$id)->get()->toArray()
        );

        return Response::json($res);

    }

    public function getcitybystate($id)
    {
        $res = Array(
                "city"  => City::select("*")->where("state_id",$id)->get()->toArray()                
        );

        return Response::json($res);

    }

    public function changeStatus($status,$id) {

        // $ar = Input::all();
        // $id = $ar['id'];
        $input_array = Input::all();
        $user_info = Session::get('session_user_info');
    
        if ($status == "enable") {
            Area::where('id', $id)->update(["disable_status" => 0,'active_by'=>$user_info['id'],'disable_reason'=>NULL]);
            Session::flash('message', 'Area has been Activated');
        } elseif ($status == "disable") {
            Area::where('id', $id)->update(["disable_status" => 1,"disable_reason" => $input_array['disable_reason'],'disable_by'=>$user_info['id']]);
            Session::flash('message', 'Area has been Dectivated');
        }
        return Redirect::back();
    }
}
