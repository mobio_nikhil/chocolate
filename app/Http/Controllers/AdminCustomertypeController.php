<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Input;
use Validator;
use App\Helpers\Helper;
use App\GpsData;
use App\Meeting;
use App\Customertype;
use App\Customer;
use DB;
use Redirect;
use Config;
use Session;
use Form;
use View;


class AdminCustomertypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customertype = Customertype::select('*')->where('delete_status',0)->orderby('customertype_name')->get()->toArray();
        $data = array();
        $data['customertype_list'] = $customertype;        
        return view('pages/customertype/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages/customertype/create');         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //public function store(Request $request)
    public function store()
    {
                $input_all = Input::all();
        /*validation start*/
                $deletevalue = 1;
           $rules = array(
                //'customertype_name' => 'required||unique:meeting_customertype,customertype_name,delete_status,'.$deletevalue.',disable_status,'.$deletevalue,
                'customertype_name' => 'required||unique:customer_type,customertype_name,'.$deletevalue.',delete_status',
            );        
           
            $validator = Validator::make($input_all, $rules);
            // check if the validator failed -----------------------
            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to(Config::get('constants.admin_path').'customertype/create')
                    ->withErrors($validator)
                    ->withInput();
            }
            
            $user_info = Session::get('session_user_info');
            $input_all['user_id'] = $user_info['id'];
            $input_all['created_on'] = date('Y-m-d H:i:s');
            Customertype::create($input_all);
            Session::flash('message', 'Customertype created'); 
            //Session::flash('alert-class', 'alert-danger');                         
            return Redirect::to(Config::get('constants.admin_path').'customertype/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customertype = Customertype::findOrFail($id);
        //return view('pages/customertype/edit')->with(compact('customertype'));       
        return View::make('pages.customertype.edit')
            ->with('customertype', $customertype);
        //return view('pages/customertype/edit')->with(compact('customertype'));       
        /*$customertype_detail = Customertype::select('*')->where('id',$id)->get()->toArray();
        if(count($customertype_detail) > 0){
            return view('pages/customertype/edit',$customertype_detail);
        }else{
            Session::flash('message', 'Customertype not found'); 
            Session::flash('alert-class', 'alert-danger');                         
            return Redirect::to(Config::get('constants.admin_path').'customertype/create');
        }*/
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //echo $this->method();
        
                $input_all = Input::all();
                $deletevalue = 0;
        /*validation start*/
           $rules = array(
                //'customertype_name' => 'required||unique:meeting_customertype,customertype_name,'.$id.',id',
                //'customertype_name' => 'required||unique:meeting_customertype,customertype_name,'.$id.',id,delete_status,'.$deletevalue,
                'customertype_name' => 'required||unique:customer_type,customertype_name,'.$id.',id,delete_status,'.$deletevalue,
            );        
           
            $validator = Validator::make($input_all, $rules);
            // check if the validator failed -----------------------
            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to(Config::get('constants.admin_path').'customertype/edit/'.$id)
                    ->withErrors($validator)
                    ->withInput();
            }
            
            $customertype = Customertype::findOrFail($id);
            $customertype->customertype_name = $request['customertype_name'];
            $customertype->save();
            Session::flash('message', 'Customertype updated'); 
            //Session::flash('alert-class', 'alert-danger');                         
            return Redirect::to(Config::get('constants.admin_path').'customertype/index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customertype = Customertype::findOrFail($id);        
         $input_array = Input::all();
         
         $R_CustomerCount = Customer::where("customer_type_id",$id)->count();
        if($R_CustomerCount != 0 )
        {
           return Redirect::back()->withErrors(["Customer type cannot be deleted. It is associated with records."]);
        }         
         
        $customertype->delete_status = 1;
        $customertype->disable_status = 1;
        $user_info = Session::get('session_user_info');
        $customertype->delete_by = $user_info['id'];
        $customertype->delete_reason = $input_array['delete_reason'];          
            if($customertype->save()){
                Session::flash('message', 'Customertype deleted'); 
                //Session::flash('alert-class', 'alert-danger');                         
                return Redirect::to(Config::get('constants.admin_path').'customertype/index');            
            }else{
                Session::flash('message', 'Customertype not deleted'); 
                Session::flash('alert-class', 'alert-danger');                         
                return Redirect::to(Config::get('constants.admin_path').'customertype/index');                        
            }        
    }

      public function changeStatus($status,$id) {

        // $ar = Input::all();
        // $id = $ar['id'];
        $input_array = Input::all();
         $user_info = Session::get('session_user_info');
        if ($status == "enable") {
            Customertype::where('id', $id)->update(["disable_status" => 0,'active_by'=>$user_info['id'],'disable_reason'=>NULL]);
            Session::flash('message', 'Customertype has been Activated');
        } elseif ($status == "disable") {
            Customertype::where('id', $id)->update(["disable_status" => 1,"disable_reason" => $input_array['disable_reason'],'disable_by'=>$user_info['id']]);
            Session::flash('message', 'Customertype has been Dectivated');
        }
        return Redirect::back();
    }
}
