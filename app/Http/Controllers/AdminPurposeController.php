<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Input;
use Validator;
use App\Helpers\Helper;
use App\GpsData;
use App\Meeting;
use App\Purpose;
use DB;
use Redirect;
use Config;
use Session;
use Form;
use View;


class AdminPurposeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purpose = Purpose::select('*')->where('delete_status',0)->orderby('purpose_name')->get()->toArray();
        $data = array();
        $data['purpose_list'] = $purpose;        
        return view('pages/purpose/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages/purpose/create');         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //public function store(Request $request)
    public function store()
    {
                $input_all = Input::all();
        /*validation start*/
                $deletevalue = 1;
           $rules = array(
                //'purpose_name' => 'required||unique:meeting_purpose,purpose_name,delete_status,'.$deletevalue.',disable_status,'.$deletevalue,
                'purpose_name' => 'required||unique:meeting_purpose,purpose_name,'.$deletevalue.',delete_status',
            );        
           
            $validator = Validator::make($input_all, $rules);
            // check if the validator failed -----------------------
            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to(Config::get('constants.admin_path').'purpose/create')
                    ->withErrors($validator)
                    ->withInput();
            }
            
            $user_info = Session::get('session_user_info');
            $input_all['user_id'] = $user_info['id'];
            $input_all['created_on'] = date('Y-m-d H:i:s');
            Purpose::create($input_all);
            Session::flash('message', 'Purpose created'); 
            //Session::flash('alert-class', 'alert-danger');                         
            return Redirect::to(Config::get('constants.admin_path').'purpose/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $purpose = Purpose::findOrFail($id);
        //return view('pages/purpose/edit')->with(compact('purpose'));       
        return View::make('pages.purpose.edit')
            ->with('purpose', $purpose);
        //return view('pages/purpose/edit')->with(compact('purpose'));       
        /*$purpose_detail = Purpose::select('*')->where('id',$id)->get()->toArray();
        if(count($purpose_detail) > 0){
            return view('pages/purpose/edit',$purpose_detail);
        }else{
            Session::flash('message', 'Purpose not found'); 
            Session::flash('alert-class', 'alert-danger');                         
            return Redirect::to(Config::get('constants.admin_path').'purpose/create');
        }*/
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //echo $this->method();
        
                $input_all = Input::all();
                $deletevalue = 0;
        /*validation start*/
           $rules = array(
                //'purpose_name' => 'required||unique:meeting_purpose,purpose_name,'.$id.',id',
                //'purpose_name' => 'required||unique:meeting_purpose,purpose_name,'.$id.',id,delete_status,'.$deletevalue,
                'purpose_name' => 'required||unique:meeting_purpose,purpose_name,'.$id.',id,delete_status,'.$deletevalue,
            );        
           
            $validator = Validator::make($input_all, $rules);
            // check if the validator failed -----------------------
            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to(Config::get('constants.admin_path').'purpose/edit/'.$id)
                    ->withErrors($validator)
                    ->withInput();
            }
            
            $purpose = Purpose::findOrFail($id);
            $purpose->purpose_name = $request['purpose_name'];
            $purpose->save();
            Session::flash('message', 'Purpose updated'); 
            //Session::flash('alert-class', 'alert-danger');                         
            return Redirect::to(Config::get('constants.admin_path').'purpose/index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $purpose = Purpose::findOrFail($id);  
        $input_array = Input::all();

        
        $R_CustomerCount = Meeting::where("purpose_id",$id)->count();
        if($R_CustomerCount != 0 )
        {
           return Redirect::back()->withErrors(["Purpose cannot be deleted. It is associated with records."]);
        }           
        
        $purpose->delete_status = 1;
        $purpose->disable_status = 1;
        $user_info = Session::get('session_user_info');
        $purpose->delete_by = $user_info['id'];
        $purpose->delete_reason = $input_array['delete_reason'];        
$user_info = Session::get('session_user_info');
        $purpose->delete_by = $user_info['id'];
        $purpose->delete_reason = $input_array['delete_reason'];        
            if($purpose->save()){
                Session::flash('message', 'Purpose deleted'); 
                //Session::flash('alert-class', 'alert-danger');                         
                return Redirect::to(Config::get('constants.admin_path').'purpose/index');            
            }else{
                Session::flash('message', 'Purpose not deleted'); 
                Session::flash('alert-class', 'alert-danger');                         
                return Redirect::to(Config::get('constants.admin_path').'purpose/index');                        
            }        
    }

      public function changeStatus($status,$id) {

        // $ar = Input::all();
        // $id = $ar['id'];
$input_array = Input::all();
         $user_info = Session::get('session_user_info');    
        if ($status == "enable") {
            Purpose::where('id', $id)->update(["disable_status" => 0,'active_by'=>$user_info['id'],'disable_reason'=>NULL]);
            Session::flash('message', 'Purpose has been Activated');
        } elseif ($status == "disable") {
            Purpose::where('id', $id)->update(["disable_status" => 1,"disable_reason" => $input_array['disable_reason'],'disable_by'=>$user_info['id']]);
            Session::flash('message', 'Purpose has been Dectivated');
        }
        return Redirect::back();
    }
}
