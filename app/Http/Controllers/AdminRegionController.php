<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Input;
use Validator;
use App\Helpers\Helper;
use App\GpsData;
use App\Meeting;
use App\Customer;
use App\Region;
use App\State;
use App\RegionDetail;
use DB;
use PDO;
use Redirect;
use Config;
use Session;
use Form;
use View;


class AdminRegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $region = region::select('*')->where('delete_status',0)->orderby('region_name')->get()->toArray();
        for($i=0;$i<count($region);$i++){
            $region[$i]['state_list'] = '';
                $state_list = array();
                $u_state = array();
                DB::setFetchMode(PDO::FETCH_ASSOC);
                $state_list = DB::connection('mysql')->select("SELECT * FROM state where id in (select state_id from region_detail WHERE region_id in (select id from region where  id  = ".$region[$i]['id']."))");
                DB::setFetchMode(PDO::FETCH_CLASS);
                
                if(count($state_list) > 0){
                    foreach($state_list as $ar){
                        $u_state[] = $ar['state_name'];                        
                    }
                    $region[$i]['state_list'] = implode(', ', $u_state);                
                }            
        }
        $data = array();
        $data['region_list'] = $region;        
        return view('pages/region/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $state_list = DB::connection('mysql')->select("SELECT * FROM state where id not in (select state_id from region_detail WHERE region_id in (select id from region where region.delete_status = '0' and region.disable_status = '0'))");
        
        $data = array();
        $data['state_list'] = $state_list;
               
        return view('pages/region/create',$data);         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //public function store(Request $request)
    public function store()
    {
                $input_all = Input::all();
        /*validation start*/
                $deletevalue = 1;
           $rules = array(
                'region_name' => 'required||unique:region,region_name,'.$deletevalue.',delete_status',
                'state' => 'required',
            );        
           
            $validator = Validator::make($input_all, $rules);
            // check if the validator failed -----------------------
            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::to(Config::get('constants.admin_path').'region/create')
                    ->withErrors($validator)
                    ->withInput();
            }
            
            $insert_array = array();
            $user_info = Session::get('session_user_info');
            
            $insert_array['user_id'] = $user_info['id'];            
            $insert_array['region_name'] = $input_all['region_name'];
            $insert_array['created_on'] = date('Y-m-d H:i:s');
            
            $rgid = region::create($insert_array);
            
            if($rgid){
                for($i=0;$i<count($input_all['state']);$i++){
                    $state_array = array();
                    $state_array['region_id'] = $rgid->id;
                    $state_array['state_id'] = $input_all['state'][$i];
                    $state_array['created_on'] = date('Y-m-d H:i:s');
                    RegionDetail::create($state_array);
                }                
            }
            
            Session::flash('message', 'New Region created'); 
            //Session::flash('alert-class', 'alert-danger');                         
            return Redirect::to(Config::get('constants.admin_path').'region/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $region = region::findOrFail($id);
        $state_list = DB::connection('mysql')->select("SELECT * FROM state where id not in (select state_id from region_detail WHERE region_id in (select id from region where region.delete_status = '0' and region.disable_status = '0'))");        
        $select_state_list = DB::connection('mysql')->select("SELECT * FROM state where id in (select state_id from region_detail WHERE region_id in (select id from region where region.delete_status = '0' and region.disable_status = '0' and region.id = '$id' ))");        
            //return view('pages/region/edit')->with(compact('region'));       
        $state_list = (array_merge($select_state_list,$state_list));

        return View::make('pages.region.edit')
        ->with('state_list', $state_list)
        ->with('select_state_list', $select_state_list)
        ->with('region', $region);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // echo $this->method();
        
                 $input_all = Input::all();
                 $deletevalue = 0;
         /*validation start*/
            $rules = array(
                 'region_name' => 'required||unique:region,region_name,'.$id.',id,delete_status,'.$deletevalue,
                 'state' => 'required',
             );        
           
             $validator = Validator::make($input_all, $rules);
             // check if the validator failed -----------------------
             if ($validator->fails()) {
                 $messages = $validator->messages();
                 return Redirect::to(Config::get('constants.admin_path').'region/edit/'.$id)
                     ->withErrors($validator)
                     ->withInput();
             }
            
             $region = region::findOrFail($id);
             $region->region_name = $request['region_name'];
             $region->save();
             
             RegionDetail::where('region_id',$id)->delete();
                for($i=0;$i<count($input_all['state']);$i++){
                    $state_array = array();
                    $state_array['region_id'] = $id;
                    $state_array['state_id'] = $input_all['state'][$i];
                    $state_array['created_on'] = date('Y-m-d H:i:s');
                    RegionDetail::create($state_array);
                }                             
             
             Session::flash('message', 'Region has been updated'); 
             //Session::flash('alert-class', 'alert-danger');                         
             return Redirect::to(Config::get('constants.admin_path').'region/index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $region = region::findOrFail($id);       
        $input_array = Input::all();
        $R_CustomerCount = Customer::where("region_id",$id)->count();
        $R_UserCount     = User::where("region_id",$id)->where("disable_status",0)->where("delete_status",0)->count();

        // print_r($R_CustomerCount);
        // print_r($R_UserCount);
    
        // exit();

        if($R_CustomerCount != 0 || $R_UserCount  != 0 )
        {
           return Redirect::back()->withErrors(["Region cannot be deleted. Region is associated with records."]);
        }


        $region->delete_status = 1;
        $region->disable_status = 1;
        $user_info = Session::get('session_user_info');
        $region->delete_by = $user_info['id'];
        $region->delete_reason = $input_array['delete_reason'];        
            if($region->save()){
                Session::flash('message', 'Region deleted'); 
                //Session::flash('alert-class', 'alert-danger');                         
                return Redirect::to(Config::get('constants.admin_path').'region/index');            
            }else{
                Session::flash('message', 'Region not deleted! Try again later'); 
                Session::flash('alert-class', 'alert-danger');
                return Redirect::to(Config::get('constants.admin_path').'region/index');                        
            }        
    }


    public function changeStatus($status,$id) {

        $input_array = Input::all();
        // $ar = Input::all();
        // $id = $ar['id'];
        $user_info = Session::get('session_user_info');
        if ($status == "enable") {
            Region::where('id', $id)->update(["disable_status" => 0,'active_by'=>$user_info['id'],'disable_reason'=>NULL]);
            Session::flash('message', 'Region has been Activated');
        } elseif ($status == "disable") {
            Region::where('id', $id)->update(["disable_status" => 1,"disable_reason" => $input_array['disable_reason'],'disable_by'=>$user_info['id']]);
            Session::flash('message', 'Region has been Dectivated');
        }
        return Redirect::back();
    }
}
