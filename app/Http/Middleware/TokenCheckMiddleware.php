<?php

namespace App\Http\Middleware;

use Closure;
use Validator;
use JWTAuth;
use App\Helpers\Helper;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;


class TokenCheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
       $rules = array(
            'token' => 'required'
        );        
       
       $input_array['token'] = $request->token;
       $token = $request->token;
       
        $validator = Validator::make($input_array, $rules);        
        if ($validator->fails()) {
            $messages = $validator->messages();

            foreach($validator->messages()->toArray() as $value)
                    $err_msg[] = $value;
            
            return Helper::response_api(false,$err_msg,'');

        }   
        
     try {
            //$token = JWTAuth::refresh($token);
            $user = JWTAuth::authenticate($token);
            if($user){
                return $next($request);            
            }else{
                return Helper::response_api(false,'token is invalid','');            
            }
        } catch (TokenExpiredException $e) {
            return Helper::response_api(false,'token is expired','');            
        }catch (JWTException $e) {
            return Helper::response_api(false,'token is invalid','');            
               // return $this->respond('tymon.jwt.invalid', 'token_invalid', $e->getStatusCode(), [$e]);
        }
    }
}
