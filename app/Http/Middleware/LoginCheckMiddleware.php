<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class LoginCheckMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if (!(Session::has('session_user_info'))) {
          return view('errors.503');
        }else{
            return $next($request);            
        }
        
    }
}
