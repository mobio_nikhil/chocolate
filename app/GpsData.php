<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GpsData extends Model
{
    //
       protected $table = "gps_data";
       public $timestamps = false;
       protected $fillable = [
                                "user_id",
                                "gps_data_type",
                                "meeting_id",
                                "cron_job_end_day",
                                "latitude",
                                "longitude",
                                "created_on",
                             ];       
       
       
       
}
