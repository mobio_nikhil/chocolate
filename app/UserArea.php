<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserArea extends Model
{
    //
       protected $table = "user_area";
       public $timestamps = false;
       protected $fillable = [
                                "user_id",
                                "area_id",
                                "created_on",
                             ];       
       
       
       
}
