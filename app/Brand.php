<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    	   protected $table = "brand";
	       public $timestamps = false;
	       protected $fillable = [
                                "brand_name",
                                "user_id",
                                "delete_status",
                                "disable_status",
                                "created_on",
                             ];  
}
