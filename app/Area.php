<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    //
       protected $table = "area";
       public $timestamps = false;
       protected $fillable = [
                                "area_name",
                                "region_id",
                                "user_id",
                                "delete_status",
                                "disable_status",
                                "created_on",
                             ];       
       
       
       
}
