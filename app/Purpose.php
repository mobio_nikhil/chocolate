<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purpose extends Model
{
         protected $table = "meeting_purpose";
       public $timestamps = false;
       protected $fillable = [
                                "purpose_name",
                                "user_id",
                                "delete_status",
                                "disable_status",
                                "created_on",
                             ];  
}
