<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $table = "customer";
    public $timestamps = false;
    protected $fillable = [
                                "customer_name",
                                "business_name",
                                "business_address",
                                "primary_contact_name",
                                "primary_image",
                                "secondary_contact_name",
                                "customer_email",
                                "customer_phone",
                                "customer_type_id",
                                "brand_id",
                                "visit_card_image_1",
                                "visit_card_image_2",
                                "visit_card_image_3",
                                "visit_card_image_4",
                                "visit_card_image_5",
                                "customer_birth_date",
                                "customer_anniversary_date",
                                "information_enter_by",
                                "region_id",
                                "state_id",        
                                "area_id",
                                "city_id",
                                "pincode",
                                "delete_status",
                                "disable_status",
                                "created_on",
                          ];       
           
}
