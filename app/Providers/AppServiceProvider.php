<?php

namespace App\Providers;

use Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        //Add this custom validation rule.
        Validator::extend('alpha_spaces', function ($attribute, $value) {

            // This will only accept alpha and spaces. 
            // If you want to accept hyphens use: /^[\pL\s-]+$/u.
            return preg_match('/^[\pL\s]+$/u', $value);
        });


        Validator::extend('only_date_month', function($attribute, $value, $formats) {
            // iterate through all formats
            
            $datearray = explode('-', $value);
            if(count($datearray) != 2){
                return false;                
            }
            if(!($datearray[0] >=1 && $datearray[0] <= 31)){
                return false;
            }

            if(!($datearray[1] >=1 && $datearray[1] <= 12)){
                return false;
            }            
            return true;
           
        });

        Validator::extend('only_date_month_text', function($attribute, $value, $formats) {
            // iterate through all formats
            
            $datearray = explode('-', $value);
            if(count($datearray) != 2){
                return false;                
            }
            if(!($datearray[0] >=1 && $datearray[0] <= 31)){
                return false;
            }

            //echo date('Y').'-'.$datearray[1].'-'.'01';exit;
            $montharray = array(
              'Jan',
                'Feb',
                'Mar',
                'Apr',
                'May',
                'Jun',
                'Jul',
                'Aug',
                'Sep',
                'Oct',
                'Nov',
                'Dec'   
            );
            
            if (!(in_array($datearray[1], $montharray))){
                return false;
            }
            
            return true;
           
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

}
