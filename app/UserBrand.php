<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBrand extends Model
{
    //
       protected $table = "user_brand";
       public $timestamps = false;
       protected $fillable = [
                                "user_id",
                                "brand_id",
                                "created_on",
                             ];       
       
       
       
}
