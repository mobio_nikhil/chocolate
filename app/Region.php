<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    //
       protected $table = "region";
       public $timestamps = false;
       protected $fillable = [
                                "region_name",
                                "delete_status",
                                "disable_status",
                                "created_on",
                             ];       
       
       
       
}
