 <!-- jQuery -->
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/metisMenu/metisMenu.min.js"></script>

    <?php /*
    <!-- DataTables JavaScript -->
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/datatables-responsive/dataTables.responsive.js"></script>
    */ ?>

    <script src="<?php echo Input::root().'/theme/' ?>/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/datatables/js/dataTables.bootstrap.min_2.js"></script>
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/datatables/js/dataTables.responsive.min_2.js"></script>
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/datatables/js/responsive.bootstrap.min_2.js"></script>
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/datatables/js/responsive.bootstrap.min_2.js"></script>
    <script src="<?php echo Input::root().'/theme/' ?>vendor/jquery/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>


<!--     
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/responsive.bootstrap.min.js"></script>
   <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>   -->
    
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/bootbox/bootbox.min.js"></script>
    
    
    
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo Input::root().'/theme/' ?>/dist/js/sb-admin-2.js"></script>
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/timepicker/js/bootstrap-timepicker.min.js"></script>
    

    <!-- Application js -->
    <script src="<?php echo Input::root().'/theme/' ?>js/app-system.js"></script>
    
    <script> 
     <?php /*
 $(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#purpose-grid tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
 
    // DataTable
    var table = $('#purpose-grid').DataTable({
        responsive: true
    });
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
} );
*/ ?>



    <?php if(Input::old('searchby') == 1) { ?>
            
            document.getElementById('div_search_id').style.display = 'block';
            document.getElementById('div_search_name').style.display = 'none';
            document.getElementById("searchbyid").checked = true;            
            
     <?php } ?>       
    <?php if(Input::old('searchby') == 2) { ?>
            document.getElementById('div_search_id').style.display = 'none';
            document.getElementById('div_search_name').style.display = 'block';
            document.getElementById("searchbyname").checked = true;            
     <?php } ?>       
</script>

<script type="text/javascript">
    
Admin_Route = "<?php echo URL::to( Config::get('constants.admin_path'));?>";
function  purpose_delete(id){
    //del_ref("<?php echo URL::to( Config::get('constants.admin_path').'purpose/destroy'); ?>" + '/' + id);
    $('#purpose_delete_id').val(id);
    bootbox.confirm("Are you sure want to delete?", function(result) {
        if(result){            
            //window.location = url;
    $("#purpose_delete_form").attr('action',"<?php echo URL::to( Config::get('constants.admin_path').'purpose/destroy'); ?>" + '/' + id);
            $("#delete_purpose_modal").modal();
        }
    })
}

function  customertype_delete(id){
    //del_ref("<?php echo URL::to( Config::get('constants.admin_path').'customertype/destroy'); ?>" + '/' + id);
     $('#customertype_delete_id').val(id);
    bootbox.confirm("Are you sure want to delete?", function(result) {
        if(result){            
            //window.location = url;
    $("#customertype_delete_form").attr('action',"<?php echo URL::to( Config::get('constants.admin_path').'customertype/destroy'); ?>" + '/' + id);
            $("#delete_customertype_modal").modal();
        }
    })
}

function  brand_delete(id){
    $('#brand_delete_id').val(id);
    //del_ref("<?php echo URL::to( Config::get('constants.admin_path').'brand/destroy'); ?>" + '/' + id);
    bootbox.confirm("Are you sure want to delete?", function(result) {
        if(result){            
            //window.location = url;
    $("#brand_delete_form").attr('action',"<?php echo URL::to( Config::get('constants.admin_path').'brand/destroy'); ?>" + '/' + id);
            $("#delete_brand_modal").modal();
        }
    })
    
}

function  region_delete(id){
    $('#region_delete_id').val(id);
    bootbox.confirm("Are you sure want to delete?", function(result) {
        if(result){            
            //window.location = url;
    $("#region_delete_form").attr('action',"<?php echo URL::to( Config::get('constants.admin_path').'region/destroy'); ?>" + '/' + id);
            $("#delete_region_modal").modal();
        }
    })
    
    //del_ref("<?php echo URL::to( Config::get('constants.admin_path').'region/destroy'); ?>" + '/' + id);
}
function  area_delete(id){
    //del_ref("<?php echo URL::to( Config::get('constants.admin_path').'area/destroy'); ?>" + '/' + id);
       $('#area_delete_id').val(id);
    bootbox.confirm("Are you sure want to delete?", function(result) {
        if(result){            
            //window.location = url;
    $("#area_delete_form").attr('action',"<?php echo URL::to( Config::get('constants.admin_path').'area/destroy'); ?>" + '/' + id);
            $("#delete_area_modal").modal();
        }
    })
}
function  cust_delete(id){
    //del_ref("<?php echo URL::to( Config::get('constants.admin_path').'customer/trash'); ?>"+ '/' + id);
     $('#customer_delete_id').val(id);
    bootbox.confirm("Are you sure want to delete?", function(result) {
        if(result){            
            //window.location = url;
    $("#customer_delete_form").attr('action',"<?php echo URL::to( Config::get('constants.admin_path').'customer/trash'); ?>" + '/' + id);
            $("#delete_customer_modal").modal();
        }
    })
}

function user_delete( id ){
    //del_ref("<?php echo URL::to( Config::get('constants.admin_path').'user/delete'); ?>"+ '/' + id);
    $('#user_delete_id').val(id);
    bootbox.confirm("Are you sure want to delete?", function(result) {
        if(result){            
            //window.location = url;
    $("#user_delete_form").attr('action',"<?php echo URL::to( Config::get('constants.admin_path').'user/delete'); ?>" + '/' + id);
            $("#delete_user_modal").modal();
        }
    })

}

</script>
<script type="text/javascript">
    $('#meeting_time').timepicker();
    $('#followup_time').timepicker();
    $(document).ready(function() {
    $( ".date" ).each(function(){        
            if($(this).val() != null && $(this).val() != ''){                
                tmp = $(this).val().split('-');
                dval = {
                        day    : tmp[0],
                        month  : tmp[1],
                        year   : tmp[2]
                    };                
                //$(this).datepicker("setDate", new Date(1966+1,01,26));                                
//                alert(dval.day);
//                alert(dval.month);
//                alert(dval.year);
                $(this).datepicker("setDate", new Date(parseInt(dval.year),dval.month - 1,dval.day));
                //$(this).datepicker("setDate", new Date('2016','10','10'));
                //$(this).datepicker("setDate", '2016-10-10');
            }else{
                //alert($(this).val());
            }
    });});
</script>
<script>
    $('#purpose_meeting').change(function(){
        value = $('#purpose_meeting').val();
        if(value == 1){
           $('#div_opther_purpose').css('display','block');
        }else{
            $('#div_opther_purpose').css('display','none');
        }
    });
    
    $('#employee_meeting').change(function(){
        var postForm = { //Fetch form data
            'user_id'     : $('#employee_meeting').val() //Store name fields value
        };
        
        /*var select = document.getElementById("employee_brand");
        var length = select.options.length;
        for (i = 0; i < length; i++) {
          select.options[i] = null;
        }  */   
        document.getElementById('employee_brand').options.length = 0;        
        $.ajax({
               type:'POST',
               url:'<?php echo URL::to( Config::get('constants.admin_path').'meeting/get_user_brand'); ?>',
               data:postForm,
               success:function(data){
                  //alert(data.msg);
                  if(data.msg == 'yes'){
                    var dropdown = document.getElementById("employee_brand");
                    if (dropdown) {
                        addOption = function(selectbox, text, value) {
                            var optn = document.createElement("OPTION");
                            optn.text = text;
                            optn.value = value;
                            selectbox.options.add(optn);  
                        }                      
                        for (var i=0; i < data.user_brand.length;++i){    
                            addOption(dropdown, data.user_brand[i]['brand_name'], data.user_brand[i]['brand_id']);
                        }
                    }

                  }
               }
            });
            
            /*customer list start*/
            
        document.getElementById('employee_customer').options.length = 0;        
        $.ajax({
               type:'POST',
               url:'<?php echo URL::to( Config::get('constants.admin_path').'meeting/get_user_customer'); ?>',
               data:postForm,
               success:function(data){
                  //alert(data.msg);
                  if(data.msg == 'yes'){
                    var dropdown = document.getElementById("employee_customer");
                    if (dropdown) {
                        addOption_customer = function(selectbox, text, value) {
                            var optn = document.createElement("OPTION");
                            optn.text = text;
                            optn.value = value;
                            selectbox.options.add(optn);  
                        }                      
                        for (var i=0; i < data.user_customer.length;++i){    
                            addOption_customer(dropdown, data.user_customer[i]['business_name'] + ' ('+ data.user_customer[i]['customer_name'] + ')', data.user_customer[i]['id']);
                        }
                    }

                  }
               }
            });            
            
            /*customer list end*/
            
    });
    
</script>