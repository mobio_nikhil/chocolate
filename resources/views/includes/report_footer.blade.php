 <!-- jQuery -->
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/metisMenu/metisMenu.min.js"></script>

    <?php /*
    <!-- DataTables JavaScript -->
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/datatables-responsive/dataTables.responsive.js"></script>
    */ ?>

    <script src="<?php echo Input::root().'/theme/' ?>/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/datatables/js/dataTables.bootstrap.min_2.js"></script>
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/datatables/js/dataTables.responsive.min_2.js"></script>
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/datatables/js/responsive.bootstrap.min_2.js"></script>
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/datatables/js/responsive.bootstrap.min_2.js"></script>
    <script src="<?php echo Input::root().'/theme/' ?>vendor/jquery/jquery-ui-1.12.1/jquery-ui.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>


<!--     
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.1.0/js/responsive.bootstrap.min.js"></script>
   <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>   -->
    
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/bootbox/bootbox.min.js"></script>
    
    
    
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo Input::root().'/theme/' ?>/dist/js/sb-admin-2.js"></script>
    <script src="<?php echo Input::root().'/theme/' ?>/vendor/timepicker/js/bootstrap-timepicker.min.js"></script>
    

    <!-- Application js -->
    <script src="<?php echo Input::root().'/theme/' ?>js/app-system.js"></script>
    
    


	
	<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
	
        
        

<script>
$('#report_employee_grid tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
 
    // DataTable
    var report_employee_grid = $('#report_employee_grid').DataTable({
bSort: false,    
dom: 'Bfrtip',
        buttons: [
             'excel' 
        ]    
    } );
 
    // Apply the search
    report_employee_grid.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );            
        
</script>