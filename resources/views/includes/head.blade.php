<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Chocolate Room</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo Input::root().'/theme/' ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo Input::root().'/theme/' ?>/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="<?php echo Input::root().'/theme/' ?>/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">    
    <link href="<?php echo Input::root().'/theme/' ?>/vendor/datatables/css/responsive.bootstrap.min.css" rel="stylesheet">  
        <link href="<?php echo Input::root().'/theme/' ?>/vendor/jquery/jquery-ui-1.12.1/jquery-ui.min.css" rel="stylesheet">    
        <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet">    
    
    <?php /*
    <!-- DataTables Responsive CSS -->
    <link href="<?php echo Input::root().'/theme/' ?>/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    */ ?>
<!--    <link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet"></link>-->
    
    <!-- Custom CSS -->
    <link href="<?php echo Input::root().'/theme/' ?>/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo Input::root().'/theme/' ?>/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo Input::root().'/theme/' ?>/vendor/timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <style type="text/css">
        div.ui-datepicker{
        font-size:12px;
        }

        .ui-datepicker th { -
        color: #3e3e3e;
        }

        .ui-datepicker th.ui-datepicker-week-end {
        color:red;
        }
    </style>