
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" style="font-weight: bold;color: white;font-size: 30px" href="<?php echo URL::to(Config::get('constants.admin_path') . 'dashboard') ?>">Chocolate Room</a>
    </div>
    <!-- /.navbar-header -->

    <?php $customers = Helper::get_customers(3); ?>
    <ul class="nav navbar-top-links navbar-right">
        <?php /*
          <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
          <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
          </a>
          <ul class="dropdown-menu dropdown-messages">

          @foreach( $customers as $c )
          <li>
          
            <a href="javascript:void(0)">
              <div>
              <strong class="text-capitalize">{{ $c['customer_name'] }}</strong>
              <span class="pull-right text-muted">
              <em>{{ date_format(date_create($c['created_on']),"d, M")}}</em>
              </span>
              </div>
              <div>
              <div class="text-capitalize">{{$c['business_name']}}</div>
              <div class="text-capitalize">contact : {{$c['customer_phone']}}</div>
              </div>
            </a>
            
          </li>
          <li class="divider"></li>
          @endforeach

          @if( count($customers)>0 )
          <li>
            <a class="text-center" href="<?php echo URL::to( Config::get('constants.admin_path').'customer/new_list') ?>">
            <strong>Not Approved Customers</strong>
            <i class="fa fa-angle-right"></i>
            </a>
          </li>
          @else
          <li>
            <a class="text-center" href="<?php echo URL::to( Config::get('constants.admin_path').'customer/new_list') ?>">
            <strong>No new Customers</strong>
            </a>
          </li>
          @endif
          </ul>
          <!-- /.dropdown-messages -->
          </li>
*/
?>
          <!-- /.dropdown -->
          <?php /*
          <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
          <i class="fa fa-tasks fa-fw"></i> <i class="fa fa-caret-down"></i>
          </a>
          <ul class="dropdown-menu dropdown-tasks">
          <li>
          <a href="#">
          <div>
          <p>
          <strong>Task 1</strong>
          <span class="pull-right text-muted">40% Complete</span>
          </p>
          <div class="progress progress-striped active">
          <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
          <span class="sr-only">40% Complete (success)</span>
          </div>
          </div>
          </div>
          </a>
          </li>
          <li class="divider"></li>
          <li>
          <a href="#">
          <div>
          <p>
          <strong>Task 2</strong>
          <span class="pull-right text-muted">20% Complete</span>
          </p>
          <div class="progress progress-striped active">
          <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
          <span class="sr-only">20% Complete</span>
          </div>
          </div>
          </div>
          </a>
          </li>
          <li class="divider"></li>
          <li>
          <a href="#">
          <div>
          <p>
          <strong>Task 3</strong>
          <span class="pull-right text-muted">60% Complete</span>
          </p>
          <div class="progress progress-striped active">
          <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
          <span class="sr-only">60% Complete (warning)</span>
          </div>
          </div>
          </div>
          </a>
          </li>
          <li class="divider"></li>
          <li>
          <a href="#">
          <div>
          <p>
          <strong>Task 4</strong>
          <span class="pull-right text-muted">80% Complete</span>
          </p>
          <div class="progress progress-striped active">
          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
          <span class="sr-only">80% Complete (danger)</span>
          </div>
          </div>
          </div>
          </a>
          </li>
          <li class="divider"></li>
          <li>
          <a class="text-center" href="#">
          <strong>See All Tasks</strong>
          <i class="fa fa-angle-right"></i>
          </a>
          </li>
          </ul>
          <!-- /.dropdown-tasks -->
          </li>
          <!-- /.dropdown -->
          <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
          <i class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i>
          </a>
          <ul class="dropdown-menu dropdown-alerts">
          <li>
          <a href="#">
          <div>
          <i class="fa fa-comment fa-fw"></i> New Comment
          <span class="pull-right text-muted small">4 minutes ago</span>
          </div>
          </a>
          </li>
          <li class="divider"></li>
          <li>
          <a href="#">
          <div>
          <i class="fa fa-twitter fa-fw"></i> 3 New Followers
          <span class="pull-right text-muted small">12 minutes ago</span>
          </div>
          </a>
          </li>
          <li class="divider"></li>
          <li>
          <a href="#">
          <div>
          <i class="fa fa-envelope fa-fw"></i> Message Sent
          <span class="pull-right text-muted small">4 minutes ago</span>
          </div>
          </a>
          </li>
          <li class="divider"></li>
          <li>
          <a href="#">
          <div>
          <i class="fa fa-tasks fa-fw"></i> New Task
          <span class="pull-right text-muted small">4 minutes ago</span>
          </div>
          </a>
          </li>
          <li class="divider"></li>
          <li>
          <a href="#">
          <div>
          <i class="fa fa-upload fa-fw"></i> Server Rebooted
          <span class="pull-right text-muted small">4 minutes ago</span>
          </div>
          </a>
          </li>
          <li class="divider"></li>
          <li>
          <a class="text-center" href="#">
          <strong>See All Alerts</strong>
          <i class="fa fa-angle-right"></i>
          </a>
          </li>
          </ul>
          <!-- /.dropdown-alerts -->
          </li>
          <!-- /.dropdown -->
          */?>
         
        <?php $session_user_info = Session::get('session_user_info'); ?>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> <span style="margin-right:10px;font-size:16px;"><?php echo $session_user_info['user_name']; ?></span><i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <?php /* <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                  </li>
                 * 
                 */ ?>
                  <li><a href="<?php echo URL::to(Config::get('constants.admin_path') . 'setting/password') ?>"><i class="fa fa-gear fa-fw"></i> Change Password</a>
                  </li>
                  <li class="divider"></li>
                <li><a href="<?php echo URL::to(Config::get('constants.admin_path') . 'user/logout') ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <?php /*
                  <li class="sidebar-search">
                  <div class="input-group custom-search-form">
                  <input type="text" class="form-control" placeholder="Search...">
                  <span class="input-group-btn">
                  <button class="btn btn-default" type="button">
                  <i class="fa fa-search"></i>
                  </button>
                  </span>
                  </div>
                  <!-- /input-group -->
                  </li> */ ?>
                <li>
                    <a href="<?php echo URL::to(Config::get('constants.admin_path') . 'dashboard') ?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>                
                <li>
                    <a class="<?php echo (Request::is(Config::get('constants.admin_path') . 'employee/*')) ? 'active' : '' ?>"  href="<?php echo URL::to(Config::get('constants.admin_path') . 'employee/employee_list') ?>"><i class="fa fa-user  fa-fw"></i> Users</a>
                </li>
                <li>
                    <a class="<?php echo (Request::is(Config::get('constants.admin_path') . 'customer/*')) ? 'active' : '' ?>" href="<?php echo URL::to(Config::get('constants.admin_path') . 'customer/customer_list') ?>"><i class="fa fa-users  fa-fw"></i> Customers</a>
                </li>
                <li>
                    <a class="<?php echo (Request::is(Config::get('constants.admin_path') . 'meeting/*')) ? 'active' : '' ?>"  href="<?php echo URL::to(Config::get('constants.admin_path') . 'meeting/meeting_list') ?>"><i class="fa fa-briefcase fa-fw"></i> Meetings</a>
                </li>
                <li>
                    <a class="<?php echo (Request::is(Config::get('constants.admin_path') . 'employee_track/*')) ? 'active' : '' ?>"  href="<?php echo URL::to(Config::get('constants.admin_path') . 'employee_track/create') ?>"><i class="fa fa-clock-o  fa-fw"></i> Users Tracking</a>
                </li>
                <?php $session_user_info = Session::get('session_user_info');
                if ($session_user_info['user_type'] == 0) {
                    ?>
                    <li class="<?php echo (Request::is(Config::get('constants.admin_path') . 'purpose/*') || Request::is(Config::get('constants.admin_path') . 'user/*')) ? 'active' : '' ?>">
                        <a href="forms.html"><i class="fa fa-th-list  fa-fw"></i> Master Management<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <?php /* <li class="<?php echo (Request::is(Config::get('constants.admin_path').'user/*')) ? 'active' : '' ?>">
                              <a href="#">User Management <span class="fa arrow"></span></a>
                              <ul class="nav nav-third-level">
                              <li>
                              <a class="<?php echo (Request::is(Config::get('constants.admin_path').'user/*')) ? 'active' : '' ?>" href="<?php echo URL::to( Config::get('constants.admin_path').'user/user_list') ?>">Users</a>
                              </li>
                              <li>
                              <a href="#">Assignments</a>
                              </li>
                              </ul>
                              <!-- /.nav-third-level -->
                              </li> */ ?>                     
                            <li>                                              
                                <a class="<?php echo (Request::is(Config::get('constants.admin_path') . 'user/*')) ? 'active' : '' ?>" href="<?php echo URL::to(Config::get('constants.admin_path') . 'user/user_list') ?>">User Management</a>
                            </li>
                            <li>                                              
                                <a class="<?php echo (Request::is(Config::get('constants.admin_path') . 'purpose/*')) ? 'active' : '' ?>" href="<?php echo URL::to(Config::get('constants.admin_path') . 'purpose/index') ?>">Purpose Management</a>
                            </li>
                            <li>                                              
                                <a class="<?php echo (Request::is(Config::get('constants.admin_path') . 'brand/*')) ? 'active' : '' ?>" href="<?php echo URL::to(Config::get('constants.admin_path') . 'brand/index') ?>">Brand Management</a>
                            </li>
                            <li> 
                                <a class="<?php echo (Request::is(Config::get('constants.admin_path') . 'region/*')) ? 'active' : '' ?>" href="<?php echo URL::to(Config::get('constants.admin_path') . 'region/index') ?>">Region Management</a>
                            </li>
                            <li>
                                <a class="<?php echo (Request::is(Config::get('constants.admin_path') . 'area/*')) ? 'active' : '' ?>" href="<?php echo URL::to(Config::get('constants.admin_path') . 'area/index') ?>">Area Management</a>
                            </li>
                            <li>
                                <a class="<?php echo (Request::is(Config::get('constants.admin_path') . 'customertype/*')) ? 'active' : '' ?>" href="<?php echo URL::to(Config::get('constants.admin_path') . 'customertype/index') ?>">Customer Type</a>
                            </li>
                        </ul>                    
                    </li>
<?php } ?>
                <li>
                    <a href="<?php echo URL::to(Config::get('constants.admin_path') . 'report/admin_index') ?>"><i class="fa fa-stack-exchange  fa-fw"></i> Reports</a>
                </li>
                <?php /* <li>
                    <a href="<?php echo URL::to(Config::get('constants.admin_path') . 'setting/index') ?>"><i class="fa fa-gear fa-fw"></i> Settings</a>
                </li> */ ?>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>