@extends('layouts.report_default')
@section('content_header')
Reports
@stop
@section('content')
<style>
    tfoot {
        display: table-header-group;
    }
    .dataTables_filter {
        display: none;
    }          
</style>
<div class="row" >
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">

            </div>

            @if ($errors->has())
            <div class="alert alert-danger">            
                @foreach ($errors->all() as $error)
                {{ $error }}<br>        
                @endforeach
            </div>
            @endif   

            @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif                          

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-1">
                        <form role="form" action="<?php echo URL::to(Config::get('constants.admin_path') . 'report/admin_index') ?>" method="post">

                            <?php if(Input::isMethod('post')){
                                $sd = $start_date;
                            }else if(Input::old('start_date')){
                                $sd = Input::old('start_date');                                
                            }else{
                                $sd = '';
                            }
                            
                            if(Input::isMethod('post')){
                                $ed = $end_date;
                            }else if(Input::old('end_date')){
                                $ed = Input::old('end_date');                                
                            }else{
                                $ed = '';
                            }                            
                            
                            ?>
                            
                            <div class="form-group">
                                <label>Start Date</label>
                                <input class="form-control start_date" placeholder="Start Date" name="start_date" type="text" value="<?php echo $sd ?>" />
                            </div>

                            <div class="form-group">
                                <label>End Date</label>
                                <input class="form-control end_date" placeholder="End Date" name="end_date" type="text" value="<?php echo $ed ?>" />
                            </div>

                            <div class="form-group">                                            
                                <label class="radio-inline">
                                    <input type="radio" name="report_type" id="report_type_1" value="1" checked>New Customer Report
                                </label>
                                <br/>
                                <label class="radio-inline">
                                    <input type="radio" name="report_type" id="report_type_2" value="2" <?php echo ($report_type == 2) ? 'checked' : '' ?>>Customer Meeting Report
                                </label>                                            
                            </div>                                        





                            <button type="submit" class="btn btn-default">Submit</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        </form>
                    </div>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->

            <?php if (Input::isMethod('post') && $report_type == 1) { ?>
                <?php /* new customer list start */ ?>
                <div class="panel-body">
                    <table id="report_employee_grid" class="display table" width="100%" cellspacing="0">
                        <thead>
                            <tr>                            
                                <th>Business Name</th>                            
                                <th>Customer Name</th>                            
                                <th>Customer Type</th>                            
                                <th>Business Address</th>                                                                                            
                                <th>Secondary Name</th>                                                                                    
                                <th>Email</th>                            
                                <th>Contact No</th>                                
                                <th>Birth Date</th>
                                <th>Anniversary Date</th>
                                <th>Entered By</th>
                                <th>Region</th>
                                <th>State</th>
                                <th>City</th>
                                <th>Area</th>                                        
                                <th>Pincode</th>                                        
                                <th>Create Date</th>                                        
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Business Name</th>                            
                                <th>Customer Name</th>                            
                                <th>Customer Type</th>                            
                                <th>Business Address</th>                                                                                            
                                <th>Secondary Name</th>                                                            
                                <th>Email</th>                            
                                <th>Contact No</th>                                
                                <th>Birth Date</th>
                                <th>Anniversary Date</th>
                                <th>Entered By</th>
                                <th>Region</th>
                                <th>State</th>
                                <th>City</th>
                                <th>Area</th>                                        
                                <th>Pincode</th>                                        
                                <th>Create Date</th>
                                <?php /* <th>Status</th>
                                  <th>Edit</th>
                                  <th>Delete</th>
                                  <th>History</th> */ ?>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php foreach ($excel_array as $customer) { 
                                if(empty($customer['customer_birth_date'])){
                                            $bdate = 'NA';
                                        }else{
                                            $bdate = date('d-M-Y',strtotime($customer['customer_birth_date']));
                                        }

                                        if(empty($customer['customer_anniversary_date'])){
                                            $adate = 'NA';
                                        }else{
                                            $adate = date('d-M-Y',strtotime($customer['customer_anniversary_date']));
                                        }
                                
                                
                                ?>
                                <tr>
                                    <td><?php echo $customer['business_name'] ?></td>
                                    <td><?php echo $customer['customer_name'] ?></td>
                                    <td><?php echo $customer['customertype_name'] ?></td>
                                    <td><?php echo $customer['business_address'] ?></td>                                                                        
                                    <td><?php echo empty($customer['secondary_contact_name']) ? 'NA' : $customer['secondary_contact_name'] ?></td>
                                    
                                    <td><a target="_blank" href="mailto:{{ $customer['customer_email'] }}"> <?php echo $customer['customer_email'] ?></a></td>
                                    <td><a target="_blank" href="tel:{{ $customer['customer_phone'] }}"> <?php echo $customer['customer_phone'] ?></a></td>
                                    <td><?php echo $bdate ?></td>
                                    <td><?php echo $adate ?></td>
                                    <td><?php echo $customer['user_name'] ?></td>                                        
                                    <td><?php echo $customer['region_name'] ?></td>                                        
                                    <td><?php echo $customer['state_name'] ?></td>                                        
                                    <td><?php echo $customer['city_name'] ?></td>                                        
                                    <td><?php echo $customer['area_name'] ?></td>                                                                                    
                                    <td><?php echo $customer['pincode'] ?></td>                                        
                                    <td><?php echo date('d-M-Y', strtotime($customer['created_on'])) ?></td>                                        
                                </tr>

                            <?php } ?>         

                        </tbody>
                    </table>


                    <!-- /.table-responsive -->                            
                </div>                        
                <?php /* new customer list end */ ?>
            <?php } ?>

            <?php if (Input::isMethod('post') && $report_type == 2) { ?>
                <?php /* meeting list start */ ?>
                <div class="panel-body">
                    <table id="report_employee_grid" class="display table" width="100%" cellspacing="0">
                        <thead>
                            <tr>                            
                                <th>User Name</th>                            
                                <th>Business Name</th>                            
                                <th>Customer Name</th>                            
                                 <th>Customer Type</th>                            
                                <th>Business Address</th>                            
                                <th>Secondary Customer Name</th>                            
                                <th>Customer Email</th>                            
                                <th>Customer Phone</th>  
                                <th>Note</th>                            
                                <th>Brand Name</th>                            
                                <th>Purpose Name</th>                            
                                <th>Other Purpose Note</th>                            
                                <th>Region</th>
                                <th>State</th>
                                <th>City</th>
                                <th>Area</th>
                                <th>Pincode</th>
                                <th>Followup Date</th>                            
                                <th>Meeting Date</th>                            
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>                            
                                <th>User Name</th>                            
                                <th>Business Name</th>                            
                                <th>Customer Name</th>                            
                                <th>Customer Type</th>                            
                                <th>Business Address</th>                            
                                <th>Secondary Customer Name</th>                            
                                <th>Customer Email</th>                            
                                <th>Customer Phone</th>                            
                                <th>Note</th>                            
                                <th>Brand Name</th>                            
                                <th>Purpose Name</th>                            
                                <th>Other Purpose Note</th>                            
                                <th>Region</th>
                                <th>State</th>
                                <th>City</th>
                                <th>Area</th>
                                <th>Pincode</th>
                                <th>Followup Date</th>                            
                                <th>Meeting Date</th>                            
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php foreach ($excel_array as $meeting) { ?>
                                <tr>                                        
                                    <td><?php echo $meeting['user_name'] ?></td>
                                    <td><?php echo $meeting['business_name'] ?></td>
                                    <td><?php echo $meeting['customer_name'] ?></td>
                                    <td><?php echo $meeting['customertype_name'] ?></td>
                                    <td><?php echo $meeting['business_address'] ?></td>
                                    <td><?php echo $meeting['secondary_contact_name'] ?></td>
                                    <td><?php echo $meeting['customer_email'] ?></td>
                                    <td><?php echo $meeting['customer_phone'] ?></td>                    
                                    <td><?php echo empty($meeting['mnote']) ? 'NA' : $meeting['mnote']; ?></td>
                                    <td><?php echo $meeting['brand_name'] ?></td>
                                    <td><?php echo $meeting['purpose_name'] ?></td>
                                    <td><?php echo empty($meeting['meeting_other_purpose']) ? 'NA' : $meeting['meeting_other_purpose']; ?></td>
                                    <td><?php echo $meeting['cregion'] ?></td>
                                    <td><?php echo $meeting['cstate'] ?></td>
                                    <td><?php echo $meeting['city_name'] ?></td>
                                    <td><?php echo $meeting['carea'] ?></td>
                                    <td><?php echo $meeting['pincode'] ?></td>
                                    <td><?php echo (!(empty($meeting['followup_date_time']) and $meeting['followup_date_time'] == null)) ? date('d-M-Y h:i:s A', strtotime($meeting['followup_date_time'])) : 'NA' ?></td>
                                    <td><?php echo date('d-M-Y h:i:s A', strtotime($meeting['meeting_date'])) ?></td>
                                </tr>

                            <?php } ?>         

                        </tbody>
                    </table>
                    <!-- /.table-responsive -->                            
                    
                    
                </div>                                                
                <?php /* meeting list end */ ?>
            <?php } ?>
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
@stop      