@extends('layouts.default')
@section('content_header')
Region
@stop

@section('content')
<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Region Edit
                        </div>
                        
                    @if ($errors->has())
                    <div class="alert alert-danger">            
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>        
                        @endforeach
                    </div>
                    @endif   
                    
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif                          
                        
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="<?php echo URL::to( Config::get('constants.admin_path').'region/update/'.$region->id) ?>" method="post">
                                        <div class="form-group">
                                            <label>Region Name</label>                                            
                                            <input class="form-control" placeholder="region name" name="region_name" type="text" value="<?php echo (Input::old('region_name')) ? Input::old('region_name') : $region->region_name ?>" autofocus/>
                                        </div>                                        
                                        <div class="form-group">
                                            <label>State</label>
                                            <select multiple="multiple" name="state[]" class="form-control">
                                                <?php
                                                $st = array();
                                                if(count(Input::old('state')) > 0){
                                                  $st = Input::old('state');
                                                }else{
                                                    for($i=0;$i<count($select_state_list);$i++){
                                                        $st[] = $select_state_list[$i]->id;
                                                    }
                                                }
                                                foreach($state_list as $state){ ?>
                                                <option <?php echo (in_array($state->id, $st)) ? 'selected' : ''; ?> value="<?php echo $state->id ?>"><?php echo $state->state_name ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>                                        
                                        <button type="submit" class="btn btn-default">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
@stop