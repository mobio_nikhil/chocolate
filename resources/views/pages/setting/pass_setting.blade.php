@extends('layouts.default')
@section('content_header')
Change Password
@stop

@section('content')

<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        </div>
                        
                    @if ($errors->has())
                    <div class="alert alert-danger">            
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>        
                        @endforeach
                    </div>
                    @endif   
                    
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif                          
                        
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" data-toggle="validator" id="cpassform"  autocomplete="off" action="<?php echo URL::to( Config::get('constants.admin_path').'setting/password'); ?>" method="post">
                                        <div class="form-group">
                                            <label>Old Pasword</label>                                            
                                            <input class="form-control required"  placeholder="Old Password" name="pass_old" type="password" value="{{ Input::old('pass_old') }}"/>
                                        </div>
                                        <div class="form-group">
                                            <label>New Pasword</label>                                            
                                            <input class="form-control required"  id="pass_new" data-match="#inputPassword" data-match-error="Whoops, these don't match"  placeholder="New Password" name="pass_new" type="password" value="{{ Input::old('pass_new') }}" />
                                        </div>        
                                        <div class="form-group">
                                            <label>Confirm Pasword</label>                                            
                                            <input class="form-control required" placeholder="Retype New Password" name="pass_new_conf" type="password" value="{{ Input::old('pass_new_conf') }}" />
                                        </div>                                
                                        <button type="submit" class="btn btn-default">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

@stop