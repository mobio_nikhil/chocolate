@extends('layouts.default')
@section('content_header')
Customer Type
<br/><br/>
<a href="<?php echo URL::to( Config::get('constants.admin_path').'customertype/create') ?>" class="btn btn-primary">Add</a>            
@stop
@section('content')
<style>
    tfoot {
        display: table-header-group;
    }
    .dataTables_filter {
    display: none;
    }     
</style>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Customer type List
            </div>
            
                @if ($errors->has())
                    <div class="alert alert-danger">            
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>        
                        @endforeach
                    </div>
                    @endif   
                    
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif             
            <!-- /.panel-heading -->
            <div class="panel-body">
            <table id="customertype_grid" class="display table" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Customer type Name</th>
                            <th>Deactivate Reason</th>
                            <th>Create Date</th>
                            <th>Status</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Customer type Name</th>
                            <th>Deactivate Reason</th>
                            <th>Create Date</th>
                            <th>Status</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </tfoot>
                    <tbody>
                                    <?php foreach($customertype_list as $customertype){ ?>
                                        <?php { ?>
                                        <tr>
                                        <td><?php echo $customertype['customertype_name'] ?></td>
                                        <td><?php echo empty($customertype['disable_reason']) ? 'NA' : $customertype['disable_reason'] ?></td>
                                        <td><?php echo date('d-M-Y',strtotime($customertype['created_on'])) ?></td>

                                        @if( $customertype['disable_status'] == 0 )
                                            <?php /*<td> <a class="btn btn-default" href="<?php echo URL::to( Config::get('constants.admin_path').'customertype/status/disable/'.$customertype['id']) ?>"><i class="fa fa-check"></i></a></td>*/ ?>
                                            <td><button type="button" onclick="deact_customertype('{{ $customertype['id'] }}','<?php echo URL::to( Config::get('constants.admin_path').'customertype/status/disable/'.$customertype['id']); ?>')" class="btn btn-default deact" ><i class="fa fa-check"></i></button></td>
                                        @else
                                            <td> <a class="btn btn-default" href="<?php echo URL::to( Config::get('constants.admin_path').'customertype/status/enable/'.$customertype['id']) ?>"><i class="fa fa-times"></i></a></td>                            
                                        @endif
                                        
                                        <td><a class="btn btn-default" href="<?php echo URL::to( Config::get('constants.admin_path').'customertype/edit/'.$customertype['id']) ?>"><i class="fa fa-pencil"></i></a></td>
                                        <td><a class="btn btn-default" href="#" onclick="customertype_delete(<?php echo $customertype['id'] ?>)"><i class="fa fa-trash"></i></a></td>                            
                                        </tr>
                                    <?php } ?>         
                                    <?php } ?>         

                    </tbody>
                </table>
                
                
                <!-- /.table-responsive -->                            
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<div id="deactivate_customertype_modal" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                      <form method="post" id="customertype_deact_form" action="<?php echo URL::to( Config::get('constants.admin_path').'brand/status/disable/'); ?>" >
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Deactivate Customer Type</h4>
                      </div>
                      <div class="modal-body">
                        <p>Give reason to deactivate this customer type</p>
                        <input type="hidden" name="id" id="customertype_deactive_id">
                        <textarea required class="form-control" class="col-sm-12" placeholder="Reason in brief.." name="disable_reason"></textarea>
                      </div>
                      <div class="modal-footer">
                        <button class="btn">Deactivate</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                    </form>
                  </div>
                </div>
<div id="delete_customertype_modal" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                      <form method="post" id="customertype_delete_form" action="<?php echo URL::to( Config::get('constants.admin_path').'brand/status/disable/'); ?>" >
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete Customer Type</h4>
                      </div>
                      <div class="modal-body">
                        <p>Give reason to delete this customer type</p>
                        <input type="hidden" name="id" id="customertype_delete_id">
                        <textarea required class="form-control" class="col-sm-12" placeholder="Reason in brief.." name="delete_reason"></textarea>
                      </div>
                      <div class="modal-footer">
                        <button class="btn">Delete</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                    </form>
                  </div>
                </div>
@stop