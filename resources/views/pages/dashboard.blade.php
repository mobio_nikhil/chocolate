@extends('layouts.default')
@section('content_header')
Dashboard
@stop
@section('content')
<style>
    .inactiveLink {
  
}
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="col-md-3 col-sm-6 dashboard_count_div_first">
            <div class="ibox float-e-margins">
                <a href="<?php echo URL::to( Config::get('constants.admin_path').'meeting/today_followup') ?>" class="request_count_link inactiveLink">
                    <div class="ibox-content content_count" style="background-color: #418BCA; ">
                        <div class="pull-left dashboard_image_div">
                            <img class="dashboard_image" src="{!! asset('images/today_dashboard.png') !!}" alt="Today Followups">
                        </div>
                        <div class="dashboard_text">
                            Today's Follow ups
                            <h1 class="dashboard_count"><?= $dashboard['TodayFollowup']; ?></h1>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 dashboard_count_div_last">
            <div class="ibox float-e-margins">
                <a href="<?php echo URL::to( Config::get('constants.admin_path').'meeting/upcoming_followup') ?>" class="request_count_link inactiveLink">
                    <div class="ibox-content content_count" style="background-color: #1CB09A; ">
                        <div class="pull-left dashboard_image_div">
                            <img class="dashboard_image" src="{!! asset('images/seven_dashboard.png') !!}" alt="Upcoming Followups">
                        </div>
                        <div class="dashboard_text">
                            Upcoming Follow ups
                            <h1 class="dashboard_count"><?= $dashboard['UpcomingFollowup']; ?></h1>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="ibox float-e-margins">
                <a href="<?php echo URL::to( Config::get('constants.admin_path').'meeting/today_meeting') ?>" class="request_count_link">
                    <div class="ibox-content content_count" style="background-color: #418BCA; ">
                        <div class="pull-left dashboard_image_div">
                            <img class="dashboard_image" src="{!! asset('images/meeting.png') !!}" alt="Today Meetings">
                        </div>
                        <div class="dashboard_text">
                            Today's Meeting
                            <h1 class="dashboard_count"><?= $dashboard['TodayMeeting']; ?></h1>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="ibox float-e-margins">
                <a href="<?php echo URL::to( Config::get('constants.admin_path').'customer/today_customer') ?>" class="request_count_link inactiveLink">
                    <div class="ibox-content content_count" style="background-color: #D9544F; ">
                        <div class="pull-left dashboard_image_div">
                            <img class="dashboard_image" src="{!! asset('images/cutomers.png') !!}" alt="Customer">
                        </div>
                        <div class="dashboard_text">
                            New Customers
                            <h1 class="dashboard_count"><?= $dashboard['TotalCustomer']; ?></h1>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <?php 
            if(!($user_session['user_type'] == 2 || $user_session['user_type'] == 3)) { ?>
        <div class="col-md-4">
            <h4 class="page-header dashboard-subtitle">Monthly Meetings</h4>
            <?php 
            if(!($user_session['user_type'] == 2 || $user_session['user_type'] == 3)) { ?>
            <div id="piechart" style="width: 100%; height: 500px;"></div>
            <?php } ?>
            <?php /* <h4 class="page-header dashboard-subtitle">Employees Location</h4>
              <div class="row">
              <form role="form" action="<?php echo URL::to(Config::get('constants.admin_path') . 'dashboard') ?>" method="post">
              <div class="form-group">
              <label for="birthday" class="col-xs-2 control-label" style="padding:5px 15px;">Filter By : </label>
              <div class="form-inline">
              <div class="form-group">
              <select class="form-control" name="region_name"  id="region_name" onchange="getFilterData(this)">
              <option value="">Select Region</option>
              <?php foreach ($region_list as $region) { ?>
              <option value="<?php echo $region['id'] ?>"><?php echo $region['region_name']; ?></option>
              <?php } ?>
              </select>
              </div>
              <div class="form-group">
              <select class="form-control" name="area_name" id="area_name" onchange="getFilterData(this)">
              <option value="">Select Area</option>
              <?php foreach ($area_list as $area) { ?>
              <option value="<?php echo $area['id'] ?>"><?php echo $area['area_name']; ?></option>
              <?php } ?>
              </select>
              </div>
              <div class="form-group">
              <select class="form-control" name="employee_name" id="employee_name">
              <option value="">Select Employee</option>
              </select>
              </div>
              <button onclick="udpate_map(event)" type="submit" class="btn btn-default">Submit</button>
              </div>
              </div>
              </form>
              </div>
              <div class="row">
              <div class="panel-body map-section-div">
              <?php
              if (count($user_gps) > 0) {
              ?>
              <style>
              #map_wrapper {
              height: 400px;
              }

              #map_canvas {
              width: 100%;
              height: 100%;
              }
              </style>

              <div id="map_wrapper">
              <div id="map_canvas" class="mapping"></div>
              </div>

              <script src="https://maps.googleapis.com/maps/api/js?sensor=false&key=<?php echo Config::get('constants.google_api_key'); ?>"></script>

              <script type="text/javascript">
              function init() {
              var map;
              var polyline;
              var markers = [];
              <?php for ($i = 0; $i < count($user_gps); $i++) { ?>
              markers['<?php echo $i; ?>'] = new google.maps.LatLng('<?php echo $user_gps[$i]['latitude']; ?>', '<?php echo $user_gps[$i]['longitude']; ?>');
              <?php } ?>

              var infoWindowContent = [];
              <?php for ($i = 0; $i < count($user_gps); $i++) { ?>
              infoWindowContent['<?php echo $i; ?>'] = ['<div class="info_content">' +
              '<p> <b>Name : </b>' + '<?php echo $user_gps[$i]['user_name']; ?>' + '</p>' +
              '<p> <b>Staus : </b>' + '<?php echo $user_gps[$i]['map_header']; ?>' + '</p>' +
              '<p> <b>Address : </b>' + '<?php echo $user_gps[$i]['map_content']; ?>' + '</p>' +
              '<p> <b>Time : </b>' + '<?php echo $user_gps[$i]['map_time_date']; ?>' + '</p>' +
              '</div>'];
              <?php } ?>


              var moptions = {
              center: new google.maps.LatLng('<?php echo $user_gps[0]['latitude'] ?>', '<?php echo $user_gps[0]['longitude']; ?>'),
              zoom: 12,
              mapTypeId: google.maps.MapTypeId.ROADMAP
              }

              map = new google.maps.Map(document.getElementById("map_canvas"), moptions);

              var iconsetngs = {
              path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
              };
              var polylineoptns = {
              strokeOpacity: 0.8,
              strokeWeight: 3,
              map: map,
              icons: [{
              repeat: '70px', //CHANGE THIS VALUE TO CHANGE THE DISTANCE BETWEEN ARROWS
              icon: iconsetngs,
              offset: '100%'}]
              };
              polyline = new google.maps.Polyline(polylineoptns);

              var z = 0;
              var path = [];
              path[z] = polyline.getPath();
              var infoWindow = new google.maps.InfoWindow(), marker, i;
              for (var i = 0; i < markers.length; i++) //LOOP TO DISPLAY THE MARKERS
              {
              var pos = markers[i];
              var marker = new google.maps.Marker({
              position: pos,
              map: map
              });

              google.maps.event.addListener(marker, 'click', (function (marker, i) {
              return function () {
              infoWindow.setContent(infoWindowContent[i][0]);
              infoWindow.open(map, marker);
              }
              })(marker, i));
              // path[z].push(marker.getPosition()); //PUSH THE NEWLY CREATED MARKER'S POSITION TO THE PATH ARRAY

              }
              }
              window.onload = init;
              </script>
              <?php } else { ?>

              No meeting data found


              <?php } ?>

              <!-- /.table-responsive -->
              </div>
              <div class="panel-body map-section-not" style="display:none;">
              No meeting data found
              </div>
              </div> */ ?>
        </div>
        <div class="col-md-3">
            <h4 class="page-header dashboard-subtitle">Attended Meetings</h4>
                <div class="panel-group" id="accordion">                    
                                <?php for($chart_display = 0;$chart_display<count($chart['region']);$chart_display++){ ?>
                                   <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#child_<?php echo $chart_display ?>"><?php echo $chart['region'][$chart_display]['region_name']. ' ('. $chart['region'][$chart_display]['meeting_count'] .')' ?></a>
                                            </h4>
                                        </div>
                                        <div id="child_<?php echo $chart_display ?>" class="panel-collapse collapse out">
                                            <div class="panel-body">
                                                <?php 
                                                for($chart_display_state = 0;$chart_display_state<count($chart['region'][$chart_display]['state']);$chart_display_state++){ ?>
                                                <?php echo $chart['region'][$chart_display]['state'][$chart_display_state]['state_name'].' ('.$chart['region'][$chart_display]['state'][$chart_display_state]['meeting_count'] .')'.'<br/>' ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div> 
                                <?php } ?>
                            </div>
        </div>
            <?php }else{ ?>
        <div class="col-md-4"></div>                
        <div class="col-md-3"></div>                
            <?php } ?>
        <div class="col-md-5">
            <h4 class="page-header dashboard-subtitle">Today's Users Status</h4>

            <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                <li class="active"><a href="#notworking" data-toggle="tab">Not logged in</a></li>
                <li><a href="#working" data-toggle="tab">Not active (<?php echo Config::get('constants.not_active_hour') ?> hour)</a></li>
            </ul>
            <div id="my-tab-content" class="tab-content">
                <div class="tab-pane active" id="notworking">
                    <?php
                    if (count($employee_status['NotworkingUser']) > 0) {
                        ?>
                        <ul class="list-group untrack-employee">
                            <?php foreach ($employee_status['NotworkingUser'] as $keys => $values) { ?>
                            <li class = "list-group-item"><span class="badge"><i class="fa fa-pause-circle" aria-hidden="true"></i></span><?= $values['user_name'] ?> <a href="tel:<?= $values['user_phone'] ?>"><?= $values['user_phone'] ?></a></li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </div>
                <div class="tab-pane" id="working">
                    <?php
                    if (count($employee_status['workingUser']) > 0) {
                        ?>
                        <ul class="list-group untrack-employee">
                            <?php foreach ($employee_status['workingUser'] as $keys => $values) { ?>
                                <li class = "list-group-item"><span class="badge"><i class="fa fa-stop-circle" aria-hidden="true"></i></span><?= $values['user_name'] ?> <a href="tel:<?= $values['user_phone'] ?>"><?= $values['user_phone'] ?></a></li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </div>

            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .ibox {
        clear: both;
        margin-bottom: 25px;
        margin-top: 0;
        padding: 0;
    }
    .untrack-employee .list-group-item > .badge {
        float: left;
        margin-right:15px;
    }

    .nav > li.active > a{
        color:gray;
        font-weight:bold;
    }
    .untrack-employee li a{
        padding-right:25px;
    }
    .dashboard-subtitle{
        font-weight:bold;
    }
    .request_count_link {
        color: #676A6C;
    }
    .dashboard_count_div_first{
        padding-left:0px;
    }
    .dashboard_count_div_last{
        padding-right:0px;
    }
    .ibox-content {
        background-color: white;
        color: inherit;
        padding: 15px 20px 20px 20px;
        border-color: #E7EAEC;
        border-image: none;
        border-style: solid solid none;
        border-width: 1px 0px;
    }
    .content_count {
        height: 110px;
    }
    .dashboard_image_div {
        vertical-align: middle;
        margin-left: -15px;
        width: 40%;
    }
    .dashboard_image {
        margin-top: 5px;
        width: 70px;
        height: 70px;
        margin-left: 6px;
    }
    .dashboard_text {
        float: right;
        width: 60%;
        color: white;
        font-size: 16px;
        float: left;
        padding-left: 11px;
    }
    .dashboard_count {
        font-weight: bold;
        margin-top:5px;
    }
</style>
<script type="text/javascript">

    jQuery(document).ready(function () {
        jQuery("#region_name").val("");
        jQuery("#area_name").val("");
        jQuery("#employee_name").val("");
    });

    function getFilterData(object) {

        var region = jQuery("#region_name").val();
        var areaId = jQuery("#area_name").val();
        var EmployeeId = jQuery("#employee_name").val();

        jQuery.ajax({
            url: '<?php echo URL::to(Config::get('constants.admin_path') . 'dashboard/map') ?>',
            type: 'POST',
            data: {'regionId': region, 'areaId': areaId, 'employeeId': EmployeeId},
            async: false,
            cache: false,
            timeout: 30000,
            error: function () {
                return true;
            },
            success: function (response) {
                if (response.statusCode == "1") {
                    jQuery('select#area_name').children('option:not(:first)').remove();
                    jQuery('select#employee_name').children('option:not(:first)').remove();
                    jQuery.each(response.data.area, function (i, item) {
                        jQuery('#area_name').append($('<option>', {
                            value: i,
                            text: item
                        }));
                    });

                    jQuery.each(response.data.employee, function (i, item) {
                        jQuery('#employee_name').append($('<option>', {
                            value: i,
                            text: item
                        }));
                    });

                    if (region == "") {
                        jQuery("#area_name").val("");
                        jQuery("#employee_name").val("");
                    } else {
                        jQuery('#region_name').val(region);
                    }

                    if (areaId == "") {
                        jQuery("#employee_name").val("");
                    } else {
                        jQuery('#area_name').val(areaId);
                    }
                }
            }
        });
    }


    function udpate_map(event) {
        event.preventDefault();
        var region = jQuery("#region_name").val();
        var areaId = jQuery("#area_name").val();
        var EmployeeId = jQuery("#employee_name").val();

        jQuery.ajax({
            url: '<?php echo URL::to(Config::get('constants.admin_path') . 'dashboard/map-updates') ?>',
            type: 'POST',
            data: {'regionId': region, 'areaId': areaId, 'employeeId': EmployeeId},
            async: false,
            cache: false,
            timeout: 30000,
            error: function () {
                return true;
            },
            success: function (response) {
                if (response.statusCode == "1") {
                    if (response.data.count > 0) {
                        jQuery(".map-section-not").hide();
                        jQuery(".map-section-div").show();
                        mapupdate(response.data.mapdata);
                    } else {
                        jQuery(".map-section-div").hide();
                        jQuery(".map-section-not").show();
                    }
                }
            }
        });
    }


    function mapupdate(dataCollection) {
        var map;
        var polyline;
        var markers = [];

        var markerCount = 0;
        for (var items in dataCollection) {
            markers[markerCount] = new google.maps.LatLng(dataCollection[items].latitude, dataCollection[items].longitude);
            markerCount++;
        }

        var infoWindowContent = [];
        var infoWindow = 0;
        for (var items in dataCollection) {
            infoWindowContent[infoWindow] = ['<div class="info_content">' +
                        '<p> <b>Name : </b>' + dataCollection[items].user_name + '</p>' +
                        '<p> <b>Status : </b>' + dataCollection[items].map_header + '</p>' +
                        '<p> <b>Address : </b>' + dataCollection[items].map_content + '</p>' +
                        '<p> <b>Time : </b>' + dataCollection[items].map_time_date + '</p>' +
                        '</div>'];
            infoWindow++;
        }
        var moptions = {
            center: new google.maps.LatLng(dataCollection[0].latitude, dataCollection[0].longitude),
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        map = new google.maps.Map(document.getElementById("map_canvas"), moptions);

        var iconsetngs = {
            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
        };
        var polylineoptns = {
            strokeOpacity: 0.8,
            strokeWeight: 3,
            map: map,
            icons: [{
                    repeat: '70px', //CHANGE THIS VALUE TO CHANGE THE DISTANCE BETWEEN ARROWS
                    icon: iconsetngs,
                    offset: '100%'}]
        };
        polyline = new google.maps.Polyline(polylineoptns);

        var z = 0;
        var path = [];
        path[z] = polyline.getPath();
        var infoWindow = new google.maps.InfoWindow(), marker, i;
        for (var i = 0; i < markers.length; i++) //LOOP TO DISPLAY THE MARKERS
        {
            var pos = markers[i];
            var marker = new google.maps.Marker({
                position: pos,
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infoWindow.setContent(infoWindowContent[i][0]);
                    infoWindow.open(map, marker);
                }
            })(marker, i));
        }
    }
</script>
<?php /*
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {

  var collection = [];
  //alert('hh');
  collection[0] = ['Region', 'Meeting of the month']       ;
  //collection[1] = ['Work',     11]      ;

  <?php for($chart_data = 0; $chart_data < count($chart['region']) ;$chart_data++) { ?>
  //collection[<?php echo $chart_data + 1 ?>] = ['<?php echo $chart['region'][$chart_data]['region_name'] ?>' + ',' + '<?php echo $chart['region'][$chart_data]['meeting_count'] ?>'];
  collection[<?php echo $chart_data + 1 ?>] = ['<?php echo $chart['region'][$chart_data]['region_name'] ?>' ,  parseInt(<?php echo $chart['region'][$chart_data]['meeting_count'] ?>)];
  //alert(collection[<?php echo $chart_data + 1 ?>]);
  //collection[<?php echo $chart_data + 1 ?>] = ['Work',     11]      ;

  <?php } ?>

  //alert(collection[1]);
  var data = google.visualization.arrayToDataTable( collection
  );

  //var data = google.visualization.arrayToDataTable([
  //['Region', 'Meeting of the month'],
  //['Work',     11],
  //['Eat',      2],
  //['Commute',  2],
  //['Watch TV', 2],
  //['Sleep',    7]
  //]);

  var options = {
  title: 'Month Activities',
  chartArea:{left:10,top:20,width:"100%",height:"100%"}
  };

  var chart = new google.visualization.PieChart(document.getElementById('piechart'));

  chart.draw(data, options);
  }

  </script> */ ?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>


<script>
google.load("visualization", "1", {
    packages: ["corechart"]
});
google.setOnLoadCallback(drawChart);

function drawChart() {
    var collection = [];
    collection[0] = ['Region', 'Meeting of the month'];

<?php for ($chart_data = 0; $chart_data < count($chart['region']); $chart_data++) { ?>
        //collection[<?php echo $chart_data + 1 ?>] = ['<?php echo $chart['region'][$chart_data]['region_name'] ?>' + ',' + '<?php echo $chart['region'][$chart_data]['meeting_count'] ?>'];
        collection[<?php echo $chart_data + 1 ?>] = ['<?php echo $chart['region'][$chart_data]['region_name'] ?>', parseInt(<?php echo $chart['region'][$chart_data]['meeting_count'] ?>)];
        //alert(collection[<?php echo $chart_data + 1 ?>]);
        //collection[<?php echo $chart_data + 1 ?>] = ['Work',     11]      ;

<?php } ?>

    //alert(collection[1]);
    var data = google.visualization.arrayToDataTable(collection
            );

    /*var data = google.visualization.arrayToDataTable([
     ['Language', 'Speakers (in millions)'],
     ['Assamese', 13],
     ['Bengali', 83],
     ['Bodo', 1.4],
     ['Dogri', 2.3],
     ['Gujarati', 46],
     ['Hindi', 300],
     ['Kannada', 38],
     ['Kashmiri', 5.5],
     ['Konkani', 5],
     ['Maithili', 20],
     ['Malayalam', 33],
     ['Manipuri', 1.5],
     ['Marathi', 72],
     ['Nepali', 2.9],
     ['Oriya', 33],
     ['Punjabi', 29],
     ['Sanskrit', 0.01],
     ['Santhali', 6.5],
     ['Sindhi', 2.5],
     ['Tamil', 61],
     ['Telugu', 74],
     ['Urdu', 52]
     ]);*/

    var options = {
        title: '',  
        'height':200,
        chartArea:{left:5,top:1,width:"100%",height:"100%"}
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart'));
    chart.draw(data, options);

}

</script>

@stop