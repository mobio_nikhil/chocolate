@extends('layouts.default')
@section('content_header')
Customer
@stop

@section('content')
<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Create Customer 
                        </div>
                        
                    @if ($errors->has())
                    <div class="alert alert-danger">            
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>        
                        @endforeach
                    </div>
                    @endif   
                    
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif                          
                        
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="<?php echo URL::to( Config::get('constants.admin_path').'customer/store') ?>" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>Customer Name</label>                                            
                                            <input class="form-control" placeholder="customer name" name="customer_name" type="text" value="{{ Input::old('customer_name') }}" autofocus/>
                                        </div>                                        
                                        <div class="form-group">
                                            <label>Business Name</label>
                                            <input class="form-control" placeholder="Business name" name="business_name" type="text" value="{{ Input::old('business_name') }}" autofocus/>
                                        </div>                                        
                                        <div class="form-group">
                                            <label>Business Address</label>
                                            <textarea class="form-control" rows="3" placeholder="Business Address" name="business_address" autofocus>{{ Input::old('business_address') }}</textarea>
                                        </div>                                        
                                        <div class="form-group">
                                            <label>Customer Type</label>
                                            <select class="form-control" id="customertypeSelect" name="customertype">
                                                <?php foreach($customertype_list as $ct){ ?>
                                                <option <?php echo (Input::old('customertype') ==  $ct['id']) ? 'selected' : '' ?> value="<?php echo $ct['id'] ?>"><?php echo $ct['customertype_name'] ?></option>                                                    
                                                <?php } ?>
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Region</label>
                                            <select class="form-control" id="regionSelect" name="region">
                                                <option value="0">Select</option>
                                                <?php foreach($region_list as $region){ ?>
                                                <option <?php echo (Input::old('region') ==  $region['id']) ? 'selected' : '' ?> value="<?php echo $region['id'] ?>"><?php echo $region['region_name'] ?></option>                                                    
                                                <?php } ?>
                                            </select>
                                        </div>                                        
                                        
                                        <div class="form-group">
                                            <label>State</label>
                                            <select class="form-control" id="stateSelect" name="state">                         </option>
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>City</label>
                                            <select class="form-control" id="citySelect" name="city">                         </option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Area</label>
                                            <select class="form-control" id="areaSelect" name="area">                                                       
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Pincode</label>
                                            <input class="form-control" placeholder="Pincode" name="pincode" type="text" value="{{ Input::old('pincode') }}" autofocus maxlength="6"/>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Photo</label>
                                            <input type="file" name="primary_image">
                                        </div>                                        
                                        
                                        <div class="form-group">
                                            <label>Visit Card Image</label>
                                            <input type="file" name="visit_card_image_1">
                                        </div>                                        
                                        <?php /*
                                        <div class="form-group">
                                            <label>Primary Contact Name</label>
                                            <input class="form-control" placeholder="Primary Contact Name" name="primary_contact_name" type="text" value="{{ Input::old('primary_contact_name') }}" autofocus/>
                                        </div>  */ ?>                                      
                                        
                                        <div class="form-group">
                                            <label>Secondary Contact Name</label>
                                            <input class="form-control" placeholder="Secondary Contact Name" name="secondary_contact_name" type="text" value="{{ Input::old('secondary_contact_name') }}" autofocus/>
                                        </div>                                        
                                        
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" placeholder="Email" name="customer_email" type="text" value="{{ Input::old('customer_email') }}" autofocus/>
                                        </div>                                        
                                        
                                        <div class="form-group">
                                            <label>Customer Phone</label>
                                            <input class="form-control" placeholder="Customer Phone" name="customer_phone" type="text" value="{{ Input::old('customer_phone') }}" autofocus/>
                                        </div>                                        
                                        
                                        <div class="form-group">
                                            <label>Birth Date</label>
                                            <input class="form-control bdate" placeholder="Birth Date" name="customer_birth_date" type="text" value="{{ Input::old('customer_birth_date') }}" autofocus/>
                                        </div>                                        
                                        
                                        <div class="form-group">
                                            <label>Anniversary Date</label>
                                            <input class="form-control adate" placeholder="Anniversary Date" name="customer_anniversary_date" type="text" value="{{ Input::old('customer_anniversary_date') }}" autofocus/>
                                        </div>                                        
                                        <button type="submit" class="btn btn-default">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
@stop   