@extends('layouts.default')
@section('content_header')
New Customers
<br/>    

@stop
@section('content')
<style>
    tfoot {
        display: table-header-group;
    }
    .dataTables_filter {
    display: none;
    }     
</style>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Customer List
            </div>
            
                @if ($errors->has())
                    <div class="alert alert-danger">            
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>        
                        @endforeach
                    </div>
                    @endif   
                    
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif             
            <!-- /.panel-heading -->
            <div class="panel-body">
            <table id="employee_grid" class="display table" width="100%" cellspacing="0">
                    <thead>
                        <tr>                            
                            <th>Business Name</th>                            
                            <th>Customer Name</th>                            
                            <th>Business Address</th>                            
                            <th>Primary Name</th>                            
                            <th>Image</th>                            
                            <th>Secondary Name</th>                            
                            <th>Email</th>                            
                            <th>Contact No</th>
                            <th>Visting Card</th>
                            <th>Birth Date</th>
                            <th>Anniversary Date</th>
                            <th>Entered By</th>
                            <th>Region</th>
                            <th>Area</th>
                            <th>Total Meeting</th>
                            <th>Create Date</th>
                            <th>Approve</th>
                            
                            <?php /*<th>Edit</th>
                            <th>Delete</th>
                            <th>History</th> 
                            <th>Status</th>*/?>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Business Name</th>                            
                            <th>Customer Name</th>                            
                            <th>Business Address</th>                            
                            <th>Primary Name</th>                            
                            <th>Image</th>                            
                            <th>Secondary Name</th>                            
                            <th>Email</th>                            
                            <th>Contact No</th>
                            <th>Visting Card</th>
                            <th>Birth Date</th>
                            <th>Anniversary Date</th>
                            <th>Entered By</th>
                            <th>Region</th>
                            <th>Area</th>
                            <th>Total Meeting</th>
                            <th>Create Date</th>
                            <th>Approve</th>
                            
                            <?php /*<th>Status</th>
                            <th>Edit</th>
                            <th>Delete</th>
                            <th>History</th> */?>
                        </tr>
                    </tfoot>
                    <tbody>
                                    <?php foreach($customer_list as $customer){ ?>
                                        <tr>
                                        <td><?php echo $customer['business_name'] ;?></td>
                                        <td><?php echo $customer['customer_name'] ;?></td>
                                        <td><?php echo $customer['business_address'] ;?></td>
                                        <td><?php echo $customer['primary_contact_name'] ;?></td>
                                        <td><a  target="_blank" href="<?php echo URL::to(Input::root().'/' .$customer['primary_image']) ;?>">Image</a></td>
                                        <td><?php echo $customer['secondary_contact_name'] ;?></td>
                                        <td><a target="_blank" href="mailto:{{ $customer['customer_email'] }}"> <?php echo $customer['customer_email'] ;?></a></td>
                                        <td><a target="_blank" href="tel:{{ $customer['customer_phone'] }}"> <?php echo $customer['customer_phone'] ;?></a></td>
                                        <td><a  target="_blank" href="<?php echo URL::to(Input::root().'/' .$customer['visit_card_image_1']) ;?>">Image</a></td>                                        
                                        <td><?php echo date('d-M-Y',strtotime($customer['customer_birth_date'])) ;?></td>
                                        <td><?php echo date('d-M-Y',strtotime($customer['customer_anniversary_date'])) ;?></td>
                                        <td><?php echo $customer['user_name'] ;?></td>                                        
                                        <td><?php echo $customer['region_name'] ;?></td>                                        
                                        <td><?php echo $customer['area_name'] ;?></td>                                        
                                        <td><?php echo $customer['meeting_count'] ;?></td>                                        
                                        <td><?php echo date('d-M-Y',strtotime($customer['created_on'])) ;?></td>                                        
                                        <?php /*<td>
                                            @if( $customer['disable_status'] == 0)  
                                            <a href="<?php echo URL::to( Config::get('constants.admin_path').'customer/change_status/disable/'.$customer['id']);?>
                                            ">Deactive</a>
                                            @else
                                                <a href="<?php echo URL::to( Config::get('constants.admin_path').'customer/change_status/enable/'.$customer['id']);?>">Active</a>
                                            @endif
                                        </td>
                                        <td><a href="<?php echo URL::to( Config::get('constants.admin_path').'customer/edit/'.$customer['id']); ?>">Edit</a></td>
                                        <td><a onclick="cust_delete(<?php echo $customer['id']; ?>)"> Delete</a></td>                                        
                                        <td><a href="<?php echo URL::to( Config::get('constants.admin_path').'customer/customer_history/'.$customer['id']); ?>">History</a></td> */ ?>
                                        
                                        <td><a href="<?php echo URL::to( Config::get('constants.admin_path').'customer/approve/'.$customer['id']); ?>">Approve</a></td>
                                        </tr>


                                    <?php } ?>         

                    </tbody>
                </table>
                
                
                <!-- /.table-responsive -->                            
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
@stop