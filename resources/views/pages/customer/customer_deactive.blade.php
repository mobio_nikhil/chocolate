@extends('layouts.default')
@section('content_header')
Deactivated Customers 
<br/>

@stop
@section('content')
<style>
    tfoot {
        display: table-header-group;
    }
    .dataTables_filter {
    display: none;
    }     
</style>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Deactivated List
            </div>
            
                @if ($errors->has())
                    <div class="alert alert-danger">            
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>        
                        @endforeach
                    </div>
                    @endif   
                    
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif             
            <!-- /.panel-heading -->
            <div class="panel-body">
                <!--  -->
                <!--  -->

<table id="employee_grid" class="display table" width="100%" cellspacing="0">
                    <thead>
                        <tr>                            
                            <th>Entered By</th>
                            <th>Business Name</th>                            
                            <th>Customer Name</th>                            
                            <th>Area</th>
                            <th>Business Address</th>                            
<!--                            <th>Primary Contact</th>                            -->
                            <th>Secondary Contact</th>                            
                            <th>Email</th>                            
                            <th>Phone No</th>
                            <th>Photo</th>                            
                            <th>Visting Card</th>
                            <th>Customer Type</th>
                            <th>Birth Date</th>
                            <th>Anniversary Date</th>
                            <th>Region</th>
                            <th>State</th>
                            <th>City</th>
                            <th>Pincode</th>                            
                            <th>Total Meeting</th>
                            <th>Create Date</th>
                            <th>Deactivate Reason</th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Entered By</th>
                            <th>Business Name</th>                            
                            <th>Customer Name</th>                            
                            <th>Area</th>
                            <th>Business Address</th>                            
<!--                            <th>Primary Contact</th>                            -->
                            <th>Secondary Contact</th>                            
                            <th>Email</th>                            
                            <th>Phone No</th>
                            <th>Photo</th>                            
                            <th>Visting Card</th>
                            <th>Customer Type</th>
                            <th>Birth Date</th>
                            <th>Anniversary Date</th>
                            <th>Region</th>
                            <th>State</th>
                            <th>City</th>
                            <th>Pincode</th>                            
                            <th>Total Meeting</th>
                            <th>Create Date</th>
                            <th>Deactivate Reason</th>
                            <th></th>
                        </tr>
                    </tfoot>
                    <tbody>
                                    <?php foreach($customer_list as $customer){ 
                                        if(empty($customer['customer_birth_date'])){
                                            $bdate = 'NA';
                                        }else{
                                            $bdate = date('d-M-Y',strtotime($customer['customer_birth_date']));
                                        }

                                        if(empty($customer['customer_anniversary_date'])){
                                            $adate = 'NA';
                                        }else{
                                            $adate = date('d-M-Y',strtotime($customer['customer_anniversary_date']));
                                        }                                        
                                        
                                        ?>
                                        <tr>
                                        <td><?php echo $customer['user_name'] ?></td>                                        
                                        
                                        <td><?php echo $customer['business_name'] ?></td>
                                        <td><?php echo $customer['customer_name'] ?></td>
                                        <td><?php echo $customer['area_name'] ?></td>                                        
                                        <td><?php echo $customer['business_address'] ?></td>
                                        <?php /*<td><?php echo $customer['primary_contact_name'] ?></td>
                                         <td><a  target="_blank" href="<?php echo URL::to(Input::root().'/' .$customer['primary_image']) ?>">Image</a></td> */ ?>
                                        <td><?php echo empty($customer['secondary_contact_name']) ? 'NA' : $customer['secondary_contact_name'] ?></td>
                                        <td><?php echo $customer['customer_email'] ?></td>
                                        <td><?php echo $customer['customer_phone'] ?></td>
                                        <td><?php if(empty($customer['primary_image'])) { echo 'NA'; } else {  ?><a  target="_blank" href="<?php echo URL::to(Input::root().'/' .$customer['primary_image']) ?>">Image</a> <?php } ?></td>
                                        <td><?php if(empty($customer['visit_card_image_1'])) { echo 'NA'; } else {  ?><a  target="_blank" href="<?php echo URL::to(Input::root().'/' .$customer['visit_card_image_1']) ?>">Image</a> <?php } ?></td>                                        
                                        <td><?php echo $customer['customertype_name'] ?></td>
                                        <td><?php echo $bdate ?></td>
                                        <td><?php echo $adate ?></td>
                                        <td><?php echo $customer['region_name'] ?></td>                                        
                                        <td><?php echo $customer['state_name'] ?></td>                                        
                                        <td><?php echo $customer['city_name'] ?></td>                                        
                                        <td><?php echo $customer['pincode'] ?></td>                                                                                
                                        <td><?php echo $customer['meeting_count'] ?></td>                                        
                                        <td><?php echo date('d-M-Y',strtotime($customer['created_on'])) ?></td>             
                                        <td> {{ $customer['disable_reason'] }}</td>
                                        <td>
                                                <form method="post" action="<?php echo URL::to( Config::get('constants.admin_path').'customer/change_status/enable/'); ?>" >
                                                         <input type="hidden" value="{{ $customer['id'] }}" name="id">
                                                         <button type="submit"  class="btn btn-default deact" >Click To Active</button>
                                                         
                                                        
                                                </form>                                      
                                        </td>                           
                                        </tr>

                                    <?php } ?>         

                    </tbody>
                </table>                
                
                <!-- /.table-responsive -->                            
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
@stop