@extends('layouts.default')
@section('content_header')
Today's Customers
<br/><br/>
   
@stop
@section('content')
<style>
    tfoot {
        display: table-header-group;
    }
    .dataTables_filter {
    display: none;
    }     
</style>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Customer List
            </div>
            
                @if ($errors->has())
                    <div class="alert alert-danger">            
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>        
                        @endforeach
                    </div>
                    @endif   
                    
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif             
            <!-- /.panel-heading -->
            <div class="panel-body">
            <table id="employee_grid" class="display table" width="100%" cellspacing="0">
                    <thead>
                        <tr>                            
                            <th>Entered By</th>
                            <th>Business Name</th>                            
                            <th>Customer Name</th>                            
                            <th>Area</th>
                            <th>Business Address</th>                            
<!--                            <th>Primary Contact</th>                            -->
                            <th>Secondary Contact</th>                            
                            <th>Email</th>                            
                            <th>Phone No</th>
                            <th>Photo</th>                            
                            <th>Visting Card</th>
                            <th>Birth Date</th>
                            <th>Anniversary Date</th>
                            <th>Region</th>
                            <th>State</th>
                            <th>City</th>
                            <th>Pincode</th>
                            <th>Total Meeting</th>
                            <th>Create Date</th>
                            <th>Edit</th>
                            <th>Status</th>
                            <th>Delete</th>
                            <th>Meeting History</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Entered By</th>
                            <th>Business Name</th>                            
                            <th>Customer Name</th>                            
                            <th>Area</th>
                            <th>Business Address</th>                            
<!--                            <th>Primary Contact</th>                            -->
                            <th>Secondary Contact</th>                            
                            <th>Email</th>                            
                            <th>Phone No</th>
                            <th>Photo</th>                            
                            <th>Visting Card</th>
                            <th>Birth Date</th>
                            <th>Anniversary Date</th>
                            <th>Region</th>
                            <th>State</th>
                            <th>City</th>
                            <th>Pincode</th>
                            <th>Total Meeting</th>
                            <th>Create Date</th>
                            <th>Edit</th>
                            <th>Status</th>
                            <th>Delete</th>
                            <th>Meeting History</th>
                        </tr>
                    </tfoot>
                    <tbody>
                                    <?php foreach($customer_list as $customer){ 
                                        if(empty($customer['customer_birth_date'])){
                                            $bdate = 'NA';
                                        }else{
                                            $bdate = date('d-M-Y',strtotime($customer['customer_birth_date']));
                                        }

                                        if(empty($customer['customer_anniversary_date'])){
                                            $adate = 'NA';
                                        }else{
                                            $adate = date('d-M-Y',strtotime($customer['customer_anniversary_date']));
                                        }
                                        ?>
                                        <tr>
                                        <td><?php echo $customer['user_name'] ?></td>                                        
                                        
                                        <td><?php echo $customer['business_name'] ?></td>
                                        <td><?php echo $customer['customer_name'] ?></td>
                                        <td><?php echo $customer['area_name'] ?></td>                                        
                                        <td><?php echo $customer['business_address'] ?></td>
                                        <?php /*<td><?php echo $customer['primary_contact_name'] ?></td>
                                         <td><a  target="_blank" href="<?php echo URL::to(Input::root().'/' .$customer['primary_image']) ?>">Image</a></td> */ ?>
                                        <td><?php echo empty($customer['secondary_contact_name']) ? 'NA' : $customer['secondary_contact_name'] ?></td>
                                        <td><a target="_blank" href="mailto:{{ $customer['customer_email'] }}"> <?php echo $customer['customer_email'] ?></a></td>
                                        <td><a target="_blank" href="tel:{{ $customer['customer_phone'] }}"> <?php echo $customer['customer_phone'] ?></a></td>
                                        <td><?php if(empty($customer['primary_image'])) { echo 'NA'; } else {  ?><a  target="_blank" href="<?php echo URL::to(Input::root().'/' .$customer['primary_image']) ?>">Image</a> <?php } ?></td>
                                        <td><?php if(empty($customer['visit_card_image_1'])) { echo 'NA'; } else {  ?><a  target="_blank" href="<?php echo URL::to(Input::root().'/' .$customer['visit_card_image_1']) ?>">Image</a> <?php } ?></td>                                        
                                        <td><?php echo $bdate ?></td>
                                        <td><?php echo $adate ?></td>
                                        <td><?php echo $customer['region_name'] ?></td>                                        
                                        <td><?php echo $customer['state_name'] ?></td>                                        
                                        <td><?php echo $customer['city_name'] ?></td>                                        
                                        <td><?php echo $customer['pincode'] ?></td>                                        
                                        <td><?php echo $customer['meeting_count'] ?></td>                                        
                                        <td><?php echo date('d-M-Y',strtotime($customer['created_on'])) ?></td>                                        
                                        <td><a href="<?php echo URL::to( Config::get('constants.admin_path').'customer/edit/'.$customer['id']) ?>">Edit</a></td>
                                        <td><div class="form-group">
                                                <button type="button" onclick="deact_cust('{{ $customer['id'] }}')" class="btn btn-default deact" ><i class="fa fa-check"></i></button>
                                                
                                            </div>
                                            
                                        </td>                                        
                                        <td><a href="javascript:void(0)" class="btn btn-default" onclick="cust_delete(<?php echo $customer['id']; ?>)"> <i class="fa fa-trash"></i></a></td>                                        
                                        <td><a href="<?php echo URL::to( Config::get('constants.admin_path').'customer/customer_history/'.$customer['id']) ?>">Meeting History</a></td>
                                        </tr>

                                    <?php } ?>         

                    </tbody>
                </table>
                <div id="deactivate_cust_modal" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                    <form method="post" action="<?php echo URL::to( Config::get('constants.admin_path').'customer/change_status/disable/'); ?>" >
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Deactivate Customer</h4>
                      </div>
                      <div class="modal-body">
                        <p>Give reason to deactivate this customer</p>
                        <input type="hidden" name="id" id="cust_id">
                        <textarea required class="form-control" class="col-sm-12" placeholder="Reason in brief.." name="disable_reason"></textarea>
                      </div>
                      <div class="modal-footer">
                        <button class="btn">Deactivate</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                    </form>
                  </div>
                </div>



                
                <!-- /.table-responsive -->                            
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
@stop