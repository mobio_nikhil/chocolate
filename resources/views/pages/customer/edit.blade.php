@extends('layouts.default')
@section('content_header')
Customer
@stop

@section('content')
<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Customer Edit
                        </div>
                        
                    @if ($errors->has())
                    <div class="alert alert-danger">            
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>        
                        @endforeach
                    </div>
                    @endif   
                    
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif                          
                        
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="<?php echo URL::to( Config::get('constants.admin_path').'customer/update/'.$customer->id) ?>" method="post"  enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>Customer Name</label>                                            
                                            <input class="form-control" placeholder="Customer name" name="customer_name" type="text" value="<?php echo (Input::old('customer_name')) ? Input::old('customer_name') : $customer->customer_name ?>" autofocus/>
                                        </div>                                        
                                        
                                        <div class="form-group">
                                            <label>Business Name</label>
                                            <input class="form-control" placeholder="Business name" name="business_name" type="text" value="<?php echo (Input::old('business_name')) ? Input::old('business_name') : $customer->business_name ?>" autofocus/>
                                        </div>                                        
                                        
                                        <div class="form-group">
                                            <label>Business Address</label>
                                            <textarea class="form-control" rows="3" placeholder="Business Address" name="business_address" autofocus><?php echo (Input::old('business_address')) ? Input::old('business_address') : $customer->business_address ?></textarea>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Customer Type</label>
                                            <select  class="form-control" name="customertype" id="customertypeSelect">
                                                <?php foreach($customertype_list as $ct){ ?>
                                                <option <?php echo ((Input::old('customertype') == $ct['id']) || ($customer->customer_type_id == $ct['id'])) ? 'selected' : '' ?> value="<?php echo $ct['id'] ?>"><?php echo $ct['customertype_name'] ?></option>
                                                <?php } ?>                                                
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Region</label>
                                            <select  class="form-control" name="region" id="regionSelect">
                                                <?php foreach($region_list as $region){ ?>
                                                <option <?php echo ((Input::old('region') == $region['id']) || ($customer->region_id == $region['id'])) ? 'selected' : '' ?> value="<?php echo $region['id'] ?>"><?php echo $region['region_name'] ?></option>                                                    
                                                <?php } ?>
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>State</label>
                                            <select class="form-control" name="state" id="stateSelect">
                                                <?php foreach($state_list as $state){ ?>
                                                <option <?php echo ((Input::old('state') == $state['id']) || ($customer->state_id == $state['id'])) ? 'selected' : '' ?> value="<?php echo $state['id'] ?>"><?php echo $state['state_name'] ?></option>                                                    
                                                <?php } ?>
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">

                                            <label>City</label>                                            
                                            <select class="form-control" name="city" id="citySelect">
                                                <?php foreach($city_list as $city){ ?>
                                                <option <?php echo ((Input::old('city') == $city['id']) || ($customer->city_id == $city['id'])) ? 'selected' : '' ?> value="<?php echo $city['id'] ?>"><?php echo $city['city_name'] ?></option>                                                    
                                                <?php } ?>
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Area</label>
                                            <select id="areaSelect" class="form-control" name="area">
                                                <?php foreach($area_list as $area){ ?>
                                                <option <?php echo ((Input::old('area') == $area['id']) || ($customer->area_id == $area['id'])) ? 'selected' : '' ?> value="<?php echo $area['id'] ?>"><?php echo $area['area_name'] ?></option>                                                    
                                                <?php } ?>
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Pincode</label>
                                            <input class="form-control" placeholder="Pincode" name="pincode" type="text" value="<?php echo (Input::old('pincode')) ? Input::old('pincode') : $customer->pincode ?>" autofocus maxlength="6"/>
                                        </div>                                        
                                        
                                        <div class="form-group">
                                            <label>Primary Image</label>
                                            <input type="file" name="primary_image">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Visit Card Image</label>
                                            <input type="file" name="visit_card_image_1">
                                        </div>
                                        <?php /*
                                        <div class="form-group">
                                            <label>Primary Contact Name</label>
                                            <input class="form-control" placeholder="Primary Contact Name" name="primary_contact_name" type="text" value="<?php echo (Input::old('primary_contact_name')) ? Input::old('primary_contact_name') : $customer->primary_contact_name ?>" autofocus/>
                                        </div> */ ?>
                                        
                                        <div class="form-group">
                                            <label>Secondary Contact Name</label>
                                            <input class="form-control" placeholder="Secondary Contact Name" name="secondary_contact_name" type="text" value="<?php echo (Input::old('secondary_contact_name')) ? Input::old('secondary_contact_name') : $customer->secondary_contact_name ?>" autofocus/>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" placeholder="Email" name="customer_email" type="text" value="<?php echo (Input::old('customer_email')) ? Input::old('customer_email') : $customer->customer_email ?>" autofocus/>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Customer Phone</label>
                                            <input class="form-control" placeholder="Customer Phone" name="customer_phone" type="text" value="<?php echo (Input::old('customer_phone')) ? Input::old('customer_phone') : $customer->customer_phone;?>" autofocus/>
                                        </div>
                                        <?php 
                                                if( $customer->customer_birth_date !='' && $customer->customer_birth_date!=NULL)
                                                { 
                                                    $datearray = explode('-', $customer->customer_birth_date);
                                                    //print_r($datearray);                                            
                                                    $b_date = date('d-M',strtotime(date('Y').'-'.$datearray[1].'-'.$datearray[0]));
                                                    $bdate_num = date('d-m',strtotime(date('Y').'-'.$datearray[1].'-'.$datearray[0]));
                                                    //$b_date = date('d-m-Y', strtotime($customer->customer_birth_date));
                                                }
                                                elseif( Input::old('customer_birth_date') ) {
                                                    $b_date  = Input::old('customer_birth_date');
                                                } 
                                                 else
                                                {
                                                    $b_date=  '';
                                                } 

                                            ?>
                                        <div class="form-group">
                                            <label>Birth Date</label>
                                            <input class="form-control bdate" placeholder="Birth Date"   name="customer_birth_date" type="text" value="<?php echo $b_date; ?>"/>
                                            <?php /*<input id="birth_date" name="customer_birth_date" type="hidden" value="<?php echo empty($bdate_num) ? '' : $bdate_num ?>">*/ ?>
                                        </div>

                                        <?php 
                                        
                                                if( $customer->customer_anniversary_date !='' && $customer->customer_anniversary_date!=NULL)
                                                {
                                                  //$a_date = date('d-m-Y', strtotime($customer->customer_anniversary_date));
                                                    $datearray = explode('-', $customer->customer_anniversary_date);
                                                    //print_r($datearray);                                            
                                                    $a_date = date('d-M',strtotime(date('Y').'-'.$datearray[1].'-'.$datearray[0]));                                                    
                                                    $adate_num = date('d-m',strtotime(date('Y').'-'.$datearray[1].'-'.$datearray[0]));                                                    
                                                }
                                                elseif(Input::old('customer_anniversary_date') ) {
                                                  $a_date =  Input::old('customer_anniversary_date');
                                                } 
                                                 else
                                                {
                                                  $a_date = '';
                                                } 

                                            ?>
                                        <div class="form-group">
                                            <label>Anniversary Date</label>
                                            <input class="form-control adate" placeholder="Anniversary Date" name="customer_anniversary_date" type="text" value="<?php echo $a_date; ?>" />
                                            <?php /*<input id="anniversary_date" type="hidden" name="customer_anniversary_date" value="<?php echo empty($adate_num) ? '' : $adate_num ?>">*/ ?>
                                        </div>
                                        
                                        <button type="submit" class="btn btn-default">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </form>
                                    
                                    
                                                                        
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
@stop