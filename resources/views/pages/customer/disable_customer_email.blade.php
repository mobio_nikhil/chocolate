<html>
    <head>
        <title>HTML email</title>
    </head>
    <body>        
        <table>
            <tr>
                <th>Business Name</th>                            
                <th>Customer Name</th>                            
                <th>Business Address</th>                            
                <th>Secondary Contact</th>                            
                <th>Email</th>                            
                <th>Phone No</th>                
                <th>Birth Date</th>
                <th>Anniversary Date</th>                
                <th>Region</th>
                <th>State</th>
                <th>Area</th>
                <th>Deactive Resone</th>
            </tr>
            <tr>
                <?php
                if (empty($customer['customer_birth_date'])) {
                    $bdate = '';
                } else {
                    $bdate = date('d-M-Y', strtotime($customer['customer_birth_date']));
                }

                if (empty($customer['customer_anniversary_date'])) {
                    $adate = '';
                } else {
                    $adate = date('d-M-Y', strtotime($customer['customer_anniversary_date']));
                }
                ?>
            <tr>                
                <td><?php echo $customer['business_name'] ?></td>
                <td><?php echo $customer['customer_name'] ?></td>
                <td><?php echo $customer['business_address'] ?></td>
                <td><?php echo $customer['secondary_contact_name'] ?></td>
                <td><a target="_blank" href="mailto:{{ $customer['customer_email'] }}"> <?php echo $customer['customer_email'] ?></a></td>
                <td><a target="_blank" href="tel:{{ $customer['customer_phone'] }}"> <?php echo $customer['customer_phone'] ?></a></td>                
                <td><?php echo $bdate ?></td>
                <td><?php echo $adate ?></td>                
                <td><?php echo $customer['region_name'] ?></td>                                        
                <td><?php echo $customer['state_name'] ?></td>                                        
                <td><?php echo $customer['area_name'] ?></td>                    
                <td><?php echo $disable_reason ?></td>    
            </tr>
        </table>
    </body>
</html>