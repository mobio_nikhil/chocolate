@extends('layouts.default')
@section('content_header')
Employee
@stop
@section('content')


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Employee Track - <?php echo $user[0]['user_name']; ?>
            </div>

            @if ($errors->has())
            <div class="alert alert-danger">            
                @foreach ($errors->all() as $error)
                {{ $error }}<br>        
                @endforeach
            </div>
            @endif   

            @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif             
            <!-- /.panel-heading -->
            <div class="panel-body">
                <?php if (count($user_gps) > 0) { ?>
                    <style>
                        #map_wrapper {
                            height: 400px;
                        }

                        #map_canvas {
                            width: 100%;
                            height: 100%;
                        }
                    </style> 

                    <div id="map_wrapper">
                        <div id="map_canvas" class="mapping"></div>
                    </div>

                    <script src="https://maps.googleapis.com/maps/api/js?sensor=false&key=<?php echo Config::get('constants.google_api_key') ?>"></script>

                    <script>
    /*var script = document.createElement('script');
     script.src = "//maps.googleapis.com/maps/api/js?sensor=false";
     document.body.appendChild(script);*/



    function init() {
        var map;
        var polyline;

        /*var markers = [
         //new google.maps.LatLng(17.43495, 78.50898333),
         new google.maps.LatLng(17.43495, 78.50898333),
         new google.maps.LatLng(17.43938333, 78.52168333),
         new google.maps.LatLng(17.43708333, 78.52925),
         new google.maps.LatLng(17.4366, 78.53336667)
         ]; */
        var markers = [];
    <?php for ($i = 0; $i < count($user_gps); $i++) { ?>
            markers['<?php echo $i ?>'] = new google.maps.LatLng('<?php echo $user_gps[$i]['latitude'] ?>', '<?php echo $user_gps[$i]['longitude'] ?>');
            //alert(markers['<?php echo $i ?>']);
    <?php } ?>

        var infoWindowContent = [];
    <?php for ($i = 0; $i < count($user_gps); $i++) { ?>
             infoWindowContent['<?php echo $i ?>'] = ['<div class="info_content">' +
                                    '<h3>' + '<?php echo str_replace("'","",$user_gps[$i]['map_header']) ?>' + '</h3>' +
                                    '<p>' + '<?php echo str_replace("'","",$user_gps[$i]['map_content']) ?>' + '</p>' +
                                    '<p>' + '<?php echo str_replace("'","",$user_gps[$i]['map_time_date']) ?>' + '</p>' +                                    
                                    '</div>'];
            //alert(markers['<?php echo $i ?>']);
    <?php } ?>


        // Info Window Content
        /* var infoWindowContent = [
         ['<div class="info_content">' +
         '<h3>Palace of Westminster1</h3>' +
         '<p>The Palace of Westminster is the meeting place of the House of Commons and the House of Lords, the two houses of the Parliament of the United Kingdom. Commonly known as the Houses of Parliament after its tenants.</p>' +
         '<p>The Palace of Westminster is the meeting place of the House of Commons and the House of Lords, the two houses of the Parliament of the United Kingdom. Commonly known as the Houses of Parliament after its tenants.</p>' +
         '</div>'],
         ['<div class="info_content">' +
         '<h3>Palace of Westminster2</h3>' +
         '<p>The Palace of Westminster is the meeting place of the House of Commons and the House of Lords, the two houses of the Parliament of the United Kingdom. Commonly known as the Houses of Parliament after its tenants.</p>' +
         '</div>'],
         ['<div class="info_content">' +
         '<h3>Palace of Westminster3</h3>' +
         '<p>The Palace of Westminster is the meeting place of the House of Commons and the House of Lords, the two houses of the Parliament of the United Kingdom. Commonly known as the Houses of Parliament after its tenants.</p>' +
         '</div>'],
         ['<div class="info_content">' +
         '<h3>Palace of Westminster4</h3>' +
         '<p>The Palace of Westminster is the meeting place of the House of Commons and the House of Lords, the two houses of the Parliament of the United Kingdom. Commonly known as the Houses of Parliament after its tenants.</p>' +
         '</div>']
         ];       
         */
        //alert(infoWindowContent[0]);




        var moptions = {
            //center: new google.maps.LatLng(17.43938333, 78.52168333),
            center: new google.maps.LatLng('<?php echo $user_gps[0]['latitude'] ?>', '<?php echo $user_gps[0]['longitude'] ?>'),
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        map = new google.maps.Map(document.getElementById("map_canvas"), moptions);

        var iconsetngs = {
            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
        };
        var polylineoptns = {
            strokeOpacity: 0.8,
            strokeWeight: 3,
            map: map,
            icons: [{
                    repeat: '70px', //CHANGE THIS VALUE TO CHANGE THE DISTANCE BETWEEN ARROWS
                    icon: iconsetngs,
                    offset: '100%'}]
        };
        polyline = new google.maps.Polyline(polylineoptns);
        var z = 0;
        var path = [];
        path[z] = polyline.getPath();
        var infoWindow = new google.maps.InfoWindow(), marker, i;
        for (var i = 0; i < markers.length; i++) //LOOP TO DISPLAY THE MARKERS
        {
            var pos = markers[i];
            var marker = new google.maps.Marker({
                position: pos,
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infoWindow.setContent(infoWindowContent[i][0]);
                    infoWindow.open(map, marker);
                }
            })(marker, i));

            path[z].push(marker.getPosition()); //PUSH THE NEWLY CREATED MARKER'S POSITION TO THE PATH ARRAY



        }
    }
    window.onload = init;




                    </script>
                <?php } else { ?>

                    Track not found


                <?php } ?>

                <!-- /.table-responsive -->                            
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
@stop