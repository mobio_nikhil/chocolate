@extends('layouts.default')
@section('content_header')
Employee Track
@stop

@section('content')
<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Track
                        </div>
                        
                    @if ($errors->has())
                    <div class="alert alert-danger">            
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>        
                        @endforeach
                    </div>
                    @endif   
                    
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif                          
                        
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="<?php echo URL::to( Config::get('constants.admin_path').'employee_track/store') ?>" method="post">
                                        <div class="form-group">
                                            <label>Track Date</label>
                                            <input class="form-control date" placeholder="Track Date" name="track_date" type="text" value="{{ Input::old('track_date') }}" autofocus/>
                                        </div>
                                        
                                        
                                        <div class="form-group">
                                            <label>Search</label>
                                            <label class="radio-inline">
                                                <input type="radio" name="searchby" id="searchbyid" value="1" checked>By Id
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="searchby" id="searchbyname" value="2">By Name
                                            </label>
                                        </div>                                    
                                        
                                        <div class="form-group"  id="div_search_id">
                                            <label>Employee Id</label>
                                            <input class="form-control" placeholder="Employee Id" name="employee_id" type="text" value="{{ Input::old('employee_id') }}" autofocus/>
                                        </div>                                   
                                        
                                        
                                        
                                        <div class="form-group" style="display: none;" id="div_search_name">
                                            <label>Employee Name</label>
                                            <select class="form-control" name="employee_name">
                                                <?php foreach($user_list as $user){ ?>
                                                <option <?php echo (Input::old('employee_name') == $user['id']) ? 'selected' : '' ?> value="<?php echo $user['id'] ?>"><?php echo $user['user_name']. ' ( '.$user['id'].' ) ' ?></option>                                                    
                                                <?php } ?>
                                            </select>
                                        </div>                                        
                                        
                                                                                
                                        <button type="submit" class="btn btn-default">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>        
@stop