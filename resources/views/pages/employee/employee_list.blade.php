@extends('layouts.default')
@section('content_header')
Users
<br/><br/>
<a href="<?php echo URL::to( Config::get('constants.admin_path').'user/create') ?>" class="btn btn-primary">Add</a>            
@stop
@section('content')
<style>
    tfoot {
        display: table-header-group;
    }
    .dataTables_filter {
    display: none;
    }     
</style>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Users List
            </div>
            
                @if ($errors->has())
                    <div class="alert alert-danger">            
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>        
                        @endforeach
                    </div>
                    @endif   
                    
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif             
            <!-- /.panel-heading -->
            <div class="panel-body">
            <table id="employee_grid" class="display table" width="100%" cellspacing="0">
                    <thead>
                        <tr>                            
                            <th>Name</th>                            
                            <th>Track</th>
                            <th>Designation</th>
                            <th>Region</th>
                            <th>State/Area</th>
                            <th>Phone</th>
                            <th>Assignment To</th>
                            <th>Status</th>
                            <th>Create Date</th>
                            <th>Customer Meetings</th>
                            <th>Details</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>                            
                            <th>Name</th>                            
                            <th>Track</th>
                            <th>Designation</th>
                            <th>Region</th>
                            <th>State/Area</th>
                            <th>Phone</th>
                            <th>Assignment To</th>
                            <th>Status</th>
                            <th>Create Date</th>
                            <th>Customer Meetings</th>
                            <th>Details</th>
                        </tr>
                    </tfoot>
                    <tbody>
                                    <?php foreach($user_list as $user){ ?>
                                        <tr>                                        
                                        <td><?php echo $user['user_name'] ?></td>
                                        <td><a href="<?php echo URL::to( Config::get('constants.admin_path').'employee/track/'.$user['id']) ?>">Track</a></td>
                                        <?php 
                                        $desgination = '';
                                        if($user['user_type'] == 0){
                                            $desgination = 'VP';
                                        }
                                        if($user['user_type'] == 1){
                                            $desgination = 'Franchisee Manager and Operations Manager';
                                        }
                                        if($user['user_type'] == 2){
                                            $desgination = 'ASM';
                                        }
                                        if($user['user_type'] == 3){
                                            $desgination = 'SM';
                                        }
                                        if($user['user_type'] == 0 and $user['user_admin_backoffice_type'] == 0 && $user['user_admin_backoffice_type'] != null){
                                            $desgination = 'SUPER ADMIN';
                                        }
                                        if($user['user_type'] == 0 and $user['user_admin_backoffice_type'] == 1 && $user['user_admin_backoffice_type'] != null){
                                            $desgination = 'BACK OFFICE';
                                        }                                        
                                        
                                        ?>
                                        
                                        
                                        <td><?php echo $desgination ?></td>
                                        <td><?php echo $user['region_name'] ?></td>
                                        <td><?php echo empty($user['area_name']) ? 'NA' : $user['area_name'] ?></td>
                                        <td><a href="tel:{{ $user['user_phone'] }}"><?php echo $user['user_phone'] ?></a></td>
                                        <td><?php echo empty($user['assign_to_user']) ? 'NA' : $user['assign_to_user'] ?></td>
                                        <td><?php echo ($user['disable_status'] == 0) ? 'Active' : 'Inactive' ?></td>
                                        <td><?php echo date('d-M-Y',strtotime($user['created_on'])) ?></td>
                                        <td><?php echo $user['customer_meeting'] ?></td>
                                        <td><a href="<?php echo URL::to( Config::get('constants.admin_path').'employee/employee_history/'.$user['id']) ?>">Detail</a></td>
                                        </tr>

                                    <?php } ?>         

                    </tbody>
                </table>
                
                
                <!-- /.table-responsive -->                            
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
@stop