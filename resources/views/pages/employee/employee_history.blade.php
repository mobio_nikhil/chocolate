@extends('layouts.default')
@section('content_header')
<?php echo $user[0]['user_name'] ?>
<br/>
@stop
@section('content')
<?php if(count($employee_capture_day) > 0){ ?>
<?php for ($i = 0; $i < count($employee_capture_day); $i++) { ?>
<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php echo '<b>'.$employee_capture_day[$i]['capture_day'].'</b>'; ?>
                        </div>
                        <!-- .panel-heading -->
                        <div class="panel-body">
                            <div class="panel-group" id="parent_<?php echo $i ?>">
                        <?php for($j=0;$j<count($employee_capture_day[$i]['capture_data']);$j++){ ?>
                                <?php if($employee_capture_day[$i]['capture_data'][$j]['log_type'] == 0){ ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#parent_<?php echo $i ?>" href="#parent_<?php echo $i ?>_child_<?php echo $j ?>"><?php echo $employee_capture_day[$i]['capture_data'][$j]['log_header'] ?> : <?php echo  date('h:i:s A',strtotime($employee_capture_day[$i]['capture_data'][$j]['log_time_date'])) ?></a>
                                            </h4>
                                        </div>
                                        <div id="parent_<?php echo $i ?>_child_<?php echo $j ?>" class="panel-collapse collapse out">
                                            <div class="panel-body">
                                                <?php echo  date('h:i:s A',strtotime($employee_capture_day[$i]['capture_data'][$j]['log_time_date'])) ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if($employee_capture_day[$i]['capture_data'][$j]['log_type'] == 1){ ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#parent_<?php echo $i ?>" href="#parent_<?php echo $i ?>_child_<?php echo $j ?>"><?php echo $employee_capture_day[$i]['capture_data'][$j]['log_header'].' with : '.$employee_capture_day[$i]['capture_data'][$j]['log_content']['business_name'] ?> - <?php echo  date('h:i:s A',strtotime($employee_capture_day[$i]['capture_data'][$j]['log_time_date'])) ?></a>
                                            </h4>
                                        </div>
                                        <div id="parent_<?php echo $i ?>_child_<?php echo $j ?>" class="panel-collapse collapse out">
                                            <div class="panel-body">
                                                <?php echo "Business Name: ".$employee_capture_day[$i]['capture_data'][$j]['log_content']['business_name']; ?><br/>
                                                <?php echo "Customer Name: ".$employee_capture_day[$i]['capture_data'][$j]['log_content']['customer_name']; ?><br/>
                                                <?php echo "Business Address: ".$employee_capture_day[$i]['capture_data'][$j]['log_content']['business_address']; ?><br/>
                                                <?php echo "Primary Name: ".$employee_capture_day[$i]['capture_data'][$j]['log_content']['primary_contact_name']; ?><br/>
                                                <?php echo "Followup Date & Time: "; ?>
                                                <?php echo ($employee_capture_day[$i]['capture_data'][$j]['log_content']['followup_date_time']) ? date('d-m-Y H:i:s A',strtotime($employee_capture_day[$i]['capture_data'][$j]['log_content']['followup_date_time'])) : ' - '; ?><br/>
                                                <?php echo "Note: ".$employee_capture_day[$i]['capture_data'][$j]['log_content']['meeting_note'].'<br/>'; ?>
                                                <?php echo  date('h:i:s A',strtotime($employee_capture_day[$i]['capture_data'][$j]['log_time_date'])) ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if($employee_capture_day[$i]['capture_data'][$j]['log_type'] == 2){ ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#parent_<?php echo $i ?>" href="#parent_<?php echo $i ?>_child_<?php echo $j ?>"><?php echo $employee_capture_day[$i]['capture_data'][$j]['log_header'] ?> : <?php echo  date('h:i:s A',strtotime($employee_capture_day[$i]['capture_data'][$j]['log_time_date'])) ?></a>
                                            </h4>
                                        </div>
                                        <div id="parent_<?php echo $i ?>_child_<?php echo $j ?>" class="panel-collapse collapse out">
                                            <div class="panel-body">
                                                <?php echo  date('h:i:s A',strtotime($employee_capture_day[$i]['capture_data'][$j]['log_time_date'])) ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>                                
                            <?php } ?>
                            </div>
                        </div>
                        <!-- .panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>


<?php } ?>
<?php }else{ ?>
No details found
<?php } ?>



@stop