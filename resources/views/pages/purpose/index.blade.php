@extends('layouts.default')
@section('content_header')
Purpose Point
<br/><br/>
<a href="<?php echo URL::to( Config::get('constants.admin_path').'purpose/create') ?>" class="btn btn-primary">Add</a>            
@stop
@section('content')
<style>
    tfoot {
        display: table-header-group;
    }
    .dataTables_filter {
    display: none;
    }     
</style>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Purpose List
            </div>
            
                @if ($errors->has())
                    <div class="alert alert-danger">            
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>        
                        @endforeach
                    </div>
                    @endif   
                    
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif             
            <!-- /.panel-heading -->
            <div class="panel-body">
            <table id="purpose_grid" class="display table" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Purpose Name</th>
                            <th>Deactivate Reason</th>
                            <th>Create Date</th>
                            <th>Status</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Purpose Name</th>
                            <th>Deactivate Reason</th>
                            <th>Create Date</th>
                            <th>Status</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </tfoot>
                    <tbody>
                                    <?php foreach($purpose_list as $purpose){ ?>
                                        <?php if($purpose['id'] != 1){ ?>
                                        <tr>
                                        <td><?php echo $purpose['purpose_name'] ?></td>
                                        <td><?php echo empty($purpose['disable_reason']) ? 'NA' : $purpose['disable_reason'] ?></td>
                                        <td><?php echo date('d-M-Y',strtotime($purpose['created_on'])) ?></td>

                                        @if( $purpose['disable_status'] == 0 )
                                            <?php /*<td> <a class="btn btn-default" href="<?php echo URL::to( Config::get('constants.admin_path').'purpose/status/disable/'.$purpose['id']) ?>"><i class="fa fa-check"></i></a></td>*/ ?>
                                            <td><button type="button" onclick="deact_purpose('{{ $purpose['id'] }}','<?php echo URL::to( Config::get('constants.admin_path').'purpose/status/disable/'.$purpose['id']); ?>')" class="btn btn-default deact" ><i class="fa fa-check"></i></button></td>
                                        @else
                                            <td> <a class="btn btn-default" href="<?php echo URL::to( Config::get('constants.admin_path').'purpose/status/enable/'.$purpose['id']) ?>"><i class="fa fa-times"></i></a></td>                            
                                        @endif
                                        
                                        <td><a class="btn btn-default" href="<?php echo URL::to( Config::get('constants.admin_path').'purpose/edit/'.$purpose['id']) ?>"><i class="fa fa-pencil"></i></a></td>
                                        <td><a class="btn btn-default" href="#" onclick="purpose_delete(<?php echo $purpose['id'] ?>)"><i class="fa fa-trash"></i></a></td>                            
                                        </tr>
                                    <?php } ?>         
                                    <?php } ?>         

                    </tbody>
                </table>
                
                
                <!-- /.table-responsive -->                            
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<div id="deactivate_purpose_modal" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                      <form method="post" id="purpose_deact_form" action="<?php echo URL::to( Config::get('constants.admin_path').'brand/status/disable/'); ?>" >
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Deactivate Purpose</h4>
                      </div>
                      <div class="modal-body">
                        <p>Give reason to deactivate this purpose</p>
                        <input type="hidden" name="id" id="purpose_deactive_id">
                        <textarea required class="form-control" class="col-sm-12" placeholder="Reason in brief.." name="disable_reason"></textarea>
                      </div>
                      <div class="modal-footer">
                        <button class="btn">Deactivate</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                    </form>
                  </div>
                </div>
<div id="delete_purpose_modal" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                      <form method="post" id="purpose_delete_form" action="<?php echo URL::to( Config::get('constants.admin_path').'brand/status/disable/'); ?>" >
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete Purpose</h4>
                      </div>
                      <div class="modal-body">
                        <p>Give reason to delete this purpose</p>
                        <input type="hidden" name="id" id="purpose_delete_id">
                        <textarea required class="form-control" class="col-sm-12" placeholder="Reason in brief.." name="delete_reason"></textarea>
                      </div>
                      <div class="modal-footer">
                        <button class="btn">Delete</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                    </form>
                  </div>
                </div>
@stop