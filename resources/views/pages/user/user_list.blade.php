@extends('layouts.default')
@section('content_header')
User
<br/><br/>
<a href="<?php echo URL::to( Config::get('constants.admin_path').'user/create') ?>" class="btn btn-primary">Add</a>            
<?php 
/*
<a href="<?php echo URL::to( Config::get('constants.admin_path').'user/deactive/list'); ?>" class="btn btn-primary">Deactive</a>            
*/
?>
@stop
@section('content')
<style>
    tfoot {
        display: table-header-group;
    }
    .dataTables_filter {
    display: none;
    }     
</style>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                User List
            </div>
            
                @if ($errors->has())
                    <div class="alert alert-danger">            
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>        
                        @endforeach
                    </div>
                    @endif   
                    
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif             
            <!-- /.panel-heading -->
            <div class="panel-body">
            <table id="user_grid" class="display table" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Designation</th>
                            <th>Brand</th>
                            <th>Region</th>
                            <th>Area</th>
                            <th>Reporting To</th>
                            <th>Deactivate Reason</th>
                            <th>Create Date</th>
                            <th>Assignment</th>
                            <th>Edit</th>
                            <th>Status</th>
                            <th>Delete</th>                                                      
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Designation</th>
                            <th>Brand</th>
                            <th>Region</th>
                            <th>Area</th>
                            <th>Reporting To</th>
                            <th>Deactivate Reason</th>                            
                            <th>Create Date</th>
                            <th>Assignment</th>
                            <th>Edit</th>
                            <th>Status</th>
                            <th>Delete</th>                                                       
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php foreach($user_list as $user){ ?>
                        <tr>
                            <td><?php echo $user['user_name'] ?></td>
                            <td><a href="mailto:{{ $user['user_email'] }}"><?php echo $user['user_email'] ?></a></td>
                            <td><a href="tel:{{ $user['user_phone'] }}"><?php echo $user['user_phone'] ?></a></td>
                            <?php 
                                        $desgination = '';
                                        if($user['user_type'] == 0){
                                            $desgination = 'VP';
                                        }
                                        if($user['user_type'] == 1){
                                            $desgination = 'Franchisee Manager and Operations Manager';
                                        }
                                        if($user['user_type'] == 2){
                                            $desgination = 'ASM';
                                        }
                                        if($user['user_type'] == 3){
                                            $desgination = 'SM';
                                        }
                                        if($user['user_type'] == 0 and $user['user_admin_backoffice_type'] == 0 && $user['user_admin_backoffice_type'] != null){
                                            $desgination = 'SUPER ADMIN';
                                        }
                                        if($user['user_type'] == 0 and $user['user_admin_backoffice_type'] == 1 && $user['user_admin_backoffice_type'] != null){
                                            $desgination = 'BACK OFFICE';
                                        }
                                        
                                        
                                        ?>
                                        
                                        
                            <td><?php echo $desgination ?></td>                            
                            <td><?php echo empty($user['brand_name']) ? 'NA' : $user['brand_name'] ?></td>
                            <td><?php echo $user['region_name'] ?></td>
                            <td><?php echo empty($user['area_name']) ? 'NA' : $user['area_name'] ?></td>
                            <?php /*<td><?php echo empty($user['assign_to_user']) ? 'NA' : $user['assign_to_user'] ?></td>*/ ?>
                            <td><?php echo empty($user['assign_to_user']) ? 'Super Admin' : 'Super Admin' ?></td>
                            <td><?php echo empty($user['disable_reason']) ? 'NA' : $user['disable_reason'] ?></td>
                            <td><?php echo date('d-M-Y',strtotime($user['created_on'])) ?></td>
                            <?php if( (! (($user['id'] == $session_user_info['id']) || $user['user_type'] == 0 && $user['user_admin_backoffice_type'] == 0 )) ){ ?>
                            <?php if($user['user_type'] != 1){ ?>
                                <td><a href="<?php echo URL::to( Config::get('constants.admin_path').'user/assignment/'.$user['id']) ?>">Assignment</a></td>
                            <?php }else{ ?>
                                <td>NA</td>
                            <?php } ?>
                            <td><a class="btn btn-default" href="<?php echo URL::to( Config::get('constants.admin_path').'user/edit/'.$user['id']) ?>"> <i class="fa fa-pencil"></i></a></td>
                                @if( $user['disable_status'] == 0 )                 
                                    <td>         
                                        <div class="form-group">
                                            <button type="button" onclick="deact_user('{{ $user['id'] }}')" class="btn btn-default deact" ><i class="fa fa-check"></i></button>
                                        </div>
                                    </td>
                                @else

                                <td>
                                    <form class="form-group" method="post" action="<?php echo URL::to( Config::get('constants.admin_path').'user/change_status/enable/'); ?>" >
                                         <input type="hidden" value="{{ $user['id'] }}" name="id">
                                        <button class="btn btn-default "><i class="fa fa-times"></i></button>
                                    </form>                                         
                                </td>
                                    
                                @endif

                            
                            <td><a href="javascript:void(0)" class="btn btn-default" onclick="user_delete(<?php echo $user['id'] ?>)"><i class="fa fa-trash"></i></a></td>                            
                            <?php }else{ ?>
                            <td>NA</td>
                            <td>NA</td>
                            <td>NA</td>
                            <td>NA</td>
                            <?php } ?>
                            </tr>
                        <?php } ?>

                    </tbody>
                </table>                
                <!-- /.table-responsive -->                            
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->

            <div id="deactivate_user_modal" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                    <form method="post" action="<?php echo URL::to( Config::get('constants.admin_path').'user/change_status/disable/'); ?>" >
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Deactivate User</h4>
                      </div>
                      <div class="modal-body">
                        <p>Give reason to deactivate this User</p>
                        <input type="hidden" name="id" id="user_id">
                        <textarea required class="form-control" class="col-sm-12" placeholder="Reason in brief.." name="disable_reason"></textarea>
                      </div>
                      <div class="modal-footer">
                        <button class="btn">Deactivate</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                    </form>
                  </div>
                </div>
    
    <div id="delete_user_modal" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                      <form method="post" id="user_delete_form" action="<?php echo URL::to( Config::get('constants.admin_path').'brand/status/disable/'); ?>" >
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete User</h4>
                      </div>
                      <div class="modal-body">
                        <p>Give reason to delete this user</p>
                        <input type="hidden" name="id" id="user_delete_id">
                        <textarea required class="form-control" class="col-sm-12" placeholder="Reason in brief.." name="delete_reason"></textarea>
                      </div>
                      <div class="modal-footer">
                        <button class="btn">Delete</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                    </form>
                  </div>
                </div>
</div>
@stop

