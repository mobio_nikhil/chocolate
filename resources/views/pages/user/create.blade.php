@extends('layouts.default')
@section('content_header')
User Management
@stop

@section('content')
<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Create User
                        </div>
                        
                    @if ($errors->has())
                    <div class="alert alert-danger">            
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>        
                        @endforeach
                    </div>
                    @endif   
                    
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif                          
                        
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="<?php echo URL::to( Config::get('constants.admin_path').'user/store') ?>" method="post">
                                        <div class="form-group">
                                            <label>Name</label>                                            
                                            <input class="form-control" placeholder="Name" name="user_name" type="text" value="{{ Input::old('user_name') }}" autofocus/>
                                        </div>                                        
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" placeholder="abc@xyz.com" name="user_email" type="text" value="{{ Input::old('user_email') }}" autofocus/>                                            
                                        </div>                                        
                                        <div class="form-group">
                                            <label>Phone no</label>
                                            <input class="form-control" placeholder="1234567890" name="user_phone" type="text" value="{{ Input::old('user_phone') }}" autofocus/>                                            
                                        </div>                                        
                                        <div class="form-group">
                                            <label>Designation</label>
                                            <select class="form-control" id="user_create_designation" name="user_type">
                                                <option value="">Select</option>
                                                <option <?php echo (Input::old('user_type') == 1) ? 'selected' : '' ?> value="1">Franchisee Manager and Operations Manager</option>
                                                <?php /*<option <?php echo (Input::old('user_type') == 2) ? 'selected' : '' ?>  value="2">ASM</option>
                                                <option <?php echo (Input::old('user_type') == 3) ? 'selected' : '' ?> value="3">SE</option>
                                                    <?php 
                                                    if($session_user_info['user_type'] == 0 && $session_user_info['user_admin_backoffice_type'] == 0){ ?>
                                                    <option <?php echo (Input::old('user_type') == 4) ? 'selected' : '' ?>  value="4">VP</option>
                                                    <option <?php echo (Input::old('user_type') == 5) ? 'selected' : '' ?> value="5">Back office</option>
                                                    <?php }
                                                    ?>  */ ?>                                              
                                            </select>
                                        </div>                                                                                
                                        <div class="form-group" id="user_create_region" style="<?php echo (Input::old('user_type') == 4 || Input::old('user_type') == 5 ) ? 'display:none' : 'display:block' ?>">
                                            <label>Region</label>
                                            <select class="form-control" name="region_id">
                                                <?php foreach($region_list as $region){ ?>
                                                    <option <?php echo (Input::old('region_id') == $region['id']) ? 'selected' : '' ?> value="<?php echo $region['id'] ?>"><?php echo $region['region_name'] ?></option>                                                
                                                <?php } ?>
                                            </select>
                                        </div>                                                                                
                                        <div class="form-group" id="user_create_brand" style="<?php echo (Input::old('user_type') == 4 || Input::old('user_type') == 5 ) ? 'display:none' : 'display:block' ?>">
                                            <label>Brand</label>
                                            <select multiple="multiple" name="brand[]" class="form-control">
                                                <?php
                                                $st = array();
                                                if(count(Input::old('brand')) > 0){
                                                  $st = Input::old('brand');
                                                }
                                                foreach($brand_list as $brand){ ?>
                                                <option <?php echo (in_array($brand['id'], $st)) ? 'selected' : ''; ?> value="<?php echo $brand['id'] ?>"><?php echo $brand['brand_name'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>                                        
                                        <button type="submit" class="btn btn-default">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
@stop