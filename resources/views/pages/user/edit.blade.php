@extends('layouts.default')
@section('content_header')
User
@stop

@section('content')
<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            User Edit
                        </div>
                        
                    @if ($errors->has())
                    <div class="alert alert-danger">            
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>        
                        @endforeach
                    </div>
                    @endif   
                    
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif                          
                        
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="<?php echo URL::to( Config::get('constants.admin_path').'user/update/'.$reqData['id']) ?>" method="post">
                                        <div class="form-group">
                                            <label>Name</label>                                            
                                            <input class="form-control" placeholder="user name" name="user_name" type="text" value="{{ $reqData['user_name'] }}" autofocus/>
                                        </div>                                        
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input class="form-control" placeholder="user email" name="user_email" type="text" value="{{ $reqData['user_email'] }}" autofocus/>                                            
                                        </div>                                        
                                        <div class="form-group">
                                            <label>Phone no</label>
                                            <input class="form-control" placeholder="user phone no" name="user_phone" type="text" value="{{ $reqData['user_phone'] }}" autofocus/>                                            
                                        </div>                                        
                                        <div class="form-group">
                                            <label>Designation</label>
                                            <select class="form-control" name="user_type">
                                                <option <?php echo ($reqData['user_type'] == 1) ? 'selected' : '' ?> value="1">Franchisee Manager and Operations Manager</option>
                                                <?php /*<option <?php echo ($reqData['user_type'] == 2) ? 'selected' : '' ?>  value="2">ASM</option>
                                                <option <?php echo ($reqData['user_type'] == 3) ? 'selected' : '' ?> value="3">SM</option>*/ ?>
                                            </select>
                                        </div>                                                                                
                                        <div class="form-group">
                                            <label>Brand</label>
                                            <?php                                            
                                                $st = array();
                                                if(count(Input::old('brand')) > 0){
                                                  $st = Input::old('brand');
                                                }else{
                                                    for($i=0;$i<count($selected_user_brand);$i++){
                                                        $st[] = $selected_user_brand[$i]['brand_id'];
                                                    }
                                                }                                                        
                                            ?>
                                            <select multiple="multiple" name="brand[]" class="form-control">
                                                <?php
                                                foreach($brand_list as $brand){ ?>
                                                <option <?php echo (in_array($brand['id'], $st)) ? 'selected' : ''; ?> value="<?php echo $brand['id'] ?>"><?php echo $brand['brand_name'] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>                                        
                                        <div class="form-group">
                                            <label>Region</label>
                                            <select class="form-control" name="region_id">
                                                <?php foreach($region_list as $region){ ?>
                                                    <option <?php echo ($reqData['region_id'] == $region['id']) ? 'selected' : '' ?> value="<?php echo $region['id'] ?>"><?php echo $region['region_name'] ?></option>                                                
                                                <?php } ?>
                                            </select>
                                        </div>                                                                                
                                        <button type="submit" class="btn btn-default">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
@stop