@extends('layouts.default')
@section('content_header')
Deactive Users
<br/><br/>
<a href="<?php echo URL::to( Config::get('constants.admin_path').'user/create') ?>" class="btn btn-primary">Add</a>            
@stop
@section('content')
<style>
    tfoot {
        display: table-header-group;
    }
    .dataTables_filter {
    display: none;
    }     
</style>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                User List
            </div>
            
                @if ($errors->has())
                    <div class="alert alert-danger">            
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>        
                        @endforeach
                    </div>
                    @endif   
                    
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif             
            <!-- /.panel-heading -->
            <div class="panel-body">
            <table id="user_grid" class="display table" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Designation</th>
                            <th>Region</th>
                            <th>Area</th>
                            <th>Reporting To</th>
                            <th>Create Date</th>
                            <th>Assignment</th>
                            <th>Reason to Deactive</th>
                            <th></th>
                                                                                
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Designation</th>
                            <th>Region</th>
                            <th>Area</th>
                            <th>Reporting To</th>
                            <th>Create Date</th>
                            <th>Assignment</th>
                            <th>Reason to Deactive</th>
                            <th></th>
                                                                                 
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php foreach($user_list as $user){ ?>
                        <tr>
                            <td><?php echo $user['user_name'] ?></td>
                            <td><?php echo $user['user_email'] ?></td>
                            <td><?php echo $user['user_phone'] ?></td>
                             <?php 
                                        $desgination = '';
                                        if($user['user_type'] == 0){
                                            $desgination = 'VP';
                                        }
                                        if($user['user_type'] == 1){
                                            $desgination = 'Franchisee Manager and Operations Manager';
                                        }
                                        if($user['user_type'] == 2){
                                            $desgination = 'ASM';
                                        }
                                        if($user['user_type'] == 3){
                                            $desgination = 'SM';
                                        }
                                        if($user['user_type'] == 0 and $user['user_admin_backoffice_type'] == 0 && $user['user_admin_backoffice_type'] != null){
                                            $desgination = 'Super Adminn';
                                        }
                                        if($user['user_type'] == 0 and $user['user_admin_backoffice_type'] == 1 && $user['user_admin_backoffice_type'] != null){
                                            $desgination = 'Back Office';
                                        }
                                        
                                        
                                        ?>
                                        
                                        
                            <td><?php echo $desgination ?></td>                            
                            <td><?php echo $user['region_name'] ?></td>
                            <td><?php echo $user['area_name'] ?></td>
                            <td><?php echo $user['assign_to_user'] ?></td>
                            <td><?php echo date('d-M-Y',strtotime($user['created_on'])) ?></td>
                            <?php if(!($user['id'] == $session_user_info['id'])){ ?>
                            <?php if($user['user_type'] != 1 ){ ?>
                                <td><a href="<?php echo URL::to( Config::get('constants.admin_path').'user/assignment/'.$user['id']) ?>">Assignment</a></td>
                            <?php }else{ ?>
                                <td></td>
                            <?php } ?>
                            <td>{{ $user['disable_reason'] }}</td>
                            <td>                          
                                    <form method="post" action="<?php echo URL::to( Config::get('constants.admin_path').'user/change_status/enable/'); ?>" >
                                         <input type="hidden" value="{{ $user['id'] }}" name="id">
                                        <button class="btn">Activate</button>
                                    </form>                                         
                                    
                            </td>
                           
                            <?php }else{ ?>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <?php } ?>
                            </tr>
                        <?php } ?>

                    </tbody>
                </table>                
                <!-- /.table-responsive -->                            
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->

</div>
@stop

