@extends('layouts.default')
@section('content_header')
User
@stop

@section('content')
<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            User Assignment - <?php echo $user_detail[0]['user_name'] ?> 
                        </div>
                        
                    @if ($errors->has())
                    <div class="alert alert-danger">            
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>        
                        @endforeach
                    </div>
                    @endif   
                    
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif                          
                        
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="<?php echo URL::to( Config::get('constants.admin_path').'user/assignment_store/'.$user_detail[0]['id']) ?>" method="post">
                                        <?php if(!empty($rsm_user[0]['user_name'])){ ?>
                                        <div class="form-group">
                                            <label>RSM Name: <?php echo $rsm_user[0]['user_name']?></label>                                            
                                        </div>
                                        <?php } ?>

                                        
                                        <?php if(!empty($asm_user[0]['user_name'])){ ?>
                                        <div class="form-group">
                                            <label>ASM Name: <?php echo $asm_user[0]['user_name']?></label>                                            
                                        </div>
                                        <?php } ?>
                                        
                                        <?php if((($user_detail[0]['user_type'] == 3))){ ?>
                                        <div class="form-group">
                                            <label>Assignment To ASM</label>
                                            <select class="form-control" name="asm_user">
                                                <?php foreach($asm_user_list as $asm_user){ ?>
                                                    <option <?php echo ($user_detail[0]['assign_to_user'] == $asm_user['id']) ? 'selected' : '' ?> value="<?php echo $asm_user['id'] ?>"><?php echo $asm_user['user_name'] ?></option>                                                
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <?php } ?>
                                        
                                        
                                        <div class="form-group">
                                            <label>Area</label>
                                            <?php                                            
                                                $st = array();
                                                if(count(Input::old('area_id')) > 0){
                                                  $st = Input::old('area_id');
                                                }else{
                                                    for($i=0;$i<count($selected_user_area);$i++){
                                                        $st[] = $selected_user_area[$i]['area_id'];
                                                    }
                                                }                                                      
                                            ?>                                                                               
                                            <select class="form-control" multiple="multiple" name="area_id[]">
                                                <?php foreach($area_list as $area){ ?>
                                                    <option <?php echo (in_array($area['id'], $st)) ? 'selected' : ''; ?> value="<?php echo $area['id'] ?>"><?php echo $area['area_name'] ?></option>                                                
                                                <?php } ?>
                                            </select>
                                        </div> 
                                        
                                        <button type="submit" class="btn btn-default">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
@stop