@extends('layouts.default')
@section('content_header')
Brand
<br/><br/>
<a href="<?php echo URL::to( Config::get('constants.admin_path').'brand/create') ?>" class="btn btn-primary">Add</a>            
@stop
@section('content')
<style>
    tfoot {
        display: table-header-group;
    }
    .dataTables_filter {
        display: none;
    }     
</style>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Brand List
            </div>
            
                @if ($errors->has())
                    <div class="alert alert-danger">            
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>        
                        @endforeach
                    </div>
                    @endif   
                    
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif             
            <!-- /.panel-heading -->
            <div class="panel-body">
            <table id="brand_grid" class="display table" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Brand Name</th>
                            <th>Deactivate Reason</th>
                            <th>Create Date</th>
                            <th>Status</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Brand Name</th>
                            <th>Deactivate Reason</th>
                            <th>Create Date</th>
                            <th>Status</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </tfoot>
                    <tbody>
                                    <?php foreach($brand_list as $brand){ ?>
                                        <tr>
                                        <td><?php echo $brand['brand_name'] ?></td>
                                        <td><?php echo empty($brand['disable_reason']) ? 'NA' : $brand['disable_reason'] ?></td>
                                        <td><?php echo date('d-M-Y',strtotime($brand['created_on'])) ?></td>
                                        @if( $brand['disable_status'] == 0 )
                                            <?php /*<td> <a class="btn btn-default" href="<?php echo URL::to( Config::get('constants.admin_path').'brand/status/disable/'.$brand['id']) ?>"><i class="fa fa-check"></i></a></td>*/ ?>
                                            <td><button type="button" onclick="deact_brand('{{ $brand['id'] }}','<?php echo URL::to( Config::get('constants.admin_path').'brand/status/disable/'.$brand['id']); ?>')" class="btn btn-default deact" ><i class="fa fa-check"></i></button></td>
                                        @else
                                             <td> <a class="btn btn-default" href="<?php echo URL::to( Config::get('constants.admin_path').'brand/status/enable/'.$brand['id']) ?>"><i class="fa fa-times"></i></a></td> 
                                        @endif
                                        
                                        <td><a class="btn btn-default" href="<?php echo URL::to( Config::get('constants.admin_path').'brand/edit/'.$brand['id']) ?>"><i class="fa fa-pencil"></i></a></td>
                                        <td><a class="btn btn-default" href="javascript:void(0)" onclick="brand_delete(<?php echo $brand['id'] ?>)"><i class="fa fa-trash"></i></a>
                                        </td>
                                        </tr>

                                    <?php } ?>         

                    </tbody>
                </table>
                
                
                <!-- /.table-responsive -->                            
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<div id="deactivate_brand_modal" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                      <form method="post" id="brand_deact_form" action="<?php echo URL::to( Config::get('constants.admin_path').'brand/status/disable/'); ?>" >
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Deactivate Brand</h4>
                      </div>
                      <div class="modal-body">
                        <p>Give reason to deactivate this brand</p>
                        <input type="hidden" name="id" id="brand_deactive_id">
                        <textarea required class="form-control" class="col-sm-12" placeholder="Reason in brief.." name="disable_reason"></textarea>
                      </div>
                      <div class="modal-footer">
                        <button class="btn">Deactivate</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                    </form>
                  </div>
                </div>
<div id="delete_brand_modal" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                      <form method="post" id="brand_delete_form" action="<?php echo URL::to( Config::get('constants.admin_path').'brand/status/disable/'); ?>" >
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete Brand</h4>
                      </div>
                      <div class="modal-body">
                        <p>Give reason to delete this brand</p>
                        <input type="hidden" name="id" id="brand_delete_id">
                        <textarea required class="form-control" class="col-sm-12" placeholder="Reason in brief.." name="delete_reason"></textarea>
                      </div>
                      <div class="modal-footer">
                        <button class="btn">Delete</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                    </form>
                  </div>
                </div>
@stop