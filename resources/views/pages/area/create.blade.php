@extends('layouts.default')
@section('content_header')
Area
@stop

@section('content')
<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Area Create
                        </div>
                        
                    @if ($errors->has())
                    <div class="alert alert-danger">            
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>        
                        @endforeach
                    </div>
                    @endif   
                    
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif                          
                        
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="<?php echo URL::to( Config::get('constants.admin_path').'area/store') ?>" method="post">
                                        
                                    <div class="form-group">
                                        <label>Region</label>
                                        <select name="region_id" class="form-control">
                                        @foreach($region as $r)
                                            <option value="{{$r['id']}}">
                                                {{$r['region_name']}}
                                            </option>
                                        @endforeach
                                        </select>
                                    </div>
                                        <div class="form-group">
                                            <label>Area Name</label>
                                            <input class="form-control" placeholder="Area name" name="area_name" type="text" value="{{ Input::old('area_name') }}" autofocus/>
                                        </div>                                        
                                        <button type="submit" class="btn btn-default">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>        
@stop