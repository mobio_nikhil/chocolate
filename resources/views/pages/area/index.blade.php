@extends('layouts.default')
@section('content_header')
Area
<br/><br/>
<a href="<?php echo URL::to( Config::get('constants.admin_path').'area/create') ?>" class="btn btn-primary">Add</a>            
@stop
@section('content')
<style>
    tfoot {
        display: table-header-group;
    }
    .dataTables_filter {
        display: none;
    } 
    .display td{
        padding-left: 10px;
        padding-top: 4px;
        padding-bottom: 4px;
    }    
</style>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Area List
            </div>
            
                @if ($errors->has())
                    <div class="alert alert-danger">            
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>        
                        @endforeach
                    </div>
                    @endif   
                    
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif             
            <!-- /.panel-heading -->
            <div class="panel-body">
            <table id="area_grid" class="display table" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Region</th>
                            <th>Area Name</th>
                            <th>Deactivate Reason</th>
                            <th>Create Date</th>
                            <th>Status</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Region</th>
                            <th>Area Name</th>
                            <th>Deactivate Reason</th>
                            <th>Create Date</th>
                            <th>Status</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </tfoot>
                    <tbody>

                                    <?php foreach($area_list as $area)
                                    { 
                                        ?>
                                        <tr>
                                        <td>
                                            <?php 
                                                echo $area['region_name'];
                                            ?>
                                        </td>
                                        <td>
                                        <?php echo $area['area_name'] ?></td>
                                        <td><?php echo empty($area['disable_reason']) ? 'NA' : $area['disable_reason'] ?></td>
                                        <td><?php echo date('d-M-Y',strtotime($area['created_on'])) ?></td>

                                        @if( $area['disable_status'] == 0 )
                                            <?php /*<td> <a class="btn btn-default" href="<?php echo URL::to( Config::get('constants.admin_path').'area/status/disable/'.$area['id']) ?>"><i class="fa fa-check"></i></a></td>*/ ?>
                                                    <td><button type="button" onclick="deact_area('{{ $area['id'] }}','<?php echo URL::to( Config::get('constants.admin_path').'area/status/disable/'.$area['id']); ?>')" class="btn btn-default deact" ><i class="fa fa-check"></i></button></td>
                                        @else
                                            <td> <a class="btn btn-default" href="<?php echo URL::to( Config::get('constants.admin_path').'area/status/enable/'.$area['id']) ?>"><i class="fa fa-times"></i></a></td>                            
                                        @endif

                                        <td><a class="btn btn-default" href="<?php echo URL::to( Config::get('constants.admin_path').'area/edit/'.$area['id']) ?>"> <i class="fa fa-pencil"></i></a></td>
                                        <td><a class="btn btn-default" href="#" onclick="area_delete(<?php echo $area['id'] ?>)"> <i class="fa fa-trash"></i></a></td>                            
                                        </tr>

                                    <?php } ?>         

                    </tbody>
                </table>
                
                
                <!-- /.table-responsive -->                            
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<div id="deactivate_area_modal" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                      <form method="post" id="area_deact_form" action="<?php echo URL::to( Config::get('constants.admin_path').'brand/status/disable/'); ?>" >
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Deactivate Area</h4>
                      </div>
                      <div class="modal-body">
                        <p>Give reason to deactivate this area</p>
                        <input type="hidden" name="id" id="area_deactive_id">
                        <textarea required class="form-control" class="col-sm-12" placeholder="Reason in brief.." name="disable_reason"></textarea>
                      </div>
                      <div class="modal-footer">
                        <button class="btn">Deactivate</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                    </form>
                  </div>
                </div>
<div id="delete_area_modal" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                      <form method="post" id="area_delete_form" action="<?php echo URL::to( Config::get('constants.admin_path').'brand/status/disable/'); ?>" >
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete Area</h4>
                      </div>
                      <div class="modal-body">
                        <p>Give reason to delete this area</p>
                        <input type="hidden" name="id" id="area_delete_id">
                        <textarea required class="form-control" class="col-sm-12" placeholder="Reason in brief.." name="delete_reason"></textarea>
                      </div>
                      <div class="modal-footer">
                        <button class="btn">Delete</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                    </form>
                  </div>
                </div>
@stop