@extends('layouts.default')
@section('content_header')
Meetings of <?php echo $user_data[0]['user_name'] ; ?>  with  <?php echo $cust_data[0]['customer_name'] ?>
<br/>
@stop
@section('content')
<?php if(count($customer_meeting) > 0){ ?>
<?php for ($i = 0; $i < count($customer_meeting); $i++) { ?>
<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php echo '<b>'.$customer_meeting[$i]['meeting_day'].'</b>'; ?>
                        </div>
                        <!-- .panel-heading -->
                        <div class="panel-body">
                            <div class="panel-group" id="parent_<?php echo $i ?>">
                        <?php for($j=0;$j<count($customer_meeting[$i]['meeting_data']);$j++){ ?>
                                
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#parent_<?php echo $i ?>" href="#parent_<?php echo $i ?>_child_<?php echo $j ?>"><?php echo $customer_meeting[$i]['meeting_data'][$j]['user_name']. ' : '.date('h:i:s A',strtotime($customer_meeting[$i]['meeting_data'][$j]['meeting_date_time'])) ?></a>
                                            </h4>
                                        </div>
                                        <div id="parent_<?php echo $i ?>_child_<?php echo $j ?>" class="panel-collapse collapse out">
                                            <div class="panel-body">
                                                <?php echo 'Meeting Note:'.$customer_meeting[$i]['meeting_data'][$j]['meeting_note'] ?><br/>                                                
                                                <?php echo "Followup Date & Time: "; ?>
                                                <?php echo ($customer_meeting[$i]['meeting_data'][$j]['followup_date_time']) ? date('d-M-Y H:i:s A',strtotime($customer_meeting[$i]['meeting_data'][$j]['followup_date_time'])) : ' - '; ?><br/>                                                
                                                <?php
                                                if(!(empty($customer_meeting[$i]['meeting_data'][$j]['meeting_image_1']))){ ?>
                                                    <a  target="_blank" href="<?php echo URL::to(Input::root().'/' .$customer_meeting[$i]['meeting_data'][$j]['meeting_image_1']) ?>">Image</a>                                                    
                                                    
                                                <?php echo '<br/>'; } 
                                                ?>
                                                <?php
                                                if(!(empty($customer_meeting[$i]['meeting_data'][$j]['meeting_image_2']))){ ?>
                                                    <a  target="_blank" href="<?php echo URL::to(Input::root().'/' .$customer_meeting[$i]['meeting_data'][$j]['meeting_image_2']) ?>">Image</a>                                                    
                                                <?php echo '<br/>'; } 
                                                ?>
                                                <?php
                                                if(!(empty($customer_meeting[$i]['meeting_data'][$j]['meeting_image_3']))){ ?>
                                                    <a  target="_blank" href="<?php echo URL::to(Input::root().'/' .$customer_meeting[$i]['meeting_data'][$j]['meeting_image_3']) ?>">Image</a>                                                    
                                                <?php echo '<br/>'; } 
                                                ?>
                                                <?php
                                                if(!(empty($customer_meeting[$i]['meeting_data'][$j]['meeting_image_4']))){ ?>
                                                    <a  target="_blank" href="<?php echo URL::to(Input::root().'/' .$customer_meeting[$i]['meeting_data'][$j]['meeting_image_4']) ?>">Image</a>                                                    
                                                <?php echo '<br/>'; } 
                                                ?>
                                                <?php
                                                if(!(empty($customer_meeting[$i]['meeting_data'][$j]['meeting_image_5']))){ ?>
                                                    <a  target="_blank" href="<?php echo URL::to(Input::root().'/' .$customer_meeting[$i]['meeting_data'][$j]['meeting_image_5']) ?>">Image</a>                                                    
                                                <?php echo '<br/>'; } 
                                                ?>
                                            </div>
                                        </div>
                                    </div>                                
                            <?php } ?>
                            </div>
                        </div>
                        <!-- .panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>


<?php } ?>
<?php }else{ ?>
No details found
<?php } ?>



@stop