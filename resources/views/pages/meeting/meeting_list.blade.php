@extends('layouts.default')
@section('content_header')
Meeting
<br/><br/>
<a href="<?php echo URL::to( Config::get('constants.admin_path').'meeting/create') ?>" class="btn btn-primary">Add</a>            
@stop
@section('content')
<style>
    tfoot {
        display: table-header-group;
    }
    .dataTables_filter {
    display: none;
    }     
</style>


<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Meeting List
            </div>
            
                @if ($errors->has())
                    <div class="alert alert-danger">            
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>        
                        @endforeach
                    </div>
                    @endif   
                    
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif             
            <!-- /.panel-heading -->
            <div class="panel-body">
            <table id="employee_grid" class="display table" width="100%" cellspacing="0">
                    <thead>
                        <tr>                            
                            <th>User Name</th>                            
                            <th>Business Name</th>                            
                            <th>Customer Name</th>                            
                            <th>Brand Name</th>                            
                            <th>Purpose Name</th>                            
                            <th>Other Purpose Reason</th>                            
                            <th>Meeting With</th>                            
                            <th>Region</th>
                            <th>State</th>
                            <th>Area</th>
                            <th>Note</th>
                            <th>Meeting Date</th>                            
                            <th>Track</th>
                            <th>History</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>                            
                            <th>User Name</th>                            
                            <th>Business Name</th>                            
                            <th>Customer Name</th>                            
                            <th>Brand Name</th>                            
                            <th>Purpose Name</th> 
                            <th>Other Purpose Reason</th>                            
                            <th>Meeting With</th>                            
                            <th>Region</th>
                            <th>State</th>
                            <th>Area</th>
                            <th>Note</th>
                            <th>Meeting Date</th>                            
                            <th>Location</th>
                            <th>History</th>
                        </tr>
                    </tfoot>
                    <tbody>
                                    <?php foreach($meeting_list as $meeting){ ?>
                                        <tr>                                        
                                        <td><?php echo $meeting['user_name'] ?></td>
                                        <td><?php echo $meeting['business_name'] ?></td>
                                        <td><?php echo $meeting['customer_name'] ?></td>
                                        <td><?php echo empty($meeting['brand_name']) ? 'NA' : $meeting['brand_name'] ?></td>
                                        <td><?php echo $meeting['purpose_name'] ?></td>
                                        <td><?php echo $meeting['meeting_other_purpose'] ?></td>
                                        <td><?php echo $meeting['customer_name'] ?></td>
                                        <td><?php echo $meeting['cregion'] ?></td>
                                        <td><?php echo $meeting['cstate'] ?></td>
                                        <td><?php echo $meeting['carea'] ?></td>
                                        <td><?php echo empty($meeting['extra_note']) ? 'NA' : $meeting['extra_note'] ?></td>                                        
                                        <td><?php echo date('d-M-Y h:i:s A',strtotime($meeting['meeting_date'])) ?></td>
                                        <td><a href="<?php echo URL::to( Config::get('constants.admin_path').'meeting/track/'.$meeting['mid']) ?>">Meeting Location</a></td>
                                        <td><a href="<?php echo URL::to( Config::get('constants.admin_path').'meeting/meeting_history/'.$meeting['mid']) ?>">History</a></td>                                        
                                        </tr>

                                    <?php } ?>         

                    </tbody>
                </table>
                
                
                <!-- /.table-responsive -->                            
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
@stop