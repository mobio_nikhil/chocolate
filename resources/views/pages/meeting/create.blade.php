@extends('layouts.default')
@section('content_header')
Meeting
@stop

@section('content')
<style>
      #map {
        width: 100%;
        height: 30%;
      }
      .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 95%;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
      #target {
        width: 345px;
      }
</style>      
<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Meeting Create
                        </div>
                        
                    @if ($errors->has())
                    <div class="alert alert-danger">            
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>        
                        @endforeach
                    </div>
                    @endif   
                    
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif                          
                        
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" action="<?php echo URL::to( Config::get('constants.admin_path').'meeting/store') ?>" method="post" enctype="multipart/form-data">
                                        
                                        <div class="form-group">
                                            <label>Employee</label>
                                            <select class="form-control" id="employee_meeting" name="employee">
                                                <option value="">Select</option>
                                                <?php foreach($user_list as $user){ ?>
                                                <option <?php echo (Input::old('employee') == $user['id']) ? 'selected' : '' ?> value="<?php echo $user['id'] ?>"><?php echo $user['user_name'] ?></option>                                                    
                                                <?php } ?>
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Brand</label>
                                            <?php
                                                $st = array();
                                                if(count(Input::old('brand')) > 0){
                                                  $st = Input::old('brand');
                                                }
                                            ?>
                                            <select class="form-control" name="brand[]" id="employee_brand" multiple="multiple">
                                                <?php 
                                                if(count($brand_list) > 0){
                                                foreach($brand_list as $brand){ 
                                                ?>
                                                <option <?php echo (in_array($brand['id'], $st)) ? 'selected' : ''; ?> value="<?php echo $brand['id'] ?>"><?php echo $brand['brand_name'] ?></option>                                                    
                                                <?php } } ?> 
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Purpose</label>
                                            <select class="form-control" id="purpose_meeting" name="purpose">
                                                <?php foreach($purpose_list as $purpose){ ?>
                                                <option <?php echo (Input::old('purpose') == $purpose['id']) ? 'selected' : '' ?> value="<?php echo $purpose['id'] ?>"><?php echo $purpose['purpose_name'] ?></option>                                                    
                                                <?php } ?>
                                            </select>
                                        </div>
                                        
                                        <div class="form-group" id="div_opther_purpose" style="display: block;">
                                            <label>Meeting Other Purpose:</label>
                                            <input class="form-control" placeholder="Meeting Other Purpose" name="meeting_other_purpose" type="text" value="{{ Input::old('meeting_other_purpose') }}" autofocus/>
                                        </div>                                        
                                        
                                        <div class="form-group">
                                            <label>Customer Name</label>
                                            <select class="form-control" id="employee_customer" name="customer">
                                                <?php 
                                                 if(count($customer_list) > 0){
                                                foreach($customer_list as $customer){ ?>
                                                <option <?php echo (Input::old('customer') == $customer['id']) ? 'selected' : '' ?> value="<?php echo $customer['id'] ?>"><?php echo $customer['customer_name'] .' ( '.$customer['business_name']. ' ) ' ?></option>                                                    
                                                 <?php } } ?> 
                                            </select>
                                        </div>
                                         <?php /*                                       
                                        <div class="form-group">
                                            <label>Region</label>
                                            <select class="form-control" name="region">
                                                <?php foreach($region_list as $region){ ?>
                                                <option <?php echo (Input::old('region') ==  $region['id']) ? 'selected' : '' ?> value="<?php echo $region['id'] ?>"><?php echo $region['region_name'] ?></option>                                                    
                                                <?php } ?>
                                            </select>
                                        </div>                                        
                                        
                                        <div class="form-group">
                                            <label>Area</label>
                                            <select class="form-control" name="area">
                                                <?php foreach($area_list as $area){ ?>
                                                <option <?php echo (Input::old('area') == 1) ? 'selected' : '' ?> value="<?php echo $area['id'] ?>"><?php echo $area['area_name'] ?></option>                                                    
                                                <?php } ?>
                                            </select>
                                        </div>
                                        */ ?>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="followup_reminder" <?php echo (Input::old('followup_reminder')) ? "checked='checked'" : ''  ?> value="yes">Followup Reminder
                                            </label>
                                        </div>                                        
                                        
                                        <div class="form-group">
                                            <label>Followup Date</label>
                                            <input class="form-control date" placeholder="Followup Date" name="followup_date" type="text" value="{{ Input::old('followup_date') }}" autofocus/>
                                        </div>                                        
                                        
                                        <div class="form-group">
                                            <label>Followup Time</label>
                                            <input class="form-control" id="followup_time" placeholder="Followup Time" name="followup_time" type="text" value="{{ Input::old('followup_time') }}" autofocus/>
                                        </div>                                                                                
                                             
                                        <div class="form-group">
                                            <label>Meeting Image 1</label>
                                            <input type="file" name="meeting_image_1">
                                        </div>                                        
                                        <div class="form-group">
                                            <label>Meeting Image 2</label>
                                            <input type="file" name="meeting_image_2">
                                        </div>                                        
                                        <div class="form-group">
                                            <label>Meeting Image 3</label>
                                            <input type="file" name="meeting_image_3">
                                        </div>                                        
                                        <div class="form-group">
                                            <label>Meeting Image 4</label>
                                            <input type="file" name="meeting_image_4">
                                        </div>                                        
                                        <div class="form-group">
                                            <label>Meeting Image 5</label>
                                            <input type="file" name="meeting_image_5">
                                        </div>                                         
                                        
                                        <div class="form-group">
                                            <label>Other Note:</label>
                                            <textarea class="form-control" rows="3" placeholder="Other Note" name="other_note" autofocus>{{ Input::old('other_note') }}</textarea>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Meeting Date</label>
                                            <input class="form-control date" placeholder="Meeting Date" name="meeting_date" type="text" value="{{ Input::old('meeting_date') }}" autofocus/>
                                        </div>                                        
                                        
                                        <div class="form-group">
                                            <label>Meeting Time</label>
                                            <input id="meeting_time" class="form-control" placeholder="Meeting Time" name="meeting_time" type="text" value="{{ Input::old('meeting_time') }}" autofocus/>
                                        </div>                                        
                                        <input type="hidden" id="latitude" name="latitude"/>
                                        <input type="hidden" id="longitude" name="longitude"/>
                                        
                                            <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                                            <div id="map"></div>
                                        
                                        <button type="submit" class="btn btn-default">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
@stop

<script>
      // This example adds a search box to a map, using the Google Place Autocomplete
      // feature. People can enter geographical searches. The search box will return a
      // pick list containing a mix of places and predicted search terms.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 23.022505, lng: 72.5713621},
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

           
          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
            //alert(place.geometry.location.lat());
          document.getElementById('latitude').value = place.geometry.location.lat();
          document.getElementById('longitude').value = place.geometry.location.lng();
//          document.getElementById('lat').value = 'ss';
//          document.getElementById('lng').value = 'ssd';
          });
          map.fitBounds(bounds);
        });
        
        var input = document.getElementById('pac-input');
        google.maps.event.addDomListener(input, 'keydown', function(e) { 
        if (e.keyCode == 13) { 
            e.preventDefault(); 
        }
        });         
        
        
      }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDimOru3JmXteUdpRe639CW5rmlEUrLXQ&libraries=places&callback=initAutocomplete"
         async defer></script>