@extends('layouts.default')
@section('content_header')
Meeting  - <?php echo $user[0]['user_name']; ?>
@stop
@section('content')
<style>
    tfoot {
        display: table-header-group;
    }
    .dataTables_filter {
    display: none;
    }     
    
    
.black_overlay {
  display: none;
  position: absolute;
  top: 0%;
  left: 0%;
  width: 100%;
  height: 100%;
  background-color: black;
  z-index: 1001;
  -moz-opacity: 0.8;
  opacity: .80;
  filter: alpha(opacity=80);
}
.white_content {
  display: none;
  position: fixed;
  top: 38%;
    left: 38%;
    width: 25%;
    height: 15%;
  padding: 16px;
  border: 1px solid #ddd;;
  background-color: white;
  z-index: 1002;
  overflow: auto;
}        
</style>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                
            </div>

            @if ($errors->has())
            <div class="alert alert-danger">            
                @foreach ($errors->all() as $error)
                {{ $error }}<br>        
                @endforeach
            </div>
            @endif   

            @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif             
            <!-- /.panel-heading -->
            <div class="panel-body">
                <?php if(count($user_gps) > 0 ){ ?>
                <style>
                    #map_wrapper {
                        height: 400px;
                    }

                    #map_canvas {
                        width: 100%;
                        height: 100%;
                    }
                </style> 
                <div id="light" class="white_content"><center>Please wait map is loading....<img src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif" height="62" width="75"></center></div>                    
                <div id="map_wrapper">
                    <div id="map_canvas" class="mapping"></div>
                </div>

                <script src="https://maps.googleapis.com/maps/api/js?sensor=false&key=<?php echo Config::get('constants.google_api_key') ?>"></script>

                <script>
                    /*var script = document.createElement('script');
                    script.src = "//maps.googleapis.com/maps/api/js?sensor=false";
                    document.body.appendChild(script);*/
    
 

                    function init() {
                        var map;
                        var polyline;

                        /*var markers = [
                            //new google.maps.LatLng(17.43495, 78.50898333),
                            new google.maps.LatLng(17.43495, 78.50898333),
                            new google.maps.LatLng(17.43938333, 78.52168333),
                            new google.maps.LatLng(17.43708333, 78.52925),
                            new google.maps.LatLng(17.4366, 78.53336667)
                        ]; */
                        var markers = [];
                          <?php for($i=0;$i<count($user_gps);$i++){ ?>
                            markers['<?php echo $i ?>'] = new google.maps.LatLng('<?php echo $user_gps[$i]['latitude'] ?>','<?php echo $user_gps[$i]['longitude'] ?>');
                            //alert(markers['<?php echo $i ?>']);
                          <?php } ?>

                        var infoWindowContent = [];
                          <?php for($i=0;$i<count($user_gps);$i++){ ?>
                             infoWindowContent['<?php echo $i ?>'] = ['<div class="info_content">' +
                                    '<h3>' + '<?php echo str_replace("'","",$user_gps[$i]['map_header']) ?>' + '</h3>' +
                                    '<p>' + '<?php echo str_replace("'","",$user_gps[$i]['map_content']) ?>' + '</p>' +
                                    '<p>' + '<?php echo str_replace("'","",$user_gps[$i]['map_time_date']) ?>' + '</p>' +                                    
                                    '</div>'];
                            //alert(markers['<?php echo $i ?>']);
                          <?php } ?>
    
    
                        // Info Window Content
                       /* var infoWindowContent = [
                            ['<div class="info_content">' +
                                    '<h3>Palace of Westminster1</h3>' +
                                    '<p>The Palace of Westminster is the meeting place of the House of Commons and the House of Lords, the two houses of the Parliament of the United Kingdom. Commonly known as the Houses of Parliament after its tenants.</p>' +
                                    '<p>The Palace of Westminster is the meeting place of the House of Commons and the House of Lords, the two houses of the Parliament of the United Kingdom. Commonly known as the Houses of Parliament after its tenants.</p>' +
                                    '</div>'],
                            ['<div class="info_content">' +
                                    '<h3>Palace of Westminster2</h3>' +
                                    '<p>The Palace of Westminster is the meeting place of the House of Commons and the House of Lords, the two houses of the Parliament of the United Kingdom. Commonly known as the Houses of Parliament after its tenants.</p>' +
                                    '</div>'],
                            ['<div class="info_content">' +
                                    '<h3>Palace of Westminster3</h3>' +
                                    '<p>The Palace of Westminster is the meeting place of the House of Commons and the House of Lords, the two houses of the Parliament of the United Kingdom. Commonly known as the Houses of Parliament after its tenants.</p>' +
                                    '</div>'],
                            ['<div class="info_content">' +
                                    '<h3>Palace of Westminster4</h3>' +
                                    '<p>The Palace of Westminster is the meeting place of the House of Commons and the House of Lords, the two houses of the Parliament of the United Kingdom. Commonly known as the Houses of Parliament after its tenants.</p>' +
                                    '</div>']
                        ];       
                        */
                        //alert(infoWindowContent[0]);
                        
                        
                        
                        
                        var moptions = {
                            //center: new google.maps.LatLng(17.43938333, 78.52168333),
                            center: new google.maps.LatLng('<?php echo $user_gps[0]['latitude'] ?>', '<?php echo $user_gps[0]['longitude'] ?>'),
                            zoom: 12,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        }

                        map = new google.maps.Map(document.getElementById("map_canvas"), moptions);

                        var iconsetngs = {
                            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
                        };
                        var polylineoptns = {
                            strokeOpacity: 0.8,
                            strokeWeight: 3,
                            map: map,
                            icons: [{
                                    repeat: '70px', //CHANGE THIS VALUE TO CHANGE THE DISTANCE BETWEEN ARROWS
                                    icon: iconsetngs,
                                    offset: '100%'}]
                        };
                        polyline = new google.maps.Polyline(polylineoptns);
                        var z = 0;
                        var path = [];
                        path[z] = polyline.getPath();
                        var infoWindow = new google.maps.InfoWindow(), marker, i;
                        for (var i = 0; i < markers.length; i++) //LOOP TO DISPLAY THE MARKERS
                        {
                            var pos = markers[i];
                            var marker = new google.maps.Marker({
                                position: pos,
                                map: map,
                                icon:'http://maps.google.com/mapfiles/ms/icons/orange-dot.png'
                            });
        
                            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                return function() {
                                    infoWindow.setContent(infoWindowContent[i][0]);
                                    infoWindow.open(map, marker);
                                }
                            })(marker, i));
        
                            google.maps.event.addListenerOnce(map, 'idle', function(){

                                theObj2 = document.getElementById("light");
                                theObj2.style.display = "block";

                            });           
        
                            path[z].push(marker.getPosition()); //PUSH THE NEWLY CREATED MARKER'S POSITION TO THE PATH ARRAY
        
        
        
                        }
                    }
                    window.onload = init;    
    
                    setTimeout(function(){
                        theObj2 = document.getElementById("light");
                        theObj2.style.display = "none";
                    }, 5000);  
  
    

                </script>
<div class="panel-body">
                        <table id="employee_track_grid" class="display table" width="100%" cellspacing="0">
                            <thead>
                                <tr>                            
                                    <th>Activity</th>                            
                                    <th>Activity Content</th>                                                
                                    <th>Acivity Date Time</th>                            
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>                            
                                    <th>Activity Name</th>                            
                                    <th>Activity Content</th>                            
                                    <th>Acivity Date Time</th>                            

                                </tr>
                            </tfoot>                            
                            <tbody>
                                <?php for($i=0;$i<count($user_gps);$i++){ ?>
                                        <tr>
                                            <td><?php echo $user_gps[$i]['map_header'] ?></td>
                                        <td><?php echo $user_gps[$i]['map_content'] ?></td>
                                        <td><?php echo $user_gps[$i]['map_time_date'] ?></td>
                                        </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>                
                <?php }else{ ?>
                
                Track not found
                
                
                <?php } ?>

                <!-- /.table-responsive -->                            
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
@stop