<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Chocolate Room</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo Input::root().'/theme/' ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo Input::root().'/theme/' ?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo Input::root().'/theme/' ?>dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo Input::root().'/theme/' ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .btn{
           background-color: #2F4050;
        }
        .btn-success:hover {
            background-color: #2F4050;
            border-color: #398439;
            color: #fff;
        }
        .btn-success:focus {
            background-color: #2F4050;
            border-color: #255625;
            color: #fff;
        }
        .btn-success:active {
            background-color: #2F4050;
            border-color: #255625;
            color: #fff;
        }
.btn-success.active.focus, .btn-success.active:focus, .btn-success.active:hover, .btn-success.focus:active, .btn-success:active:focus, .btn-success:active:hover, .open > .dropdown-toggle.btn-success.focus, .open > .dropdown-toggle.btn-success:focus, .open > .dropdown-toggle.btn-success:hover {
            background-color: #17987e;
            border-color: #255625;
            color: #fff;
}        
.login-logo{
    margin-top: 50px;
    font-weight: 700;
    letter-spacing: 3px;
    color:white;
}
.logo-india{
    color:#48CFAD;
}


    </style>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="logo" align="center">
                    <h1 class="login-logo">Chocolate <span class="logo-india">Room</span></h1>
                </div>
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><center>Sign In</center></h3>
                    </div>
                    
                    @if ($errors->has())
                    <div class="alert alert-danger">            
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>        
                        @endforeach
                    </div>
                    @endif   
                    
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif                    
                                        
                    <div class="panel-body">
                        <form role="form" action="{{ Config::get('constants.admin_path') }}login" method="post">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="User Email" name="user_email" type="text" value="{{ Input::old('user_code') }}" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
<!--                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>-->
                                <!-- Change this to a button or input when using this as a form -->

                                <input type="submit" class="btn btn-lg btn-success btn-block" value="Login"/>
                                <div class="form-group" align="right"><sub><a href="/user/forgot">( Forgot Password? )</a></sub></div>
<!--                                <a href="index.html" class="btn btn-lg btn-success btn-block">Login</a>-->
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo Input::root().'/theme/' ?>vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo Input::root().'/theme/' ?>vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo Input::root().'/theme/' ?>vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo Input::root().'/theme/' ?>dist/js/sb-admin-2.js"></script>

</body>

</html>
