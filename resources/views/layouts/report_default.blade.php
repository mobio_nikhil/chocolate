<!DOCTYPE html>
<html lang="en">

<head>
@include('includes.head')
</head>
<body>
    <div id="wrapper">
         @include('includes.header')
         
                 <!-- Page Content -->
                 <div id="page-wrapper" style="width: 4500px;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">@yield('content_header')</h1>                        
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                
                @yield('content')
                
            </div>
            <!-- /.container-fluid -->
        </div>
        <center style="color:#fff;font-weight:bold;font-size: 16px;padding:10px 0px;">&copy; <?= date("Y") ?> All Rights Reserved By Chocolate Room </center>
        <!-- /#page-wrapper -->
       
    </div>
    
     @include('includes.report_footer')
    
</body>
</html>