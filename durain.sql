-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 27, 2016 at 02:36 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `durain`
--

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `area_name` varchar(20) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `region_id` int(11) DEFAULT NULL,
  `delete_status` enum('0','1') NOT NULL DEFAULT '0',
  `disable_status` enum('0','1') NOT NULL DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id`, `area_name`, `user_id`, `region_id`, `delete_status`, `disable_status`, `created_on`) VALUES
(1, 'Ahmedabad', 1, 1, '0', '0', '2016-09-12 09:00:33'),
(2, 'Vadodara', 1, 1, '0', '0', '2016-09-12 09:00:33'),
(3, 'Delhi', 1, 4, '0', '0', '2016-09-19 06:37:55'),
(4, 'Panjab', 1, 4, '0', '0', '2016-09-20 05:04:28'),
(5, 'Bihar', 1, 3, '0', '0', '2016-09-20 05:04:36'),
(6, 'Karnataka', 1, 3, '0', '0', '2016-09-20 05:04:47'),
(7, 'Banglore', 1, 2, '0', '0', '2016-09-20 05:04:57'),
(8, 'Chennai', 1, 2, '0', '0', '2016-09-20 05:05:04');

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` int(11) NOT NULL,
  `brand_name` varchar(20) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `delete_status` enum('0','1') NOT NULL DEFAULT '0',
  `disable_status` enum('0','1') NOT NULL DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `brand_name`, `user_id`, `delete_status`, `disable_status`, `created_on`) VALUES
(1, 'Brand_Abc', 0, '0', '0', '2016-09-06 07:32:03'),
(2, 'Brand Xyz', 0, '0', '0', '2016-09-06 07:32:03');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `customer_code` int(11) DEFAULT NULL,
  `customer_name` varchar(50) DEFAULT NULL,
  `business_name` varchar(50) DEFAULT NULL,
  `business_address` text,
  `primary_contact_name` varchar(255) DEFAULT NULL,
  `primary_image` text,
  `secondary_contact_name` varchar(30) DEFAULT NULL,
  `customer_email` varchar(255) DEFAULT NULL,
  `customer_phone` varchar(30) DEFAULT NULL,
  `customer_type_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `visit_card_image_1` text,
  `visit_card_image_2` text,
  `visit_card_image_3` text,
  `visit_card_image_4` text,
  `visit_card_image_5` text,
  `customer_birth_date` date DEFAULT NULL,
  `customer_anniversary_date` date DEFAULT NULL,
  `information_enter_by` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `delete_status` enum('0','1') NOT NULL DEFAULT '0',
  `disable_status` enum('0','1') NOT NULL DEFAULT '0',
  `disable_reason` text,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `customer_code`, `customer_name`, `business_name`, `business_address`, `primary_contact_name`, `primary_image`, `secondary_contact_name`, `customer_email`, `customer_phone`, `customer_type_id`, `brand_id`, `visit_card_image_1`, `visit_card_image_2`, `visit_card_image_3`, `visit_card_image_4`, `visit_card_image_5`, `customer_birth_date`, `customer_anniversary_date`, `information_enter_by`, `region_id`, `state_id`, `area_id`, `delete_status`, `disable_status`, `disable_reason`, `created_on`) VALUES
(1, NULL, 'west RSM added', 'west RSM added', 'ahmedabad', 'abc', 'upload_images/customer_1/primary_image_1.jpg', 'vv ff', 's@dd.com', '666', 1, NULL, 'upload_images/customer_1/visit_card_image_1.jpg', NULL, NULL, NULL, NULL, '1988-10-26', '1988-10-26', 51, 1, 12, 1, '0', '0', NULL, '2016-09-21 07:03:50'),
(2, NULL, 'west aSM added', 'west aSM added', 'ahmedabad', 'abc', 'upload_images/customer_2/primary_image_1.jpg', 'vv ff', 's@dd.com', '666', 1, NULL, 'upload_images/customer_2/visit_card_image_1.jpg', NULL, NULL, NULL, NULL, '1988-10-26', '1988-10-26', 53, 1, 12, 1, '0', '0', NULL, '2016-09-21 07:04:30'),
(3, NULL, 'west SM added', 'west SM added', 'ahmedabad', 'abc', 'upload_images/customer_3/primary_image_1.jpg', 'vv ff', 's@dd.com', '666', 1, NULL, 'upload_images/customer_3/visit_card_image_1.jpg', NULL, NULL, NULL, NULL, '1988-10-26', '1988-10-26', 57, 1, 12, 1, '0', '0', NULL, '2016-09-21 07:05:04'),
(4, NULL, 'Bhavin', 'Bhavin infocom', 'Ahmedabad', NULL, NULL, 'Shalin', 'bhavin32345@gmail.com', '1234567890', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 12, 8, '0', '0', '', '2016-09-21 13:14:49');

-- --------------------------------------------------------

--
-- Table structure for table `customer_type`
--

CREATE TABLE `customer_type` (
  `id` int(11) NOT NULL,
  `customer_type` varchar(20) DEFAULT NULL,
  `delete_status` enum('0','1') NOT NULL DEFAULT '0',
  `disable_status` enum('0','1') NOT NULL DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `folloup_meeting_customer`
--

CREATE TABLE `folloup_meeting_customer` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `followup_date_time` datetime DEFAULT NULL,
  `meeting_id` int(11) DEFAULT NULL,
  `extra_note` text,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `folloup_meeting_customer`
--

INSERT INTO `folloup_meeting_customer` (`id`, `user_id`, `customer_id`, `followup_date_time`, `meeting_id`, `extra_note`, `created_on`) VALUES
(1, 51, 1, '1988-10-26 10:12:00', NULL, '', '2016-09-20 07:18:06'),
(2, 1, 24, '1988-10-26 10:12:00', NULL, '', '2016-09-21 07:34:02'),
(3, 55, 1, '2016-09-09 12:15:00', NULL, NULL, '2016-09-30 06:45:00'),
(4, 61, 2, '2016-09-30 03:45:00', NULL, NULL, '2016-09-27 10:15:00'),
(5, 61, 1, '2016-09-27 05:00:00', NULL, NULL, '2016-09-27 07:30:00'),
(6, 61, 1, '2016-09-28 05:00:00', 13, NULL, '2016-09-27 07:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `global_setting`
--

CREATE TABLE `global_setting` (
  `id` int(11) NOT NULL,
  `setting_key` varchar(50) DEFAULT NULL,
  `setting_value` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `global_setting`
--

INSERT INTO `global_setting` (`id`, `setting_key`, `setting_value`) VALUES
(1, 'last_gps_duration_gap', '3');

-- --------------------------------------------------------

--
-- Table structure for table `gps_data`
--

CREATE TABLE `gps_data` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `gps_data_type` enum('0','1','2') DEFAULT NULL COMMENT '0-start day,1-log meeting,2-end day',
  `meeting_id` int(11) DEFAULT NULL,
  `latitude` varchar(50) DEFAULT NULL,
  `longitude` varchar(50) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gps_data`
--

INSERT INTO `gps_data` (`id`, `user_id`, `gps_data_type`, `meeting_id`, `latitude`, `longitude`, `created_on`) VALUES
(1, 51, '0', NULL, '23.0134135', '72.5624095', '2016-09-20 07:00:06'),
(2, 51, '1', 1, '22.9961698', '72.5995843', '2016-09-20 07:18:06'),
(3, 55, '1', 2, '22.9961698', '72.59958430000006', '2016-09-20 13:31:33'),
(4, 55, '1', 3, '22.9961698', '72.59958430000006', '2016-09-21 05:08:35'),
(5, 55, '1', 4, '22.9961698', '72.59958430000006', '2016-09-21 05:40:06'),
(6, 55, '1', 5, '23.0323307', '72.5620768', '2016-09-22 02:00:00'),
(7, 1, '1', 6, '44', '55', '2016-09-21 07:34:02'),
(8, 55, '1', 7, '23.0263517', '72.58190130000003', '2016-09-08 07:15:00'),
(9, 1, '0', NULL, '22.9961698', '72.5995843', '2016-09-23 06:06:31'),
(10, 1, '1', 8, '23.0336769', '72.4634117', '2016-09-23 06:11:57'),
(11, 1, '1', 9, '23.0336769', '72.4634117', '2016-09-23 11:23:48'),
(12, 55, '1', 10, '22.9961698', '72.59958430000006', '2016-09-30 06:45:00'),
(13, 61, '1', 11, '22.9961698', '72.59958430000006', '2016-09-27 10:15:00'),
(14, 61, '1', 12, '22.9961698', '72.59958430000006', '2016-09-27 07:30:00'),
(15, 61, '1', 13, '22.9961698', '72.59958430000006', '2016-09-27 07:30:00'),
(16, 59, '1', 14, '23.0336769', '72.4634117', '2016-09-27 11:24:47'),
(17, 61, '1', 15, '22.9961698', '72.59958430000006', '2016-09-28 11:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `meeting_customer`
--

CREATE TABLE `meeting_customer` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer_name` varchar(50) DEFAULT NULL,
  `brand_id` text,
  `purpose_id` int(11) DEFAULT NULL,
  `extra_note` text,
  `meeting_other_purpose` text,
  `meeting_image_5` text,
  `meeting_image_4` text,
  `meeting_image_3` text,
  `meeting_image_2` text,
  `meeting_image_1` text,
  `region_sales_manager_id` int(11) DEFAULT NULL,
  `area_sales_manager_id` int(11) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meeting_customer`
--

INSERT INTO `meeting_customer` (`id`, `user_id`, `customer_id`, `customer_name`, `brand_id`, `purpose_id`, `extra_note`, `meeting_other_purpose`, `meeting_image_5`, `meeting_image_4`, `meeting_image_3`, `meeting_image_2`, `meeting_image_1`, `region_sales_manager_id`, `area_sales_manager_id`, `created_on`) VALUES
(1, 51, 1, NULL, '1', 1, 'test', 'other purpose if purpose id is 1', NULL, NULL, NULL, NULL, NULL, 51, NULL, '2016-09-20 07:18:06'),
(2, 55, 1, NULL, '1', 1, '', 'other test', NULL, NULL, NULL, NULL, NULL, 52, 55, '2016-09-14 04:45:00'),
(3, 55, 2, NULL, '1', 2, '', '', NULL, NULL, NULL, NULL, NULL, 52, 55, '2016-09-20 05:00:00'),
(4, 55, 1, NULL, '1', 2, '', '', NULL, NULL, NULL, NULL, NULL, 52, 55, '0000-00-00 00:00:00'),
(5, 55, 1, NULL, '1', 1, '', 'tt', NULL, NULL, NULL, NULL, NULL, 52, 55, '2016-09-22 02:00:00'),
(6, 1, 1, 'abc', '1', 1, 'test', 'other purpose if purpose id is 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-09-21 07:34:02'),
(7, 55, 4, 'Bhavin', '1', 3, '', '', NULL, NULL, NULL, NULL, NULL, 52, 55, '2016-09-08 07:15:00'),
(8, 1, 1, 'abc', '1', 1, 'test', 'other purpose if purpose id is 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-09-23 06:11:57'),
(9, 1, 1, 'abc', '1', 1, 'test', 'other purpose if purpose id is 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-09-23 11:23:48'),
(10, 55, 1, 'west RSM added', '1', 1, 'note', 'other test', NULL, NULL, NULL, NULL, NULL, 52, 55, '2016-09-30 06:45:00'),
(11, 61, 2, 'west aSM added', '1', 2, '', '', NULL, NULL, NULL, NULL, NULL, 51, 61, '2016-09-27 10:15:00'),
(12, 61, 1, 'west RSM added', '1', 3, '', '', NULL, NULL, NULL, NULL, NULL, 51, 61, '2016-09-27 07:30:00'),
(13, 61, 1, 'west RSM added', '1', 3, '', '', NULL, NULL, NULL, NULL, NULL, 51, 61, '2016-09-27 07:30:00'),
(14, 59, 1, 'abc', '1', 1, 'test', 'other purpose if purpose id is 1', NULL, NULL, NULL, NULL, NULL, 52, 56, '2016-09-27 11:24:46'),
(15, 61, 1, 'west RSM added', '1,2', 2, '', '', NULL, NULL, NULL, NULL, NULL, 51, 61, '2016-09-28 11:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `meeting_purpose`
--

CREATE TABLE `meeting_purpose` (
  `id` int(11) NOT NULL,
  `purpose_name` varchar(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `delete_status` enum('0','1') NOT NULL DEFAULT '0',
  `disable_status` enum('0','1') NOT NULL DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meeting_purpose`
--

INSERT INTO `meeting_purpose` (`id`, `purpose_name`, `user_id`, `delete_status`, `disable_status`, `created_on`) VALUES
(1, 'Other', 1, '0', '0', '2016-09-10 09:54:53'),
(2, 'Purpose 2', 1, '0', '0', '2016-09-10 09:55:01'),
(3, 'purpose_1', 1, '0', '0', '2016-09-16 05:53:21');

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

CREATE TABLE `region` (
  `id` int(11) NOT NULL,
  `region_name` varchar(20) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `delete_status` enum('0','1') NOT NULL DEFAULT '0',
  `disable_status` enum('0','1') NOT NULL DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `region`
--

INSERT INTO `region` (`id`, `region_name`, `user_id`, `delete_status`, `disable_status`, `created_on`) VALUES
(1, 'West', 0, '0', '0', '2016-09-12 06:37:38'),
(2, 'South', 0, '0', '0', '2016-09-12 06:37:38'),
(3, 'East', 0, '0', '0', '2016-09-12 06:37:38'),
(4, 'North', 0, '0', '0', '2016-09-12 06:37:38'),
(6, 'aa', 0, '0', '0', '2016-09-26 10:40:08');

-- --------------------------------------------------------

--
-- Table structure for table `region_detail`
--

CREATE TABLE `region_detail` (
  `id` int(11) NOT NULL,
  `region_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `region_detail`
--

INSERT INTO `region_detail` (`id`, `region_id`, `state_id`, `created_on`) VALUES
(3, 2, 3, NULL),
(11, 6, 4, '2016-09-26 11:08:25'),
(12, 1, 12, '2016-09-27 10:46:32'),
(13, 1, 21, '2016-09-27 10:46:32');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(11) NOT NULL,
  `state_name` varchar(50) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `state_name`, `created_on`) VALUES
(1, 'ANDAMAN AND NICOBAR ISLANDS', NULL),
(2, 'ANDHRA PRADESH', NULL),
(3, 'ARUNACHAL PRADESH', NULL),
(4, 'ASSAM', NULL),
(5, 'BIHAR', NULL),
(6, 'CHATTISGARH', NULL),
(7, 'CHANDIGARH', NULL),
(8, 'DAMAN AND DIU', NULL),
(9, 'DELHI', NULL),
(10, 'DADRA AND NAGAR HAVELI', NULL),
(11, 'GOA', NULL),
(12, 'GUJARAT', NULL),
(13, 'HIMACHAL PRADESH', NULL),
(14, 'HARYANA', NULL),
(15, 'JAMMU AND KASHMIR', NULL),
(16, 'JHARKHAND', NULL),
(17, 'KERALA', NULL),
(18, 'KARNATAKA', NULL),
(19, 'LAKSHADWEEP', NULL),
(20, 'MEGHALAYA', NULL),
(21, 'MAHARASHTRA', NULL),
(22, 'MANIPUR', NULL),
(23, 'MADHYA PRADESH', NULL),
(24, 'MIZORAM', NULL),
(25, 'NAGALAND', NULL),
(26, 'ORISSA', NULL),
(27, 'PUNJAB', NULL),
(28, 'PONDICHERRY', NULL),
(29, 'RAJASTHAN', NULL),
(30, 'SIKKIM', NULL),
(31, 'TAMIL NADU', NULL),
(32, 'TRIPURA', NULL),
(33, 'UTTARAKHAND', NULL),
(34, 'UTTAR PRADESH', NULL),
(35, 'WEST BENGAL', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_phone` varchar(20) DEFAULT NULL,
  `user_type` enum('0','1','2','3','4') DEFAULT NULL COMMENT '0-vp,1-rsm,2-asm,3-sm',
  `region_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `assign_to_user` int(11) DEFAULT NULL,
  `user_code` int(11) DEFAULT NULL,
  `user_password` text,
  `device_token` text,
  `app_os` enum('0','1') NOT NULL DEFAULT '0',
  `notification_token` text,
  `disable_status` enum('0','1') NOT NULL DEFAULT '0',
  `delete_status` enum('0','1') NOT NULL DEFAULT '0',
  `user_passcode` varchar(10) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user_name`, `user_email`, `user_phone`, `user_type`, `region_id`, `area_id`, `assign_to_user`, `user_code`, `user_password`, `device_token`, `app_os`, `notification_token`, `disable_status`, `delete_status`, `user_passcode`, `created_on`) VALUES
(1, 'vp', 'vp@durain.com', '123456', '0', 1, NULL, NULL, NULL, '$2y$10$PTOMXmwS94HWny0y4IsjeOn1HuojYkIId5qmzLB8nQKwrDrovtm3q', 'hjadhiqewuqhw213kj1h2j3', '0', '38748hfhr74hd74hf74hd737474hd73hdh37h47g', '0', '0', NULL, '2016-09-06 09:35:07'),
(51, 'west rsm', 'vivek.mobio@gmail.com', '12345', '1', 1, NULL, NULL, NULL, '$2y$10$PTOMXmwS94HWny0y4IsjeOn1HuojYkIId5qmzLB8nQKwrDrovtm3q', 'hjadhiqewuqhw213kj1h2j3', '0', '38748hfhr74hd74hf74hd737474hd73hdh37h47g', '0', '0', NULL, '2016-09-20 05:05:51'),
(52, 'east rsm', 'vivek.mobio@gmail.commm', '12345', '1', 3, NULL, NULL, NULL, '$2y$10$PTOMXmwS94HWny0y4IsjeOn1HuojYkIId5qmzLB8nQKwrDrovtm3q', NULL, '0', NULL, '0', '0', NULL, '2016-09-20 05:07:33'),
(53, 'west asm one', 'vivek.mobio@gmail.coasm', '12345', '2', 1, 1, NULL, NULL, '$2y$10$PTOMXmwS94HWny0y4IsjeOn1HuojYkIId5qmzLB8nQKwrDrovtm3q', 'hjadhiqewuqhw213kj1h2j3', '0', '38748hfhr74hd74hf74hd737474hd73hdh37h47g', '0', '0', NULL, '2016-09-20 05:08:11'),
(54, 'west asm two', 'vivek.mobio@gmail.coasm2', '12345', '2', 1, 2, NULL, NULL, '$2y$10$PTOMXmwS94HWny0y4IsjeOn1HuojYkIId5qmzLB8nQKwrDrovtm3q', NULL, '0', NULL, '0', '0', NULL, '2016-09-20 05:08:36'),
(55, 'east asm one', 'vivek.mobio@gmail.coasm1', '12345', '2', 3, 5, NULL, NULL, '$2y$10$PTOMXmwS94HWny0y4IsjeOn1HuojYkIId5qmzLB8nQKwrDrovtm3q', 'hjadhiqewuqhw213kj1h2j3', '0', '38748hfhr74hd74hf74hd737474hd73hdh37h47g', '0', '0', NULL, '2016-09-20 05:09:05'),
(56, 'east asm two', 'vivek.mobio@gmail.coasm123', '12345', '2', 3, 6, NULL, NULL, '$2y$10$PTOMXmwS94HWny0y4IsjeOn1HuojYkIId5qmzLB8nQKwrDrovtm3q', NULL, '0', NULL, '0', '0', NULL, '2016-09-20 05:09:17'),
(57, 'west sm', 'vivek.mobio@gmail.coasm12322', '12345', '3', 1, 1, NULL, NULL, '$2y$10$PTOMXmwS94HWny0y4IsjeOn1HuojYkIId5qmzLB8nQKwrDrovtm3q', 'hjadhiqewuqhw213kj1h2j3', '0', '38748hfhr74hd74hf74hd737474hd73hdh37h47g', '0', '0', NULL, '2016-09-20 05:11:31'),
(58, 'west sm two', 'vivek.mobio@gmail.coasm12322dd', '12345', '3', 1, 2, NULL, NULL, '$2y$10$PTOMXmwS94HWny0y4IsjeOn1HuojYkIId5qmzLB8nQKwrDrovtm3q', NULL, '0', NULL, '0', '0', NULL, '2016-09-20 05:11:50'),
(59, 'east sm one', 'vivek.mobio@gmail.coasm12322ddsdf', '12345', '3', 3, 5, 56, NULL, '$2y$10$PTOMXmwS94HWny0y4IsjeOn1HuojYkIId5qmzLB8nQKwrDrovtm3q', 'hjadhiqewuqhw213kj1h2j3', '0', '38748hfhr74hd74hf74hd737474hd73hdh37h47g', '0', '0', NULL, '2016-09-20 05:12:04'),
(60, 'east sm two', 'vivek.mobio@gmail.coasm12322ddsdfsdf', '12345', '3', 3, 6, NULL, NULL, '$2y$10$PTOMXmwS94HWny0y4IsjeOn1HuojYkIId5qmzLB8nQKwrDrovtm3q', NULL, '0', NULL, '0', '0', NULL, '2016-09-20 05:12:15'),
(61, 'asm west three', 'westasm3@gmail.com', '9632356896', '2', 1, NULL, NULL, NULL, '$2y$10$FxERk287.BzF4nypcXjVKefGBzA4qDM5iykCriys5YlT.J5UIIHoS', 'hjadhiqewuqhw213kj1h2j3', '0', '38748hfhr74hd74hf74hd737474hd73hdh37h47g', '0', '0', NULL, '2016-09-26 11:12:23'),
(62, 'west asm four', 'westasmfour@gmail.com', '9562563', '2', 6, NULL, NULL, NULL, '$2y$10$RO61RY4Y5cQlBbPVXm6jUO5sBO2qtsgVyHNr13QTNag2BnSsLIXUC', NULL, '0', NULL, '0', '0', NULL, '2016-09-26 11:26:58');

-- --------------------------------------------------------

--
-- Table structure for table `user_area`
--

CREATE TABLE `user_area` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_area`
--

INSERT INTO `user_area` (`id`, `user_id`, `area_id`, `created_on`) VALUES
(9, 61, 1, '2016-09-27 07:16:21'),
(10, 61, 2, '2016-09-27 07:16:21'),
(11, 59, 5, '2016-09-27 09:33:42'),
(12, 59, 6, '2016-09-27 09:33:42');

-- --------------------------------------------------------

--
-- Table structure for table `user_brand`
--

CREATE TABLE `user_brand` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_brand`
--

INSERT INTO `user_brand` (`id`, `user_id`, `brand_id`, `created_on`) VALUES
(4, 62, 1, '2016-09-26 11:48:21'),
(5, 62, 2, '2016-09-26 11:48:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_type`
--
ALTER TABLE `customer_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `folloup_meeting_customer`
--
ALTER TABLE `folloup_meeting_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `global_setting`
--
ALTER TABLE `global_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gps_data`
--
ALTER TABLE `gps_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meeting_customer`
--
ALTER TABLE `meeting_customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meeting_purpose`
--
ALTER TABLE `meeting_purpose`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `region_detail`
--
ALTER TABLE `region_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_area`
--
ALTER TABLE `user_area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_brand`
--
ALTER TABLE `user_brand`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `customer_type`
--
ALTER TABLE `customer_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `folloup_meeting_customer`
--
ALTER TABLE `folloup_meeting_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `global_setting`
--
ALTER TABLE `global_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `gps_data`
--
ALTER TABLE `gps_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `meeting_customer`
--
ALTER TABLE `meeting_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `meeting_purpose`
--
ALTER TABLE `meeting_purpose`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `region`
--
ALTER TABLE `region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `region_detail`
--
ALTER TABLE `region_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `user_area`
--
ALTER TABLE `user_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `user_brand`
--
ALTER TABLE `user_brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
