
var region = {};


$(document).ready(function() {

   $( ".start_date" ).each(function(){
        $(this).datepicker({
            "changeMonth": true,
            "changeYear": true,
            "showButtonPanel": true,
            "yearRange": "-100:+0",
            "dateFormat": "dd-M-yy",            
        });
   })

    $( ".end_date" ).each(function(){
        $(this).datepicker({
            "changeMonth": true,
            "changeYear": true,
            "showButtonPanel": true,
            "yearRange": "-100:+0",
            "dateFormat": "dd-M-yy",            
        });
   })
  
    $( ".bdate" ).each(function(){
        $(this).datepicker({
            "changeMonth": true,
            "changeYear": false,
            "showButtonPanel": true,
            "yearRange": "-100:+0",
            "dateFormat": "dd-M",    
            //"altFormat" : "dd-mm",
            //"altField"  : "#birth_date"
        });
   })

    $( ".adate" ).each(function(){
        $(this).datepicker({
            "changeMonth": true,
            "changeYear": false,
            "showButtonPanel": true,
            "yearRange": "-100:+0",
            "dateFormat": "dd-M", 
            //"altFormat" : "dd-mm",
            //"altField"  : "#anniversary_date"
        });
   })
   
   $( ".date" ).each(function(){
        var self  = this;
        var dval = $( self ).attr('value');
        console.log(dval);
        var d   =  "<input type='hidden' class='actualDate' value='"+dval+"' />" ;
        
        var $d  = $(d).insertAfter( $( self ) );
        
            $d.attr("name",  $( self ).attr('name'));
            $( self ).attr('name',"");
            
            
            console.log($(self).siblings(".actualDate")[0]);

            $(self).datepicker({
                "changeMonth": true,
                "changeYear": true,
                "showButtonPanel": true,
                "yearRange": "-100:+0",
                "dateFormat": "dd-M-yy",
                "altFormat" : "dd-mm-yy",
                "altField"  : $(self).siblings(".actualDate")[0],
                "defaultDate"  : ''
            }); 
            

       
            //$(".date").datepicker("setDate", new Date(1966+1,01,26));
       });    
       
            /*function d_update (self){
                //alert($(self).val() );
                  if($(self).val() == ""){
                      $(self).siblings(".actualDate").val("");                      
                      $(self).datepicker("setDate", null);
                  }

             }
                        
            $('.date').change(function(){
                d_update(this);
            });     */
   
   //start
   /*$( ".date" ).each(function(){
        var self  = this;
        var dval = $( self ).attr('value');
        console.log(dval);
        var d   =  "<input type='hidden' class='actualDate' value='"+dval+"' />" ;
        
        var $d  = $(d).insertAfter( $( self ) );
        
            $d.attr("name",  $( self ).attr('name'));
            $( self ).attr('name',"");

        if( dval != "" ){
            tmp = dval.split('-');

            dval = {
                    day    : tmp[0],
                    month  : tmp[1],
                    year   : tmp[2]
                };
            
        console.log(dval);
        }

        var sibl = $(self).siblings(".actualDate")[0];
        console.log(sibl);
            function isSafari() {
                return /^((?!chrome).)*safari/i.test(navigator.userAgent);
            }

        $(self).datepicker({
            "changeMonth": true,
            "changeYear": true,
            "showButtonPanel": true,
            "yearRange": "-100:+0",
            "dateFormat": "dd-M-yy",
            "altFormat" : "dd-mm-yy",
            "altField"  : sibl
        });

        if(dval != ""){
            dval = new Date(dval.year,dval.month,dval.day);
            $(self).datepicker('setDate', dval );
        }

        $( self ).change(function(){
            if($(self).val() == ""){
                $(self).siblings(".actualDate").val("");
                $(self).datepicker('setDate', "" );
               
            }
       });
   })
*/
//end
   

   // console.log( $( ".date" ) );
   $('#searchbyname').click(function(){
       $('#div_search_name').css('display','block');
       $('#div_search_id').css('display','none');
   });

    $('#searchbyid').click(function(){
       $('#div_search_id').css('display','block');
       $('#div_search_name').css('display','none');
   });
   
   
   //employee grid start
    $('#employee_grid tfoot th').each( function () {
        
       var title = $(this).text();
        removeSearch = ['Edit','Delete','Status'];
        if( removeSearch.indexOf(title) == -1 ){
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        }else{
            $(this).text("");
        }
    } );
 
    // DataTable
    var employee_grid = $('#employee_grid').DataTable({
    responsive: true,
    bSort: false
    } );
 
    // Apply the search
    employee_grid.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );    
    // employww Grid end
    
   //purpose grid start
    $('#purpose_grid tfoot th').each( function () {
          
       var title = $(this).text();
        removeSearch = ['Edit','Delete','Status'];
        if( removeSearch.indexOf(title) == -1 ){
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        }else{
            $(this).text("");
        }
    } );
 
    // DataTable
    var purpose_grid = $('#purpose_grid').DataTable({
    responsive: true,
    bSort: false
    } );
 
    // Apply the search
    purpose_grid.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );    
    // purpose Grid end
    
   //customer type grid start
    $('#customertype_grid tfoot th').each( function () {
          
       var title = $(this).text();
        removeSearch = ['Edit','Delete','Status'];
        if( removeSearch.indexOf(title) == -1 ){
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        }else{
            $(this).text("");
        }
    } );
 
    // DataTable
    var customertype_grid = $('#customertype_grid').DataTable({
    responsive: true,
    bSort: false
    } );
 
    // Apply the search
    customertype_grid.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );    
    // customer type Grid end
   
   //user grid start
    $('#user_grid tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
 
    // DataTable
    var user_grid = $('#user_grid').DataTable({
    responsive: true,
    bSort: false

    } );
 
    // Apply the search
    user_grid.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );    
    // user Grid end
   
   //brand grid start
    $('#brand_grid tfoot th').each( function () {
     
       var title = $(this).text();
        removeSearch = ['Edit','Delete','Status'];
        if( removeSearch.indexOf(title) == -1 ){
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        }else{
            $(this).text("");
        }
        
    } );
 
    // DataTable
    var brand_grid = $('#brand_grid').DataTable({
    responsive: true,
    bSort: false
    } );
 
    // Apply the search
    brand_grid.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );    
    // brand Grid end


   //region grid start
    $('#region_grid tfoot th').each( function () {
        
       var title = $(this).text();
        removeSearch = ['Edit','Delete','Status'];
        if( removeSearch.indexOf(title) == -1 ){
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        }else{
            $(this).text("");
        }
    });
 
    // DataTable
    var region_grid = $('#region_grid').DataTable({
    responsive: true,
    bSort: false
    } );
 
    // Apply the search
    region_grid.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                    that
                    .search( this.value )
                    .draw();
            }
        } );
    } );    
    // region Grid end
   


   //area grid start
    $('#area_grid tfoot th').each( function () {
        var title = $(this).text();
        areaSearchdata = ['Edit','Delete','Status'];
        if( areaSearchdata.indexOf(title) == -1 ){
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        }else{
            $(this).text("");
        }
    } );
 
    // DataTable
    var area_grid = $('#area_grid').DataTable({
    responsive: true,
    bSort: false
    } );
 
    // Apply the search
    area_grid.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );    
    // area Grid end
    
 //report_new_customer_grid grid start
    $('#report_new_customer_grid tfoot th').each( function () {
        var title = $(this).text();
        areaSearchdata = ['Edit','Delete','Status'];
        if( areaSearchdata.indexOf(title) == -1 ){
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        }else{
            $(this).text("");
        }
    } );
 
    // DataTable
    var report_new_customer_grid = $('#report_new_customer_grid').DataTable({
         responsive: true,
         bSort: false,
         defaultText:"NA"
    });
 
    // Apply the search
    report_new_customer_grid.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );    
    // report_new_customer_grid Grid end
   
    function applyToSelect(id, fields, data) {
        $(id).html("");
        if(data.length == 0){
            return; 
        }
        for( i in data){
            htm = '<option value="'+data[i][fields.value]+'">'+data[i][fields.name]+'</option>';
            $(id).append(htm);        
        }
    }
    // console.log($("#regionSelect").html() );
    $("#regionSelect").change(function () {
        var self = this;

        var v = $(self).val();
        if( region[ v ] == undefined ){
            
                $("select").attr("disabled","");
                $.get(Admin_Route+"/helper/api/v1/stateArea/"+v).done(function (d) {


                region[ v ] = d;
                console.log(d);
                applyToSelect("#stateSelect",{value:"id",name:"state_name"},d.state);
                applyToSelect("#areaSelect",{value:"id",name:"area_name"},d.area);
                
                

                //stid = $("#stateSelect").val($("#stateSelect option:first").val());
                stid = $("#stateSelect option:first").val();                
                $.get(Admin_Route+"/helper/api/v1/getcitybystate/"+stid).done(function (d) {                                        
                    applyToSelect("#citySelect",{value:"id",name:"city_name"},d.city);
                    $("select").removeAttr("disabled");
                });


                $("select").removeAttr("disabled");
            });
        } else{
                applyToSelect("#stateSelect",{value:"id",name:"state_name"},region[ v ].state);
                applyToSelect("#areaSelect",{value:"id",name:"area_name"},region[ v ].area);
                
            //apply data in boths 
        }

    });
    $("#stateSelect").change(function () {
        var self = this;

        var v = $(self).val();
        
        if( region[ v ] == undefined ){
            
                $("select").attr("disabled","");
                $.get(Admin_Route+"/helper/api/v1/getcitybystate/"+v).done(function (d) {


                region[ v ] = d;
                console.log(d);
                applyToSelect("#citySelect",{value:"id",name:"city_name"},d.city);
                //applyToSelect("#areaSelect",{value:"id",name:"area_name"},d.area);

                $("select").removeAttr("disabled");
            });
        } else{
                applyToSelect("#citySelect",{value:"id",name:"city_name"},region[ v ].city);
                //applyToSelect("#areaSelect",{value:"id",name:"area_name"},region[ v ].area);
            //apply data in boths 
        }

    });
    //$("#regionSelect").trigger('change');


    // Dcument . ready

});

function addCustomer(){
    $("#regionSelect").trigger('change');
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
function del_ref( url ){
    bootbox.confirm("Are you sure want to delete?", function(result) {
        if(result){            
            window.location = url;
        }
    })
}

function deact_cust( id ){
    $("#cust_id").val(id);
    $("#deactivate_cust_modal").modal();
}

function deact_user( id ) {    
    $("#user_id").val(id);
    $("#deactivate_user_modal").modal();
}

function deact_brand( id,url ) {
    $("#brand_deactive_id").val(id);
    $("#brand_deact_form").attr('action',url);
    $("#deactivate_brand_modal").modal();
}

function deact_region( id,url ) {
    $("#region_deactive_id").val(id);
    $("#region_deact_form").attr('action',url);
    $("#deactivate_region_modal").modal();
}

function deact_area( id,url ) {
    $("#area_deactive_id").val(id);
    $("#area_deact_form").attr('action',url);
    $("#deactivate_area_modal").modal();
}

function deact_customertype( id,url ) {
    $("#customertype_deactive_id").val(id);
    $("#customertype_deact_form").attr('action',url);
    $("#deactivate_customertype_modal").modal();
}

function deact_purpose( id,url ) {
    $("#purpose_deactive_id").val(id);
    $("#purpose_deact_form").attr('action',url);
    $("#deactivate_purpose_modal").modal();
}

$('#meeting_time').timepicker();
$('#followup_time').timepicker();


$('#purpose_meeting').change(function(){
    value = $('#purpose_meeting').val();
    if(value == 1){
       $('#div_opther_purpose').css('display','block');
    }else{
        $('#div_opther_purpose').css('display','none');
    }
});
//region grid start
    $('#employee_track_grid tfoot th').each( function () {
          var title = $(this).text();
        removeSearch = ['Edit','Delete','Status'];
        if( removeSearch.indexOf(title) == -1 ){
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        }else{
            $(this).text("");
        }
        
    } );
 
    // DataTable
    var employee_track_grid = $('#employee_track_grid').DataTable({
    responsive: true,
    bSort: false
    } );
 
    // Apply the search
    employee_track_grid.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );    
    // region Grid end    
$(document).ready(function() {    
    $('#user_create_designation').change(function(){
       
       if($('#user_create_designation').val() == 4 || $('#user_create_designation').val() == 5){
           $('#user_create_region').css('display','none');
           $('#user_create_brand').css('display','none');
       }else{
           $('#user_create_region').css('display','block');
           $('#user_create_brand').css('display','block');       
       }
    });
});


//$('td').each(function(){ 
$('td').each(function(){ 
//   if( $(this).children().length == 0 ){
//    $(this).text(capitalizeFirstLetter($(this).text()));    
//   }
//   
if( ! ($(this).find('a').length > 0 || $(this).find('button').length > 0 || $(this).find('img').length > 0 )){
    //$(this).text(capitalizeFirstLetter('dd'));    
    $(this).text(capitalizeFirstLetter($(this).text()));    
}
       


});