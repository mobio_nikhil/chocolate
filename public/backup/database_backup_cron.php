<?php
//DB_CONNECTION=mysql
//DB_HOST=127.0.0.1
//DB_DATABASE=cedarind_durain
//DB_USERNAME=cedarind_durain
//DB_PASSWORD=@Jewf3FlwzI!

$mysqlUserName = "cedarind_durain";
$mysqlPassword = "@Jewf3FlwzI!";
$mysqlHostName = "localhost";
$DbName = "cedarind_durain";
$backup_name = "mybackup.sql";
// $tables             = "Your tables";
//or add 5th parameter(array) of specific tables:    array("mytable1","mytable2","mytable3") for multiple tables

Export_Database($mysqlHostName, $mysqlUserName, $mysqlPassword, $DbName, $tables = false, $backup_name = false);

function Export_Database($host, $user, $pass, $name, $tables = false, $backup_name = false) {
    $mysqli = new mysqli($host, $user, $pass, $name);
    $mysqli->select_db($name);
    $mysqli->query("SET NAMES 'utf8'");

    $queryTables = $mysqli->query('SHOW TABLES');
    while ($row = $queryTables->fetch_row()) {
        $target_tables[] = $row[0];
    }
    if ($tables !== false) {
        $target_tables = array_intersect($target_tables, $tables);
    }
    foreach ($target_tables as $table) {
        $result = $mysqli->query('SELECT * FROM ' . $table);
        $fields_amount = $result->field_count;
        $rows_num = $mysqli->affected_rows;
        $res = $mysqli->query('SHOW CREATE TABLE ' . $table);
        $TableMLine = $res->fetch_row();
        $content = (!isset($content) ? '' : $content) . "\n\n" . $TableMLine[1] . ";\n\n";

        for ($i = 0, $st_counter = 0; $i < $fields_amount; $i++, $st_counter = 0) {
            while ($row = $result->fetch_row()) { //when started (and every after 100 command cycle):
                if ($st_counter % 100 == 0 || $st_counter == 0) {
                    $content .= "\nINSERT INTO " . $table . " VALUES";
                }
                $content .= "\n(";
                for ($j = 0; $j < $fields_amount; $j++) {
                    $row[$j] = str_replace("\n", "\\n", addslashes($row[$j]));
                    if (isset($row[$j])) {
                        $content .= '"' . $row[$j] . '"';
                    } else {
                        $content .= '""';
                    }
                    if ($j < ($fields_amount - 1)) {
                        $content.= ',';
                    }
                }
                $content .=")";
                //every after 100 command cycle [or at last line] ....p.s. but should be inserted 1 cycle eariler
                if ((($st_counter + 1) % 100 == 0 && $st_counter != 0) || $st_counter + 1 == $rows_num) {
                    $content .= ";";
                } else {
                    $content .= ",";
                }
                $st_counter = $st_counter + 1;
            }
        } $content .="\n\n\n";
    }
    $backup_name = $backup_name ? $backup_name : $name . "_" . date('d-M-Y_G-i-s_a') . ".sql";
//    $backup_name_zip = $backup_name_zip ? $backup_name_zip : $name."_".date('d-m-Y')."_".date('H-i-s').".zip";

    if (!file_exists(dirname(__FILE__) . "/web-admin/view/db_bkp/")) {
        mkdir(dirname(__FILE__) . "/web-admin/view/db_bkp/", 0777, true);
        $backup_name = dirname(__FILE__) . "/web-admin/view/db_bkp/" . $backup_name;
        $fd = fopen($backup_name, "w");
        fputs($fd, $content);
        fclose($fd);
    } else {

        $backup_name = dirname(__FILE__) . "/web-admin/view/db_bkp/" . $backup_name;
        $fd = fopen($backup_name, "w");
        fputs($fd, $content);
        fclose($fd);
    }
    
    //delete_old_backup();
    exit;
}


function delete_old_backup(){
    $get_folders = glob(dirname(__FILE__) . "/web-admin/view/db_bkp/*");
    if(count($get_folders) > 2){
        $files = array_combine($get_folders, array_map("filemtime", $get_folders));
        asort($files);
        $oldest_file = key($files);
        unlink($oldest_file);
    }
}

?>