-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2016 at 10:12 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chocolate`
--

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `area_name` varchar(20) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `region_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `delete_status` enum('0','1') NOT NULL DEFAULT '0',
  `disable_status` enum('0','1') NOT NULL DEFAULT '0',
  `disable_reason` text,
  `disable_by` int(11) DEFAULT NULL,
  `active_by` int(11) DEFAULT NULL,
  `delete_by` int(11) DEFAULT NULL,
  `delete_reason` text,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` int(11) NOT NULL,
  `brand_name` varchar(20) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `delete_status` enum('0','1') NOT NULL DEFAULT '0',
  `disable_status` enum('0','1') NOT NULL DEFAULT '0',
  `disable_reason` text,
  `disable_by` int(11) DEFAULT NULL,
  `active_by` int(11) DEFAULT NULL,
  `delete_by` int(11) DEFAULT NULL,
  `delete_reason` text,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `brand_name`, `user_id`, `delete_status`, `disable_status`, `disable_reason`, `disable_by`, `active_by`, `delete_by`, `delete_reason`, `created_on`) VALUES
(1, 'DURIAN ROMANIA', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:20:44'),
(2, 'DURIAN ITALIA', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:20:52'),
(3, 'DURIAN BRITANNIA', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:21:03'),
(4, 'DURIAN PROJECT', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:21:12'),
(5, 'DURIAN ESPANIA ', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:21:23'),
(6, 'CEDARLAM GRANDEUR', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:21:32'),
(7, 'CEDARLAM SPLENDID', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:21:42'),
(8, 'CEDARLAM TRENDY', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:21:51'),
(9, 'IPL ELEGANCE', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:22:00'),
(10, 'IPL BOUNCER', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:22:09'),
(11, 'AROMA ', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:22:17'),
(12, 'DIXON', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:22:22'),
(13, 'DURIAN FIGUREWOODZ', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:22:31'),
(14, 'DURIAN BARCELONA', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:22:39'),
(15, 'CEDARLAM CASABLANCA', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:22:53'),
(16, 'DURIAN DIGIMAXX ', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:23:51'),
(17, 'CEDARLAM DIGIVISION', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:23:59'),
(18, 'IPL DIGILAM', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:24:08'),
(19, 'DURIAN ECGL', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:24:18'),
(20, 'EDSB', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:24:33'),
(21, 'LINERS ', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:24:44'),
(22, 'DURIAN EXPORTS', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:25:06'),
(23, 'MIX BRANDS', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:37:19'),
(24, 'B GRADE', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-11-30 08:55:02');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `state_id` int(11) DEFAULT NULL,
  `city_name` varchar(255) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `state_id`, `city_name`, `created_on`) VALUES
(1, 1, 'Nicobar', NULL),
(2, 1, 'North and Middle Andaman', NULL),
(3, 1, 'South Andaman', NULL),
(4, 2, 'Anantapur', NULL),
(5, 2, 'Chittoor', NULL),
(6, 2, 'Cuddapah', NULL),
(7, 2, 'East Godavari', NULL),
(8, 2, 'Guntur', NULL),
(9, 2, 'Krishna', NULL),
(10, 2, 'Kurnool', NULL),
(11, 2, 'Nellore', NULL),
(12, 2, 'Prakasam', NULL),
(13, 2, 'Srikakulam', NULL),
(14, 2, 'Visakhapatnam', NULL),
(15, 2, 'Vizianagaram', NULL),
(16, 2, 'West Godavari', NULL),
(17, 3, 'Anjaw', NULL),
(18, 3, 'Changlang', NULL),
(19, 3, 'Dibang Valley', NULL),
(20, 3, 'East Kameng', NULL),
(21, 3, 'East Siang', NULL),
(22, 3, 'Kurung Kumey', NULL),
(23, 3, 'Lohit', NULL),
(24, 3, 'Longding', NULL),
(25, 3, 'Lower Dibang Valley', NULL),
(26, 3, 'Lower Subansiri', NULL),
(27, 3, 'Papum Pare', NULL),
(28, 3, 'Tawang', NULL),
(29, 3, 'Tirap', NULL),
(30, 3, 'Upper Siang', NULL),
(31, 3, 'Upper Subansiri', NULL),
(32, 3, 'West Kameng', NULL),
(33, 3, 'West Siang', NULL),
(34, 4, 'Baksa', NULL),
(35, 4, 'Barpeta', NULL),
(36, 4, 'Bongaigaon', NULL),
(37, 4, 'Cachar', NULL),
(38, 4, 'Chirang', NULL),
(39, 4, 'Darrang', NULL),
(40, 4, 'Dhemaji', NULL),
(41, 4, 'Dhubri', NULL),
(42, 4, 'Dibrugarh', NULL),
(43, 4, 'Dima Hasao', NULL),
(44, 4, 'Goalpara', NULL),
(45, 4, 'Golaghat', NULL),
(46, 4, 'Hailakandi', NULL),
(47, 4, 'Jorhat', NULL),
(48, 4, 'Kamrup Metropolitan', NULL),
(49, 4, 'Kamrup', NULL),
(50, 4, 'Karbi Anglong', NULL),
(51, 4, 'Karimganj', NULL),
(52, 4, 'Kokrajhar', NULL),
(53, 4, 'Lakhimpur', NULL),
(54, 4, 'Morigaon', NULL),
(55, 4, 'Nagaon', NULL),
(56, 4, 'Nalbari', NULL),
(57, 4, 'Sivasagar', NULL),
(58, 4, 'Sonitpur', NULL),
(59, 4, 'Tinsukia', NULL),
(60, 4, 'Udalguri', NULL),
(61, 5, 'Araria', NULL),
(62, 5, 'Arwal', NULL),
(63, 5, 'Aurangabad', NULL),
(64, 5, 'Banka', NULL),
(65, 5, 'Begusarai', NULL),
(66, 5, 'Bhagalpur', NULL),
(67, 5, 'Bhojpur', NULL),
(68, 5, 'Buxar', NULL),
(69, 5, 'Darbhanga', NULL),
(70, 5, 'East Champaran (Motihari)', NULL),
(71, 5, 'Gaya', NULL),
(72, 5, 'Gopalganj', NULL),
(73, 5, 'Jamui', NULL),
(74, 5, 'Jehanabad', NULL),
(75, 5, 'Kaimur (Bhabua)', NULL),
(76, 5, 'Katihar', NULL),
(77, 5, 'Khagaria', NULL),
(78, 5, 'Kishanganj', NULL),
(79, 5, 'Lakhisarai', NULL),
(80, 5, 'Madhepura', NULL),
(81, 5, 'Madhubani', NULL),
(82, 5, 'Munger (Monghyr)', NULL),
(83, 5, 'Muzaffarpur', NULL),
(84, 5, 'Nalanda', NULL),
(85, 5, 'Nawada', NULL),
(86, 5, 'Patna', NULL),
(87, 5, 'Purnia (Purnea)', NULL),
(88, 5, 'Rohtas', NULL),
(89, 5, 'Saharsa', NULL),
(90, 5, 'Samastipur', NULL),
(91, 5, 'Saran', NULL),
(92, 5, 'Sheikhpura', NULL),
(93, 5, 'Sheohar', NULL),
(94, 5, 'Sitamarhi', NULL),
(95, 5, 'Siwan', NULL),
(96, 5, 'Supaul', NULL),
(97, 5, 'Vaishali', NULL),
(98, 5, 'West Champaran', NULL),
(99, 7, 'Chandigarh', NULL),
(100, 6, 'Balod', NULL),
(101, 6, 'Baloda Bazar', NULL),
(102, 6, 'Balrampur', NULL),
(103, 6, 'Bastar', NULL),
(104, 6, 'Bemetara', NULL),
(105, 6, 'Bijapur', NULL),
(106, 6, 'Bilaspur', NULL),
(107, 6, 'Dantewada (South Bastar)', NULL),
(108, 6, 'Dhamtari', NULL),
(109, 6, 'Durg', NULL),
(110, 6, 'Gariaband', NULL),
(111, 6, 'Janjgir-Champa', NULL),
(112, 6, 'Jashpur', NULL),
(113, 6, 'Kabirdham (Kawardha)', NULL),
(114, 6, 'Kanker (North Bastar)', NULL),
(115, 6, 'Kondagaon', NULL),
(116, 6, 'Korba', NULL),
(117, 6, 'Korea (Koriya)', NULL),
(118, 6, 'Mahasamund', NULL),
(119, 6, 'Mungeli', NULL),
(120, 6, 'Narayanpur', NULL),
(121, 6, 'Raigarh', NULL),
(122, 6, 'Raipur', NULL),
(123, 6, 'Rajnandgaon', NULL),
(124, 6, 'Sukma', NULL),
(125, 6, 'Surajpur', NULL),
(126, 6, 'Surguja', NULL),
(127, 10, 'Dadra & Nagar Haveli', NULL),
(128, 8, 'Daman', NULL),
(129, 8, 'Diu', NULL),
(130, 9, 'Central Delhi', NULL),
(131, 9, 'East Delhi', NULL),
(132, 9, 'New Delhi', NULL),
(133, 9, 'North Delhi', NULL),
(134, 9, 'North East Delhi', NULL),
(135, 9, 'North West Delhi', NULL),
(136, 9, 'South Delhi', NULL),
(137, 9, 'South West Delhi', NULL),
(138, 9, 'West Delhi', NULL),
(139, 11, 'North Goa', NULL),
(140, 11, 'South Goa', NULL),
(141, 12, 'Ahmedabad', NULL),
(142, 12, 'Amreli', NULL),
(143, 12, 'Anand', NULL),
(144, 12, 'Aravalli', NULL),
(145, 12, 'Banaskantha (Palanpur)', NULL),
(146, 12, 'Bharuch', NULL),
(147, 12, 'Bhavnagar', NULL),
(148, 12, 'Botad', NULL),
(149, 12, 'Chhota Udepur', NULL),
(150, 12, 'Dahod', NULL),
(151, 12, 'Dangs (Ahwa)', NULL),
(152, 12, 'Devbhoomi Dwarka', NULL),
(153, 12, 'Gandhinagar', NULL),
(154, 12, 'Gir Somnath', NULL),
(155, 12, 'Jamnagar', NULL),
(156, 12, 'Junagadh', NULL),
(157, 12, 'Kachchh', NULL),
(158, 12, 'Kheda (Nadiad)', NULL),
(159, 12, 'Mahisagar', NULL),
(160, 12, 'Mehsana', NULL),
(161, 12, 'Morbi', NULL),
(162, 12, 'Narmada (Rajpipla)', NULL),
(163, 12, 'Navsari', NULL),
(164, 12, 'Panchmahal (Godhra)', NULL),
(165, 12, 'Patan', NULL),
(166, 12, 'Porbandar', NULL),
(167, 12, 'Rajkot', NULL),
(168, 12, 'Sabarkantha (Himmatnagar)', NULL),
(169, 12, 'Surat', NULL),
(170, 12, 'Surendranagar', NULL),
(171, 12, 'Tapi (Vyara)', NULL),
(172, 12, 'Vadodara', NULL),
(173, 12, 'Valsad', NULL),
(174, 14, 'Ambala', NULL),
(175, 14, 'Bhiwani', NULL),
(176, 14, 'Faridabad', NULL),
(177, 14, 'Fatehabad', NULL),
(178, 14, 'Gurgaon', NULL),
(179, 14, 'Hisar', NULL),
(180, 14, 'Jhajjar', NULL),
(181, 14, 'Jind', NULL),
(182, 14, 'Kaithal', NULL),
(183, 14, 'Karnal', NULL),
(184, 14, 'Kurukshetra', NULL),
(185, 14, 'Mahendragarh', NULL),
(186, 14, 'Mewat', NULL),
(187, 14, 'Palwal', NULL),
(188, 14, 'Panchkula', NULL),
(189, 14, 'Panipat', NULL),
(190, 14, 'Rewari', NULL),
(191, 14, 'Rohtak', NULL),
(192, 14, 'Sirsa', NULL),
(193, 14, 'Sonipat', NULL),
(194, 14, 'Yamunanagar', NULL),
(195, 13, 'Bilaspur', NULL),
(196, 13, 'Chamba', NULL),
(197, 13, 'Hamirpur', NULL),
(198, 13, 'Kangra', NULL),
(199, 13, 'Kinnaur', NULL),
(200, 13, 'Kullu', NULL),
(201, 13, 'Lahaul & Spiti', NULL),
(202, 13, 'Mandi', NULL),
(203, 13, 'Shimla', NULL),
(204, 13, 'Sirmaur (Sirmour)', NULL),
(205, 13, 'Solan', NULL),
(206, 13, 'Una', NULL),
(207, 15, 'Anantnag', NULL),
(208, 15, 'Bandipora', NULL),
(209, 15, 'Baramulla', NULL),
(210, 15, 'Budgam', NULL),
(211, 15, 'Doda', NULL),
(212, 15, 'Ganderbal', NULL),
(213, 15, 'Jammu', NULL),
(214, 15, 'Kargil', NULL),
(215, 15, 'Kathua', NULL),
(216, 15, 'Kishtwar', NULL),
(217, 15, 'Kulgam', NULL),
(218, 15, 'Kupwara', NULL),
(219, 15, 'Leh', NULL),
(220, 15, 'Poonch', NULL),
(221, 15, 'Pulwama', NULL),
(222, 15, 'Rajouri', NULL),
(223, 15, 'Ramban', NULL),
(224, 15, 'Reasi', NULL),
(225, 15, 'Samba', NULL),
(226, 15, 'Shopian', NULL),
(227, 15, 'Srinagar', NULL),
(228, 15, 'Udhampur', NULL),
(229, 16, 'Bokaro', NULL),
(230, 16, 'Chatra', NULL),
(231, 16, 'Deoghar', NULL),
(232, 16, 'Dhanbad', NULL),
(233, 16, 'Dumka', NULL),
(234, 16, 'East Singhbhum', NULL),
(235, 16, 'Garhwa', NULL),
(236, 16, 'Giridih', NULL),
(237, 16, 'Godda', NULL),
(238, 16, 'Gumla', NULL),
(239, 16, 'Hazaribag', NULL),
(240, 16, 'Jamtara', NULL),
(241, 16, 'Khunti', NULL),
(242, 16, 'Koderma', NULL),
(243, 16, 'Latehar', NULL),
(244, 16, 'Lohardaga', NULL),
(245, 16, 'Pakur', NULL),
(246, 16, 'Palamu', NULL),
(247, 16, 'Ramgarh', NULL),
(248, 16, 'Ranchi', NULL),
(249, 16, 'Sahibganj', NULL),
(250, 16, 'Seraikela-Kharsawan', NULL),
(251, 16, 'Simdega', NULL),
(252, 16, 'West Singhbhum', NULL),
(253, 18, 'Bagalkot', NULL),
(254, 18, 'Bangalore Rural', NULL),
(255, 18, 'Bangalore Urban', NULL),
(256, 18, 'Belgaum', NULL),
(257, 18, 'Bellary', NULL),
(258, 18, 'Bidar', NULL),
(259, 18, 'Bijapur', NULL),
(260, 18, 'Chamarajanagar', NULL),
(261, 18, 'Chickmagalur', NULL),
(262, 18, 'Chikballapur', NULL),
(263, 18, 'Chitradurga', NULL),
(264, 18, 'Dakshina Kannada', NULL),
(265, 18, 'Davangere', NULL),
(266, 18, 'Dharwad', NULL),
(267, 18, 'Gadag', NULL),
(268, 18, 'Gulbarga', NULL),
(269, 18, 'Hassan', NULL),
(270, 18, 'Haveri', NULL),
(271, 18, 'Kodagu', NULL),
(272, 18, 'Kolar', NULL),
(273, 18, 'Koppal', NULL),
(274, 18, 'Mandya', NULL),
(275, 18, 'Mysore', NULL),
(276, 18, 'Raichur', NULL),
(277, 18, 'Ramnagara', NULL),
(278, 18, 'Shimoga', NULL),
(279, 18, 'Tumkur', NULL),
(280, 18, 'Udupi', NULL),
(281, 18, 'Uttara Kannada (Karwar)', NULL),
(282, 18, 'Yadgir', NULL),
(283, 17, 'Alappuzha', NULL),
(284, 17, 'Ernakulam', NULL),
(285, 17, 'Idukki', NULL),
(286, 17, 'Kannur', NULL),
(287, 17, 'Kasaragod', NULL),
(288, 17, 'Kollam', NULL),
(289, 17, 'Kottayam', NULL),
(290, 17, 'Kozhikode', NULL),
(291, 17, 'Malappuram', NULL),
(292, 17, 'Palakkad', NULL),
(293, 17, 'Pathanamthitta', NULL),
(294, 17, 'Thiruvananthapuram', NULL),
(295, 17, 'Thrissur', NULL),
(296, 17, 'Wayanad', NULL),
(297, 19, 'Lakshadweep', NULL),
(298, 20, 'Alirajpur', NULL),
(299, 20, 'Anuppur', NULL),
(300, 20, 'Ashoknagar', NULL),
(301, 20, 'Balaghat', NULL),
(302, 20, 'Barwani', NULL),
(303, 20, 'Betul', NULL),
(304, 20, 'Bhind', NULL),
(305, 20, 'Bhopal', NULL),
(306, 20, 'Burhanpur', NULL),
(307, 20, 'Chhatarpur', NULL),
(308, 20, 'Chhindwara', NULL),
(309, 20, 'Damoh', NULL),
(310, 20, 'Datia', NULL),
(311, 20, 'Dewas', NULL),
(312, 20, 'Dhar', NULL),
(313, 20, 'Dindori', NULL),
(314, 20, 'Guna', NULL),
(315, 20, 'Gwalior', NULL),
(316, 20, 'Harda', NULL),
(317, 20, 'Hoshangabad', NULL),
(318, 20, 'Indore', NULL),
(319, 20, 'Jabalpur', NULL),
(320, 20, 'Jhabua', NULL),
(321, 20, 'Katni', NULL),
(322, 20, 'Khandwa', NULL),
(323, 20, 'Khargone', NULL),
(324, 20, 'Mandla', NULL),
(325, 20, 'Mandsaur', NULL),
(326, 20, 'Morena', NULL),
(327, 20, 'Narsinghpur', NULL),
(328, 20, 'Neemuch', NULL),
(329, 20, 'Panna', NULL),
(330, 20, 'Raisen', NULL),
(331, 20, 'Rajgarh', NULL),
(332, 20, 'Ratlam', NULL),
(333, 20, 'Rewa', NULL),
(334, 20, 'Sagar', NULL),
(335, 20, 'Satna', NULL),
(336, 20, 'Sehore', NULL),
(337, 20, 'Seoni', NULL),
(338, 20, 'Shahdol', NULL),
(339, 20, 'Shajapur', NULL),
(340, 20, 'Sheopur', NULL),
(341, 20, 'Shivpuri', NULL),
(342, 20, 'Sidhi', NULL),
(343, 20, 'Singrauli', NULL),
(344, 20, 'Tikamgarh', NULL),
(345, 20, 'Ujjain', NULL),
(346, 20, 'Umaria', NULL),
(347, 20, 'Vidisha', NULL),
(348, 21, 'Ahmednagar', NULL),
(349, 21, 'Akola', NULL),
(350, 21, 'Amravati', NULL),
(351, 21, 'Aurangabad', NULL),
(352, 21, 'Beed', NULL),
(353, 21, 'Bhandara', NULL),
(354, 21, 'Buldhana', NULL),
(355, 21, 'Chandrapur', NULL),
(356, 21, 'Dhule', NULL),
(357, 21, 'Gadchiroli', NULL),
(358, 21, 'Gondia', NULL),
(359, 21, 'Hingoli', NULL),
(360, 21, 'Jalgaon', NULL),
(361, 21, 'Jalna', NULL),
(362, 21, 'Kolhapur', NULL),
(363, 21, 'Latur', NULL),
(364, 21, 'Mumbai City', NULL),
(365, 21, 'Mumbai Suburban', NULL),
(366, 21, 'Nagpur', NULL),
(367, 21, 'Nanded', NULL),
(368, 21, 'Nandurbar', NULL),
(369, 21, 'Nashik', NULL),
(370, 21, 'Osmanabad', NULL),
(371, 21, 'Parbhani', NULL),
(372, 21, 'Pune', NULL),
(373, 21, 'Raigad', NULL),
(374, 21, 'Ratnagiri', NULL),
(375, 21, 'Sangli', NULL),
(376, 21, 'Satara', NULL),
(377, 21, 'Sindhudurg', NULL),
(378, 21, 'Solapur', NULL),
(379, 21, 'Thane', NULL),
(380, 21, 'Wardha', NULL),
(381, 21, 'Washim', NULL),
(382, 21, 'Yavatmal', NULL),
(383, 22, 'Bishnupur', NULL),
(384, 22, 'Chandel', NULL),
(385, 22, 'Churachandpur', NULL),
(386, 22, 'Imphal East', NULL),
(387, 22, 'Imphal West', NULL),
(388, 22, 'Senapati', NULL),
(389, 22, 'Tamenglong', NULL),
(390, 22, 'Thoubal', NULL),
(391, 22, 'Ukhrul', NULL),
(392, 23, 'East Garo Hills', NULL),
(393, 23, 'East Jaintia Hills', NULL),
(394, 23, 'East Khasi Hills', NULL),
(395, 23, 'North Garo Hills', NULL),
(396, 23, 'Ri Bhoi', NULL),
(397, 23, 'South Garo Hills', NULL),
(398, 23, 'South West Garo Hills', NULL),
(399, 23, 'South West Khasi Hills', NULL),
(400, 23, 'West Garo Hills', NULL),
(401, 23, 'West Jaintia Hills', NULL),
(402, 23, 'West Khasi Hills', NULL),
(403, 24, 'Aizawl', NULL),
(404, 24, 'Champhai', NULL),
(405, 24, 'Kolasib', NULL),
(406, 24, 'Lawngtlai', NULL),
(407, 24, 'Lunglei', NULL),
(408, 24, 'Mamit', NULL),
(409, 24, 'Saiha', NULL),
(410, 24, 'Serchhip', NULL),
(411, 25, 'Dimapur', NULL),
(412, 25, 'Kiphire', NULL),
(413, 25, 'Kohima', NULL),
(414, 25, 'Longleng', NULL),
(415, 25, 'Mokokchung', NULL),
(416, 25, 'Mon', NULL),
(417, 25, 'Peren', NULL),
(418, 25, 'Phek', NULL),
(419, 25, 'Tuensang', NULL),
(420, 25, 'Wokha', NULL),
(421, 25, 'Zunheboto', NULL),
(422, 26, 'Angul', NULL),
(423, 26, 'Balangir', NULL),
(424, 26, 'Balasore', NULL),
(425, 26, 'Bargarh', NULL),
(426, 26, 'Bhadrak', NULL),
(427, 26, 'Boudh', NULL),
(428, 26, 'Cuttack', NULL),
(429, 26, 'Deogarh', NULL),
(430, 26, 'Dhenkanal', NULL),
(431, 26, 'Gajapati', NULL),
(432, 26, 'Ganjam', NULL),
(433, 26, 'Jagatsinghapur', NULL),
(434, 26, 'Jajpur', NULL),
(435, 26, 'Jharsuguda', NULL),
(436, 26, 'Kalahandi', NULL),
(437, 26, 'Kandhamal', NULL),
(438, 26, 'Kendrapara', NULL),
(439, 26, 'Kendujhar (Keonjhar)', NULL),
(440, 26, 'Khordha', NULL),
(441, 26, 'Koraput', NULL),
(442, 26, 'Malkangiri', NULL),
(443, 26, 'Mayurbhanj', NULL),
(444, 26, 'Nabarangpur', NULL),
(445, 26, 'Nayagarh', NULL),
(446, 26, 'Nuapada', NULL),
(447, 26, 'Puri', NULL),
(448, 26, 'Rayagada', NULL),
(449, 26, 'Sambalpur', NULL),
(450, 26, 'Sonepur', NULL),
(451, 26, 'Sundargarh', NULL),
(452, 27, 'Karaikal', NULL),
(453, 27, 'Mahe', NULL),
(454, 27, 'Pondicherry', NULL),
(455, 27, 'Yanam', NULL),
(456, 28, 'Amritsar', NULL),
(457, 28, 'Barnala', NULL),
(458, 28, 'Bathinda', NULL),
(459, 28, 'Faridkot', NULL),
(460, 28, 'Fatehgarh Sahib', NULL),
(461, 28, 'Fazilka', NULL),
(462, 28, 'Ferozepur', NULL),
(463, 28, 'Gurdaspur', NULL),
(464, 28, 'Hoshiarpur', NULL),
(465, 28, 'Jalandhar', NULL),
(466, 28, 'Kapurthala', NULL),
(467, 28, 'Ludhiana', NULL),
(468, 28, 'Mansa', NULL),
(469, 28, 'Moga', NULL),
(470, 28, 'Muktsar', NULL),
(471, 28, 'Nawanshahr', NULL),
(472, 28, 'Pathankot', NULL),
(473, 28, 'Patiala', NULL),
(474, 28, 'Rupnagar', NULL),
(475, 28, 'Sangrur', NULL),
(476, 28, 'SAS Nagar (Mohali)', NULL),
(477, 28, 'Tarn Taran', NULL),
(478, 29, 'Ajmer', NULL),
(479, 29, 'Alwar', NULL),
(480, 29, 'Banswara', NULL),
(481, 29, 'Baran', NULL),
(482, 29, 'Barmer', NULL),
(483, 29, 'Bharatpur', NULL),
(484, 29, 'Bhilwara', NULL),
(485, 29, 'Bikaner', NULL),
(486, 29, 'Bundi', NULL),
(487, 29, 'Chittorgarh', NULL),
(488, 29, 'Churu', NULL),
(489, 29, 'Dausa', NULL),
(490, 29, 'Dholpur', NULL),
(491, 29, 'Dungarpur', NULL),
(492, 29, 'Hanumangarh', NULL),
(493, 29, 'Jaipur', NULL),
(494, 29, 'Jaisalmer', NULL),
(495, 29, 'Jalore', NULL),
(496, 29, 'Jhalawar', NULL),
(497, 29, 'Jhunjhunu', NULL),
(498, 29, 'Jodhpur', NULL),
(499, 29, 'Karauli', NULL),
(500, 29, 'Kota', NULL),
(501, 29, 'Nagaur', NULL),
(502, 29, 'Pali', NULL),
(503, 29, 'Pratapgarh', NULL),
(504, 29, 'Rajsamand', NULL),
(505, 29, 'Sawai Madhopur', NULL),
(506, 29, 'Sikar', NULL),
(507, 29, 'Sirohi', NULL),
(508, 29, 'Sri Ganganagar', NULL),
(509, 29, 'Tonk', NULL),
(510, 29, 'Udaipur', NULL),
(511, 30, 'East Sikkim', NULL),
(512, 30, 'North Sikkim', NULL),
(513, 30, 'South Sikkim', NULL),
(514, 30, 'West Sikkim', NULL),
(515, 31, 'Ariyalur', NULL),
(516, 31, 'Chennai', NULL),
(517, 31, 'Coimbatore', NULL),
(518, 31, 'Cuddalore', NULL),
(519, 31, 'Dharmapuri', NULL),
(520, 31, 'Dindigul', NULL),
(521, 31, 'Erode', NULL),
(522, 31, 'Kanchipuram', NULL),
(523, 31, 'Kanyakumari', NULL),
(524, 31, 'Karur', NULL),
(525, 31, 'Krishnagiri', NULL),
(526, 31, 'Madurai', NULL),
(527, 31, 'Nagapattinam', NULL),
(528, 31, 'Namakkal', NULL),
(529, 31, 'Nilgiris', NULL),
(530, 31, 'Perambalur', NULL),
(531, 31, 'Pudukkottai', NULL),
(532, 31, 'Ramanathapuram', NULL),
(533, 31, 'Salem', NULL),
(534, 31, 'Sivaganga', NULL),
(535, 31, 'Thanjavur', NULL),
(536, 31, 'Theni', NULL),
(537, 31, 'Thoothukudi (Tuticorin)', NULL),
(538, 31, 'Tiruchirappalli', NULL),
(539, 31, 'Tirunelveli', NULL),
(540, 31, 'Tiruppur', NULL),
(541, 31, 'Tiruvallur', NULL),
(542, 31, 'Tiruvannamalai', NULL),
(543, 31, 'Tiruvarur', NULL),
(544, 31, 'Vellore', NULL),
(545, 31, 'Viluppuram', NULL),
(546, 31, 'Virudhunagar', NULL),
(547, 32, 'Adilabad', NULL),
(548, 32, 'Hyderabad', NULL),
(549, 32, 'Karimnagar', NULL),
(550, 32, 'Khammam', NULL),
(551, 32, 'Mahabubnagar', NULL),
(552, 32, 'Medak', NULL),
(553, 32, 'Nalgonda', NULL),
(554, 32, 'Nizamabad', NULL),
(555, 32, 'Rangareddy', NULL),
(556, 32, 'Warangal', NULL),
(557, 33, 'Dhalai', NULL),
(558, 33, 'Gomati', NULL),
(559, 33, 'Khowai', NULL),
(560, 33, 'North Tripura', NULL),
(561, 33, 'Sepahijala', NULL),
(562, 33, 'South Tripura', NULL),
(563, 33, 'Unakoti', NULL),
(564, 33, 'West Tripura', NULL),
(565, 34, 'Agra', NULL),
(566, 34, 'Aligarh', NULL),
(567, 34, 'Allahabad', NULL),
(568, 34, 'Ambedkar Nagar', NULL),
(569, 34, 'Auraiya', NULL),
(570, 34, 'Azamgarh', NULL),
(571, 34, 'Baghpat', NULL),
(572, 34, 'Bahraich', NULL),
(573, 34, 'Ballia', NULL),
(574, 34, 'Balrampur', NULL),
(575, 34, 'Banda', NULL),
(576, 34, 'Barabanki', NULL),
(577, 34, 'Bareilly', NULL),
(578, 34, 'Basti', NULL),
(579, 34, 'Sambhal (Bhim Nagar)', NULL),
(580, 34, 'Bijnor', NULL),
(581, 34, 'Budaun', NULL),
(582, 34, 'Bulandshahr', NULL),
(583, 34, 'Chandauli', NULL),
(584, 34, 'Amethi (Chatrapati Sahuji Mahraj Nagar)', NULL),
(585, 34, 'Chitrakoot', NULL),
(586, 34, 'Deoria', NULL),
(587, 34, 'Etah', NULL),
(588, 34, 'Etawah', NULL),
(589, 34, 'Faizabad', NULL),
(590, 34, 'Farrukhabad', NULL),
(591, 34, 'Fatehpur', NULL),
(592, 34, 'Firozabad', NULL),
(593, 34, 'Gautam Buddha Nagar', NULL),
(594, 34, 'Ghaziabad', NULL),
(595, 34, 'Ghazipur', NULL),
(596, 34, 'Gonda', NULL),
(597, 34, 'Gorakhpur', NULL),
(598, 34, 'Hamirpur', NULL),
(599, 34, 'Hardoi', NULL),
(600, 34, 'Hathras', NULL),
(601, 34, 'Jalaun', NULL),
(602, 34, 'Jaunpur', NULL),
(603, 34, 'Jhansi', NULL),
(604, 34, 'Amroha (J.P. Nagar)', NULL),
(605, 34, 'Kannauj', NULL),
(606, 34, 'Kanpur Dehat', NULL),
(607, 34, 'Kanpur Nagar', NULL),
(608, 34, 'Kanshiram Nagar (Kasganj)', NULL),
(609, 34, 'Kaushambi', NULL),
(610, 34, 'Kushinagar (Padrauna)', NULL),
(611, 34, 'Lakhimpur - Kheri', NULL),
(612, 34, 'Lalitpur', NULL),
(613, 34, 'Lucknow', NULL),
(614, 34, 'Maharajganj', NULL),
(615, 34, 'Mahoba', NULL),
(616, 34, 'Mainpuri', NULL),
(617, 34, 'Mathura', NULL),
(618, 34, 'Mau', NULL),
(619, 34, 'Meerut', NULL),
(620, 34, 'Mirzapur', NULL),
(621, 34, 'Moradabad', NULL),
(622, 34, 'Muzaffarnagar', NULL),
(623, 34, 'Hapur (Panchsheel Nagar)', NULL),
(624, 34, 'Pilibhit', NULL),
(625, 34, 'Shamali (Prabuddh Nagar)', NULL),
(626, 34, 'Pratapgarh', NULL),
(627, 34, 'RaeBareli', NULL),
(628, 34, 'Rampur', NULL),
(629, 34, 'Saharanpur', NULL),
(630, 34, 'Sant Kabir Nagar', NULL),
(631, 34, 'Sant Ravidas Nagar', NULL),
(632, 34, 'Shahjahanpur', NULL),
(633, 34, 'Shravasti', NULL),
(634, 34, 'Siddharth Nagar', NULL),
(635, 34, 'Sitapur', NULL),
(636, 34, 'Sonbhadra', NULL),
(637, 34, 'Sultanpur', NULL),
(638, 34, 'Unnao', NULL),
(639, 34, 'Varanasi', NULL),
(640, 35, 'Almora', NULL),
(641, 35, 'Bageshwar', NULL),
(642, 35, 'Chamoli', NULL),
(643, 35, 'Champawat', NULL),
(644, 35, 'Dehradun', NULL),
(645, 35, 'Haridwar', NULL),
(646, 35, 'Nainital', NULL),
(647, 35, 'Pauri Garhwal', NULL),
(648, 35, 'Pithoragarh', NULL),
(649, 35, 'Rudraprayag', NULL),
(650, 35, 'Tehri Garhwal', NULL),
(651, 35, 'Udham Singh Nagar', NULL),
(652, 35, 'Uttarkashi', NULL),
(653, 36, 'Bankura', NULL),
(654, 36, 'Birbhum', NULL),
(655, 36, 'Burdwan (Bardhaman)', NULL),
(656, 36, 'Cooch Behar', NULL),
(657, 36, 'Dakshin Dinajpur (South Dinajpur)', NULL),
(658, 36, 'Darjeeling', NULL),
(659, 36, 'Hooghly', NULL),
(660, 36, 'Howrah', NULL),
(661, 36, 'Jalpaiguri', NULL),
(662, 36, 'Kolkata', NULL),
(663, 36, 'Malda', NULL),
(664, 36, 'Murshidabad', NULL),
(665, 36, 'Nadia', NULL),
(666, 36, 'North 24 Parganas', NULL),
(667, 36, 'Paschim Medinipur (West Medinipur)', NULL),
(668, 36, 'Purba Medinipur (East Medinipur)', NULL),
(669, 36, 'Purulia', NULL),
(670, 36, 'South 24 Parganas', NULL),
(671, 36, 'Uttar Dinajpur (North Dinajpur)', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `customer_code` int(11) DEFAULT NULL,
  `customer_name` varchar(50) DEFAULT NULL,
  `business_name` varchar(50) DEFAULT NULL,
  `business_address` text,
  `primary_contact_name` varchar(255) DEFAULT NULL,
  `primary_image` text,
  `secondary_contact_name` varchar(30) DEFAULT NULL,
  `customer_email` varchar(255) DEFAULT NULL,
  `customer_phone` varchar(30) DEFAULT NULL,
  `customer_type_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `visit_card_image_1` text,
  `visit_card_image_2` text,
  `visit_card_image_3` text,
  `visit_card_image_4` text,
  `visit_card_image_5` text,
  `customer_birth_date` varchar(20) DEFAULT NULL,
  `customer_anniversary_date` varchar(20) DEFAULT NULL,
  `information_enter_by` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `pincode` varchar(6) DEFAULT NULL,
  `delete_status` enum('0','1') NOT NULL DEFAULT '0',
  `disable_status` enum('0','1') NOT NULL DEFAULT '0',
  `disable_reason` text,
  `disable_by` int(11) DEFAULT NULL,
  `active_by` int(11) DEFAULT NULL,
  `delete_by` int(11) DEFAULT NULL,
  `delete_reason` text,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer_type`
--

CREATE TABLE `customer_type` (
  `id` int(11) NOT NULL,
  `customertype_name` varchar(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `delete_status` enum('0','1') NOT NULL DEFAULT '0',
  `disable_status` enum('0','1') NOT NULL DEFAULT '0',
  `disable_reason` text,
  `disable_by` int(11) DEFAULT NULL,
  `active_by` int(11) DEFAULT NULL,
  `delete_by` int(11) DEFAULT NULL,
  `delete_reason` text,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_type`
--

INSERT INTO `customer_type` (`id`, `customertype_name`, `user_id`, `delete_status`, `disable_status`, `disable_reason`, `disable_by`, `active_by`, `delete_by`, `delete_reason`, `created_on`) VALUES
(1, 'DISTRIBUTOR', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:33:31'),
(2, 'NEW DISTRIBUTOR', 93, '1', '1', NULL, NULL, NULL, 93, 'WRONG ENTRY', '2016-10-26 13:33:41'),
(3, 'DEALER', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:33:51'),
(4, 'NEW DEALER', 93, '1', '1', NULL, NULL, NULL, 93, 'WRONG ENTRY', '2016-10-26 13:34:01'),
(5, 'OEM', 93, '1', '1', NULL, NULL, NULL, 93, 'WE', '2016-10-26 13:34:10'),
(6, 'OEM', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:34:17'),
(7, 'A&ID', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:34:36'),
(8, 'FACADE FABRICATOR', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:34:45'),
(9, 'CONTRACTOR ', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:35:02'),
(10, 'CARPENTER', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:35:17'),
(11, 'DISPLAY CENTRE', 93, '1', '1', NULL, NULL, NULL, 93, 'na', '2016-11-16 08:07:48');

-- --------------------------------------------------------

--
-- Table structure for table `folloup_meeting_customer`
--

CREATE TABLE `folloup_meeting_customer` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `followup_date_time` datetime DEFAULT NULL,
  `meeting_id` int(11) DEFAULT NULL,
  `extra_note` text,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `global_setting`
--

CREATE TABLE `global_setting` (
  `id` int(11) NOT NULL,
  `setting_key` varchar(50) DEFAULT NULL,
  `setting_value` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `global_setting`
--

INSERT INTO `global_setting` (`id`, `setting_key`, `setting_value`) VALUES
(1, 'last_gps_duration_gap', '3');

-- --------------------------------------------------------

--
-- Table structure for table `gps_data`
--

CREATE TABLE `gps_data` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `gps_data_type` enum('0','1','2') DEFAULT NULL COMMENT '0-start day,1-log meeting,2-end day',
  `meeting_id` int(11) DEFAULT NULL,
  `cron_job_end_day` enum('0','1') DEFAULT NULL COMMENT '1 - cron job added entry',
  `latitude` varchar(50) DEFAULT NULL,
  `longitude` varchar(50) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `meeting_customer`
--

CREATE TABLE `meeting_customer` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer_name` varchar(50) DEFAULT NULL,
  `brand_id` text,
  `purpose_id` int(11) DEFAULT NULL,
  `extra_note` text,
  `meeting_other_purpose` text,
  `meeting_image_5` text,
  `meeting_image_4` text,
  `meeting_image_3` text,
  `meeting_image_2` text,
  `meeting_image_1` text,
  `region_sales_manager_id` int(11) DEFAULT NULL,
  `area_sales_manager_id` int(11) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `meeting_purpose`
--

CREATE TABLE `meeting_purpose` (
  `id` int(11) NOT NULL,
  `purpose_name` varchar(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `delete_status` enum('0','1') NOT NULL DEFAULT '0',
  `disable_status` enum('0','1') NOT NULL DEFAULT '0',
  `disable_reason` text,
  `disable_by` int(11) DEFAULT NULL,
  `active_by` int(11) DEFAULT NULL,
  `delete_by` int(11) DEFAULT NULL,
  `delete_reason` text,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meeting_purpose`
--

INSERT INTO `meeting_purpose` (`id`, `purpose_name`, `user_id`, `delete_status`, `disable_status`, `disable_reason`, `disable_by`, `active_by`, `delete_by`, `delete_reason`, `created_on`) VALUES
(1, 'Other', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:12:15'),
(2, 'NEW DEALER APPOINTME', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:12:36'),
(3, 'DISTRIBUTOR FOLLOW U', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:13:12'),
(4, 'DEALER FOLLOW UP', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:13:23'),
(5, 'NEW DISTRIBUTOR APPO', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:15:19'),
(6, 'NEW OEM MEETING', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:15:38'),
(7, 'OEM FOLLOW UP', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:15:51'),
(8, 'A&ID MEETING', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:16:39'),
(9, 'FOLLOW UP', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:16:51'),
(10, 'SITE VISIT FOR NEW P', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:18:49'),
(11, 'PRODUCT COMPLAINT VI', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:19:54'),
(12, ' POP ', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:20:19'),
(13, 'test', 103, '1', '1', NULL, NULL, NULL, 103, 'test delete', '2016-10-29 06:25:14'),
(14, 'NEW DISTRIBUTOR APPO', 93, '1', '1', NULL, NULL, NULL, 93, 'DUPLICATE TASK', '2016-10-26 13:12:15'),
(15, 'SALES MEETING AT HO', 93, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-11-16 08:13:13');

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

CREATE TABLE `region` (
  `id` int(11) NOT NULL,
  `region_name` varchar(20) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `delete_status` enum('0','1') NOT NULL DEFAULT '0',
  `disable_status` enum('0','1') NOT NULL DEFAULT '0',
  `disable_reason` text,
  `disable_by` int(11) DEFAULT NULL,
  `active_by` int(11) DEFAULT NULL,
  `delete_by` int(11) DEFAULT NULL,
  `delete_reason` text,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `region`
--

INSERT INTO `region` (`id`, `region_name`, `user_id`, `delete_status`, `disable_status`, `disable_reason`, `disable_by`, `active_by`, `delete_by`, `delete_reason`, `created_on`) VALUES
(1, 'WEST', 0, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-09-11 18:07:38'),
(7, 'EAST', 0, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:26:18'),
(8, 'NORTH', 0, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:29:11'),
(9, 'NORTH - EAST', 0, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:29:32'),
(10, 'SOUTH', 0, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:31:34'),
(11, 'CENTRAL', 0, '0', '0', NULL, NULL, NULL, NULL, NULL, '2016-10-26 13:32:01');

-- --------------------------------------------------------

--
-- Table structure for table `region_detail`
--

CREATE TABLE `region_detail` (
  `id` int(11) NOT NULL,
  `region_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `region_detail`
--

INSERT INTO `region_detail` (`id`, `region_id`, `state_id`, `created_on`) VALUES
(60, 8, 7, '2016-11-30 10:30:33'),
(61, 8, 13, '2016-11-30 10:30:33'),
(62, 8, 14, '2016-11-30 10:30:33'),
(63, 8, 15, '2016-11-30 10:30:33'),
(64, 8, 27, '2016-11-30 10:30:33'),
(65, 7, 20, '2016-11-30 10:31:39'),
(66, 7, 3, '2016-11-30 10:31:39'),
(67, 7, 4, '2016-11-30 10:31:39'),
(68, 7, 22, '2016-11-30 10:31:39'),
(69, 7, 24, '2016-11-30 10:31:39'),
(70, 7, 25, '2016-11-30 10:31:39'),
(71, 7, 26, '2016-11-30 10:31:39'),
(72, 7, 30, '2016-11-30 10:31:39'),
(73, 7, 32, '2016-11-30 10:31:39'),
(74, 7, 35, '2016-11-30 10:31:39'),
(75, 9, 5, '2016-11-30 10:31:50'),
(76, 9, 34, '2016-11-30 10:31:50'),
(77, 9, 33, '2016-11-30 10:31:50'),
(78, 11, 6, '2016-11-30 10:32:22'),
(79, 11, 16, '2016-11-30 10:32:22'),
(80, 11, 23, '2016-11-30 10:32:22'),
(92, 1, 8, '2016-11-30 10:34:01'),
(93, 1, 12, '2016-11-30 10:34:01'),
(94, 1, 21, '2016-11-30 10:34:01'),
(95, 1, 29, '2016-11-30 10:34:01'),
(96, 1, 10, '2016-11-30 10:34:01'),
(97, 10, 2, '2016-11-30 10:34:15'),
(98, 10, 11, '2016-11-30 10:34:15'),
(99, 10, 17, '2016-11-30 10:34:15'),
(100, 10, 18, '2016-11-30 10:34:15'),
(101, 10, 19, '2016-11-30 10:34:15'),
(102, 10, 28, '2016-11-30 10:34:15'),
(103, 10, 31, '2016-11-30 10:34:15'),
(104, 10, 1, '2016-11-30 10:34:15');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(11) NOT NULL,
  `state_name` varchar(50) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `state_name`, `created_on`) VALUES
(1, 'ANDAMAN AND NICOBAR ISLANDS', NULL),
(2, 'ANDHRA PRADESH', NULL),
(3, 'ARUNACHAL PRADESH', NULL),
(4, 'ASSAM', NULL),
(5, 'BIHAR', NULL),
(6, 'CHATTISGARH', NULL),
(7, 'CHANDIGARH', NULL),
(8, 'DAMAN AND DIU', NULL),
(9, 'DELHI', NULL),
(10, 'DADRA AND NAGAR HAVELI', NULL),
(11, 'GOA', NULL),
(12, 'GUJARAT', NULL),
(13, 'HIMACHAL PRADESH', NULL),
(14, 'HARYANA', NULL),
(15, 'JAMMU AND KASHMIR', NULL),
(16, 'JHARKHAND', NULL),
(17, 'KERALA', NULL),
(18, 'KARNATAKA', NULL),
(19, 'LAKSHADWEEP', NULL),
(20, 'MEGHALAYA', NULL),
(21, 'MAHARASHTRA', NULL),
(22, 'MANIPUR', NULL),
(23, 'MADHYA PRADESH', NULL),
(24, 'MIZORAM', NULL),
(25, 'NAGALAND', NULL),
(26, 'ORISSA', NULL),
(27, 'PUNJAB', NULL),
(28, 'PONDICHERRY', NULL),
(29, 'RAJASTHAN', NULL),
(30, 'SIKKIM', NULL),
(31, 'TAMIL NADU', NULL),
(32, 'TRIPURA', NULL),
(33, 'UTTARAKHAND', NULL),
(34, 'UTTAR PRADESH', NULL),
(35, 'WEST BENGAL', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_phone` varchar(20) DEFAULT NULL,
  `user_type` enum('0','1','2','3','4') DEFAULT NULL COMMENT '0-vp,1-rsm,2-asm,3-sm',
  `user_admin_backoffice_type` enum('0','1') DEFAULT NULL COMMENT '0:superadmin,1:backuser',
  `region_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `assign_to_user` int(11) DEFAULT NULL,
  `user_code` int(11) DEFAULT NULL,
  `user_password` text,
  `device_token` text,
  `app_os` enum('0','1') NOT NULL DEFAULT '0',
  `notification_token` text,
  `disable_status` enum('0','1') NOT NULL DEFAULT '0',
  `delete_status` enum('0','1') NOT NULL DEFAULT '0',
  `user_passcode` varchar(10) DEFAULT NULL,
  `disable_reason` text,
  `disable_by` int(11) DEFAULT NULL,
  `active_by` int(11) DEFAULT NULL,
  `delete_by` int(11) DEFAULT NULL,
  `delete_reason` text,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user_name`, `user_email`, `user_phone`, `user_type`, `user_admin_backoffice_type`, `region_id`, `area_id`, `assign_to_user`, `user_code`, `user_password`, `device_token`, `app_os`, `notification_token`, `disable_status`, `delete_status`, `user_passcode`, `disable_reason`, `disable_by`, `active_by`, `delete_by`, `delete_reason`, `created_on`) VALUES
(1, 'Chocolate', 'admin@chocolate.com', '123456', '0', '0', 1, NULL, NULL, NULL, '$2y$10$PTOMXmwS94HWny0y4IsjeOn1HuojYkIId5qmzLB8nQKwrDrovtm3q', 'hjadhiqewuqhw213kj1h2j3', '0', '38748hfhr74hd74hf74hd737474hd73hdh37h47g', '0', '0', NULL, NULL, NULL, NULL, NULL, NULL, '2016-09-05 21:05:07');

-- --------------------------------------------------------

--
-- Table structure for table `user_area`
--

CREATE TABLE `user_area` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_brand`
--

CREATE TABLE `user_brand` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_state`
--

CREATE TABLE `user_state` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`),
  ADD KEY `area_id` (`id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`),
  ADD KEY `brand_id` (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`id`);

--
-- Indexes for table `customer_type`
--
ALTER TABLE `customer_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `folloup_meeting_customer`
--
ALTER TABLE `folloup_meeting_customer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `folloup_meeting_customer_id` (`id`);

--
-- Indexes for table `global_setting`
--
ALTER TABLE `global_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gps_data`
--
ALTER TABLE `gps_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gps_data_id` (`id`);

--
-- Indexes for table `meeting_customer`
--
ALTER TABLE `meeting_customer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meeting_customer_id` (`id`);

--
-- Indexes for table `meeting_purpose`
--
ALTER TABLE `meeting_purpose`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meeting_purpose_id` (`id`);

--
-- Indexes for table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`),
  ADD KEY `region_id` (`id`);

--
-- Indexes for table `region_detail`
--
ALTER TABLE `region_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `region_detail_id` (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`),
  ADD KEY `state_id` (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`id`);

--
-- Indexes for table `user_area`
--
ALTER TABLE `user_area`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_area_id` (`id`);

--
-- Indexes for table `user_brand`
--
ALTER TABLE `user_brand`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_brand_id` (`id`);

--
-- Indexes for table `user_state`
--
ALTER TABLE `user_state`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_area_id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=672;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_type`
--
ALTER TABLE `customer_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `folloup_meeting_customer`
--
ALTER TABLE `folloup_meeting_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `global_setting`
--
ALTER TABLE `global_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `gps_data`
--
ALTER TABLE `gps_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `meeting_customer`
--
ALTER TABLE `meeting_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `meeting_purpose`
--
ALTER TABLE `meeting_purpose`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `region`
--
ALTER TABLE `region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `region_detail`
--
ALTER TABLE `region_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_area`
--
ALTER TABLE `user_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_brand`
--
ALTER TABLE `user_brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_state`
--
ALTER TABLE `user_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
